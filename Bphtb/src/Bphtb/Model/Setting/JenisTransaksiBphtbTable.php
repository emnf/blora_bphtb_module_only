<?php

namespace Bphtb\Model\Setting;

use Bphtb\Helper\CacheHelper;
use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Where;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Expression;

class JenisTransaksiBphtbTable extends AbstractTableGateway
{

    protected $table = 's_jenistransaksi';

    public function __construct(Adapter $adapter)
    {
        $this->adapter = $adapter;
        $this->resultSetPrototype = new ResultSet();
        $this->resultSetPrototype->setArrayObjectPrototype(new JenisTransaksiBphtbBase());
        $this->initialize();
    }

    public function checkExist(JenisTransaksiBphtbBase $kc)
    {
        $rowset = $this->select(array('s_kodejenistransaksi' => $kc->s_kodejenistransaksi));
        $row = $rowset->current();
        return $row;
    }

    public function checkId(JenisTransaksiBphtbBase $kc)
    {
        $rowset = $this->select(array('s_idjenistransaksi' => $kc->s_idjenistransaksi));
        $row = $rowset->current();
        return $row;
    }

    public function savedata(JenisTransaksiBphtbBase $kc)
    {
        $data = array(
            's_kodejenistransaksi' => $kc->s_kodejenistransaksi,
            's_namajenistransaksi' => $kc->s_namajenistransaksi
        );
        $id = (int) $kc->s_idjenistransaksi;
        if ($id == 0) {
            $this->insert($data);
        } else {
            if ($this->checkId($kc)) {
                $this->update($data, array('s_idjenistransaksi' => $kc->s_idjenistransaksi));
            } else {
                throw new \Exception('Form id does not exist');
            }
        }
    }

    public function getGridCount(JenisTransaksiBphtbBase $base)
    {
        $sql = new \Zend\Db\Sql\Sql($this->adapter);
        $select = $sql->select();
        $select->from(["a" => $this->table]);
        $select->join(["b" => "s_koderekening"], "a.s_korekid = b.s_korekid", [
            "korek" => new Expression("(
                b.s_korektipe || '.' || b.s_korekkelompok || '.' || b.s_korekjenis || '.' || b.s_korekobjek || '.' || b.s_korekrincian || '.' || b.s_korekrinciansub
            )"),
            "s_koreknama"
        ], "LEFT");
        $where = new \Zend\Db\Sql\Where();
        if ($base->s_kodejenistransaksi != 'undefined')
            $where->literal("a.s_kodejenistransaksi::text LIKE '%$base->s_kodejenistransaksi%'");
        if ($base->s_namajenistransaksi != 'undefined')
            $where->literal("a.s_namajenistransaksi::text LIKE '%$base->s_namajenistransaksi%'");
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res->count();
    }

    public function getGridData(JenisTransaksiBphtbBase $base, $offset)
    {
        $sql = new \Zend\Db\Sql\Sql($this->adapter);
        $select = $sql->select();
        $select->from(["a" => $this->table]);
        $select->join(["b" => "s_koderekening"], "a.s_korekid = b.s_korekid", [
            "korek" => new Expression("(
                b.s_korektipe || '.' || b.s_korekkelompok || '.' || b.s_korekjenis || '.' || b.s_korekobjek || '.' || b.s_korekrincian || '.' || b.s_korekrinciansub
            )"),
            "s_koreknama"
        ], "LEFT");
        $where = new \Zend\Db\Sql\Where();
        if ($base->s_kodejenistransaksi != 'undefined')
            $where->literal("a.s_kodejenistransaksi::text LIKE '%$base->s_kodejenistransaksi%'");
        if ($base->s_namajenistransaksi != 'undefined')
            $where->literal("a.s_namajenistransaksi::text LIKE '%$base->s_namajenistransaksi%'");
        $select->where($where);
        $select->order("a.s_kodejenistransaksi ASC");
        $select->limit($base->rows = (int) $base->rows);
        $select->offset($offset = (int) $offset);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res;
    }

    public function getDataId($id)
    {
        $rowset = $this->select(array('s_idjenistransaksi' => $id));
        $row = $rowset->current();
        return $row;
    }

    public function hapusData($kb)
    {
        $this->delete(array('s_idjenistransaksi' => $kb));
    }

    public function comboBox()
    {
        // $resultSet = $this->select();
        // return $resultSet;

        $key = 'JenisTransaksiBphtbTable-comboBox';
        $cache = (new CacheHelper)->cache();
        if (!$cache->hasItem($key)) {
            $sql = new Sql($this->adapter);
            $select = $sql->select($this->table)->order("s_kodejenistransaksi");
            $res = $sql->prepareStatementForSqlObject($select)->execute();
            $result = new ResultSet();
            $result->initialize($res);
            // return $result->toArray();

            $cache->setItem($key, $result->toArray());
        }

        return $cache->getItem($key);
    }

    public function jumlahsyarat($id_syarat)
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from('s_persyaratan');
        $where = new Where();

        $where->literal("s_idjenistransaksi = '$id_syarat'");

        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res->count();
    }
}
