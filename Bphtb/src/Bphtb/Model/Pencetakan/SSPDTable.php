<?php

namespace Bphtb\Model\Pencetakan;

use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Where;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Select;

class SSPDTable extends AbstractTableGateway
{

    public $table = "view_sspd";

    public function __construct(Adapter $adapter)
    {
        $this->adapter = $adapter;
        $this->resultSetPrototype = new ResultSet();
        $this->resultSetPrototype->setArrayObjectPrototype(new SSPDBase());
        $this->initialize();
    }

    public function getDataId($id)
    {
        $rowset = $this->select(function (Select $select) use ($id) {
            $select->where(array('t_spt.t_idspt' => $id));
            $select->join('t_spt', 't_detailsptbphtb.t_idspt = t_spt.t_idspt');
        });
        $row = $rowset->current();
        return $row;
    }

    public function getDataId_ppob($id)
    {
        $rowset = $this->select(function (Select $select) use ($id) {
            $select->where(array('t_spt.t_idspt' => $id));
            $select->join('t_spt', 't_detailsptbphtb.t_idspt = t_spt.t_idspt');
        });
        $row = $rowset->current();
        return $row;
    }

    public function getDataId_cek($id)
    {
        $rowset = $this->select(array('t_idspt' => $id));
        $row = $rowset->current();
        return $row;
    }

    public function getdatasspd(SSPDBase $db, $id)
    {
        if ($id == 2) {
            $where = " ";
        } else {
            $where = " AND t_idnotarisspt::text = '" . $id . "' ";
        }

        $sql = "select *
                from view_sspd a
                LEFT JOIN t_pembayaranspt b ON b.t_idspt = a.t_idspt
                where a.t_kohirspt = " . $db->no_spt1 . " and a.t_periodespt=" . $db->periode_spt . " " . $where . "";
        $statement = $this->adapter->query($sql);
        return $statement->execute();
    }

    public function getdatasspd_ppob(SSPDBase $db, $id)
    {
        if ($id == 2) {
            $where = " ";
        } else {
            $where = " AND t_idnotarisspt::text = '" . $id . "' ";
        }

        $sql = "select *
                from view_sspd a
                LEFT JOIN t_pembayaranspt b ON b.t_idspt = a.t_idspt
                where a.t_kohirspt = " . $db->no_spt1 . " and a.t_periodespt=" . $db->periode_spt . " " . $where . "";
        $statement = $this->adapter->query($sql);
        return $statement->execute();
    }

    public function getdatasspd_mutasi(SSPDBase $db, $id)
    {
        if ($id == 2) {
            $where = " ";
        } else {
            $where = " AND t_idnotarisspt::text = '" . $id . "' ";
        }

        $sql = "select *
                from view_sspd a
                LEFT JOIN t_pembayaranspt b ON b.t_idspt = a.t_idspt
                where a.t_kohirspt = " . $db->no_spt1 . " and a.t_periodespt=" . $db->periode_spt . " " . $where . "";
        $statement = $this->adapter->query($sql);
        return $statement->execute();
    }
    public function getdatasspd_spop(SSPDBase $db, $id)
    {
        if ($id == 2) {
            $where = " ";
        } else {
            $where = " AND t_idnotarisspt::text = '" . $id . "' ";
        }

        $sql = "select *
                from view_sspd a
                LEFT JOIN t_pembayaranspt b ON b.t_idspt = a.t_idspt
                where a.t_kohirspt = " . $db->no_spt1 . " and a.t_periodespt=" . $db->periode_spt . " " . $where . "";
        $statement = $this->adapter->query($sql);
        return $statement->execute();
    }

    public function getdatasspdbuktipenerimaan(SSPDBase $db, $id, $s_tipe_pejabat)
    {
        if (($s_tipe_pejabat == 1) || ($s_tipe_pejabat == 0)) {
            $where = " ";
        } else {
            $where = " AND t_idnotarisspt::text = '" . $id . "' ";
        }

        $sql = "select *
                from view_sspd a
                LEFT JOIN t_pembayaranspt b ON b.t_idspt = a.t_idspt
                where a.t_kohirspt = " . $db->no_spt1 . " and a.t_periodespt=" . $db->periode_spt . " " . $where . "";
        $statement = $this->adapter->query($sql);
        return $statement->execute();
    }
    // ===============================================================
    public function cetakbuktipenerimaanvalidasi(SSPDBase $db)
    {


        $sql = "select *
                from view_sspd a
                LEFT JOIN t_pembayaranspt b ON b.t_idspt = a.t_idspt
                where a.t_kohirspt = " . $db->no_spt1 . " and a.t_periodespt=" . $db->periode_spt . " ";
        $statement = $this->adapter->query($sql);
        return $statement->execute();
    }
    // coba


    // end coba
    public function getdatapenelitian($data_get)
    {
        $sql = "select *
                from view_sspd a
                LEFT JOIN t_pembayaranspt b ON b.t_idspt = a.t_idspt
                where a.t_kohirspt = " . $data_get->no_spt_penelitian . " and a.t_periodespt=" . $data_get->periode_spt_penelitian . "";
        $statement = $this->adapter->query($sql);
        return $statement->execute();
    }
    // tambahan BLORA
    public function cetakcobavalidasi(COBAbase $db)
    {


        $sql = "select *
                from view_sspd a
                LEFT JOIN t_pembayaranspt b ON b.t_idspt = a.t_idspt
                where a.t_kohirspt = " . $db->no_spt1 . " and a.t_periodespt=" . $db->periode_spt . " ";
        $statement = $this->adapter->query($sql);
        return $statement->execute();
    }
    // END tambahan BLORA

    public function getdatasurathasilpenelitian($data_get)
    {
        // $sql = "select * from view_sspd_pembayaran where
        //         t_kohirspt = " . $data_get->no_sptpenelitian . " and t_periodespt=" . $data_get->periode_sptpenelitian . "";
        //         // die($sql);
        // $statement = $this->adapter->query($sql);
        // return $statement->execute();

        $sql = new Sql($this->adapter);
        $select = $sql->select(["a" => $this->view_sppd_pembayaran()]);
        $where = new Where();
        $where->equalTo("a.t_kohirspt", $data_get->no_sptpenelitian);
        $where->equalTo("a.t_periodespt", $data_get->periode_sptpenelitian);
        $select->where($where);
        // die($sql->getSqlStringForSqlObject($select));
        $res = $sql->prepareStatementForSqlObject($select)->execute();
        return $res;
    }

    public function getdatassspd($t_idspt)
    {
        $sql = "select a.*, b.*, d.s_namanotaris , e.s_namapejabat
                from view_sspd a
                LEFT JOIN t_pembayaranspt b ON b.t_idspt = a.t_idspt
                LEFT JOIN s_users c ON c.s_iduser = a.t_idnotarisspt
		LEFT JOIN s_notaris d ON d.s_idnotaris::text = c.s_idpejabat_idnotaris::text
		left join s_pejabat e on e.s_idpejabat = b.t_pejabatverifikasispt
                where a.t_idspt =" . $t_idspt . "";
        $statement = $this->adapter->query($sql);
        return $statement->execute()->buffer();
    }

    public function getdataidsptsebelumnya($t_idspt)
    {
        $sql = "select t_idsptsebelumnya
                from view_sspd a
                LEFT JOIN t_pembayaranspt b ON b.t_idspt = a.t_idspt
                where a.t_idspt =" . $t_idspt . "";
        $statement = $this->adapter->query($sql);
        return $statement->execute()->current();
    }
    // BLORA 
    public function getdataidsptsebelumnya_ppob($t_idspt)
    {
        $sql = "select t_idsptsebelumnya
                from view_sspd a
                LEFT JOIN t_pembayaranspt b ON b.t_idspt = a.t_idspt
                where a.t_idspt =" . $t_idspt . "";
        $statement = $this->adapter->query($sql);
        return $statement->execute()->current();
    }

    public function getdatassspdsebelumnya($t_idspt)
    {
        $sql = "select *
                from view_sspd a
                LEFT JOIN t_pembayaranspt b ON b.t_idspt = a.t_idspt
                where a.t_idspt =" . $t_idspt . "";
        $statement = $this->adapter->query($sql);
        return $statement->execute()->current();
    }
    // BLORA 
    public function getdatassspdsebelumnya_ppob($t_idspt)
    {
        $sql = "select *
                from view_sspd a
                LEFT JOIN t_pembayaranspt b ON b.t_idspt = a.t_idspt
                where a.t_idspt =" . $t_idspt . "";
        $statement = $this->adapter->query($sql);
        return $statement->execute()->current();
    }

    public function getdata(SSPDBase $db)
    {
        $sql = "select *
                from view_sspd a
                LEFT JOIN t_pembayaranspt b ON b.t_idspt = a.t_idspt
                where a.t_kohirspt >=" . $db->no_spt1 . " and a.t_kohirspt <=" . $db->no_spt2 . " and a.t_periodespt=" . $db->periode_spt . " and b.t_ketetapanspt=1";
        $statement = $this->adapter->query($sql);
        return $statement->execute();
    }

    public function getdatas($t_idspt)
    {
        $sql = "select *
                from view_sspd a
                LEFT JOIN t_pembayaranspt b ON b.t_idspt = a.t_idspt
                where a.t_idspt =" . $t_idspt . " and b.t_ketetapanspt=1";
        $statement = $this->adapter->query($sql);
        return $statement->execute();
    }

    public function getDataPencatatanSetoran($tgl_cetak)
    {
        $sql = "select *
                from view_sspd a
                INNER JOIN t_pembayaranspt b ON b.t_idspt = a.t_idspt
                where b.t_tanggalpembayaran <= '" . date('Y-m-d', strtotime($tgl_cetak)) . "'";
        $statement = $this->adapter->query($sql);
        return $statement->execute();
    }

    public function getDataPencatatanSetoranBulanan($tgl_setor1, $tgl_setor2)
    {
        // $sql = "select *
        //         from view_sspd a
        //         INNER JOIN t_pembayaranspt b ON b.t_idspt = a.t_idspt
        //         where b.t_tanggalpembayaran between '" . date('Y-m-d', strtotime($tgl_setor1)) . "' and '" . date('Y-m-d', strtotime($tgl_setor2)) . "' order by t_tanggalpembayaran asc";
        // $statement = $this->adapter->query($sql);
        // return $statement->execute();

        $sql = new Sql($this->adapter);
        $select = $sql->select(["a" => "t_spt"]);
        $select->join(["bb" => "t_detailsptbphtb"], "a.t_idspt = bb.t_idspt", [
            "t_namawppembeli", "t_alamatwppembeli", "t_namawppenjual",
            "t_alamatop"
        ], "LEFT");
        $select->join(["b" => "t_pembayaranspt"], "a.t_idspt = b.t_idspt", [
            "t_tanggalpembayaran", "t_nilaipembayaranspt"
        ], "LEFT");
        $select->join(["c" => "s_jenistransaksi"], "a.t_idjenistransaksi = c.s_idjenistransaksi", [
            "s_namajenistransaksi"
        ], "LEFT");
        $select->join(["d" => "s_koderekening"], "c.s_korekid = d.s_korekid", [
            "korek" => new Expression("(
                d.s_korektipe || '.' || d.s_korekkelompok || '.' || d.s_korekjenis || '.' || d.s_korekobjek || '.' || d.s_korekrincian || '.' || d.s_korekrinciansub
            )"),
            "s_koreknama"
        ], "LEFT");
        $select->join(["e" => "s_users"], "a.t_idnotarisspt = e.s_iduser", [], "LEFT");
        $select->join(["f" => "s_notaris"], new Expression("e.s_idpejabat_idnotaris::INTEGER = f.s_idnotaris::INTEGER"), [
            "s_namanotaris"
        ], "LEFT");
        $where = new Where();
        $where->between("b.t_tanggalpembayaran", date('Y-m-d', strtotime($tgl_setor1)), date('Y-m-d', strtotime($tgl_setor2)));
        $select->where($where);
        $select->order("c.s_korekid, b.t_tanggalpembayaran");
        // die($sql->getSqlStringForSqlObject($select));
        $res = $sql->prepareStatementForSqlObject($select)->execute();
        return $res;
    }

    public function getDataSetoranXML($tgl_setorxml1, $tgl_setorxml2)
    {
        // $sql = "select *
        //         from view_sspd a
        //         INNER JOIN t_pembayaranspt b ON b.t_idspt = a.t_idspt
        //         where b.t_nilaipembayaranspt > 0 and b.t_tanggalpembayaran between '" . date('Y-m-d', strtotime($tgl_setorxml1)) . "' and '" . date('Y-m-d', strtotime($tgl_setorxml2)) . "' order by t_tanggalpembayaran asc";
        // $statement = $this->adapter->query($sql);
        // return $statement->execute();

        $sql = new Sql($this->adapter);
        $select = $sql->select(["a" => "t_spt"]);
        $select->join(["bb" => "t_detailsptbphtb"], "a.t_idspt = bb.t_idspt", [
            "t_namawppembeli", "t_alamatwppembeli", "t_rtwppembeli",
            "t_rwwppembeli", "t_kelurahanwppembeli", "t_kecamatanwppembeli",
            "t_npwpwppembeli",
            "t_namawppenjual",
            "t_alamatop"
        ], "LEFT");
        $select->join(["b" => "t_pembayaranspt"], "a.t_idspt = b.t_idspt", [
            "t_tanggalpembayaran", "t_nilaipembayaranspt", "t_idpembayaranspt"
        ], "LEFT");
        $select->join(["c" => "s_jenistransaksi"], "a.t_idjenistransaksi = c.s_idjenistransaksi", [
            "s_namajenistransaksi"
        ], "LEFT");
        $select->join(["d" => "s_koderekening"], "c.s_korekid = d.s_korekid", [
            "korek" => new Expression("(
                d.s_korektipe || '.' || d.s_korekkelompok || '.' || d.s_korekjenis || '.' || d.s_korekobjek || '.' || d.s_korekrincian || '.' || d.s_korekrinciansub
            )"),
            "s_koreknama"
        ], "LEFT");
        $select->join(["e" => "s_users"], "a.t_idnotarisspt = e.s_iduser", [], "LEFT");
        $select->join(["f" => "s_notaris"], new Expression("e.s_idpejabat_idnotaris::INTEGER = f.s_idnotaris::INTEGER"), [
            "s_namanotaris"
        ], "LEFT");
        $where = new Where();
        $where->between("b.t_tanggalpembayaran", date('Y-m-d', strtotime($tgl_setorxml1)), date('Y-m-d', strtotime($tgl_setorxml2)));
        $select->where($where);
        $select->order("c.s_korekid, b.t_tanggalpembayaran");
        // die($sql->getSqlStringForSqlObject($select));
        $res = $sql->prepareStatementForSqlObject($select)->execute();
        return $res;
    }

    public function getDataPencatatanSetoranBulanan_kpppratama($tgl_setor1, $tgl_setor2)
    {
        $sql = "select *
                from fr_pembayaran_v5 
                where t_tanggalpembayaran between '" . date('Y-m-d', strtotime($tgl_setor1)) . "' and '" . date('Y-m-d', strtotime($tgl_setor2)) . "' order by t_tanggalpembayaran asc";
        $statement = $this->adapter->query($sql);
        return $statement->execute();
    }

    public function getDataVerifikasi($periode_spt, $tgl_verifikasi1, $tgl_verifikasi2)
    {
        $sql = "select *
                from view_sspd_pembayaran
                where t_periodespt=$periode_spt and t_tglverifikasispt between '" . date('Y-m-d', strtotime($tgl_verifikasi1)) . "' and '" . date('Y-m-d', strtotime($tgl_verifikasi2)) . "' order by t_idspt asc";
        $statement = $this->adapter->query($sql);
        return $statement->execute();
    }

    public function getDataVerifikasiBerkas($periode_spt, $tgl_verifikasi1, $tgl_verifikasi2)
    {
        $sql = "select *
                from view_sspd_pembayaran
                where t_periodespt=$periode_spt and t_tglverifikasiberkas between '" . date('Y-m-d', strtotime($tgl_verifikasi1)) . "' and '" . date('Y-m-d', strtotime($tgl_verifikasi2)) . "' order by t_idspt asc";
        $statement = $this->adapter->query($sql);
        return $statement->execute();
    }

    public function ambildatakodebayarpendaftaran($data_get)
    {
        $sql = "SELECT * FROM view_pendaftaran where t_idspt=$data_get->t_idspt";
        $statement = $this->adapter->query($sql);
        return $statement->execute()->current();
    }

    public function ambildatakodebayardaripendaftaran($data_get)
    {
        //        var_dump($data_get->no_spt1);
        //        exit();
        $sql = "SELECT * FROM view_data_verifikasi_isi where t_kohirspt = " . $data_get->no_spt1 . " and t_periodespt=" . $data_get->periode_spt . " and t_kodebayarbanksppt_pembayaran is not null";
        $statement = $this->adapter->query($sql);
        return $statement->execute()->current();
    }

    public function getDataBelumBayar($tgl_verifikasi11, $tgl_verifikasi22)
    {

        $sql = "select *
                from view_sspd_sudah_tervalidasi
                where t_statusbayarspt is null and t_tglverifikasispt between '" . date('Y-m-d', strtotime($tgl_verifikasi11)) . "' and '" . date('Y-m-d', strtotime($tgl_verifikasi22)) . "' order by t_idspt asc";
        $statement = $this->adapter->query($sql);
        return $statement->execute();
    }

    public function getDataKurangBayar($tgl_verifikasi111, $tgl_verifikasi222)
    {

        $sql = "select *
                from view_sspd
                where fr_validasidua=1  and t_tglprosesspt between '" . date('Y-m-d', strtotime($tgl_verifikasi111)) . "' and '" . date('Y-m-d', strtotime($tgl_verifikasi222)) . "' order by t_idspt asc";
        $statement = $this->adapter->query($sql);
        return $statement->execute();
    }

    public function getDataLapHarian($tgl_cetak)
    {
        $sql = "select *
                from view_sspd_pembayaran
                where t_tglverifikasispt ='" . date('Y-m-d', strtotime($tgl_cetak)) . "'";
        $statement = $this->adapter->query($sql);
        return $statement->execute();
    }

    public function getDataLapBphtb($tgl_cetak1, $tgl_cetak2)
    {
        $sql = "select *
                from view_sspd_pembayaran
                where t_tglverifikasispt between '" . date('Y-m-d', strtotime($tgl_cetak1)) . "' and '" . date('Y-m-d', strtotime($tgl_cetak2)) . "'";
        $statement = $this->adapter->query($sql);
        return $statement->execute();
    }

    public function getDataLapHarianSetor($tgl_cetak, $idnotaris)
    {

        if (!empty($idnotaris)) {
            $where = " AND t_idnotarisspt = " . $idnotaris;
        } else {
            $where = " ";
        }

        $sql = "select *
                from view_sspd_pembayaran
                where t_tanggalpembayaran ='" . date('Y-m-d', strtotime($tgl_cetak)) . "'  " . $where . "  order by t_idspt asc";
        $statement = $this->adapter->query($sql);
        return $statement->execute();
    }

    public function getDataPerNotaris($periode_spt,  $tgl_verifikasi1, $tgl_verifikasi2, $idnotaris, $s_tipe_pejabat)
    {

        if (!empty($idnotaris)) {
            $where = " AND t_idnotarisspt = " . $idnotaris;
        } else {
            $where = " ";
        }


        $arr_tgl = explode("-", $tgl_verifikasi1);
        $tgl_awal = $arr_tgl[2] . "-" . $arr_tgl[1] . "-" . $arr_tgl[0];
        $arr_tgl2 = explode("-", $tgl_verifikasi2);
        $tgl_akhir = $arr_tgl2[2] . "-" . $arr_tgl2[1] . "-" . $arr_tgl2[0];
        $sql = "select *
                from view_pendaftaran
                where t_periodespt=$periode_spt and t_tglprosesspt between '" . $tgl_awal . "' and '" . $tgl_akhir . "' " . $idnotaris . "";
        $statement = $this->adapter->query($sql);
        return $statement->execute();;
    }

    // public function getDataLapBphtbBulanan_v2($tgl_cetak1, $tgl_cetak2, $idnotaris)
    // {

    //     if (!empty($idnotaris)) {
    //         $where = " AND t_idnotarisspt = " . $idnotaris;
    //     } else {
    //         $where = " ";
    //     }

    //     $sql = "select *
    //             from view_sspd_pembayaran
    //             where t_tanggalpembayaran between '" . date('Y-m-d', strtotime($tgl_cetak1)) . "' and '" . date('Y-m-d', strtotime($tgl_cetak2)) . "' AND t_nilaipembayaranspt IS NOT NULL AND t_nilaipembayaranspt != 0 " . $where . "  order by t_idspt asc";
    //     $statement = $this->adapter->query($sql);
    //     return $statement->execute();
    // }

    public function getDataLapBphtbBulanan($tgl_cetak1, $tgl_cetak2, $idnotaris, $order, $nihiltidaktampil, $data_get)
    {
        // var_dump($data_get);exit();
        $sql = new Sql($this->adapter);
        $select = $sql->select(['x' => $this->view_sppd_pembayaran()]);
        $where = new Where();
        if (!empty($idnotaris)) {
            $where->equalTo('t_idnotarisspt', $idnotaris);
        }
        if ($nihiltidaktampil != '') $where->literal($nihiltidaktampil);
        $where->between('t_tanggalpembayaran', date('Y-m-d', strtotime($tgl_cetak1)), date('Y-m-d', strtotime($tgl_cetak2)));
        if ($data_get['jenis_transaksi'] != '') {
            $where->equalTo('t_idjenistransaksi', $data_get['jenis_transaksi']);
        }
        if ($data_get['kecamatan'] != '') {
            $where->equalTo('s_kodekecamatan', $data_get['kecamatan']);
        }
        if ($data_get['kelurahan'] != '') {
            $where->equalTo('s_kodekelurahan', $data_get['kelurahan']);
        }
        if ($data_get['nop'] != '') {
            $where->equalTo('t_nopbphtbsppt', $data_get['nop']);
        }
        // var_dump($data_get);exit();
        $select->where($where);
        $select->order($order);
        // die($sql->buildSqlString($select));
        $res = $sql->prepareStatementForSqlObject($select)->execute();
        return $res;
    }


    public function getDataLapBphtbBulanan_perdesa($tgl_cetak1, $tgl_cetak2, $idnotaris, $kodekecamatan, $kodekelurahan, $nihiltidaktampil)
    {

        // if (!empty($idnotaris)) {
        //     $where = " AND t_idnotarisspt = " . $idnotaris;
        // } else {
        //     $where = " ";
        // }

        // if (!empty($kodekecamatan)) {
        //     $wherekec = " AND substring(t_nopbphtbsppt from 7 for 3) = '" . $kodekecamatan . "' ";
        // } else {
        //     $wherekec = " ";
        // }
        // if (!empty($kodekelurahan)) {
        //     $wherekel = " AND substring(t_nopbphtbsppt from 10 for 3) = '" . $kodekelurahan . "' ";
        // } else {
        //     $wherekel = " ";
        // }


        // $sql = "select *
        //         from view_sspd_pembayaran
        //         where t_tanggalpembayaran between '" . date('Y-m-d', strtotime($tgl_cetak1)) . "' and '" . date('Y-m-d', strtotime($tgl_cetak2)) . "' " . $nihiltidaktampil . " " . $where . " " . $wherekec . " " . $wherekel . " order by s_kodekecamatan asc";
        // die($sql);
        // $statement = $this->adapter->query($sql);
        // return $statement->execute();

        $sql = new Sql($this->adapter);
        $select = $sql->select(["a" => $this->view_sppd_pembayaran()]);
        $where = new Where();
        if (!empty($idnotaris)) {
            $where->equalTo("a.t_idnotarisspt", $idnotaris);
        }
        if (!empty($kodekecamatan)) {
            $where->literal("substring(a.t_nopbphtbsppt from 7 for 3) = '" . $kodekecamatan . "'");
        }
        if (!empty($kodekelurahan)) {
            // $where->literal("substring(a.t_nopbphtbsppt from 11 for 3) = '" . $kodekelurahan . "'");
            $where->literal("'0'||SUBSTRING ( A.t_nopbphtbsppt FROM 12 FOR 2 )  = '" . $kodekelurahan . "'");
        }
        if ($nihiltidaktampil != "") {
            $where->literal($nihiltidaktampil);
        }
        $where->literal("a.t_tanggalpembayaran between '" . date('Y-m-d', strtotime($tgl_cetak1)) . "' and '" . date('Y-m-d', strtotime($tgl_cetak2)) . "'");
        $select->where($where);
        $select->order("a.s_kodekecamatan");
        $select->order("a.s_kodekelurahan");
        $select->order("a.t_tanggalpembayaran");
        // die($sql->getSqlStringForSqlObject($select));
        $res = $sql->prepareStatementForSqlObject($select)->execute();
        return $res;
    }

    public function view_sppd_pembayaran($string = null)
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select(["a" => "t_spt"]);
        $select->columns([
            "t_idspt", "t_tglprosesspt", "t_periodespt", "t_nopbphtbsppt",
            "t_totalspt", "t_nilaitransaksispt", "t_kohirspt",
            "t_potonganspt", "t_idjenistransaksi", "t_idjenishaktanah",
            "t_persyaratan", "t_idsptsebelumnya", "t_kohirketetapanspt",
            "t_idtarifbphtb", "t_persenbphtb", "t_tgljatuhtempospt",
            "t_dasarspt", "t_idjenisdoktanah", "t_potongan_waris_hibahwasiat",
            "t_thnsppt", "t_idnotarisspt", "t_tarif_pembagian_aphb_kali",
            "t_tarif_pembagian_aphb_bagi",
            "t_namawppembeli" => new Expression("b.t_namawppembeli"),
            "t_nikwppembeli" => new Expression("b.t_nikwppembeli"),
            "t_alamatwppembeli" => new Expression("b.t_alamatwppembeli"),
            "t_kecamatanwppembeli" => new Expression("b.t_kecamatanwppembeli"),
            "t_kelurahanwppembeli" => new Expression("b.t_kelurahanwppembeli"),
            "t_kabkotawppembeli" => new Expression("b.t_kabkotawppembeli"),
            "t_telponwppembeli" => new Expression("b.t_telponwppembeli"),
            "t_kodeposwppembeli" => new Expression("b.t_kodeposwppembeli"),
            "t_npwpwppembeli" => new Expression("b.t_npwpwppembeli"),
            "t_namawppenjual" => new Expression("b.t_namawppenjual"),
            "t_nikwppenjual" => new Expression("b.t_nikwppenjual"),
            "t_alamatwppenjual" => new Expression("b.t_alamatwppenjual"),
            "t_kecamatanwppenjual" => new Expression("b.t_kecamatanwppenjual"),
            "t_kelurahanwppenjual" => new Expression("b.t_kelurahanwppenjual"),
            "t_kabkotawppenjual" => new Expression("b.t_kabkotawppenjual"),
            "t_telponwppenjual" => new Expression("b.t_telponwppenjual"),
            "t_kodeposwppenjual" => new Expression("b.t_kodeposwppenjual"),
            "t_npwpwppenjual" => new Expression("b.t_npwpwppenjual"),
            "t_luastanah" => new Expression("b.t_luastanah"),
            "t_njoptanah" => new Expression("b.t_njoptanah"),
            "t_luasbangunan" => new Expression("b.t_luasbangunan"),
            "t_njopbangunan" => new Expression("b.t_njopbangunan"),
            "t_totalnjoptanah" => new Expression("b.t_totalnjoptanah"),
            "t_totalnjopbangunan" => new Expression("b.t_totalnjopbangunan"),
            "t_grandtotalnjop" => new Expression("b.t_grandtotalnjop"),
            "t_nosertifikathaktanah" => new Expression("b.t_nosertifikathaktanah"),
            "t_iddetailsptbphtb" => new Expression("b.t_iddetailsptbphtb"),
            "t_kelurahanop" => new Expression("b.t_kelurahanop"),
            "t_kecamatanop" => new Expression("b.t_kecamatanop"),
            "t_kabupatenop" => new Expression("b.t_kabupatenop"),
            "t_rtwppembeli" => new Expression("b.t_rtwppembeli"),
            "t_rwwppembeli" => new Expression("b.t_rwwppembeli"),
            "t_alamatop" => new Expression("b.t_alamatop"),
            "t_rtop" => new Expression("b.t_rtop"),
            "t_rwop" => new Expression("b.t_rwop"),
            "t_rtwppenjual" => new Expression("b.t_rtwppenjual"),
            "t_rwwppenjual" => new Expression("b.t_rwwppenjual"),
            "t_inputbpn" => new Expression("b.t_inputbpn"),
            "t_noajbbaru" => new Expression("b.t_noajbbaru"),
            "t_tglajbbaru" => new Expression("b.t_tglajbbaru"),
            "t_tglajb" => new Expression("b.t_tglajb"),
            "t_namasppt" => new Expression("b.t_namasppt"),
            "t_grandtotalnjop_aphb" => new Expression("b.t_grandtotalnjop_aphb"),
            "s_namajenistransaksi" => new Expression("c.s_namajenistransaksi"),
            "s_namahaktanah" => new Expression("d.s_namahaktanah"),
            "t_idpembayaranspt" => new Expression("e.t_idpembayaranspt"),
            "t_kohirpembayaran" => new Expression("e.t_kohirpembayaran"),
            "t_periodepembayaran" => new Expression("e.t_periodepembayaran"),
            "t_tanggalpembayaran" => new Expression("e.t_tanggalpembayaran"),
            "t_idnotaris" => new Expression("e.t_idnotaris"),
            "t_nilaipembayaranspt" => new Expression("e.t_nilaipembayaranspt"),
            "t_kodebayarspt" => new Expression("e.t_kodebayarspt"),
            "t_tglverifikasispt" => new Expression("e.t_tglverifikasispt"),
            "t_verifikasispt" => new Expression("e.t_verifikasispt"),
            "t_tglverifikasiberkas" => new Expression("e.t_tglverifikasiberkas"),
            "t_verifikasiberkas" => new Expression("e.t_verifikasiberkas"),
            "t_pejabatverifikasispt" => new Expression("e.t_pejabatverifikasispt"),
            "t_statusbayarspt" => new Expression("e.t_statusbayarspt"),
            "t_ketetapanspt" => new Expression("e.t_ketetapanspt"),
            "t_kodebayarbanksppt" => new Expression("e.t_kodebayarbanksppt"),
            "p_idpemeriksaan" => new Expression("f.p_idpemeriksaan"),
            "p_idpembayaranspt" => new Expression("f.p_idpembayaranspt"),
            "p_luastanah" => new Expression("f.p_luastanah"),
            "p_luasbangunan" => new Expression("f.p_luasbangunan"),
            "p_njoptanah" => new Expression("f.p_njoptanah"),
            "p_grandtotalnjop_aphb" => new Expression("f.p_grandtotalnjop_aphb"),
            "p_njopbangunan" => new Expression("f.p_njopbangunan"),
            "p_totalnjoptanah" => new Expression("f.p_totalnjoptanah"),
            "p_totalnjopbangunan" => new Expression("f.p_totalnjopbangunan"),
            "p_grandtotalnjop" => new Expression("f.p_grandtotalnjop"),
            "p_nilaitransaksispt" => new Expression("f.p_nilaitransaksispt"),
            "p_potonganspt" => new Expression("f.p_potonganspt"),
            "p_totalspt" => new Expression("f.p_totalspt"),
            "s_idnotaris" => new Expression("h.s_idnotaris"),
            "s_namanotaris" => new Expression("h.s_namanotaris"),
            "namapejabatverifikasi" => new Expression("j.s_namapejabat"),
            "s_kodekecamatan" => new Expression("l.s_kodekecamatan"),
            "s_namakecamatan" => new Expression("l.s_namakecamatan"),
            "s_kodekelurahan" => new Expression("l.s_kodekelurahan"),
            "s_namakelurahan" => new Expression("l.s_namakelurahan"),
            "s_namadoktanah" => new Expression("p.s_namadoktanah"),
            'kode_kecamatan' => new Expression("substr(a.t_nopbphtbsppt, 7, 3)"),
            'kode_kelurahan' => new Expression("'0'||substr(a.t_nopbphtbsppt, 11, 2)"),
        ]);
        $select->join(["b" => "t_detailsptbphtb"], "b.t_idspt = a.t_idspt", [], "LEFT");
        $select->join(["c" => "s_jenistransaksi"], "c.s_idjenistransaksi = a.t_idjenistransaksi", [], "LEFT");
        $select->join(["d" => "s_jenishaktanah"], "d.s_idhaktanah = a.t_idjenishaktanah", [], "LEFT");
        $select->join(["e" => "t_pembayaranspt"], "e.t_idspt = a.t_idspt", [], "LEFT");
        $select->join(["f" => "t_pemeriksaan"], "f.p_idpembayaranspt = e.t_idpembayaranspt", [
            "p_verifikasi_adm_nilai_wajar", "p_verifikasi_adm_pertimbangan",
            "p_verifikasi_lap_nilai_wajar", "p_verifikasi_lap_pertimbangan", "p_petugas_berita_acara"
        ], "LEFT");
        $select->join(["g" => "s_users"], "g.s_iduser = a.t_idnotarisspt", [], "LEFT");
        $select->join(["h" => "s_notaris"], new Expression("h.s_idnotaris::text = g.s_idpejabat_idnotaris::text"), [], "LEFT");
        $select->join(["i" => "s_users"], new Expression("i.s_iduser = e.t_pejabatverifikasispt"), [], "LEFT");
        $select->join(["j" => "s_pejabat"], new Expression("j.s_idpejabat::text = i.s_idpejabat_idnotaris::text"), [], "LEFT");
        $select->join(["l" => "view_kelurahan"], new Expression("SUBSTRING ( A.t_nopbphtbsppt FROM 7 FOR 4 )||'0'||SUBSTRING ( A.t_nopbphtbsppt FROM 12 FOR 2 ) = ((l.s_kodekecamatan::text || '.'::text) || l.s_kodekelurahan::text)"), [], "LEFT");
        $select->join(["p" => "s_jenisdoktanah"], new Expression("a.t_idjenisdoktanah = p.s_iddoktanah"), [], "LEFT");
        if ($string != null) {
            return $sql->getSqlStringForSqlObject($select);
        } else {
            return $select;
        }
    }

    public function getDataLapBulananBphtb($bulanpelaporan, $iduser)
    {


        $sql = "select * from view_data_terbit_ajb 
                where date_part('month',t_tglajbbaru) ='" . $bulanpelaporan . "' ";
        if (!empty($iduser)) {
            $sql .= "and t_idnotaris = $iduser";
        }
        $statement = $this->adapter->query($sql);
        return $statement->execute();
    }

    public function getDataLapBulananBphtbNotaris($bulanpelaporan, $periode_spt, $idnotarisdipilih, $idnotaris, $s_tipe_pejabat)
    {

        if (!empty($idnotarisdipilih)) {
            $whereidnotaris = ' AND t_idnotarisspt = ' . $idnotarisdipilih . '';
        } else {
            if (!empty($idnotaris)) {
                if ($s_tipe_pejabat == 2) {
                    $whereidnotaris = ' AND t_idnotarisspt = ' . $idnotaris . '';
                } else {
                    $whereidnotaris = '';
                }
            }
        }

        $sql = "select * from view_data_terbit_ajb 
                where date_part('month',t_tglajbbaru) ='" . $bulanpelaporan . "' AND date_part('year',t_tglajbbaru) ='" . $periode_spt . "'  " . $whereidnotaris . " ";

        //var_dump($sql);
        //exit();

        $statement = $this->adapter->query($sql);
        return $statement->execute();
    }

    public function getDataKonfBulananBphtb($bulanpelaporankonf, $iduser)
    {
        $sql = "select * from view_data_terbit_ajb 
                where date_part('month',t_tglajbbaru) ='" . $bulanpelaporankonf . "' and t_statuskonfirmasinotaris = true and t_tglkonfirmasinotaris is not null ";
        if (!empty($iduser)) {
            $sql .= "and t_idnotaris = $iduser";
        }
        $statement = $this->adapter->query($sql);
        return $statement->execute();
    }

    public function ambildatapengurangan($data_get)
    {
        $sql = "SELECT * FROM view_pengurangan where t_idspt=$data_get->t_idspt";
        $statement = $this->adapter->query($sql);
        return $statement->execute()->current();
    }

    public function getDataHasilBpn($tgl_hasilbpn1, $tgl_hasilbpn2)
    {
        $sql = "select * from view_hasilbpn
                where t_tglprosesspt between '" . date('Y-m-d', strtotime($tgl_hasilbpn1)) . "' and '" . date('Y-m-d', strtotime($tgl_hasilbpn2)) . "'";
        $statement = $this->adapter->query($sql);
        return $statement->execute();
    }

    public function getNotaris($iduser)
    {
        $sql = "select s_namanotaris, s_alamatnotaris from view_data_notaris where s_iduser = $iduser";
        $statement = $this->adapter->query($sql);
        $state = $statement->execute();
        return $state->current();
    }


    public function getDataRealisasi($periode_spt, $tgl_cetakrealisasi)
    {
        $kalender = explode('-', $tgl_cetakrealisasi);
        $bulan = $kalender[1];

        // $sql = "select s_namajenistransaksi, 
        //             (select coalesce(sum(a.t_nilaipembayaranspt))
        //                 from view_sspd_pembayaran a
        //                 left join s_jenistransaksi b 
        //                 on b.s_idjenistransaksi = a.t_idjenistransaksi
        //                 where a.t_periodespt=$periode_spt 
        //                 and date_part('month',a.t_tanggalpembayaran) < '$bulan'
        //                 and  s_jenistransaksi.s_idjenistransaksi = b.s_idjenistransaksi
        //                 ) as real_sd_bln_lalu,
        //             (select coalesce(sum(a.t_nilaipembayaranspt))
        //                 from view_sspd_pembayaran a
        //                 left join s_jenistransaksi b 
        //                 on b.s_idjenistransaksi = a.t_idjenistransaksi
        //                 where a.t_periodespt=$periode_spt 
        //                 and date_part('month',a.t_tanggalpembayaran) = '$bulan'
        //                 and  s_jenistransaksi.s_idjenistransaksi = b.s_idjenistransaksi
        //                 ) as real_bln_ini,
        //             (select coalesce(sum(a.t_nilaipembayaranspt))
        //                 from view_sspd_pembayaran a
        //                 left join s_jenistransaksi b 
        //                 on b.s_idjenistransaksi = a.t_idjenistransaksi
        //                 where a.t_periodespt=$periode_spt 
        //                 and date_part('month',a.t_tanggalpembayaran) <= '$bulan'
        //                 and  s_jenistransaksi.s_idjenistransaksi = b.s_idjenistransaksi
        //                 ) as real_sd_bln_ini
        //         from s_jenistransaksi ";
        // $statement = $this->adapter->query($sql);
        // return $statement->execute();

        $sql = new Sql($this->adapter);
        $select = $sql->select(["a" => "s_jenistransaksi"]);
        $select->columns([
            "s_namajenistransaksi",
            "real_sd_bln_lalu" => new Expression("(
                SELECT COALESCE(SUM(bb.t_nilaipembayaranspt))
                FROM t_spt aa
                LEFT JOIN t_pembayaranspt bb on aa.t_idspt = bb.t_idspt
                WHERE aa.t_periodespt=$periode_spt 
                AND date_part('month', bb.t_tanggalpembayaran) < '$bulan'
                AND aa.t_idjenistransaksi = a.s_idjenistransaksi
            )"),
            "real_bln_ini" => new Expression("(
                SELECT COALESCE(SUM(bb.t_nilaipembayaranspt))
                FROM t_spt aa
                LEFT JOIN t_pembayaranspt bb on aa.t_idspt = bb.t_idspt
                WHERE aa.t_periodespt=$periode_spt 
                AND date_part('month', bb.t_tanggalpembayaran) = '$bulan'
                AND aa.t_idjenistransaksi = a.s_idjenistransaksi
            )"),
            "real_sd_bln_ini" => new Expression("(
                SELECT COALESCE(SUM(bb.t_nilaipembayaranspt))
                FROM t_spt aa
                LEFT JOIN t_pembayaranspt bb on aa.t_idspt = bb.t_idspt
                WHERE aa.t_periodespt=$periode_spt 
                AND date_part('month', bb.t_tanggalpembayaran) <= '$bulan'
                AND aa.t_idjenistransaksi = a.s_idjenistransaksi
            )")
        ]);
        $select->join(["b" => "s_koderekening"], "a.s_korekid = b.s_korekid", [
            "korek" => new Expression("(
                b.s_korektipe || '.' || b.s_korekkelompok || '.' || b.s_korekjenis || '.' || b.s_korekobjek || '.' || b.s_korekrincian || '.' || b.s_korekrinciansub
            )"),
            "s_koreknama"
        ], "LEFT");
        $select->order("a.s_korekid, a.s_idjenistransaksi");
        $res = $sql->prepareStatementForSqlObject($select)->execute();
        return $res;
    }

    public function getDataValidasiSkpdkb($no_sspd1, $no_sspd2)
    {
        $sql = "select *
                from view_sspd_pembayaran a
                left join t_pemeriksaan b on b.p_idpembayaranspt = a.t_idpembayaranspt
                where a.t_verifikasispt = true and a.t_kohirspt between " . $no_sspd1 . " and " . $no_sspd2 . "";
        $statement = $this->adapter->query($sql);
        return $statement->execute();
    }

    //===================== menu cetak data pendaftaran
    public function getDataPendaftaran($periode_spt, $tgl_verifikasi1, $tgl_verifikasi2, $idnotaris, $idnotarisdipilih, $s_tipe_pejabat, $post)
    {
        // if (!empty($idnotarisdipilih)) {
        //     $whereidnotaris = ' AND t_idnotarisspt = ' . $idnotarisdipilih . '';
        // } else {
        //     if (!empty($idnotaris)) {
        //         if ($s_tipe_pejabat == 2) {
        //             $whereidnotaris = ' AND t_idnotarisspt = ' . $idnotaris . '';
        //         } else {
        //             $whereidnotaris = '';
        //         }
        //     }
        // }

        // $arr_tgl = explode("-", $tgl_verifikasi1);
        // $tgl_awal = $arr_tgl[2] . "-" . $arr_tgl[1] . "-" . $arr_tgl[0];
        // $arr_tgl2 = explode("-", $tgl_verifikasi2);
        // $tgl_akhir = $arr_tgl2[2] . "-" . $arr_tgl2[1] . "-" . $arr_tgl2[0];
        // $sql = "select *
        //         from view_pendaftaran
        //         where t_periodespt=$periode_spt and t_tglprosesspt between '" . $tgl_awal . "' and '" . $tgl_akhir . "' " . @$whereidnotaris . "";
        // $statement = $this->adapter->query($sql);
        // return $statement->execute();

        $sql = new Sql($this->adapter);
        $select = $sql->select(["a" => "t_spt"]);
        $select->columns([
            "t_idspt", "fr_tervalidasidua", "t_kohirspt", "t_kohirketetapanspt",
            "t_tglprosesspt", "t_totalspt", "t_idsptsebelumnya",
            "jml_syarat_input" => new Expression("(length(translate(a.t_persyaratan::text, '[\"]1234567890'::text, ''::text)) + 1)"),
            "jml_syarat_validasi" => new Expression("(length(translate(e.t_verifikasispt::text, '[\"]1234567890'::text, ''::text)) + 1)"),
            "status_pendaftaran" => new Expression("(
                CASE
                    WHEN (length(translate(a.t_persyaratan::text, '[\"]1234567890'::text, ''::text)) + 1) = j.jmlsyarat THEN 1
                    ELSE 2
                END
            )"),
            "status_validasi" => new Expression("(
                CASE
                    WHEN (length(translate(e.t_verifikasispt::text, '[\"]1234567890'::text, ''::text)) + 1) = j.jmlsyarat AND a.t_kohirketetapanspt IS NOT NULL THEN 1
                    WHEN (length(translate(e.t_verifikasispt::text, '[\"]1234567890'::text, ''::text)) + 1) = j.jmlsyarat THEN 4
                    WHEN (length(translate(e.t_verifikasispt::text, '[\"]1234567890'::text, ''::text)) + 1) IS NULL THEN 3
                    ELSE 2
                END
            )"),
            "t_statusbayarspt" => new Expression("(
                CASE
                    WHEN e.t_statusbayarspt::text = 'true'::text THEN 'TRUE'::text
                    ELSE 'FALSE'::text
                END 
            )"),
            "jml_pajak_v1" => new Expression("(
                CASE
                    WHEN f.p_idpemeriksaan IS NOT NULL THEN f.p_totalspt
                    ELSE a.t_totalspt
                END 
            )"),
            "jml_pajak_v2" => new Expression("(
                CASE
                    WHEN f.p_idpemeriksaan IS NOT NULL THEN
                    CASE
                        WHEN a.t_totalspt < f.p_totalspt THEN f.p_totalspt
                        ELSE a.t_totalspt
                    END
                    ELSE a.t_totalspt
                END
            )"), "fr_tervalidasidua", "t_nopbphtbsppt",
            "t_thnsppt", "t_nilaitransaksispt", "t_tglprosesspt"
        ]);
        $select->join(["b" => "t_detailsptbphtb"], "b.t_idspt = a.t_idspt", [
            "fr_validasidua", "t_inputbpn", "t_namawppembeli", "t_nikwppembeli",
            "t_kelurahanwppembeli", "t_kecamatanwppembeli", "t_kabkotawppembeli",
            "t_telponwppembeli", "t_namawppenjual", "t_kelurahanwppenjual",
            "t_kecamatanwppenjual", "t_kabkotawppenjual", "t_telponwppenjual",
            "t_namasppt", "t_luastanah", "t_njoptanah", "t_luasbangunan",
            "t_njopbangunan", "t_totalnjoptanah", "t_totalnjopbangunan",
            "t_grandtotalnjop", "t_alamatwppembeli",
            "t_alamatwppenjual", "t_nikwppenjual",
            "t_alamatop", "t_kelurahanop", "t_kecamatanop", "t_kabupatenop"
        ], "LEFT");
        $select->join(["c" => "s_jenistransaksi"], "c.s_idjenistransaksi = a.t_idjenistransaksi", [
            "s_namajenistransaksi", "s_idjenistransaksi"
        ], "LEFT");
        $select->join(["e" => "t_pembayaranspt"], "e.t_idspt = a.t_idspt", [
            "ntpd" => "t_kodebayarbanksppt", "t_kodebayarbanksppt", "t_verifikasispt",
            "t_ket_verifikasi_berkas"
        ], "LEFT");
        $select->join(["f" => "t_pemeriksaan"], "f.p_idpembayaranspt = e.t_idpembayaranspt", [
            "p_idpemeriksaan"
        ], "LEFT");
        $select->join(["h" => "s_users"], "h.s_iduser = a.t_idnotarisspt", [], "LEFT");
        $select->join(["i" => "s_notaris"], new Expression("i.s_idnotaris::text = h.s_idpejabat_idnotaris::text"), [
            "s_namanotaris"
        ], "LEFT");
        $select->join(["j" => "fr_count_s_persyaratan"], "a.t_idjenistransaksi = j.s_idjenistransaksi", [
            "jml_syarat_sebenarnya" => "jmlsyarat"
        ], "LEFT");
        $where = new Where();
        if ($periode_spt != "") {
            $where->equalTo("a.t_periodespt", $periode_spt);
        }
        if ($tgl_verifikasi1 != "") {
            $where->literal("a.t_tglprosesspt >= '" . date("Y-m-d", strtotime($tgl_verifikasi1)) . "'");
        }
        if ($tgl_verifikasi2 != "") {
            $where->literal("a.t_tglprosesspt <= '" . date("Y-m-d", strtotime($tgl_verifikasi2)) . "'");
        }
        if ($s_tipe_pejabat == 2) {
            $where->equalTo("a.t_idnotarisspt", $idnotaris);
        }
        if ($idnotarisdipilih != "") {
            $where->equalTo("a.t_idnotarisspt", $idnotarisdipilih);
        }
        if ($post["status_validasi"] != "") {
            $where->equalTo(new Expression("(
                CASE
                    WHEN (length(translate(e.t_verifikasispt::text, '[\"]1234567890'::text, ''::text)) + 1) = j.jmlsyarat AND a.t_kohirketetapanspt IS NOT NULL THEN 1
                    WHEN (length(translate(e.t_verifikasispt::text, '[\"]1234567890'::text, ''::text)) + 1) = j.jmlsyarat THEN 4
                    WHEN (length(translate(e.t_verifikasispt::text, '[\"]1234567890'::text, ''::text)) + 1) IS NULL THEN 3
                    ELSE 2
                END
            )"), $post["status_validasi"]);
        }
        $select->where($where);
        $select->order("a.t_tglprosesspt");
        // die($sql->getSqlStringForSqlObject($select));
        $res = $sql->prepareStatementForSqlObject($select)->execute();
        return $res;
    }

    //===================== end menu cetak data pendaftaran
    //===================== menu cetak data status berkas
    public function getDataStatusBerkas($periode_spt, $tgl_verifikasi1, $tgl_verifikasi2, $idnotaris, $idnotarisdipilih, $s_tipe_pejabat)
    {
        if (!empty($idnotarisdipilih)) {
            $whereidnotaris = ' AND t_idnotarisspt = ' . $idnotarisdipilih . '';
        } else {
            if (!empty($idnotaris)) {
                if ($s_tipe_pejabat == 2) {
                    $whereidnotaris = ' AND t_idnotarisspt = ' . $idnotaris . '';
                } else {
                    $whereidnotaris = '';
                    //$whereidnotaris = ' AND t_idnotarisspt = ' . $idnotaris . '';
                }
            }
        }
        $arr_tgl = explode("-", $tgl_verifikasi1);
        $tgl_awal = $arr_tgl[2] . "-" . $arr_tgl[1] . "-" . $arr_tgl[0];
        $arr_tgl2 = explode("-", $tgl_verifikasi2);
        $tgl_akhir = $arr_tgl2[2] . "-" . $arr_tgl2[1] . "-" . $arr_tgl2[0];
        $sql = "select *
                from view_pendaftaran
                where t_periodespt=$periode_spt and t_tglprosesspt between '" . $tgl_awal . "' and '" . $tgl_akhir . "' " . @$whereidnotaris . "";
        $statement = $this->adapter->query($sql);
        return $statement->execute();
    }

    //===================== end menu cetak data status berkas
    //===================== menu cetak kode bayar
    public function ambilsemuadatasptvalidasi($idnotaris, $no_spt1, $no_spt2, $periode_spt)
    {
        if (!empty($idnotaris)) {
            $whereidnotaris = " WHERE t_idnotarisspt = " . $idnotaris . " and t_kohirspt >=" . $no_spt1 . " and t_kohirspt <=" . $no_spt2 . " and t_periodespt=" . $periode_spt . "";
        } else {
            $whereidnotaris = " Where t_kohirspt >=" . $no_spt1 . " and t_kohirspt <=" . $no_spt2 . " and t_periodespt=" . $periode_spt . "";
        }
        $sql = "select *
                from view_data_verifikasi_isi
                " . @$whereidnotaris . "";
        $statement = $this->adapter->query($sql);
        return $statement->execute();
    }

    public function ambildatainsptvalidasi($data_tidspt)
    {
        if (@$data_tidspt) {
            if (!is_array($data_tidspt)) {
                if (!is_numeric($data_tidspt))
                    die('format salah salah');
                $where = "where t_idspt='{$data_tidspt}' ";
            } else {
                foreach ($data_tidspt as $val) {
                    if (strlen($val) && !is_numeric($val)) {
                        return false;
                    }
                }

                $data_tidspt = implode(',', $data_tidspt);
                $where = "where t_idspt in ({$data_tidspt})";
            }
        } else {
            $where = "where t_idspt::text = 'x'";
        }
        $where = htmlspecialchars_decode($where);
        $sql = "SELECT * FROM view_data_verifikasi_isi $where ";
        $statement = $this->adapter->query($sql);
        return $statement->execute();
    }

    public function ambildatakodebayar($data_get)
    {
        //        var_dump($data_get->no_spt1);
        //        exit();
        $sql = "SELECT * FROM view_data_verifikasi_isi where t_kohirspt between $data_get->no_spt1 and $data_get->no_spt2 and t_kodebayarbanksppt_pembayaran is not null";
        $statement = $this->adapter->query($sql);
        return $statement->execute();
    }

    public function getviewcetakssp($t_idspt)
    {
        // $sql = "select * from view_cetak_sspd where t_idspt =" . $t_idspt . "";
        // $statement = $this->adapter->query($sql);
        // return $statement->execute();

        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->columns([
            "t_idspt", "t_kohirspt", "t_nilaitransaksispt", "t_approve_pengurangpembebas",
            "t_totalspt", "t_kohirketetapanspt", "t_tglprosesspt", "t_periodespt",
            "t_idnotarisspt", "t_idtarifspt", "t_tglketetapanspt", "t_tgljatuhtempospt",
            "t_nopbphtbsppt", "t_idjenistransaksi", "t_idjenishaktanah", "t_idrefspt",
            "t_dasarspt", "t_potonganspt", "t_thnsppt", "t_persyaratan",
            "t_idjenisdoktanah", "t_idsptsebelumnya", "t_tarif_pembagian_aphb_kali",
            "t_tarif_pembagian_aphb_bagi", "t_idtarifbphtb", "t_persenbphtb",
            "t_potongan_waris_hibahwasiat",
            "t_nilaipembayaranspt_sebelumnya" => new Expression("(
                CASE WHEN a.t_idsptsebelumnya IS NOT NULL THEN (SELECT x.t_nilaipembayaranspt FROM t_pembayaranspt x WHERE x.t_idspt = a.t_idsptsebelumnya LIMIT 1)
                ELSE NULL END
            )"),

            // untuk espop
            't_id_tipe_espop', 't_konfirm_espop', 't_tgl_konfirm_espop', 't_user_konfirm_espop'
        ]);
        $select->from(["a" => "t_spt"]);
        $select->join(["b" => "t_detailsptbphtb"], "a.t_idspt = b.t_idspt", [
            "t_iddetailsptbphtb", "t_namawppembeli", "t_nikwppembeli",
            "t_alamatwppembeli", "t_kecamatanwppembeli", "t_kelurahanwppembeli",
            "t_kabkotawppembeli", "t_telponwppembeli", "t_kodeposwppembeli",
            "t_npwpwppembeli", "t_namawppenjual", "t_nikwppenjual", "t_alamatwppenjual",
            "t_kecamatanwppenjual", "t_kelurahanwppenjual", "t_kabkotawppenjual",
            "t_telponwppenjual", "t_kodeposwppenjual", "t_npwpwppenjual",
            "t_luastanah", "t_njoptanah", "t_luasbangunan", "t_njopbangunan",
            "t_totalnjoptanah", "t_totalnjopbangunan", "t_grandtotalnjop",
            "t_nosertifikathaktanah", "t_kelurahanop", "t_kecamatanop",
            "t_ketwaris", "t_terbukti", "t_rtwppembeli", "t_rwwppembeli",
            "t_alamatop", "t_rtop", "t_rwop", "t_dokpersyaratan", "t_namasppt",
            "t_tglajb", "t_luastanahbpn", "t_luasbangunanbpn", "t_tglajbbaru",
            "t_noajbbaru", "t_statuspelaporannotaris", "t_tglpelaporannotaris",
            "t_kabupatenop", "t_rtwppenjual", "t_rwwppenjual", "t_nosertifikatbaru",
            "t_tglsertifikatbaru", "t_inputbpn", "t_statuskonfirmasinotaris",
            "t_tglkonfirmasinotaris", "t_grandtotalnjop_aphb"
        ], "LEFT");
        $select->join(["c" => "t_pembayaranspt"], "a.t_idspt = c.t_idspt", [
            "t_idpembayaranspt", "t_kohirpembayaran", "t_periodepembayaran",
            "t_tanggalpembayaran", "t_idnotaris", "t_nilaipembayaranspt", "t_idkorekspt",
            "t_kodebayarspt", "t_verifikasispt", "t_tglverifikasispt", "t_tglverifikasiberkas",
            "t_pejabatverifikasispt", "t_statusbayarspt", "t_kodebayarbanksppt",
            "t_dendabulan", "t_pejabatpembayaranspt", "t_idpenerimasetoran"
            // 't_kodebayarbanksppt_pembayaran' => 't_kodebayarbanksppt'
        ], "LEFT");
        $select->join(["d" => "t_pemeriksaan"], "d.p_idpembayaranspt = c.t_idpembayaranspt", [
            "p_totalspt", "p_idpemeriksaan", "p_idpembayaranspt", "p_luastanah",
            "p_luasbangunan", "p_njoptanah", "p_njopbangunan", "p_totalnjoptanah",
            "p_totalnjopbangunan", "p_grandtotalnjop", "p_nilaitransaksispt",
            "p_potonganspt", "p_ketwaris", "p_terbukti", "p_idjenistransaksi",
            "p_idjenishaktanah", "p_nilaipembayaranspt", "p_nilaikurangbayar",
            "p_kohirskpdkb", "p_pembayaranskpdkb", "p_grandtotalnjop_aphb"
        ], "LEFT");
        $select->join(["e" => "s_users"], "e.s_iduser = a.t_idnotarisspt", [
            "s_iduser", "s_username", "s_password", "s_jabatan", "s_akses",
            "s_idpejabat_idnotaris", "s_tipe_pejabat",
        ], "LEFT");
        $select->join(["f" => "s_notaris"], new Expression("f.s_idnotaris::text = e.s_idpejabat_idnotaris::text"), [
            "s_idnotaris", "s_namanotaris",
        ], "LEFT");
        $select->join(["g" => "s_pejabat"], "g.s_idpejabat = c.t_pejabatverifikasispt", [
            "s_idpejabat", "s_namapejabat", "s_jabatanpejabat", "s_nippejabat",
            "s_golonganpejabat",
        ], "LEFT");
        $select->join(["h" => "s_users"], "h.s_iduser = c.t_idpenerimasetoran", [], "LEFT");
        $select->join(["i" => "s_pejabat"], new Expression("i.s_idpejabat::text = h.s_idpejabat_idnotaris::text"), [
            "namapenerimasetoran" => "s_namapejabat"
        ], "LEFT");
        $select->join(["j" => "s_jenistransaksi"], new Expression("a.t_idjenistransaksi::text = j.s_idjenistransaksi::text"), [
            "s_namajenistransaksi"
        ], "LEFT");
        $select->join(["k" => "t_spt_ptsl"], "a.t_idspt = k.t_idspt", [
            "t_id_ptsl", "t_tgl_ptsl", "t_ket_ptsl"
        ], "LEFT");
        $select->join(["l" => "t_pengurangan"], "a.t_idspt = l.t_idsptpengurangan", [
            "t_totalsptsebelumnya", "t_persenpengurangan", "t_pengurangan", "t_totalpajak"
        ], "LEFT");
        $where = new Where();
        $where->equalTo("a.t_idspt", (int) $t_idspt);
        $select->where($where);
        $res = $sql->prepareStatementForSqlObject($select)->execute();
        $resultSet = new ResultSet();
        $resultSet->initialize($res);
        $resultSet->buffer();
        $res = $resultSet->toArray();
        return $res;
    }

    public function getviewcetaformulirsurvey($t_idspt)
    {
        $sql = "select * from view_cetak_sspd where t_idspt =" . $t_idspt . "";
        $statement = $this->adapter->query($sql);
        return $statement->execute();
    }

    public function getviewcetakssp_ppob($t_idspt)
    {
        $sql = "select * from view_cetak_sspd where t_idspt =" . $t_idspt . "";
        $statement = $this->adapter->query($sql);
        return $statement->execute();
    }

    public function getviewcetakssp_mutasi($t_idspt)
    {
        $sql = "select * from view_cetak_sspd LEFT JOIN s_jenistransaksi ON view_cetak_sspd.t_idjenistransaksi=s_jenistransaksi.s_idjenistransaksi where t_idspt =" . $t_idspt . "";
        $statement = $this->adapter->query($sql);
        return $statement->execute();
    }

    public function getviewcetakssp_spop($t_idspt)
    {
        $sql = "select * from view_cetak_sspd LEFT JOIN s_jenistransaksi ON view_cetak_sspd.t_idjenistransaksi=s_jenistransaksi.s_idjenistransaksi where t_idspt =" . $t_idspt . "";
        $statement = $this->adapter->query($sql);
        return $statement->execute();
    }

    public function getviewcetakvalidasipembayaran($t_idspt)
    {
        $sql = "select * from view_sspd_semua_pembayaran where t_tanggalpembayaran is not null and t_statusbayarspt=true t_idspt = $t_idspt";
        $statement = $this->adapter->query($sql);
        $state = $statement->execute();
        return $state->current();
    }


    public function jumlahpembayaranperiodeini($tahun, $idnotaris, $tgl_cetak1, $tgl_cetak2)
    {
        if (!empty($idnotaris)) {
            $where = " AND t_idnotarisspt = " . $idnotaris;
        } else {
            $where = " ";
        }

        $sql = "select sum(t_nilaipembayaranspt)
                from view_sspd_pembayaran
                where date_part('year', t_tanggalpembayaran) = " . $tahun . " AND t_tanggalpembayaran between '" . date('Y-m-d', strtotime($tgl_cetak1)) . "' and '" . date('Y-m-d', strtotime($tgl_cetak2)) . "' AND t_tanggalpembayaran IS NOT NULL " . $where . " ";
        $statement = $this->adapter->query($sql);
        $state = $statement->execute();
        return $state->current();
    }

    public function jumlahpembayaranperiodesebelum($tahun, $idnotaris, $tgl_cetak1)
    {
        if (!empty($idnotaris)) {
            $where = " AND t_idnotarisspt = " . $idnotaris;
        } else {
            $where = " ";
        }

        $sql = "select sum(t_nilaipembayaranspt)
                from view_sspd_pembayaran
                where date_part('year', t_tanggalpembayaran) = " . $tahun . " AND t_tanggalpembayaran < '" . date('Y-m-d', strtotime($tgl_cetak1)) . "' AND t_tanggalpembayaran IS NOT NULL " . $where . " ";
        $statement = $this->adapter->query($sql);
        $state = $statement->execute();
        return $state->current();
    }


    public function getDataSetorBphtb($idnotaris, $tgl1, $tgl2)
    {

        if (!empty($idnotaris)) {
            $where = " AND t_idnotarisspt = " . $idnotaris;
        } else {
            $where = " ";
        }

        $sql = "select *
                from view_sspd_pembayaran
                where t_tanggalpembayaran between '" . date('Y-m-d', strtotime($tgl1)) . "' and '" . date('Y-m-d', strtotime($tgl2)) . "' order by t_tanggalpembayaran asc";
        //die($sql);
        $statement = $this->adapter->query($sql);
        return $statement->execute();
    }

    public function getRealiasiBphtbDesa($data_get)
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select(["a" => "s_kecamatan"]);
        $select->columns(["s_kodekecamatan", "s_namakecamatan"]);
        $select->join(["b" => "s_kelurahan"], "a.s_idkecamatan = b.s_idkecamatan", [
            "s_kodekelurahan", "s_namakelurahan"
        ], "INNER");
        $select->join(
            ["c" => "t_spt"],
            new Expression("b.s_kodekelurahan = '0'||substr(c.t_nopbphtbsppt, 12, 2) AND a.s_kodekecamatan = substr(c.t_nopbphtbsppt, 7, 3)"),
            [],
            "LEFT"
        );
        $select->join(["d" => "t_pembayaranspt"], new Expression("
        d.t_tanggalpembayaran::DATE 
        BETWEEN '" . date("Y-m-d", strtotime($data_get["tgl_cetakbphtb_perdesa"])) . "' AND '" . date("Y-m-d", strtotime($data_get["tgl_cetakbphtb2_perdesa"])) . "' 
        AND c.t_idspt = d.t_idspt"), [
            "jml_bayar" => new Expression("(COALESCE(SUM(d.t_nilaipembayaranspt),0))")
        ], "LEFT");
        $where = new Where();
        // $where->between(new Expression("d.t_tanggalpembayaran::DATE"), date("Y-m-d", strtotime($data_get["tgl_cetakbphtb_perdesa"])), date("Y-m-d", strtotime($data_get["tgl_cetakbphtb2_perdesa"])));
        $select->where($where);
        $select->group(new Expression("a.s_kodekecamatan, a.s_namakecamatan, b.s_kodekelurahan, b.s_namakelurahan"));
        $select->order(new Expression("a.s_kodekecamatan, b.s_kodekelurahan"));
        // die($sql->getSqlStringForSqlObject($select));
        $res = $sql->prepareStatementForSqlObject($select)->execute();
        return $res;
    }
}
