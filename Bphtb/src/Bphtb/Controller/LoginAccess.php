<?php

namespace Bphtb\Controller;

use Zend\Authentication\Adapter\DbTable as DbTableAuthAdapter;

class LoginAccess extends \Zend\Mvc\Controller\AbstractActionController
{

    protected $tbl_user, $tbl_pemda;

    public function indexAction()
    {
        $frm = new \Bphtb\Form\LoginAccessFrm();
        $ar_pemda = $this->getPemda()->getdata();
        $view = new \Zend\View\Model\ViewModel(array(
            "form" => $frm,
            'data_pemda' => $ar_pemda
        ));

        if ($this->getRequest()->isPost()) {
            $post = $this->getRequest()->getPost();

            $secretAPIkey = '6LcCH7kpAAAAADviJBJkWN_dL3XzdHnBq9fC4D1B';

            $ch = curl_init();

            curl_setopt($ch, CURLOPT_URL, 'https://www.google.com/recaptcha/api/siteverify');
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query(array(
                'secret' => $secretAPIkey,
                'response' => $post['g-recaptcha-response']
            )));

            $verifyResponse = curl_exec($ch);

            curl_close($ch);

            $response = json_decode($verifyResponse);

            if ($response->success) {
                $frm->setData($this->getRequest()->getPost());
                if ($frm->isValid()) {
                    $this->getServiceLocator()->get('EtaxService')
                        ->getAdapter()
                        ->setIdentity($this->getRequest()->getPost('s_username'))
                        ->setCredential($this->getRequest()->getPost('s_password'));
                    if (empty($this->getRequest()->getPost('s_username'))) {
                        return $this->redirect()->toRoute('sign_in');
                    }
                    $hasil = $this->getServiceLocator()->get('EtaxService')->authenticate();
                    if ($hasil->isValid()) {
                        $data_user = $this->getTbl()->getuserdata($this->getRequest()->getPost('s_username'));
                        $resultRowObject = $this->getServiceLocator()->get('EtaxService')->getAdapter()->getResultRowObject();
                        $this->getServiceLocator()->get('EtaxService')->getStorage()->write(
                            array(
                                's_iduser' => $resultRowObject->s_iduser,
                                's_username' => $resultRowObject->s_username,
                                's_jabatan' => $resultRowObject->s_jabatan,
                                's_akses' => $resultRowObject->s_akses,
                                's_tipe_pejabat' => $resultRowObject->s_tipe_pejabat,
                                's_namauserrole' => $data_user['role_name'],
                                's_idpejabat' => $data_user["s_idpejabat_idnotaris"],
                                's_id_user_espop' => $data_user['s_id_user_espop']
                            )
                        );
                        $this->getServiceLocator()->get('LoginLogTable')->saveLog([
                            "s_username" => $this->getRequest()->getPost('s_username'),
                            "s_message" => "Sukses",
                            "s_ipaddress" => $this->getUserIP()
                        ]);
                        return $this->redirect()->toRoute('main_bphtb');
                    } else {
                        $this->getServiceLocator()->get('LoginLogTable')->saveLog([
                            "s_username" => $this->getRequest()->getPost('s_username'),
                            "s_message" => "Username dengan Password tidak cocok",
                            "s_ipaddress" => $this->getUserIP()
                        ]);
                    }
                }
            } else {
                $this->logoutAction();
            }
        }

        $data = array('nilai' => '1');
        $this->layout()->setVariables($data);
        return $view;
    }

    public function logoutAction()
    {
        $this->getServiceLocator()->get('EtaxService')->clearIdentity();
        return $this->redirect()->toRoute('sign_in');
    }

    public function getTbl()
    {
        if (!$this->tbl_user) {
            $this->tbl_user = $this->getServiceLocator()->get('UserTable');
        }
        return $this->tbl_user;
    }

    public function getPemda()
    {
        if (!$this->tbl_pemda) {
            $sm = $this->getServiceLocator();
            $this->tbl_pemda = $sm->get("PemdaTable");
        }
        return $this->tbl_pemda;
    }

    public function getUserIP()
    {
        $ipaddress = '';
        if (isset($_SERVER['HTTP_CLIENT_IP']))
            $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
        else if (isset($_SERVER['HTTP_X_FORWARDED_FOR']))
            $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
        else if (isset($_SERVER['HTTP_X_FORWARDED']))
            $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
        else if (isset($_SERVER['HTTP_X_CLUSTER_CLIENT_IP']))
            $ipaddress = $_SERVER['HTTP_X_CLUSTER_CLIENT_IP'];
        else if (isset($_SERVER['HTTP_FORWARDED_FOR']))
            $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
        else if (isset($_SERVER['HTTP_FORWARDED']))
            $ipaddress = $_SERVER['HTTP_FORWARDED'];
        else if (isset($_SERVER['REMOTE_ADDR']))
            $ipaddress = $_SERVER['REMOTE_ADDR'];
        else
            $ipaddress = 'UNKNOWN';
        return $ipaddress;
    }
}
