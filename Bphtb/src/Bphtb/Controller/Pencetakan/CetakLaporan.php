<?php

namespace Bphtb\Controller\Pencetakan;

use Zend\Debug\Debug;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class CetakLaporan extends AbstractActionController
{

    protected $tbl_pemda, $tbl_sspd, $tbl_pejabat, $tbl_pendataan, $tbl_jenistransaksi, $tbl_haktanah, $tbl_pembayaran, $tbl_pelaporan;

    public function indexAction()
    {
        $ar_pemda = $this->getPemda()->getdata();
        $Pejabat = $this->getTblPejabat()->getdata();
        $Pejabatbphtb2 = $this->getTblPejabat()->getdata();
        $Mengetahui = $this->getTblPejabat()->getdata();
        $PejabatRealisasi = $this->getTblPejabat()->getdata();
        $MengetahuiRealisasi = $this->getTblPejabat()->getdata();
        $Mengetahuibphtb = $this->getTblPejabat()->getdata();

        $Mengetahuibphtb2 = $this->getTblPejabat()->getdata();
        $PejabatSkpdkb = $this->getTblPejabat()->getdata();

        //========= Model\Setting\PejabatBphtbTable
        $notaris = $this->getTblPejabat()->getdataNotaris();
        $notaris_harian = $this->getTblPejabat()->getdataNotaris();
        $notarisbphtb2 = $this->getTblPejabat()->getdataNotaris();

        $PejabatRealisasi_perdesa = $this->getTblPejabat()->getdata();
        $MengetahuiRealisasi_perdesa = $this->getTblPejabat()->getdata();

        $data_kecamatan = $this->getTblPejabat()->getdataKecamatan();
        $data_kelurahan = $this->getTblPejabat()->getdataKelurahan();
        $notaris_perdesa = $this->getTblPejabat()->getdataNotaris();
        $jenisTransaksi = $this->getServiceLocator()->get('JenisTransaksiBphtbTable')->comboBox();

        $view = new ViewModel(array(
            'data_pejabat' => $Pejabat,
            'data_pejabatbphtb2' => $Pejabatbphtb2,
            'data_mengetahui' => $Mengetahui,
            'data_pejabatrealisasi' => $PejabatRealisasi,
            'data_mengetahuirealisasi' => $MengetahuiRealisasi,
            'data_mengetahuibphtb' => $Mengetahuibphtb,
            'data_mengetahuibphtb2' => $Mengetahuibphtb2,
            'data_pejabatskpdkb' => $PejabatSkpdkb,
            'data_notaris' => $notaris,
            'data_notarisbphtb2' => $notarisbphtb2,
            'data_notaris_harian' => $notaris_harian,
            'data_pejabatrealisasi_perdesa' => $PejabatRealisasi_perdesa,
            'data_mengetahuirealisasi_perdesa' => $MengetahuiRealisasi_perdesa,
            'data_kecamatan' => $data_kecamatan,
            'data_kelurahan' => $data_kelurahan,
            'data_notaris_perdesa' => $notaris_perdesa,
            'jenisTransaksi' => $jenisTransaksi
        ));
        $session = $this->getServiceLocator()->get('EtaxService')->getStorage()->read();
        $data = array(
            'menu_cetaklaporan' => 'active',
            'role_id' => $session['s_akses'],
            'data_pemda' => $ar_pemda,
            'aturgambar' => 1,
            'username' => $session['s_username']
        );
        $this->layout()->setVariables($data);
        return $view;
    }


    public function cetaklapbphtbAction()
    {
        ini_set('memory_limit', '-1');
        $req = $this->getRequest();
        $data_get = $req->getQuery();
        // var_dump($data_get);exit();
        // idnotaris

        if ($data_get->nihiltampil == 2) {
            // $nihiltidaktampil = " AND t_nilaipembayaranspt != 0";
            $nihiltidaktampil = "t_nilaipembayaranspt != 0";
        } else if ($data_get->nihiltampil == 3) {
            // $nihiltidaktampil = " AND t_nilaipembayaranspt=0";
            $nihiltidaktampil = "t_nilaipembayaranspt=0";
        } else {
            // $nihiltidaktampil = " ";
            $nihiltidaktampil = "";
        }

        //=================== Model\Pencetakan\SSPDTable
        if (($data_get->cetak == 1) || ($data_get->cetak == 2)) {
            $order = "t_tanggalpembayaran";
            $ar_LapBphtb = $this->getTableSSPD()->getDataLapBphtbBulanan($data_get->tgl_cetakbphtb1, $data_get->tgl_cetakbphtb2, $data_get->idnotaris, $order, $nihiltidaktampil, $data_get);
        } elseif (($data_get->cetak == 5) || ($data_get->cetak == 6)) {
            $order = "s_idnotaris, t_tanggalpembayaran";

            $ar_LapBphtb = $this->getTableSSPD()->getDataLapBphtbBulanan($data_get->tgl_cetakbphtb1, $data_get->tgl_cetakbphtb2, $data_get->idnotaris, $order, $nihiltidaktampil, $data_get);
        } elseif (($data_get->cetak == 7) || ($data_get->cetak == 8)) {
            $order = "t_idjenistransaksi, t_tanggalpembayaran";

            $ar_LapBphtb = $this->getTableSSPD()->getDataLapBphtbBulanan($data_get->tgl_cetakbphtb1, $data_get->tgl_cetakbphtb2, $data_get->idnotaris, $order, $nihiltidaktampil, $data_get);
        } else {
            $order = "t_tanggalpembayaran";

            $ar_LapBphtb = $this->getTableSSPD()->getDataLapBphtbBulanan($data_get->tgl_cetakbphtb1, $data_get->tgl_cetakbphtb2, $data_get->idnotaris, $order, $nihiltidaktampil, $data_get);
            //$ar_LapBphtb = $this->getTableSSPD()->getDataLapBphtbBulanan_v2($data_get->tgl_cetakbphtb1, $data_get->tgl_cetakbphtb2, $data_get->idnotaris);
        }

        $ar_Mengetahui = $this->getTblPejabat()->getdataid($data_get->mengetahuibphtb);
        $ar_pemda = $this->getPemda()->getdata();
        $session = $this->getServiceLocator()->get('EtaxService')->getStorage()->read();

        $tanggal_bayar = explode('-', $data_get->tgl_cetakbphtb1);
        $tahun = $tanggal_bayar[2];

        $periodeini = $this->getTableSSPD()->jumlahpembayaranperiodeini($tahun, $data_get->idnotaris, $data_get->tgl_cetakbphtb1, $data_get->tgl_cetakbphtb2);

        $periodesebelum = $this->getTableSSPD()->jumlahpembayaranperiodesebelum($tahun, $data_get->idnotaris, $data_get->tgl_cetakbphtb1);

        $view = new ViewModel(array(
            'data_lapBphtb' => $ar_LapBphtb,
            'data_mengetahui' => $ar_Mengetahui,
            'tgl_cetakbphtb1' => $data_get->tgl_cetakbphtb1,
            'tgl_cetakbphtb2' => $data_get->tgl_cetakbphtb2,
            'ar_pemda' => $ar_pemda,
            'nama_login' => $session['s_namauserrole'],
            'tgl_cetakan' => $data_get->tgl_cetakan,
            'cetak' => $data_get->cetak,
            'periodeini' => $periodeini['sum'],
            'periodesebelum' => $periodesebelum['sum']
        ));
        $data = array(
            'nilai' => '3' //no layout
        );
        $this->layout()->setVariables($data);
        return $view;
    }

    public function cetaklapharianAction()
    {
        $req = $this->getRequest();
        $data_get = $req->getQuery();

        //=================== Model\Pencetakan\SSPDTable
        $ar_LapHarian = $this->getTableSSPD()->getDataLapHarianSetor($data_get->tgl_cetak, $data_get->idnotaris_harian); //getDataLapHarian
        $ar_Mengetahui = $this->getTblPejabat()->getdataid($data_get->mengetahui);
        $ar_Pejabat = $this->getTblPejabat()->getdataid($data_get->pejabat);
        $ar_pemda = $this->getPemda()->getdata();
        //$session = new \Zend\Session\Container('user_session');
        $session = $this->getServiceLocator()->get('EtaxService')->getStorage()->read();
        /*$pdf = new \DOMPDFModule\View\Model\PdfModel();
        $pdf->setOption('filename', 'Daftar Laporan Harian BPHTB');
        $pdf->setOption('paperSize', 'legal');
        $pdf->setOption('paperOrientation', 'potrait');

        $pdf->setVariables(array(
            'data_lapharian' => $ar_LapHarian,
            'data_mengetahui' => $ar_Mengetahui,
            'data_pejabat' => $ar_Pejabat,
            'tgl_cetak' => $data_get->tgl_cetak,
            'data_pemda' => $ar_pemda,
            'nama_login' => $session['s_iduser'],
            'tgl_cetakan_harian' => $data_get->tgl_cetakan_harian,
        ));
        return $pdf;*/
        $view = new ViewModel(array(
            'data_lapharian' => $ar_LapHarian,
            'data_mengetahui' => $ar_Mengetahui,
            'data_pejabat' => $ar_Pejabat,
            'tgl_cetak' => $data_get->tgl_cetak,
            'ar_pemda' => $ar_pemda,
            'nama_login' => $session['s_iduser'],
            'tgl_cetakan_harian' => $data_get->tgl_cetakan_harian,
            'cetak' => $data_get->cetak
        ));
        $data = array(
            'nilai' => '3' //no layout
        );
        $this->layout()->setVariables($data);
        return $view;
    }

    public function cetaklapbulananbphtbAction()
    {
        $session = $this->getServiceLocator()->get('EtaxService')->getStorage()->read();
        $req = $this->getRequest();
        $data_get = $req->getQuery();
        $panggildata = $this->getServiceLocator()->get("SSPDTable");
        if ($session['s_namauserrole'] == "Administrator" || $session['s_namauserrole'] == "Pegawai") {
            if (!empty($data_get->t_idnotarisspt1)) {
                $data_notaris = $panggildata->getNotaris($data_get->t_idnotarisspt1);
            } else {
                $data_notaris = '';
            }
            //$idnotaris = $data_get->t_idnotarisspt;
        } else if ($session['s_namauserrole'] == "Notaris") {
            //$idnotaris = $session['s_iduser'];
            $data_notaris = $panggildata->getNotaris($session['s_iduser']);
        }
        //========================== Model\Pencetakan\SSPDTable


        $ar_LapbulananBphtb = $panggildata->getDataLapBulananBphtbNotaris($data_get->bulanpelaporan, $data_get->periode_spt, $data_get->t_idnotarisspt1, $session['s_iduser'], $session['s_tipe_pejabat']);
        //if (!empty($idnotaris)) {
        //    $data_notaris = $panggildata->getNotaris($session['s_iduser']);
        //}
        $ar_pemda = $this->getPemda()->getdata();
        /*$pdf = new \DOMPDFModule\View\Model\PdfModel();
        $pdf->setOption('filename', 'laporanBPHTB.pdf');
        $pdf->setOption('paperSize', 'legal');
        $pdf->setOption('paperOrientation', 'landscape');

        $pdf->setVariables(array(
            'data_lapBulananBphtb' => $ar_LapbulananBphtb,
            'tgl_cetak' => $data_get->tgl_cetaklapbulanan,
            'bulanpelaporan' => $data_get->bulanpelaporan,
            'data_pemda' => $ar_pemda,
            'data_notaris' => $data_notaris,
            'periode_spt' => $data_get->periode_spt    
        ));
        return $pdf;*/
        $view = new ViewModel(array(
            'data_lapBulananBphtb' => $ar_LapbulananBphtb,
            'tgl_cetak' => $data_get->tgl_cetaklapbulanan,
            'bulanpelaporan' => $data_get->bulanpelaporan,
            'ar_pemda' => $ar_pemda,
            'data_notaris' => $data_notaris,
            'periode_spt' => $data_get->periode_spt,
            'cetak' => $data_get->cetak
        ));
        $data = array(
            'nilai' => '3' //no layout
        );
        $this->layout()->setVariables($data);
        return $view;
    }

    public function cetaklapbulananbphtb2Action()
    {
        $session = $this->getServiceLocator()->get('EtaxService')->getStorage()->read();
        $req = $this->getRequest();
        $data_get = $req->getQuery();
        /*if ($session['s_namauserrole'] == "Administrator" || $session['s_namauserrole'] == "Pegawai") {
            $idnotaris = $data_get->t_idnotarisspt;
        } else if ($session['s_namauserrole'] == "Notaris") {
            $idnotaris = $session['s_iduser'];
        }*/
        //========================== Model\Pencetakan\SSPDTable
        $ar_LapbulananBphtb = $this->getTableSSPD()->getDataLapBulananBphtbNotaris($data_get->bulanpelaporan, $data_get->periode_spt, $data_get->t_idnotarisspt1, $session['s_iduser'], $session['s_tipe_pejabat']);
        //if (!empty($idnotaris)) {
        $data_notaris = $this->getTableSSPD()->getNotaris($session['s_iduser']);
        //}
        $ar_pemda = $this->getPemda()->getdata();
        $pdf = new \DOMPDFModule\View\Model\PdfModel();
        $pdf->setOption('filename', 'Daftar Laporan Bulanan BPHTB');
        $pdf->setOption('paperSize', 'legal');
        $pdf->setOption('paperOrientation', 'landscape');

        $pdf->setVariables(array(
            'data_lapBulananBphtb' => $ar_LapbulananBphtb,
            'tgl_cetak' => $data_get->tgl_cetaklapbulanan,
            'bulanpelaporan' => $data_get->bulanpelaporan,
            'data_pemda' => $ar_pemda,
            'data_notaris' => $data_notaris,
            'periode_spt' => $data_get->periode_spt
        ));
        return $pdf;
    }

    public function cetakrealisasibphtbAction()
    {
        $req = $this->getRequest();
        $data_get = $req->getQuery();

        //=================== Model\Pencetakan\SSPDTable
        $ar_DataRealisasi = $this->getTableSSPD()->getDataRealisasi($data_get->periode_sptrealisasi, $data_get->tgl_cetakrealisasi);
        $ar_Mengetahui = $this->getTblPejabat()->getdataid($data_get->mengetahui);
        $ar_Pejabat = $this->getTblPejabat()->getdataid($data_get->pejabat);
        $ar_pemda = $this->getPemda()->getdata();
        /*$pdf = new \DOMPDFModule\View\Model\PdfModel();
        $pdf->setOption('filename', 'Daftar Realisasi');
        $pdf->setOption('paperSize', 'legal');
        $pdf->setOption('paperOrientation', 'potrait');
        $pdf->setVariables(array(
            'data_Realisasi' => $ar_DataRealisasi,
            'data_mengetahui' => $ar_Mengetahui,
            'data_pejabat' => $ar_Pejabat,
            'tgl_cetak' => $data_get->tgl_cetakrealisasi,
            'periode_spt' => $data_get->periode_sptrealisasi,
            'data_pemda' => $ar_pemda,
            'tgl_cetakan_realisasi' => $data_get->tgl_cetakan_realisasi
        ));
        return $pdf;*/
        $view = new ViewModel(array(
            'data_Realisasi' => $ar_DataRealisasi,
            'data_mengetahui' => $ar_Mengetahui,
            'data_pejabat' => $ar_Pejabat,
            'tgl_cetak' => $data_get->tgl_cetakrealisasi,
            'periode_spt' => $data_get->periode_sptrealisasi,
            'ar_pemda' => $ar_pemda,
            'tgl_cetakan_realisasi' => $data_get->tgl_cetakan_realisasi,
            'cetak' => $data_get->cetak
        ));
        $data = array(
            'nilai' => '3' //no layout
        );
        $this->layout()->setVariables($data);
        return $view;
    }

    public function cetakskpdkbAction()
    {
        $req = $this->getRequest();
        $data_get = $req->getQuery();
        $ar_DataSkpdkb = $this->getTableSSPD()->getDataValidasiSkpdkb($data_get->no_sspdbphtb1, $data_get->no_sspdbphtb2);
        $ar_Pejabat = $this->getTblPejabat()->getdataid($data_get->pejabatskpdkb);
        $ar_pemda = $this->getPemda()->getdata();
        $pdf = new \DOMPDFModule\View\Model\PdfModel();
        $pdf->setOption('filename', 'Data SKPDKB');
        $pdf->setOption('paperSize', 'legal');
        $pdf->setOption('paperOrientation', 'potrait');
        $pdf->setVariables(array(
            'data_Skpdkb' => $ar_DataSkpdkb,
            'data_pejabat' => $ar_Pejabat,
            'tgl_cetak' => $data_get->tgl_cetakskpdkb,
            'periode_skpdkb' => $data_get->periode_skpdkb,
            'data_pemda' => $ar_pemda
        ));
        return $pdf;
    }

    public function dataGridSkpdkbAction()
    {
        $res = $this->getResponse();
        $page = $this->getRequest()->getPost('page') ? $this->getRequest()->getPost('page') : 1;
        $rp = $this->getRequest()->getPost('rp') ? $this->getRequest()->getPost('rp') : 10;
        $sortname = $this->getRequest()->getPost('sortname') ? $this->getRequest()->getPost('sortname') : 't_kohirspt';
        $sortorder = $this->getRequest()->getPost('sortorder') ? $this->getRequest()->getPost('sortorder') : 'desc';
        $query = $this->getRequest()->getPost('query') ? $this->getRequest()->getPost('query') : false;
        $qtype = $this->getRequest()->getPost('qtype') ? $this->getRequest()->getPost('qtype') : false;
        $count = $this->getTblPembayaran()->getGridCountValidasi($query, $qtype);
        $start = (($page - 1) * $rp);
        $res->getHeaders()->addheaders(array('Content-type' => 'text/xml'));
        $s = "<?xml version='1.0' encoding='utf-8'?>";
        $s .= "<rows>";
        $s .= "<page>" . $page . "</page>";
        $s .= "<total>" . $count . "</total>";
        $s .= "<records>" . $count . "</records>";
        $data = $this->getTblPembayaran()->getGridDataValidasi($sortname, $sortorder, $query, $qtype, $start, $rp);
        foreach ($data as $row) {
            $s .= "<row id='" . $row['t_idspt'] . "'>";
            $s .= "<cell>" . $row['t_kohirspt'] . "</cell>";
            $s .= "<cell>" . $row['t_periodespt'] . "</cell>";
            $s .= "<cell>" . $row['t_tglprosesspt'] . "</cell>";
            $s .= "<cell>" . $row['t_namawppembeli'] . "</cell>";
            $s .= "<cell><![CDATA[<a href='#' onclick='pilihSkpdkbBphtb(" . $row['t_idspt'] . ");return false;' >Pilih</a>]]></cell>";
            $s .= "</row>";
        }
        $s .= "</rows>";
        $res->setContent($s);
        return $res;
    }

    public function pilihSkpdkbBphtbAction()
    {
        $frm = new \Bphtb\Form\Pendataan\SSPDFrm($this->populateComboJenisTransaksi(), $this->populateComboHakTanah());
        $req = $this->getRequest();
        $res = $this->getResponse();
        if ($req->isPost()) {
            $ex = new \Bphtb\Model\Pendataan\SSPDBphtbBase();
            $frm->setData($req->getPost());
            if (!$frm->isValid()) {
                $ex->exchangeArray($frm->getData());
                $data = $this->getPendataanBphtb()->getPendataanSspdBphtb($ex);
                $res->setContent(\Zend\Json\Json::encode($data));
            }
        }
        return $res;
    }

    public function dataGridSkpdkb2Action()
    {
        $res = $this->getResponse();
        $page = $this->getRequest()->getPost('page') ? $this->getRequest()->getPost('page') : 1;
        $rp = $this->getRequest()->getPost('rp') ? $this->getRequest()->getPost('rp') : 10;
        $sortname = $this->getRequest()->getPost('sortname') ? $this->getRequest()->getPost('sortname') : 't_kohirspt';
        $sortorder = $this->getRequest()->getPost('sortorder') ? $this->getRequest()->getPost('sortorder') : 'desc';
        $query = $this->getRequest()->getPost('query') ? $this->getRequest()->getPost('query') : false;
        $qtype = $this->getRequest()->getPost('qtype') ? $this->getRequest()->getPost('qtype') : false;
        $count = $this->getTblPembayaran()->getGridCountValidasi($query, $qtype);
        $start = (($page - 1) * $rp);
        $res->getHeaders()->addheaders(array('Content-type' => 'text/xml'));
        $s = "<?xml version='1.0' encoding='utf-8'?>";
        $s .= "<rows>";
        $s .= "<page>" . $page . "</page>";
        $s .= "<total>" . $count . "</total>";
        $s .= "<records>" . $count . "</records>";
        $data = $this->getTblPembayaran()->getGridDataValidasi($sortname, $sortorder, $query, $qtype, $start, $rp);
        foreach ($data as $row) {
            $s .= "<row id='" . $row['t_idspt'] . "'>";
            $s .= "<cell>" . $row['t_kohirspt'] . "</cell>";
            $s .= "<cell>" . $row['t_periodespt'] . "</cell>";
            $s .= "<cell>" . $row['t_tglprosesspt'] . "</cell>";
            $s .= "<cell>" . $row['t_namawppembeli'] . "</cell>";
            $s .= "<cell><![CDATA[<a href='#' onclick='pilihSkpdkbBphtb2(" . $row['t_idspt'] . ");return false;' >Pilih</a>]]></cell>";
            $s .= "</row>";
        }
        $s .= "</rows>";
        $res->setContent($s);
        return $res;
    }

    public function pilihSkpdkbBphtb2Action()
    {
        $frm = new \Bphtb\Form\Pendataan\SSPDFrm($this->populateComboJenisTransaksi(), $this->populateComboHakTanah());
        $req = $this->getRequest();
        $res = $this->getResponse();
        if ($req->isPost()) {
            $ex = new \Bphtb\Model\Pendataan\SSPDBphtbBase();
            $frm->setData($req->getPost());
            if (!$frm->isValid()) {
                $ex->exchangeArray($frm->getData());
                $data = $this->getPendataanBphtb()->getPendataanSspdBphtb($ex);
                $res->setContent(\Zend\Json\Json::encode($data));
            }
        }
        return $res;
    }

    public function cetaklapbphtbperdesaAction()
    {
        $req = $this->getRequest();
        $data_get = $req->getQuery();

        //=================== Model\Pencetakan\SSPDTable
        if ($data_get->nihil == 2) {
            $nihiltidaktampil = "a.t_nilaipembayaranspt != 0";
            $ar_LapBphtb = $this->getTableSSPD()->getDataLapBphtbBulanan_perdesa($data_get->tgl_cetakbphtb_perdesa, $data_get->tgl_cetakbphtb2_perdesa, $data_get->idnotaris_perdesa, $data_get->kecamatan_perdesa, $data_get->kelurahan_perdesa, $nihiltidaktampil);
        } else if ($data_get->nihil == 3) {
            $nihiltidaktampil = "a.t_nilaipembayaranspt = 0";
            $ar_LapBphtb = $this->getTableSSPD()->getDataLapBphtbBulanan_perdesa($data_get->tgl_cetakbphtb_perdesa, $data_get->tgl_cetakbphtb2_perdesa, $data_get->idnotaris_perdesa, $data_get->kecamatan_perdesa, $data_get->kelurahan_perdesa, $nihiltidaktampil);
        } else {
            $nihiltidaktampil = "";
            $ar_LapBphtb = $this->getTableSSPD()->getDataLapBphtbBulanan_perdesa($data_get->tgl_cetakbphtb_perdesa, $data_get->tgl_cetakbphtb2_perdesa, $data_get->idnotaris_perdesa, $data_get->kecamatan_perdesa, $data_get->kelurahan_perdesa, $nihiltidaktampil);
        }
        $ar_Mengetahui = $this->getTblPejabat()->getdataid($data_get->mengetahui_perdesa);
        $ar_pemda = $this->getPemda()->getdata();
        //$session = new \Zend\Session\Container('user_session');
        $session = $this->getServiceLocator()->get('EtaxService')->getStorage()->read();
        /*$pdf = new \DOMPDFModule\View\Model\PdfModel();
        $pdf->setOption('filename', 'daftarlaporanperdesa.pdf');
        $pdf->setOption('paperSize', 'legal');
        $pdf->setOption('paperOrientation', 'potrait');

        $pdf->setVariables(array(
            'data_lapBphtb' => $ar_LapBphtb,
            'data_mengetahui' => $ar_Mengetahui,
            'tgl_cetakbphtb1' => $data_get->tgl_cetakbphtb_perdesa,
            'tgl_cetakbphtb2' => $data_get->tgl_cetakbphtb2_perdesa,
            'data_pemda' => $ar_pemda,
            'nama_login' => $session['s_namauserrole'],
            'tgl_cetakan' => $data_get->tgl_cetakan_perdesa
        ));
        return $pdf;*/
        // foreach ($ar_LapBphtb as $key => $value) {
        // }
        // Debug::dump($value);
        // exit;
        $view = new ViewModel(array(
            'data_lapBphtb' => $ar_LapBphtb,
            'data_mengetahui' => $ar_Mengetahui,
            'tgl_cetakbphtb1' => $data_get->tgl_cetakbphtb_perdesa,
            'tgl_cetakbphtb2' => $data_get->tgl_cetakbphtb2_perdesa,
            'ar_pemda' => $ar_pemda,
            'nama_login' => $session['s_namauserrole'],
            'tgl_cetakan' => $data_get->tgl_cetakan_perdesa,
            'cetak' => $data_get->cetak
        ));
        $data = array(
            'nilai' => '3' //no layout
        );
        $this->layout()->setVariables($data);
        return $view;
    }

    public function getTableSSPD()
    {
        if (!$this->tbl_sspd) {
            $sm = $this->getServiceLocator();
            $this->tbl_sspd = $sm->get("SSPDTable");
        }
        return $this->tbl_sspd;
    }

    public function getTblPejabat()
    {
        if (!$this->tbl_pejabat) {
            $sm = $this->getServiceLocator();
            $this->tbl_pejabat = $sm->get("PejabatBphtbTable");
        }
        return $this->tbl_pejabat;
    }

    public function getPemda()
    {
        if (!$this->tbl_pemda) {
            $sm = $this->getServiceLocator();
            $this->tbl_pemda = $sm->get("PemdaTable");
        }
        return $this->tbl_pemda;
    }

    public function getPendataanBphtb()
    {
        if (!$this->tbl_pendataan) {
            $sm = $this->getServiceLocator();
            $this->tbl_pendataan = $sm->get('SSPDBphtbTable');
        }
        return $this->tbl_pendataan;
    }

    public function populateComboJenisTransaksi()
    {
        $data = $this->getTblJenTran()->comboBox();
        $selectData = array();
        foreach ($data as $row) {
            $selectData[$row->s_idjenistransaksi] = $row->s_namajenistransaksi;
        }
        return $selectData;
    }

    public function populateComboHakTanah()
    {
        $data = $this->getTblHakTan()->comboBox();
        $selectData = array();
        foreach ($data as $row) {
            $selectData[$row->s_idhaktanah] = $row->s_namahaktanah;
        }
        return $selectData;
    }

    public function getTblJenTran()
    {
        if (!$this->tbl_jenistransaksi) {
            $sm = $this->getServiceLocator();
            $this->tbl_jenistransaksi = $sm->get('JenisTransaksiBphtbTable');
        }
        return $this->tbl_jenistransaksi;
    }

    public function getTblHakTan()
    {
        if (!$this->tbl_haktanah) {
            $sm = $this->getServiceLocator();
            $this->tbl_haktanah = $sm->get('HakTanahTable');
        }
        return $this->tbl_haktanah;
    }

    public function getTblPembayaran()
    {
        if (!$this->tbl_pembayaran) {
            $sm = $this->getServiceLocator();
            $this->tbl_pembayaran = $sm->get('PembayaranSptTable');
        }
        return $this->tbl_pembayaran;
    }

    public function getTblPelaporan()
    {
        if (!$this->tbl_pelaporan) {
            $sm = $this->getServiceLocator();
            $this->tbl_pelaporan = $sm->get('PelaporanTable');
        }
        return $this->tbl_pelaporan;
    }

    public function cetakbphtb2Action()
    {
        $req = $this->getRequest();
        $data_get = $req->getQuery();

        //=================== Model\Pencetakan\SSPDTable
        $data = $this->getTableSSPD()->getDataSetorBphtb($data_get->notarisbphtb2, $data_get->tgl_cetakbphtbnew1, $data_get->tgl_cetakbphtbnew2);
        $ar_Mengetahui = $this->getTblPejabat()->getdataid($data_get->mengetahuibphtb2);
        $ar_Pejabat = $this->getTblPejabat()->getdataid($data_get->pejabatbphtb2);
        $ar_pemda = $this->getPemda()->getdata();

        $session = $this->getServiceLocator()->get('EtaxService')->getStorage()->read();
        $view = new ViewModel(array(
            'data' => $data,
            'data_mengetahui' => $ar_Mengetahui,
            'data_pejabat' => $ar_Pejabat,
            'tgl_cetak' => $data_get->tgl_cetak,
            'ar_pemda' => $ar_pemda,
            'nama_login' => $session['s_iduser'],
            'tgl_cetak' => $data_get->tgl_cetakannew,
            'tglbayar1' => $data_get->tgl_cetakbphtbnew1,
            'tglbayar2' => $data_get->tgl_cetakbphtbnew2,
            'cetak' => $data_get->cetak
        ));
        $data = array(
            'nilai' => '3' //no layout
        );
        $this->layout()->setVariables($data);
        return $view;
    }

    public function cetakbphtbmutasiAction()
    {
        $req = $this->getRequest();
        $data_get = $req->getQuery();
        // Debug::dump($data_get);
        // exit;
        //=================== Model\Pencetakan\SSPDTable
        $data = $this->getServiceLocator()->get('EspopTable')->getDataPelayananBphtb($data_get->tgl_cetaklapmutasi1, $data_get->tgl_cetaklapmutasi2, $data_get->kecamatan, $data_get->kelurahan);

        $ar_Mengetahui = $this->getTblPejabat()->getdataid($data_get->mengetahuibphtblapmutasi2);
        $ar_Pejabat = $this->getTblPejabat()->getdataid($data_get->pejabatbphtblapmutasi2);
        $ar_pemda = $this->getPemda()->getdata();

        $session = $this->getServiceLocator()->get('EtaxService')->getStorage()->read();
        $view = new ViewModel(array(
            'data' => $data,
            'data_mengetahui' => $ar_Mengetahui,
            'data_pejabat' => $ar_Pejabat,
            'tgl_cetak' => $data_get->tgl_cetak,
            'ar_pemda' => $ar_pemda,
            'nama_login' => $session['s_iduser'],
            'tgl_cetak' => $data_get->tgl_cetakanlapmutasi2,
            'tglbayar1' => $data_get->tgl_cetaklapmutasi1,
            'tglbayar2' => $data_get->tgl_cetaklapmutasi2,
            'cetak' => $data_get->cetak
        ));
        $data = array(
            'nilai' => '3' //no layout
        );
        $this->layout()->setVariables($data);
        return $view;
    }

    public function cetakRealisasiDesaAction()
    {
        $req = $this->getRequest();
        $data_get = $req->getQuery();
        // var_dump($data_get);exit();

        $ar_LapBphtb = $this->getTableSSPD()->getRealiasiBphtbDesa($data_get);
        $ar_Mengetahui = $this->getTblPejabat()->getdataid($data_get->mengetahui_perdesa);
        $ar_pemda = $this->getPemda()->getdata();
        $view = new ViewModel(array(
            'ar_pemda' => $ar_pemda,
            'cetak' => $data_get->cetak,
            'ar_Mengetahui' => $ar_Mengetahui,
            'ar_LapBphtb' => $ar_LapBphtb,
            'data_get' => $data_get
        ));
        $data = array(
            'nilai' => '3' //no layout
        );
        $this->layout()->setVariables($data);
        return $view;
    }
}
