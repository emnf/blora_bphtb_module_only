<?php

namespace Bphtb\Controller\Setting;

class SettingJenisTransaksi extends \Zend\Mvc\Controller\AbstractActionController
{

    protected $tbl_pemda, $tbl_jenis_transaksi;

    public function indexAction()
    {
        //$session = new \Zend\Session\Container('user_session');
        $session = $this->getServiceLocator()->get('EtaxService')->getStorage()->read();
        $ar_pemda = $this->getPemda()->getdata();
        $form = new \Bphtb\Form\Setting\JenisTransaksiFrm();
        $view = new \Zend\View\Model\ViewModel(array('form' => $form));
        $data = array(
            'menu_setting' => 'active',
            'side_setting' => 'active',
            'side_jenis_transaksi' => 'active',
            'role_id' => $session['s_akses'],
            'data_pemda' => $ar_pemda,
            'aturgambar' => 1,
            'username' => $session['s_username']
        );
        $this->layout()->setVariables($data);
        return $view;
    }

    public function dataGridAction()
    {
        $allParams = (array) $this->getEvent()->getRouteMatch()->getParams();
        $base = new \Bphtb\Model\Setting\JenisTransaksiBphtbBase();
        $base->exchangeArray($allParams);
        if ($base->direction == 2)
            $base->page = $base->page + 1;
        if ($base->direction == 1)
            $base->page = $base->page - 1;
        if ($base->page <= 0)
            $base->page = 1;
        $page = $base->page;
        $limit = $base->rows;
        $count = $this->getTbl()->getGridCount($base);
        if ($count > 0 && $limit > 0) {
            $total_pages = ceil($count / $limit);
        } else {
            $total_pages = 0;
        }

        if ($page > $total_pages)
            $page = $total_pages;
        $start = $limit * $page - $limit;
        if ($start < 0)
            $start = 0;
        $data = $this->getTbl()->getGridData($base, $start);

        $s = "";
        foreach ($data as $row) {
            $s .= "<tr>";
            $s .= "<td>" . $row['s_kodejenistransaksi'] . "</td>";
            $s .= "<td>" . $row['s_namajenistransaksi'] . "</td>";
            $s .= "<td>" . $row['korek'] . " - " . $row["s_koreknama"] . "</td>";
            $s .= "<td><center><a href='setting_jenis_transaksi_bphtb/edit?s_idjenistransaksi=$row[s_idjenistransaksi]' class='btn btn-warning btn-sm btn-flat' style='width:100px'>Edit</a> <a href='#' onclick='hapus(" . $row['s_idjenistransaksi'] . ");return false;' class='btn btn-danger btn-sm btn-flat' style='width:100px'>Hapus</a></center></td>";
            $s .= "</tr>";
        }
        $data_render = array(
            "grid" => $s,
            "rows" => $base->rows,
            "count" => $count,
            "page" => $page,
            "start" => $start,
            "total_halaman" => $total_pages
        );
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($data_render));
    }

    public function tambahAction()
    {
        //$session = new \Zend\Session\Container('user_session');
        $session = $this->getServiceLocator()->get('EtaxService')->getStorage()->read();
        $ar_pemda = $this->getPemda()->getdata();
        $comboRek = $this->getServiceLocator()->get("KodeRekeningTable")->comboBox();
        $form = new \Bphtb\Form\Setting\JenisTransaksiFrm($comboRek);
        if ($this->getRequest()->isPost()) {
            $base = new \Bphtb\Model\Setting\JenisTransaksiBphtbBase();
            $form->setInputFilter($base->getInputFilter());
            $form->setData($this->getRequest()->getPost());
            if ($form->isValid()) {
                $base->exchangeArray($form->getData());
                $this->getTbl()->savedata($base);
                return $this->redirect()->toRoute('setting_jenis_transaksi_bphtb');
            }
        }
        $view = new \Zend\View\Model\ViewModel(array("frm" => $form));
        $data = array(
            'menu_setting' => 'active',
            'side_setting' => 'active',
            'side_jenis_transaksi' => 'active',
            'role_id' => $session['s_akses'],
            'data_pemda' => $ar_pemda,
            'aturgambar' => 2,
            'username' => $session['s_username']
        );
        $this->layout()->setVariables($data);
        return $view;
    }

    public function editAction()
    {
        //$session = new \Zend\Session\Container('user_session');
        $session = $this->getServiceLocator()->get('EtaxService')->getStorage()->read();
        $ar_pemda = $this->getPemda()->getdata();
        $comboRek = $this->getServiceLocator()->get("KodeRekeningTable")->comboBox();
        $frm = new \Bphtb\Form\Setting\JenisTransaksiFrm($comboRek);
        $req = $this->getRequest();
        if ($req->isGet()) {
            $id = (int) $req->getQuery()->get('s_idjenistransaksi');
            $data = $this->getTbl()->getDataId($id);
            $frm->bind($data);
        }
        $view = new \Zend\View\Model\ViewModel(array(
            'frm' => $frm
        ));
        $data = array(
            'menu_setting' => 'active',
            'side_setting' => 'active',
            'side_jenis_transaksi' => 'active',
            'role_id' => $session['s_akses'],
            'data_pemda' => $ar_pemda,
            'aturgambar' => 2,
            'username' => $session['s_username']
        );
        $this->layout()->setVariables($data);
        return $view;
    }

    public function hapusAction()
    {
        $this->gettbl()->hapusData($this->params('page'));
        return $this->getResponse();
    }

    public function getTbl()
    {
        if (!$this->tbl_jenis_transaksi) {
            $this->tbl_jenis_transaksi = $this->getServiceLocator()->get("JenisTransaksiBphtbTable");
        }
        return $this->tbl_jenis_transaksi;
    }

    public function getPemda()
    {
        if (!$this->tbl_pemda) {
            $sm = $this->getServiceLocator();
            $this->tbl_pemda = $sm->get("PemdaTable");
        }
        return $this->tbl_pemda;
    }
}
