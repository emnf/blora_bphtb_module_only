<?php

namespace Bphtb\Controller\Dokumen;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class Dokumen extends AbstractActionController
{

    public function downloadAction()
    {
        $allParams = (array) $this->getEvent()->getRouteMatch()->getParams();
        $data = $this->getServiceLocator()->get("PenandatangananTable")->getDataGuid($allParams["id"]);
        // var_dump($allParams);exit();
        // var_dump($data);exit();
        if (empty($data)) {
            die("DATA TIDAK DITEMUKAN");
        }
        if ($data["s_idsurat"] == 1) {
            return $this->ControllerHelper()->openFile("SSPD-" . date("d-m-Y-H-i-s") . ".pdf", $data["t_path_file"] . $data["t_name_file"]);
        }
    }

    public function informasiAction()
    {
        $this->layout('layout/informasi');
        $allParams = (array) $this->getEvent()->getRouteMatch()->getParams();
        $ar_pemda = $this->getServiceLocator()->get('PemdaTable')->getdata();
        $dokumen = $this->getServiceLocator()->get("PenandatangananTable")->getDataGuid($allParams["id"]);
        if ($dokumen) {
            $ar_sspd = $this->getServiceLocator()->get("SSPDTable")->getviewcetakssp($dokumen["t_idspt"]);

            $data = [
                'lembaga' => strtoupper($ar_pemda->s_namainstansi),
                'namattd' => $dokumen["t_nama_penandatanganan"],
                'tglttd' => date("d-m-Y H:i:s", strtotime($dokumen["created_at"])),
                'namapemda' => strtoupper($ar_pemda->s_namakabkot),
                'namawp' => $ar_sspd[0]["t_namawppembeli"],
                'nop' => $ar_sspd[0]["t_nopbphtbsppt"],
                'namapajak' => 'BPHTB',
                'nodaftar' => $ar_sspd[0]["t_kohirspt"] . "/" . $ar_sspd[0]["t_periodespt"],

            ];
            $view = new ViewModel([
                'data_pemda' => $ar_pemda,
                'data' => $data
            ]);
            return $view;
        } else {
            die("DATA TIDAK DITEMUKAN");
        }
    }
}
