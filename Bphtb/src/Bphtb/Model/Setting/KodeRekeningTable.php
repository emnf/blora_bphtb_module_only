<?php

namespace Bphtb\Model\Setting;

use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Sql;

class KodeRekeningTable extends AbstractTableGateway
{

    protected $table = "s_koderekening";

    public function __construct(Adapter $adapter)
    {
        $this->adapter = $adapter;
        $this->resultSetPrototype = new ResultSet();
        $this->resultSetPrototype->setArrayObjectPrototype(new KodeRekeningBase());
        $this->initialize();
    }

    public function comboBox()
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select("s_koderekening");
        $res = $sql->prepareStatementForSqlObject($select)->execute();

        $comboBox = array();
        foreach ($res as $res) {
            $comboBox[$res["s_korekid"]] = $res["s_korektipe"] . "." . $res["s_korekkelompok"] . "." . $res["s_korekjenis"] . "." . $res["s_korekobjek"] . "." . $res["s_korekrincian"] . "." . $res["s_korekrinciansub"] . " - " . $res["s_koreknama"];
        }
        return $comboBox;
    }
}
