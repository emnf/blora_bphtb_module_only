<?php

namespace Bphtb\Helper;

use CURLFile;
use Zend\Mvc\Controller\Plugin\AbstractPlugin;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class Esign extends AbstractPlugin implements ServiceLocatorAwareInterface
{
    const ESIGN_OK = 200;
    const ESIGN_CREATED = 201;
    const ESIGN_UNAUTHORIZED = 401;
    const ESIGN_FORBIDDEN = 403;
    const ESIGN_NOT_FOUND = 404;

    protected $service;

    public function getServiceLocator()
    {
        return $this->serviceLocator;
    }

    public function setServiceLocator(ServiceLocatorInterface $serviceLocator)
    {
        $this->serviceLocator = $serviceLocator;
    }

    /**
     *  Function to access your predefined service in Module
     */
    public function getService($serviceName)
    {
        $this->service = $this->getServiceLocator()->getServiceLocator()->get($serviceName);
        return $this->service;
    }

    public function signed_document($fileoutput = null, $filesave = null, $nik = null, $passphrase = null, $linkQR = null, $width = null, $height = null, $imageQr = null)
    {
        // nik = 0803202100007062
        // passphrase = #1234Qwer*
        // linkQr = https://bppkad.blorakab.go.id

        $config = $this->getService("PenandatangananTable")->getConfig();

        $authorization = sprintf('Authorization: Basic %s', base64_encode(sprintf('%s:%s', $config["s_username"], $config["s_password"])));
        $postArr = array(
            'file' => new CURLFile(
                str_replace("\\", "/", realpath(".")) . '/' . $fileoutput,
                'application/pdf'
            ),
            'imageTTD' => new CURLFile(
                str_replace("\\", "/", realpath(".")) . '/' . $imageQr,
                'image/png'
            ),
            'nik' => $nik,
            'passphrase' => $passphrase,
            'tampilan' => 'visible',
            // 'page' => '1',
            // 'image' => 'false',
            'image' => 'true',
            // 'linkQR' => $linkQR,
            'width' => $width,
            'height' => $height,
            'tag_koordinat' => '#',
            'location' => 'Blora'
        );
        $curl = curl_init();
        $headers = [];
        curl_setopt_array($curl, array(
            CURLOPT_URL => $config["s_url"] . '/api/sign/pdf',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_HEADER => true,
            CURLOPT_SSL_VERIFYHOST => false,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => $postArr,
            CURLOPT_HTTPHEADER => array(
                $authorization,
                "Content-Type:multipart/form-data"
            ),
            CURLOPT_HEADERFUNCTION => function ($curl, $header) use (&$headers) {
                $len = strlen($header);
                $header = explode(':', $header, 2);
                // ignore invalid headers
                if (count($header) < 2) {
                    return $len;
                }
                $headers[strtolower(trim($header[0]))][] = trim($header[1]);
                return $len;
            }
        ));

        $response = curl_exec($curl);
        $curl_error = curl_error($curl);

        $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        $header_size = curl_getinfo($curl, CURLINFO_HEADER_SIZE);
        $body = substr($response, $header_size);

        curl_close($curl);

        $saveDocument = false;
        if ($httpcode == 200) {
            $saveDocument = file_put_contents($filesave, $body);
        }

        return [
            "saveDocument" => $saveDocument,
            "headers" => $headers,
            "curl_error" => $curl_error,
            "httpcode" => $httpcode,
            "body" => $body
        ];
    }
}
