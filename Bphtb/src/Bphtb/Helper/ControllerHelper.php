<?php

/*
 * Created on Fri Mar 12 2021
 *
 * Copyright (c) 2021 Irfan Aribowo
 */

namespace Bphtb\Helper;

use Zend\Debug\Debug;

class ControllerHelper extends \Zend\Mvc\Controller\Plugin\AbstractPlugin implements \Zend\ServiceManager\ServiceLocatorAwareInterface
{

    protected $serviceLocator;

    public function getServiceLocator()
    {
        return $this->serviceLocator->getServiceLocator();
    }

    public function setServiceLocator(\Zend\ServiceManager\ServiceLocatorInterface $serviceLocator)
    {
        $this->serviceLocator = $serviceLocator;
    }

    public function hitungBphtb($post)
    {
        $data = [];

        if (strpos($post["t_luastanah"], ".")) {
            $t_luastanah = str_ireplace('.', '', $post['t_luastanah']) / 100;
        } else {
            $t_luastanah = str_ireplace(".", "", $post["t_luastanah"]);
        }

        if (strpos($post["t_luasbangunan"], ".")) {
            $t_luasbangunan = str_ireplace('.', '', $post['t_luasbangunan']) / 100;
        } else {
            $t_luasbangunan = str_ireplace(".", "", $post["t_luasbangunan"]);
        }

        $data["t_luastanah"] = $t_luastanah;
        $data["t_luasbangunan"] = $t_luasbangunan;

        $t_njoptanah = str_ireplace(".", "", $post["t_njoptanah"]);
        $data["t_njoptanah"] = $t_njoptanah;
        $t_njopbangunan = str_ireplace(".", "", $post["t_njopbangunan"]);
        $data["t_njopbangunan"] = $t_njopbangunan;

        $t_nilaitransaksispt = str_ireplace(".", "", $post["t_nilaitransaksispt"]);
        $data["t_nilaitransaksispt"] = $t_nilaitransaksispt;

        $aphb_kali = $post["t_tarif_pembagian_aphb_kali"];
        $data["t_tarif_pembagian_aphb_kali"] = $aphb_kali;
        $aphb_bagi = $post["t_tarif_pembagian_aphb_bagi"];
        $data["t_tarif_pembagian_aphb_bagi"] = $aphb_bagi;

        if (($aphb_kali == null) || ($aphb_kali == '') || ($aphb_kali == 0) || ($aphb_bagi == null) || ($aphb_bagi == '') || ($aphb_bagi == 0)) {
            $t_totalnjoptanah = $t_luastanah * $t_njoptanah;
            $t_totalnjopbangunan = $t_luasbangunan * $t_njopbangunan;
            $t_grandtotalnjop = $t_totalnjoptanah + $t_totalnjopbangunan;

            $t_grandtotalnjop_aphb = $t_grandtotalnjop;
        } else {
            $aphb = $aphb_kali / $aphb_bagi;

            $t_totalnjoptanah = $t_luastanah * $t_njoptanah;
            $t_totalnjopbangunan = $t_luasbangunan * $t_njopbangunan;
            $t_grandtotalnjop = $t_totalnjoptanah + $t_totalnjopbangunan;

            $t_grandtotalnjop_hitung = $t_grandtotalnjop * $aphb;
            $t_grandtotalnjop_aphb = ceil($t_grandtotalnjop_hitung);
        }

        $data['t_totalnjoptanah'] = $t_totalnjoptanah;
        $data['t_totalnjopbangunan'] = $t_totalnjopbangunan;
        $data['t_grandtotalnjop'] = $t_grandtotalnjop;
        $data['t_grandtotalnjop_aphb'] = $t_grandtotalnjop_aphb;

        if ($post["t_idjenistransaksi"] == 8) {
            $data['t_npopspt'] = $t_nilaitransaksispt;
        } else if ($post["t_idjenistransaksi"] == 14) {
            // untuk transaksi fasum /fasos /nihil
            $data['t_npopspt'] = 0;
        } else {
            if (($aphb_kali == null) || ($aphb_kali == '') || ($aphb_kali == 0) || ($aphb_bagi == null) || ($aphb_bagi == '') || ($aphb_bagi == 0)) {

                if ($t_grandtotalnjop > $t_nilaitransaksispt) {
                    $data['t_npopspt'] = $t_grandtotalnjop;
                } else {
                    $data['t_npopspt'] = $t_nilaitransaksispt;
                }
            } else {
                if ($t_grandtotalnjop_aphb > $t_nilaitransaksispt) {
                    $data['t_npopspt'] = $t_grandtotalnjop_aphb;
                } else {
                    $data['t_npopspt'] = $t_nilaitransaksispt;
                }
            }
        }

        //========================= cek dapat potongan apa gak
        $tahunproses = date('Y', strtotime($post["t_tglprosesspt"]));

        $ceknik = $this->getServiceLocator()->get("SPTTable")->ceknik($post["t_nikwppembeli"], $tahunproses);
        $ceknpoptkp = $this->getServiceLocator()->get("SPTTable")->cekpotongannpoptkp($post["t_idjenistransaksi"]);
        $id = (int) $post["t_idspt"];
        if (($id == 0) || ($id == null)) {
            if (!empty($ceknik['t_idspt'])) {
                $t_potonganspt = 0;
                $data['ket_npoptkp'] = 'Tidak Dapat NPOPTKP';
            } else {
                $t_potonganspt = $ceknpoptkp['s_tarifnpotkp'];
                $data['ket_npoptkp'] = 'Dapat NPOPTKP';
            }
        } else {
            if (!empty($ceknik['t_idspt'])) {
                if ($ceknik['t_idspt'] == $id) {
                    $t_potonganspt = $ceknpoptkp['s_tarifnpotkp'];
                    $data['ket_npoptkp'] = 'Dapat NPOPTKP';
                } else {
                    $t_potonganspt = 0;
                    $data['ket_npoptkp'] = 'Tidak Dapat NPOPTKP';
                }
            } else {
                $t_potonganspt = $ceknpoptkp['s_tarifnpotkp'];
                $data['ket_npoptkp'] = 'Dapat NPOPTKP';
            }
        }
        //========================= cek dapat potongan apa gak   

        if (($post["t_idjenistransaksi"] == 4) || ($post["t_idjenistransaksi"] == 5)) {
            $data['t_potongan_waris_hibahwasiat'] = 0;
            $hitung_potonganwaris = 1;
        } else {
            $data['t_potongan_waris_hibahwasiat'] = 0;
            $hitung_potonganwaris = 1;
        }

        $t_persenbphtb = str_ireplace(".", "", $post["t_persenbphtb"]);
        if ($t_persenbphtb == "") {
            if ($id == 0 || $id == null) {
                $cektarifbphtb = $this->getServiceLocator()->get("SPTTable")->gettarifbphtbsekarang();
                $t_persenbphtb = $cektarifbphtb["s_tarifbphtb"];
            } else {
                $cektarifbphtb = $this->getServiceLocator()->get("SPTTable")->gettarifbphtbspt($id);
                $t_persenbphtb = $cektarifbphtb["t_persenbphtb"];
            }
        }
        $data["t_persenbphtb"] = $t_persenbphtb;
        if ($t_potonganspt == '0') {
            $data['t_potonganspt'] = 0;
            $data['t_npopkpspt'] = $data['t_npopspt'];
            $data['t_totalspt'] = ceil($data['t_npopkpspt'] * $t_persenbphtb / 100 * $hitung_potonganwaris);
        } else {
            $data['t_potonganspt'] = $t_potonganspt;
            $npop = $data['t_npopspt'];
            $npopkp = $npop - $data['t_potonganspt'];
            if ($npopkp <= 0) {
                $data['t_npopkpspt'] = 0;
                $data['t_totalspt'] = 0;
            } else {
                $data['t_npopkpspt'] = $npopkp;
                $data['t_totalspt'] = ceil($data['t_npopkpspt'] * $t_persenbphtb / 100 * $hitung_potonganwaris);
            }
        }

        return $data;
    }

    public function hitungBphtbCetakan($data)
    {
        if (!empty($data['p_luastanah'])) {
            $luastanah = $data['p_luastanah'];
        } else {
            $luastanah = $data['t_luastanah'];
        }

        if (!empty($data['p_njoptanah'])) {
            $njoptanah = str_ireplace('.', '',  $data['p_njoptanah']);
        } else {
            $njoptanah = str_ireplace('.', '', $data['t_njoptanah']);
        }

        if (!empty($data['p_totalnjoptanah'])) {
            $totalnjoptanah = str_ireplace('.', '', $data['p_totalnjoptanah']);
        } else {
            $totalnjoptanah = str_ireplace('.', '', $data['t_totalnjoptanah']);
        }

        if (!empty($data['p_luasbangunan'])) {
            $luasbangunan = $data['p_luasbangunan'];
        } else {
            $luasbangunan = $data['t_luasbangunan'];
        }

        if (!empty($data['p_njopbangunan'])) {
            $njopbangunan = str_ireplace('.', '',  $data['p_njopbangunan']);
        } else {
            $njopbangunan = str_ireplace('.', '', $data["t_njopbangunan"]);
        }

        if (!empty($data['p_totalnjopbangunan'])) {
            $totalnjopbangunan = str_ireplace('.', '', $data['p_totalnjopbangunan']);
        } else {
            $totalnjopbangunan = str_ireplace('.', '', $data["t_totalnjopbangunan"]);
        }

        if (!empty($data['p_grandtotalnjop'])) {
            $grandnjop = $data['p_grandtotalnjop'];
        } else {
            $grandnjop = $data["t_grandtotalnjop"];
        }

        if (!empty($data['p_nilaitransaksispt'])) {
            $nilaitransaksispt = $data['p_nilaitransaksispt'];
        } else {
            $nilaitransaksispt = $data["t_nilaitransaksispt"];
        }

        $aphb_kali = $data['t_tarif_pembagian_aphb_kali'];
        $aphb_bagi = $data['t_tarif_pembagian_aphb_bagi'];

        if (!empty($data['p_grandtotalnjop_aphb'])) {
            $grandnjop_aphb = $data['p_grandtotalnjop_aphb'];
        } else {
            $grandnjop_aphb = $data["t_grandtotalnjop_aphb"];
        }

        if (($aphb_kali == null) || ($aphb_kali == '') || ($aphb_kali == 0) || ($aphb_bagi == null) || ($aphb_bagi == '') || ($aphb_bagi == 0)) {
            $ket_aphb = 2;
        } else {
            if (!empty($data['p_grandtotalnjop_aphb'])) {
                $grandnjop_aphb = $data['p_grandtotalnjop_aphb'];
            } else {
                $grandnjop_aphb = $data["t_grandtotalnjop_aphb"];
            }
            $ket_aphb = 1;
        }

        if ($data["t_idjenistransaksi"] == 8) {
            $npop = $nilaitransaksispt;
        } else if ($data["t_idjenistransaksi"] == 14) {
            $npop = 0;
        } else {
            if (($aphb_kali == null) || ($aphb_kali == '') || ($aphb_kali == 0) || ($aphb_bagi == null) || ($aphb_bagi == '') || ($aphb_bagi == 0)) {

                if ($grandnjop  > $nilaitransaksispt) {
                    $npop = $grandnjop;
                } else {
                    $npop = $nilaitransaksispt;
                }
            } else {
                if ($grandnjop_aphb  > $nilaitransaksispt) {
                    $npop = $grandnjop_aphb;
                } else {
                    $npop = $nilaitransaksispt;
                }
            }
        }

        $potongan_waris_hibahwasiat = $data["t_potongan_waris_hibahwasiat"];
        if (($data['t_idjenistransaksi'] == 4) || ($data['t_idjenistransaksi'] == 5)) {
            $ket_potongan_waris_hibahwasiat = 1;
            if (($potongan_waris_hibahwasiat == null) || ($potongan_waris_hibahwasiat == 0)) {
                $hitung_potonganwaris = 1;
            } else {
                $hitung_potonganwaris = $potongan_waris_hibahwasiat / 100;
            }
        } else {
            $ket_potongan_waris_hibahwasiat = 2;
            $hitung_potonganwaris = 1;
        }
        $potonganspt = $data['t_potonganspt'];
        $persenbphtb = $data["t_persenbphtb"];
        $approve_pengurangpembebas = $data["t_approve_pengurangpembebas"];
        $npopkp = $npop - $potonganspt;
        if ($data["t_id_ptsl"] != null) {
            $npopkp = 0;
        } else if ($npopkp > 0) {
            $npopkp = $npopkp;
        } else {
            $npopkp = 0;
        }

        if ($npopkp == 0) {
            $totalpajak = 0;
            $totalpajak_sebelum_potongan_waris = 0;
        } else if ($approve_pengurangpembebas != null) {
            $totalpajak_sebelum_potongan_waris = $data["t_totalsptsebelumnya"];
            $totalpajak = $data["t_totalpajak"];
        } else {
            $totalpajak_sebelum_potongan_waris = ceil($npopkp * $persenbphtb / 100);
            $totalpajak = ceil($npopkp * $persenbphtb / 100 * $hitung_potonganwaris);
        }

        $idsptsebelumnya = $data["t_idsptsebelumnya"];
        $nilaipembayaranspt_sebelumnya = $data["t_nilaipembayaranspt_sebelumnya"];
        if (!empty($idsptsebelumnya)) {
            $total_pembayaran_sspd = $totalpajak - $nilaipembayaranspt_sebelumnya;
            $ket_pembayaran_sebelumnya = 1;
        } else {
            $total_pembayaran_sspd = $totalpajak;
            $ket_pembayaran_sebelumnya = 2;
        }

        return [
            "luastanah" => $luastanah,
            "njoptanah" => $njoptanah,
            "totalnjoptanah" => $totalnjoptanah,
            "luasbangunan" => $luasbangunan,
            "njopbangunan" => $njopbangunan,
            "totalnjopbangunan" => $totalnjopbangunan,
            "grandnjop" => $grandnjop,
            "nilaitransaksispt" => $nilaitransaksispt,
            "aphb_kali" => $aphb_kali,
            "aphb_bagi" => $aphb_bagi,
            "grandnjop_aphb" => $grandnjop_aphb,
            "ket_aphb" => $ket_aphb,
            "npop" => $npop,
            "ket_potongan_waris_hibahwasiat" => $ket_potongan_waris_hibahwasiat,
            "potongan_waris_hibahwasiat" => $potongan_waris_hibahwasiat,
            "hitung_potonganwaris" => $hitung_potonganwaris,
            "npopkp" => $npopkp,
            "totalpajak_sebelum_potongan_waris" => $totalpajak_sebelum_potongan_waris,
            "totalpajak" => $totalpajak,
            "potonganspt" => $potonganspt,
            "persenbphtb" => $persenbphtb,
            "ket_pembayaran_sebelumnya" => $ket_pembayaran_sebelumnya,
            "idsptsebelumnya" => $idsptsebelumnya,
            "nilaipembayaranspt_sebelumnya" => $nilaipembayaranspt_sebelumnya,
            "total_pembayaran_sspd" => $total_pembayaran_sspd,
            "approve_pengurangpembebas" => $approve_pengurangpembebas,
            "t_id_ptsl" => $data["t_id_ptsl"]
        ];
    }

    public function mkdir($filepath)
    {
        if (!file_exists($filepath)) {
            mkdir($filepath, 0777, true);
        }
        return true;
    }

    function guidv4($data = null)
    {
        // Generate 16 bytes (128 bits) of random data or use the data passed into the function.
        // $data = $data ?? random_bytes(16); //PHP7
        $data = ($data != null) ? $data : openssl_random_pseudo_bytes(16); //PHP56
        assert(strlen($data) == 16);

        // Set version to 0100
        $data[6] = chr(ord($data[6]) & 0x0f | 0x40);
        // Set bits 6-7 to 10
        $data[8] = chr(ord($data[8]) & 0x3f | 0x80);

        // Output the 36 character UUID.
        return vsprintf('%s%s-%s-%s-%s-%s%s%s', str_split(bin2hex($data), 4));
    }

    public function openFile($filename = null, $filesave = null, $contentType = "application/pdf")
    {
        $response = new \Zend\Http\Response\Stream();
        $response->getHeaders()->addHeaders(array(
            'Content-Type' => $contentType,
            'Content-Disposition' => "attachement; filename=\"" . $filename . "\""
        ));
        $response->setStream(fopen($filesave, 'r'));
        return $response;
    }
}
