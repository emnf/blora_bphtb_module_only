<?php

namespace Bphtb\Model\Setting;

use Zend\InputFilter\InputFilter;
use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class FormatPesanBase implements InputFilterAwareInterface {    
    protected $inputFilter;    
    public $page, $direction, $rows, $offset, $limit;

    public function exchangeArray($data){
        $this->page = isset($data["page"]) ? $data["page"] : null;
        $this->direction = isset($data["direction"]) ? $data["direction"] : null;
        $this->rows = isset($data["rows"]) ? $data["rows"] : null;
        $this->offset = isset($data["offset"]) ? $data["offset"] : null;
        $this->limit = isset($data["limit"]) ? $data["limit"] : null;
    }
    
    public function getArrayCopy() {
        return get_object_vars($this);
    }

    public function setInputFilter(InputFilterInterface $inputFilter) {
        throw new \Exception("Not used");
    }

    public function getInputFilter() {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();

            $factory = new InputFactory();

            $this->inputFilter = $inputFilter;
        }
        return $this->inputFilter;
    }
}