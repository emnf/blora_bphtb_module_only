<?php

namespace Bphtb\Helper;

use pdf_parser;
use Zend\Debug\Debug;
use Zend\Form\View\Helper\AbstractHelper;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class UploadHelper extends AbstractHelper implements ServiceLocatorAwareInterface
{

    protected $message = null;

    public function setServiceLocator(ServiceLocatorInterface $serviceLocator)
    {
        $this->serviceLocator = $serviceLocator;
        return $this;
    }

    public function getServiceLocator()
    {
        return $this->serviceLocator;
    }

    public function setMessage($message)
    {
        $this->message = $message;
        return $this;
    }

    public function getMessage()
    {
        if ($this->message === null) {
            // for example, get the default value from app config
            $sm = $this->getServiceLocator()->getServiceLocator();
            $config = $sm->get('Sessi_Manager')->getStorage();
            $this->setMessage($config);
        }
        return $this->message;
    }

    public function __invoke()
    {
        $message = $this->getMessage();
        return $message;
    }

    public function validateAndUploadFile($file)
    {
        $maxSizeUpload = 50000000;

        // Periksa ukuran file
        if ($file["size"] > $maxSizeUpload) {
            return [
                'status' => false,
                'message' => 'Maaf, ukuran file terlalu besar.',
            ];
        }

        // Periksa tipe file menggunakan getimagesize() untuk gambar
        if (strpos($file["type"], "image") !== false || $file["type"] == "application/pdf") {
            if (strpos($file["type"], "image") !== false) {
                // masuk validasi gambar
                $imageInfo = getimagesize($file["tmp_name"]);
                if ($imageInfo === false) {
                    return [
                        'status' => false,
                        'message' => 'File bukan gambar valid.',
                    ];
                } else {
                    $allowedImageTypes = ["image/jpeg", "image/png", "image/gif"];
                    if (!in_array($imageInfo["mime"], $allowedImageTypes)) {
                        return [
                            'status' => false,
                            'message' => 'Hanya file JPEG, PNG, dan GIF yang diizinkan.',
                        ];
                    }
                }
            } else {
                // masuk validasi pdf
                // Periksa tipe file menggunakan ekstensi file dan konten file
                $allowedExtensions = ["pdf"];
                $fileExtension = pathinfo($file["name"], PATHINFO_EXTENSION);
                if (!in_array(strtolower($fileExtension), $allowedExtensions)) {
                    return [
                        'status' => false,
                        'message' => 'Hanya file PDF yang diizinkan.',
                    ];
                }

                // Periksa konten sebenarnya dari file PDF
                $fileContent = file_get_contents($file["tmp_name"]);

                if (strpos($fileContent, '<?php') !== false || strpos($fileContent, '__halt_compiler()') !== false) {
                    return [
                        'status' => false,
                        'message' => 'File bukan PDF valid.',
                    ];
                }

                if (mime_content_type($file["tmp_name"]) != 'application/pdf') {
                    return [
                        'status' => false,
                        'message' => 'File bukan PDF valid.',
                    ];
                }
            }
        } else {
            return [
                'status' => false,
                'message' => 'Hanya file gambar dan PDF yang diizinkan.',
            ];
        }


        // clear semua
        return [
            'status' => true,
            'message' => "File Terpantau Aman",
        ];
    }
}
