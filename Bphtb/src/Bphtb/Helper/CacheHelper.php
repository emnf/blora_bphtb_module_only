<?php

namespace Bphtb\Helper;

class CacheHelper
{
    public function cache()
    {
        return \Zend\Cache\StorageFactory::factory(array(
            'adapter' => array(
                'name' => 'filesystem',
                'options' => array(
                    'dirLevel' => 2,
                    'cacheDir' => 'data/cache',
                    'dirPermission' => 0755,
                    'filePermission' => 0666,
                    'namespaceSeparator' => '-db-',
                    // 'ttl' => 60 * 60 * 24,
                ),
            ),
            'plugins' => array('serializer'),
        ));
    }

    public function clearCache()
    {
        return $this->cache()->flush();
    }
}
