<?php

namespace Bphtb\Model\Penandatanganan;

use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Predicate\IsNotNull;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Where;
use Zend\Debug\Debug;

class PenandatangananTable extends AbstractTableGateway
{

    public $table = "t_penandatanganan";

    public function __construct(Adapter $adapter)
    {
        $this->adapter = $adapter;
        $this->resultSetPrototype = new ResultSet();
        $this->resultSetPrototype->setArrayObjectPrototype(new PenandatangananBase());
        $this->initialize();
    }

    public function fr_count_s_persyaratan($string = null, $where = null)
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select(["a" => "s_persyaratan"]);
        $select->columns([
            "jmlsyarat" => new Expression("(COUNT(*))"),
            "s_idjenistransaksi"
        ]);
        $select->order("s_idjenistransaksi");
        $select->group("s_idjenistransaksi");
        $where = new Where();
        $select->where($where);
        if ($string != null) {
            return $select->getSqlString();
        } else {
            return $select;
        }
    }

    public function fr_pendaftaran_v5($string = null, $where = null)
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select(["a" => "t_spt"]);
        $select->columns([
            "t_idspt", "t_tglprosesspt",
            "jml_syarat_input" => new Expression("(length(translate(a.t_persyaratan::text, '[\"]1234567890'::text, ''::text)) + 1)"),
            "jml_syarat_validasi" => new Expression("(length(translate(e.t_verifikasispt::text, '[\"]1234567890'::text, ''::text)) + 1)"),
            "jml_syarat_sebenarnya" => new Expression("j.jmlsyarat"),
            "status_pendaftaran" => new Expression("(
                CASE
                    WHEN (length(translate(a.t_persyaratan::text, '[\"]1234567890'::text, ''::text)) + 1) = j.jmlsyarat THEN 1
                    ELSE 2
                END 
            )"),
            "status_validasi" => new Expression("(
                CASE
                    WHEN (length(translate(e.t_verifikasispt::text, '[\"]1234567890'::text, ''::text)) + 1) = j.jmlsyarat THEN 1
                    WHEN (length(translate(e.t_verifikasispt::text, '[\"]1234567890'::text, ''::text)) + 1) IS NULL THEN 3
                    ELSE 2
                END 
            )"), "t_kohirspt",
            "s_idjenistransaksi" => new Expression("c.s_idjenistransaksi"),
            "t_periodespt",
            "t_namawppembeli" => new Expression("b.t_namawppembeli"),
            "t_statusbayarspt" => new Expression("(
                CASE
                    WHEN e.t_statusbayarspt::text = 'true'::text THEN 'TRUE'::text
                    ELSE 'FALSE'::text
                END
            )"),
            "s_namajenistransaksi" => new Expression("c.s_namajenistransaksi"),
            "t_persyaratan",
            "t_verifikasispt" => new Expression("e.t_verifikasispt"),
            "t_idjenistransaksi", "t_inputbpn" => new Expression("b.t_inputbpn"),
            "t_tglverifikasispt" => new Expression("e.t_tglverifikasispt"),
            "p_idpemeriksaan" => new Expression("f.p_idpemeriksaan"),
            "t_idpembayaranspt" => new Expression("e.t_idpembayaranspt"),
            "t_kodebayarbanksppt" => new Expression("e.t_kodebayarbanksppt"),
            "t_idnotarisspt", "p_totalspt" => new Expression("f.p_totalspt"),
            "t_totalspt",
            "jml_pajak_v1" => new Expression("( 
                CASE
                    WHEN f.p_idpemeriksaan IS NOT NULL THEN f.p_totalspt
                    ELSE a.t_totalspt
                END
            )"),
            "jml_pajak_v2" => new Expression("(
                CASE
                    WHEN f.p_idpemeriksaan IS NOT NULL THEN
                    CASE
                        WHEN a.t_totalspt < f.p_totalspt THEN f.p_totalspt
                        ELSE a.t_totalspt
                    END
                    ELSE a.t_totalspt
                END
            )"), "t_kohirketetapanspt",
            "s_namanotaris" => new Expression("i.s_namanotaris"),
            "t_namawppenjual" => new Expression("b.t_namawppenjual"),
            "t_nopbphtbsppt", "fr_tervalidasidua",
            "fr_validasidua" => new Expression("b.fr_validasidua"),
            "t_grandtotalnjop" => new Expression("b.t_grandtotalnjop"),
            "p_grandtotalnjop" => new Expression("f.p_grandtotalnjop"),
            "p_grandtotalnjop_aphb" => new Expression("f.p_grandtotalnjop_aphb"),
            "t_nilaitransaksispt",
            "p_nilaitransaksispt" => new Expression("f.p_nilaitransaksispt"),
            "t_alamatwppembeli" => new Expression("b.t_alamatwppembeli"),
            "t_idsptsebelumnya", "t_kodedaftarspt", "t_jenispembelian",
            "ntpd" => new Expression("e.t_kodebayarbanksppt"),
            "t_nikwppembeli" => new Expression("b.t_nikwppembeli"),
            "t_rtop" => new Expression("b.t_rtop"),
            "t_rwop" => new Expression("b.t_rwop"),
            "t_kelurahanop" => new Expression("b.t_kelurahanop"),
            "t_kecamatanop" => new Expression("b.t_kecamatanop"),
            "t_kabupatenop" => new Expression("b.t_kabupatenop"),
            "t_luastanah" => new Expression("b.t_luastanah"),
            "t_luasbangunan" => new Expression("b.t_luasbangunan"),
            "t_nilaipembayaranspt" => new Expression("e.t_nilaipembayaranspt"),
            "t_tanggalpembayaran" => new Expression("e.t_tanggalpembayaran"),
        ]);
        $select->join(["b" => "t_detailsptbphtb"], "b.t_idspt = a.t_idspt", [], "LEFT");
        $select->join(["c" => "s_jenistransaksi"], "c.s_idjenistransaksi = a.t_idjenistransaksi", [], "LEFT");
        $select->join(["e" => "t_pembayaranspt"], "e.t_idspt = a.t_idspt", [], "LEFT");
        $select->join(["f" => "t_pemeriksaan"], "f.p_idpembayaranspt = e.t_idpembayaranspt", [], "LEFT");
        $select->join(["h" => "s_users"], "h.s_iduser = a.t_idnotarisspt", [], "LEFT");
        $select->join(["i" => "s_notaris"], new Expression("i.s_idnotaris::text = h.s_idpejabat_idnotaris::text"), [], "LEFT");
        $select->join(["j" => $this->fr_count_s_persyaratan()], new Expression("a.t_idjenistransaksi = j.s_idjenistransaksi"), [], "LEFT");
        if ($string != null) {
            return $select->getSqlString();
        } else {
            return $select;
        }
    }

    public function gridBelum($input, $aColumns, $session, $cekurl, $allParams)
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from(["a" => $this->fr_pendaftaran_v5()]);
        $select->join(["b" => "t_paraf_penandatanganan"], "a.t_idspt = b.t_idspt", [], "LEFT");
        $where = new \Zend\Db\Sql\Where();
        $where->IsNotNull("a.t_kodebayarbanksppt");
        $where->isNotNull("b.t_idspt");
        $where->literal("NOT EXISTS (SELECT * FROM t_penandatanganan xa WHERE xa.t_idspt = a.t_idspt)");
        if ($input->getPost("sSearch_2") != "") {
            $where->literal("a.t_kohirspt::TEXT ILIKE '%" . $input->getPost("sSearch_2") . "%'");
        }
        if ($input->getPost("sSearch_3") != "") {
            $where->literal("a.t_kohirketetapanspt::TEXT ILIKE '%" . $input->getPost("sSearch_3") . "%'");
        }
        if ($input->getPost("sSearch_4") != "") {
            $where->literal("a.t_namawppembeli::TEXT ILIKE '%" . $input->getPost("sSearch_4") . "%'");
        }
        if ($input->getPost("sSearch_5") != "") {
            $where->literal("a.t_nopbphtbsppt::TEXT ILIKE '%" . $input->getPost("sSearch_5") . "%'");
        }
        if ($input->getPost("sSearch_6") != "") {
            $where->literal("a.t_kodebayarbanksppt::TEXT ILIKE '%" . $input->getPost("sSearch_6") . "%'");
        }
        $select->where($where);

        $iTotal = $sql->prepareStatementForSqlObject($select)->execute()->count();

        //================ ordernya coy
        $aOrderingRules = array();
        if ($input->getPost('iSortCol_0')) {
            $iSortingCols = intval($input->getPost('iSortingCols'));
            for ($i = 0; $i < $iSortingCols; $i++) {
                if ($input->getPost('bSortable_' . intval($input->getPost('iSortCol_' . $i))) == 'true') {
                    $aOrderingRules[] = $aColumns[intval($input->getPost('iSortCol_' . $i))] . " " . ($input->getPost('sSortDir_' . $i) === 'asc' ? 'asc' : 'desc');
                }
            }
        }


        if (!empty($aOrderingRules)) {
            $select->order(implode(", ", $aOrderingRules));
        } else {
            $select->order("a.t_tglprosesspt DESC");
        }

        if ($input->getPost('iDisplayStart') && $input->getPost('iDisplayLength') != '-1') {
            $select->limit(intval($input->getPost('iDisplayLength')));
            $select->offset(intval($input->getPost('iDisplayStart')));
            $no = 1 + intval($input->getPost('iDisplayStart'));
        } else {
            if (intval($input->getPost('iDisplayLength')) >= 1) {
                $select->limit(intval($input->getPost('iDisplayLength')));
                $select->offset(intval($input->getPost('iDisplayStart')));
                $no = 1 + intval($input->getPost('iDisplayStart'));
            } else {
                $select->limit(10);
                $select->offset(0);
                $no = 1;
            }
        }

        $statement = $sql->prepareStatementForSqlObject($select);
        $rResult = $statement->execute();

        return [
            'rResult' => $rResult,
            'iTotal' => $iTotal,
            'no' => $no
        ];
    }

    public function arPejabatTandaTangan($s_idpejabat)
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select(["a" => "s_pejabat"]);
        $select->where(["s_idpejabat" => $s_idpejabat]);
        $res = $sql->prepareStatementForSqlObject($select)->execute();

        $array = [];
        foreach ($res as $res) {
            $array[$res["s_idpejabat"]] = $res["s_namapejabat"] . " || " . $res["s_jabatanpejabat"];
        }
        return $array;
    }

    public function getConfig()
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select("s_penandatanganan_config")->where(["s_active" => 1]);
        $res = $sql->prepareStatementForSqlObject($select)->execute()->current();
        return $res;
    }

    public function saveLog($session, $post)
    {
        $data = [
            "created_by" => $session["s_iduser"],
            "t_nik" => $post["t_nik"],
            "s_idpejabat" => $post["s_idpejabat"],
            "s_idsurat" => $post["s_idsurat"],
            "t_idspt" => $post["t_idspt"],
            "t_response_code" => $post["t_response_code"],
            "t_response_message" => $post["t_response_message"]
        ];
        // var_dump($data);exit();

        $sql = new Sql($this->adapter);
        return $sql->prepareStatementForSqlObject($sql->insert("t_penandatanganan_log")->values($data))->execute();
    }

    public function save($session, $post)
    {
        $data = [
            "created_by" => $session["s_iduser"],
            "t_idspt" => $post["t_idspt"],
            "s_idsurat" => ((isset($post["s_idsurat"])) ? $post["s_idsurat"] : null),
            "t_path_file" => $post["t_path_file"],
            "t_name_file" => $post["t_name_file"],
            "s_idpejabat" => $post["s_idpejabat"],
            "id_dokumen" => $post["id_dokumen"],
            "guid" => $post["guid"],
            "t_nik" => $post["t_nik"],
            "t_nama_penandatanganan" => $post["t_nama_penandatanganan"]
        ];
        // var_dump($data);exit();

        $sql = new Sql($this->adapter);
        return $sql->prepareStatementForSqlObject($sql->insert("t_penandatanganan")->values($data))->execute();
    }

    public function gridSudah($input, $aColumns, $session, $cekurl, $allParams)
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from(["a" => $this->fr_pendaftaran_v5()]);
        $select->join(["b" => "t_paraf_penandatanganan"], "a.t_idspt = b.t_idspt", [], "LEFT");
        $select->join(["c" => "t_penandatanganan"], "a.t_idspt = c.t_idspt", [
            "guid", "t_id_penandatanganan"
        ], "LEFT");
        $where = new \Zend\Db\Sql\Where();
        $where->IsNotNull("a.t_kodebayarbanksppt");
        $where->IsNotNull("b.t_idspt");
        $where->isNotNull("c.t_idspt");
        // $where->literal("EXISTS (SELECT * FROM t_penandatanganan xa WHERE xa.t_idspt = a.t_idspt)");
        if ($input->getPost("sSearch_2") != "") {
            $where->literal("a.t_kohirspt::TEXT ILIKE '%" . $input->getPost("sSearch_2") . "%'");
        }
        if ($input->getPost("sSearch_3") != "") {
            $where->literal("a.t_kohirketetapanspt::TEXT ILIKE '%" . $input->getPost("sSearch_3") . "%'");
        }
        if ($input->getPost("sSearch_4") != "") {
            $where->literal("a.t_namawppembeli::TEXT ILIKE '%" . $input->getPost("sSearch_4") . "%'");
        }
        if ($input->getPost("sSearch_5") != "") {
            $where->literal("a.t_nopbphtbsppt::TEXT ILIKE '%" . $input->getPost("sSearch_5") . "%'");
        }
        if ($input->getPost("sSearch_6") != "") {
            $where->literal("a.t_kodebayarbanksppt::TEXT ILIKE '%" . $input->getPost("sSearch_6") . "%'");
        }
        $select->where($where);

        $iTotal = $sql->prepareStatementForSqlObject($select)->execute()->count();

        //================ ordernya coy
        $aOrderingRules = array();
        if ($input->getPost('iSortCol_0')) {
            $iSortingCols = intval($input->getPost('iSortingCols'));
            for ($i = 0; $i < $iSortingCols; $i++) {
                if ($input->getPost('bSortable_' . intval($input->getPost('iSortCol_' . $i))) == 'true') {
                    $aOrderingRules[] = $aColumns[intval($input->getPost('iSortCol_' . $i))] . " " . ($input->getPost('sSortDir_' . $i) === 'asc' ? 'asc' : 'desc');
                }
            }
        }


        if (!empty($aOrderingRules)) {
            $select->order(implode(", ", $aOrderingRules));
        } else {
            $select->order("b.created_at DESC");
        }

        if ($input->getPost('iDisplayStart') && $input->getPost('iDisplayLength') != '-1') {
            $select->limit(intval($input->getPost('iDisplayLength')));
            $select->offset(intval($input->getPost('iDisplayStart')));
            $no = 1 + intval($input->getPost('iDisplayStart'));
        } else {
            if (intval($input->getPost('iDisplayLength')) >= 1) {
                $select->limit(intval($input->getPost('iDisplayLength')));
                $select->offset(intval($input->getPost('iDisplayStart')));
                $no = 1 + intval($input->getPost('iDisplayStart'));
            } else {
                $select->limit(10);
                $select->offset(0);
                $no = 1;
            }
        }

        $statement = $sql->prepareStatementForSqlObject($select);
        $rResult = $statement->execute();

        return [
            'rResult' => $rResult,
            'iTotal' => $iTotal,
            'no' => $no
        ];
    }

    public function gridLog($input, $aColumns, $session, $cekurl, $allParams)
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from(["a" => "t_penandatanganan_log"]);
        $where = new \Zend\Db\Sql\Where();
        $select->where($where);

        $iTotal = $sql->prepareStatementForSqlObject($select)->execute()->count();

        //================ ordernya coy
        $aOrderingRules = array();
        if ($input->getPost('iSortCol_0')) {
            $iSortingCols = intval($input->getPost('iSortingCols'));
            for ($i = 0; $i < $iSortingCols; $i++) {
                if ($input->getPost('bSortable_' . intval($input->getPost('iSortCol_' . $i))) == 'true') {
                    $aOrderingRules[] = $aColumns[intval($input->getPost('iSortCol_' . $i))] . " " . ($input->getPost('sSortDir_' . $i) === 'asc' ? 'asc' : 'desc');
                }
            }
        }


        if (!empty($aOrderingRules)) {
            $select->order(implode(", ", $aOrderingRules));
        } else {
            $select->order("a.created_at DESC");
        }

        if ($input->getPost('iDisplayStart') && $input->getPost('iDisplayLength') != '-1') {
            $select->limit(intval($input->getPost('iDisplayLength')));
            $select->offset(intval($input->getPost('iDisplayStart')));
            $no = 1 + intval($input->getPost('iDisplayStart'));
        } else {
            if (intval($input->getPost('iDisplayLength')) >= 1) {
                $select->limit(intval($input->getPost('iDisplayLength')));
                $select->offset(intval($input->getPost('iDisplayStart')));
                $no = 1 + intval($input->getPost('iDisplayStart'));
            } else {
                $select->limit(10);
                $select->offset(0);
                $no = 1;
            }
        }

        $statement = $sql->prepareStatementForSqlObject($select);
        $rResult = $statement->execute();

        return [
            'rResult' => $rResult,
            'iTotal' => $iTotal,
            'no' => $no
        ];
    }

    public function getDataGuid($guid = null)
    {
        // Debug::dump($guid);
        // exit;
        $sql = new Sql($this->adapter);
        $select = $sql->select($this->table)->where(["guid" => $guid]);
        $res = $sql->prepareStatementForSqlObject($select)->execute()->current();
        return $res;
    }

    public function hapus($guid = null, $session = null)
    {
        $data = $this->getDataGuid($guid);

        $sql = new Sql($this->adapter);
        $insert = $sql->insert($this->table . "_cancel");
        $data["s_datetime_cancel"] = date("Y-m-d H:i:s");
        $data["s_user_cancel"] = $session["s_iduser"];
        $data["s_alasan_cancel"] = "Hapus Penandatanganan";
        $insert->values($data);
        $ex = $sql->prepareStatementForSqlObject($insert)->execute();

        if ($ex) {
            $del = $sql->delete($this->table);
            $del->where(["guid" => $guid]);
            $sql->prepareStatementForSqlObject($del)->execute();

            unlink($data["t_path_file"] . $data["t_name_file"]);
        }

        return true;
    }

    public function gridBelumParaf($input, $aColumns, $session, $cekurl, $allParams)
    {
        // var_dump($input->getPost());exit();
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from(["a" => $this->fr_pendaftaran_v5()]);
        $where = new \Zend\Db\Sql\Where();
        $where->IsNotNull("a.t_kodebayarbanksppt");
        $where->literal("NOT EXISTS (SELECT * FROM t_paraf_penandatanganan xa WHERE xa.t_idspt = a.t_idspt)");
        $where->equalTo("a.t_statusbayarspt", 'TRUE');
        if ($input->getPost("sSearch_2") != "") {
            $where->literal("a.t_tglprosesspt::TEXT ILIKE '%" . $input->getPost("sSearch_2") . "%'");
        }
        if ($input->getPost("sSearch_3") != "") {
            $where->literal("a.t_kohirspt::TEXT ILIKE '%" . $input->getPost("sSearch_3") . "%'");
        }
        if ($input->getPost("sSearch_4") != "") {
            $where->literal("a.t_kohirketetapanspt::TEXT ILIKE '%" . $input->getPost("sSearch_4") . "%'");
        }
        if ($input->getPost("sSearch_5") != "") {
            $where->literal("a.t_namawppembeli::TEXT ILIKE '%" . $input->getPost("sSearch_5") . "%'");
        }
        if ($input->getPost("sSearch_6") != "") {
            $where->literal("a.t_nopbphtbsppt::TEXT ILIKE '%" . $input->getPost("sSearch_6") . "%'");
        }
        if ($input->getPost("sSearch_7") != "") {
            $where->literal("a.t_kodebayarbanksppt::TEXT ILIKE '%" . $input->getPost("sSearch_7") . "%'");
        }
        // $where->equalTo(new Expression("EXTRACT(YEAR FROM a.t_tglprosesspt)"), date("Y"));
        $select->where($where);
        // die($sql->getSqlStringForSqlObject($select));
        $iTotal = $sql->prepareStatementForSqlObject($select)->execute()->count();

        //================ ordernya coy
        $aOrderingRules = array();
        if ($input->getPost('iSortCol_0')) {
            $iSortingCols = intval($input->getPost('iSortingCols'));
            for ($i = 0; $i < $iSortingCols; $i++) {
                if ($input->getPost('bSortable_' . intval($input->getPost('iSortCol_' . $i))) == 'true') {
                    $aOrderingRules[] = $aColumns[intval($input->getPost('iSortCol_' . $i))] . " " . ($input->getPost('sSortDir_' . $i) === 'asc' ? 'asc' : 'desc');
                }
            }
        }


        if (!empty($aOrderingRules)) {
            $select->order(implode(", ", $aOrderingRules));
        } else {
            $select->order("a.t_tglprosesspt DESC");
        }

        if ($input->getPost('iDisplayStart') && $input->getPost('iDisplayLength') != '-1') {
            $select->limit(intval($input->getPost('iDisplayLength')));
            $select->offset(intval($input->getPost('iDisplayStart')));
            $no = 1 + intval($input->getPost('iDisplayStart'));
        } else {
            if (intval($input->getPost('iDisplayLength')) >= 1) {
                $select->limit(intval($input->getPost('iDisplayLength')));
                $select->offset(intval($input->getPost('iDisplayStart')));
                $no = 1 + intval($input->getPost('iDisplayStart'));
            } else {
                $select->limit(10);
                $select->offset(0);
                $no = 1;
            }
        }

        $statement = $sql->prepareStatementForSqlObject($select);
        $rResult = $statement->execute();

        return [
            'rResult' => $rResult,
            'iTotal' => $iTotal,
            'no' => $no
        ];
    }

    public function gridSudahParaf($input, $aColumns, $session, $cekurl, $allParams)
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from(["a" => $this->fr_pendaftaran_v5()]);
        $select->join(["b" => "t_paraf_penandatanganan"], "a.t_idspt = b.t_idspt", [], "LEFT");
        $select->join(["c" => "t_penandatanganan"], "a.t_idspt = c.t_idspt", [
            "t_id_penandatanganan",
        ], "LEFT");
        $where = new \Zend\Db\Sql\Where();
        $where->IsNotNull("a.t_kodebayarbanksppt");
        $where->isNotNull("b.t_idspt");
        if ($input->getPost("sSearch_2") != "") {
            $where->literal("a.t_kohirspt::TEXT ILIKE '%" . $input->getPost("sSearch_2") . "%'");
        }
        if ($input->getPost("sSearch_3") != "") {
            $where->literal("a.t_kohirketetapanspt::TEXT ILIKE '%" . $input->getPost("sSearch_3") . "%'");
        }
        if ($input->getPost("sSearch_4") != "") {
            $where->literal("a.t_namawppembeli::TEXT ILIKE '%" . $input->getPost("sSearch_4") . "%'");
        }
        if ($input->getPost("sSearch_5") != "") {
            $where->literal("a.t_nopbphtbsppt::TEXT ILIKE '%" . $input->getPost("sSearch_5") . "%'");
        }
        if ($input->getPost("sSearch_6") != "") {
            $where->literal("a.t_kodebayarbanksppt::TEXT ILIKE '%" . $input->getPost("sSearch_6") . "%'");
        }
        $select->where($where);

        $iTotal = $sql->prepareStatementForSqlObject($select)->execute()->count();

        //================ ordernya coy
        $aOrderingRules = array();
        if ($input->getPost('iSortCol_0')) {
            $iSortingCols = intval($input->getPost('iSortingCols'));
            for ($i = 0; $i < $iSortingCols; $i++) {
                if ($input->getPost('bSortable_' . intval($input->getPost('iSortCol_' . $i))) == 'true') {
                    $aOrderingRules[] = $aColumns[intval($input->getPost('iSortCol_' . $i))] . " " . ($input->getPost('sSortDir_' . $i) === 'asc' ? 'asc' : 'desc');
                }
            }
        }


        if (!empty($aOrderingRules)) {
            $select->order(implode(", ", $aOrderingRules));
        } else {
            $select->order("b.created_at DESC");
        }

        if ($input->getPost('iDisplayStart') && $input->getPost('iDisplayLength') != '-1') {
            $select->limit(intval($input->getPost('iDisplayLength')));
            $select->offset(intval($input->getPost('iDisplayStart')));
            $no = 1 + intval($input->getPost('iDisplayStart'));
        } else {
            if (intval($input->getPost('iDisplayLength')) >= 1) {
                $select->limit(intval($input->getPost('iDisplayLength')));
                $select->offset(intval($input->getPost('iDisplayStart')));
                $no = 1 + intval($input->getPost('iDisplayStart'));
            } else {
                $select->limit(10);
                $select->offset(0);
                $no = 1;
            }
        }

        $statement = $sql->prepareStatementForSqlObject($select);
        $rResult = $statement->execute();

        return [
            'rResult' => $rResult,
            'iTotal' => $iTotal,
            'no' => $no
        ];
    }

    public function pejabatById($id)
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select("s_pejabat")->where(["s_idpejabat" => $id]);
        $res = $sql->prepareStatementForSqlObject($select)->execute()->current();
        return $res;
    }

    public function simpanParaf($post, $session)
    {
        $explode = explode(',', $post["t_idspt"]);
        $pejabat = $this->pejabatById($post["s_idpejabat"]);

        foreach ($explode as $key => $value) {
            $data = [
                "t_idspt" => $value,
                "s_idsurat" => 1,
                "s_idpejabat" => $post["s_idpejabat"],
                "created_at" => date("Y-m-d H:i:s"),
                "created_by" => $session["s_iduser"],
                "t_nama_paraf" => $pejabat["s_namapejabat"]
            ];

            $sql = new Sql($this->adapter);
            $insert = $sql->insert("t_paraf_penandatanganan")->values($data);
            $sql->prepareStatementForSqlObject($insert)->execute();
        }
    }

    public function dataParafByIdSpt($idSpt)
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select("t_paraf_penandatanganan")->where(["t_idspt" => $idSpt]);
        $res = $sql->prepareStatementForSqlObject($select)->execute()->current();
        return $res;
    }

    public function hapusParaf($id, $session)
    {
        $data = $this->dataParafByIdSpt($id);

        $sql = new Sql($this->adapter);
        $data["s_datetime_cancel"] = date("Y-m-d H:i:s");
        $data["s_user_cancel"] = $session["s_iduser"];
        $data["s_alasan_cancel"] = "Hapus Data";
        $insert = $sql->insert("t_paraf_penandatanganan_cancel")->values($data);
        $res = $sql->prepareStatementForSqlObject($insert)->execute();
        if ($res) {
            $delete = $sql->delete("t_paraf_penandatanganan")->where(["t_idspt" => $id]);
            $sql->prepareStatementForSqlObject($delete)->execute();
        }
    }

    public function getDataByIdSpt($idSpt)
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select("t_penandatanganan")->where(["t_idspt" => (int) $idSpt]);
        $res = $sql->prepareStatementForSqlObject($select)->execute()->current();
        return $res;
    }
}
