<?php

namespace Bphtb\Model\Ptsl;

use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Sql;

class PtslTable extends AbstractTableGateway
{

    protected $table = "t_spt_ptsl";

    public function __construct(Adapter $adapter)
    {
        $this->adapter = $adapter;
    }

    public function simpanPtsl($post)
    {
        $data = [
            "t_no_ptsl" => (($post["t_no_ptsl"] != "") ? $post["t_no_ptsl"] : null),
            "t_tgl_ptsl" => (($post["t_tgl_ptsl"] != "") ? date("Y-m-d", strtotime($post["t_tgl_ptsl"])) : null),
            "t_ket_ptsl" => (($post["t_ket_ptsl"] != "") ? $post["t_ket_ptsl"] : null),
        ];
        $id = (int) $post["t_id_ptsl"];
        $sql = new Sql($this->adapter);
        if ($id == 0) {
            $dataSpt = $this->getDataSpt($post["t_idspt"]);
            $data["t_idspt"] = $post["t_idspt"];
            $data["t_totalspt_sblm_ptsl"] = $dataSpt["t_totalspt"];
            $query = $sql->insert($this->table)->values($data);
        } else {
            $query = $sql->update($this->table)->set($data)->where(["t_id_ptsl" => $id]);
        }
        $return = $sql->prepareStatementForSqlObject($query)->execute();
        return $return;
    }

    public function getDataSpt($idspt)
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select("t_spt")->where(["t_idspt" => (int) $idspt]);
        $res = $sql->prepareStatementForSqlObject($select)->execute()->current();
        return $res;
    }

    public function updateSptPtsl($idspt, $session)
    {
        // UPDATE SPT NILAI NIHIL
        $sql = new Sql($this->adapter);
        $data = [
            "t_totalspt" => 0
        ];
        $update = $sql->update("t_spt")->set($data)->where(["t_idspt" => (int) $idspt]);
        $res = $sql->prepareStatementForSqlObject($update)->execute();

        if ($res) {
            // UPDATE PEMBAYARAN
            $update = $sql->update("t_pembayaranspt");
            $update->set([
                't_statusbayarspt' => TRUE,
                't_periodepembayaran' => date('Y'),
                't_tanggalpembayaran' => date('Y-m-d'),
                't_ketetapanspt' => 1,
                't_nilaipembayaranspt' => 0,
                't_idpenerimasetoran' => $session["s_iduser"]
            ])->where(["t_idspt" => $idspt]);
            $res = $sql->prepareStatementForSqlObject($update)->execute();
        }

        return $res;
    }

    public function getDataSptPtsl($idspt)
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select($this->table)->where(["t_idspt" => (int) $idspt]);
        $res = $sql->prepareStatementForSqlObject($select)->execute()->current();
        return $res;
    }

    public function deletePtsl($idSpt)
    {
        $sql = new Sql($this->adapter);
        $delete = $sql->delete($this->table)->where(["t_idspt" => (int) $idSpt]);
        $res = $sql->prepareStatementForSqlObject($delete)->execute();
        return $res;
    }

    public function returnTotalSpt($idSpt)
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select($this->table)->where(["t_idspt" => (int) $idSpt]);
        $res = $sql->prepareStatementForSqlObject($select)->execute()->current();

        $update = $sql->update("t_spt")->set(["t_totalspt" => $res["t_totalspt_sblm_ptsl"]])->where(["t_idspt" => (int) $idSpt]);
        $res = $sql->prepareStatementForSqlObject($update)->execute();
        return $res;
    }
}
