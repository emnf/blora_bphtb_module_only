<?php

namespace Bphtb\Helper\Api;

use Bphtb\Controller\Api\ApiController;
use Zend\Mvc\Controller\Plugin\AbstractPlugin;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class ApiLoginHelper extends AbstractPlugin implements ServiceLocatorAwareInterface
{
    protected $serviceLocator;

    public function getServiceLocator()
    {
        return $this->serviceLocator->getServiceLocator();
    }

    public function setServiceLocator(ServiceLocatorInterface $serviceLocator)
    {
        $this->serviceLocator = $serviceLocator;
    }

    public function checkUserLogin($authorization)
    {
        $contains = $this->decodeBasicAuth($authorization);

        $data = $this->getServiceLocator()->get('UserTable')->getDataByUsername($contains['username']);

        if (!$data) {
            $code = ApiConstHelper::CODE_USER_NOT_FOUND;
            $message = ApiConstHelper::MESSAGE_USER_NOT_FOUND;
        } else {

            if ($this->passwordConfirm($data['s_password'], $contains['password']) != ApiConstHelper::CODE_SUCCESS) {
                $code = ApiConstHelper::CODE_USER_AND_PASSWORD_ERROR;
                $message = ApiConstHelper::MESSAGE_USER_AND_PASSWORD_ERROR;
            } else {
                $code = ApiConstHelper::CODE_SUCCESS;
                $message = ApiConstHelper::MESSAGE_SUCCESS;
            }
        }
        return [
            'resp_code' => $code,
            'resp_message' => $message
        ];
    }

    public function decodeBasicAuth($authorization)
    {
        $explodeAuthorization = explode(' ', $authorization);
        $decoded = base64_decode($explodeAuthorization[1]);
        list($username, $password) = explode(":", $decoded);
        return [
            'username' => $username,
            'password' => $password
        ];
    }

    public function passwordConfirm($password, $confirmPassword)
    {
        if ($password != md5($confirmPassword)) {
            return ApiConstHelper::CODE_ERROR;
        } else {
            return ApiConstHelper::CODE_SUCCESS;
        }
    }
}
