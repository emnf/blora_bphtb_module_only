<?php

namespace Bphtb\Model\Spop;

use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Where;
use Zend\Debug\Debug;

class InputSpopTable extends AbstractTableGateway
{

    protected $table_spop = "t_spt_spop";
    protected $table_lspop = "t_spt_lspop";

    public function __construct(Adapter $adapter)
    {
        $this->adapter = $adapter;
    }

    public function getDataSptByIdSpt($idSpt)
    {
        // die($idSpt);
        $sql = new Sql($this->adapter);
        $select = $sql->select(['a' => 't_spt']);
        $select->columns([
            't_kohirspt', 't_tglprosesspt', 't_periodespt', 't_nopbphtbsppt', 't_idspt'
        ]);
        $select->join(['b' => 't_detailsptbphtb'], "a.t_idspt = b.t_idspt", [
            't_namawppembeli',    't_nikwppembeli', 't_alamatwppembeli', 't_kecamatanwppembeli', 't_kelurahanwppembeli', 't_kabkotawppembeli', 't_telponwppembeli', 't_kodeposwppembeli', 't_npwpwppembeli', 't_luastanah', 't_njoptanah', 't_luasbangunan', 't_njopbangunan', 't_totalnjoptanah', 't_totalnjopbangunan', 't_grandtotalnjop', 't_nosertifikathaktanah', 't_kelurahanop', 't_kecamatanop', 't_rtwppembeli', 't_rwwppembeli', 't_alamatop', 't_rtop', 't_rwop', 't_namasppt', 't_kabupatenop', 't_nosertifikatbaru', 't_tglsertifikatbaru', 't_luastanah_sismiop', 't_luasbangunan_sismiop', 't_njoptanah_sismiop', 't_njopbangunan_sismiop', 't_grandtotalnjop_aphb',
        ], 'LEFT');
        $select->join(['c' => 't_pembayaranspt'], "a.t_idspt = c.t_idspt", [
            't_statusbayarspt', 't_tanggalpembayaran', 't_nilaipembayaranspt'
        ], 'LEFT');
        $select->join(['d' => 't_pemeriksaan'], "c.t_idpembayaranspt = d.p_idpembayaranspt", [
            'p_idpemeriksaan', 'p_luastanah', 'p_luasbangunan', 'p_njoptanah', 'p_njopbangunan', 'p_totalnjoptanah', 'p_totalnjopbangunan', 'p_grandtotalnjop', 'p_nilaitransaksispt', 'p_potonganspt', 'p_idjenistransaksi', 'p_idjenishaktanah', 'p_totalspt', 'p_nilaipembayaranspt', 'p_grandtotalnjop_aphb'
        ], 'LEFT');
        $where = new Where();
        $where->equalTo('a.t_idspt', $idSpt);
        $select->where($where);
        // die($sql->buildSqlString($select));
        $res = $sql->prepareStatementForSqlObject($select)->execute()->current();
        return $res;
    }

    public function simpanDataSpop($post, $session, $id = null)
    {
        $explodeNop = explode('.', $post['t_nop']);

        $data = [
            't_idspt' => $post['t_idspt'],
            't_jenis_kepemilikan' => $post['t_jenis_kepemilikan'],
            't_nik_wp' => $post['t_nik_wp'],
            't_nama_wp' => $post['t_nama_wp'],
            't_rt_wp' => $post['t_rt_wp'],
            't_rw_wp' => $post['t_rw_wp'],
            't_jalan_wp' => $post['t_jalan_wp'],
            't_kelurahan_wp' => $post['t_kelurahan_wp'],
            't_kecamatan_wp' => $post['t_kecamatan_wp'],
            't_kabupaten_wp' => $post['t_kabupaten_wp'],
            't_no_hp_wp' => $post['t_no_hp_wp'],
            'kd_propinsi' => $explodeNop[0],
            'kd_dati2' => $explodeNop[1],
            'kd_kecamatan' => $explodeNop[2],
            'kd_kelurahan' => $explodeNop[3],
            'kd_blok' => $explodeNop[4],
            'no_urut' => $explodeNop[5],
            'kd_jns_op' => $explodeNop[6],
            't_rt_op' => $post['t_rt_op'],
            't_rw_op' => $post['t_rw_op'],
            't_jalan_op' => $post['t_jalan_op'],
            't_kelurahan_op' => $post['t_kelurahan_op'],
            't_kecamatan_op' => $post['t_kecamatan_op'],
            't_jenis_tanah' => $post['t_jenis_tanah'],
            't_luas_tanah' => $post['t_luas_tanah'],
            // 't_kd_znt' => $post['t_kd_znt'],
            't_jmlh_bangunan' => ($post['t_jmlh_bangunan'] != '' ? $post['t_jmlh_bangunan'] : null),
            't_latitude' => $post['t_latitude'],
            't_longitude' => $post['t_longitude'],
            'created_date' => date("Y-m-d H:i:s"),
            'created_by' => $session['s_iduser'],
            // 't_id_jenis_pelayanan' => $post['t_id_jenis_pelayanan'],
            // 't_nomor_bidang' => $post['t_nomor_bidang'],
            't_nomor_sertifikat' => $post['t_nomor_sertifikat'],
            't_blok_op' => $post['t_blok_op'],
            't_blok_wp' => $post['t_blok_wp'],
            't_pekerjaan_wp' => $post['t_pekerjaan_wp'],
            't_npwp_wp' => $post['t_npwp_wp'],
            't_kode_pos_wp' => $post['t_kode_pos_wp'],
            // 't_luas_bangunan' => $post['t_luas_bangunan'],
            't_kode_blok_op' => $post['t_kode_blok_op'],
            // 't_nop_asal' => $post['t_nop_asal'],
            // 't_keterangan' => $post['t_keterangan'],
            // 'thn_pelayanan' => $post['thn_pelayanan'],
            // 'bundel_pelayanan' => $post['bundel_pelayanan'],
            // 'no_urut_pelayanan' => $post['no_urut_pelayanan'],
            // 't_tahun_spop' => $post['t_tahun_spop'],
            // 't_no_urut_spop' => $post['t_no_urut_spop'],
            // 't_no_spop' => $post['t_no_spop'],
            // 't_id_spop' => $post['t_id_spop'],
            // 't_id_permohonan' => $post['t_id_permohonan'],
            // 't_jmlh_bidang' => $post['t_jmlh_bidang'],
            't_nop' => $post['t_nop'],
            // 't_nop_terdekat' => $post['t_nop_terdekat'],

        ];
        // var_dump($data);
        // exit();

        $sql = new Sql($this->adapter);
        if ($id == null) {
            $insert = $sql->insert('t_spt_spop')->values($data);
            $sql->prepareStatementForSqlObject($insert)->execute();

            $idSpop = $this->adapter->getDriver()->getConnection()->getLastGeneratedValue('t_spt_spop_id_seq');
        } else {
            $update = $sql->update('t_spt_spop')->set($data)->where(['id' => $id]);
            $sql->prepareStatementForSqlObject($update)->execute();

            $idSpop = $id;
        }

        // var_dump($post);exit();
        // clear dulu
        // data lspop yg dihapus dari form akan dihapus
        $this->clearDataLspop($idSpop, $post['id_lspop']);

        // simpan lspop klo ada
        if (isset($post['id_lspop']) && count($post['id_lspop']) > 0) {
            for ($i = 0; $i < count($post['id_lspop']); $i++) {
                // ekseskusi satu per satu
                $this->simpanDataLspop($post, $i, $session, $idSpop, $post['id_lspop'][$i]);
            }
        }
    }

    public function simpanDataSPopMutasi($post, $session, $id = null)
    {
        $explodeNop = explode('.', $post['t_nop']);

        $data = [
            't_idspt' => $post['t_idspt'],
            't_jenis_kepemilikan' => $post['t_jenis_kepemilikan'],
            't_nik_wp' => $post['t_nik_wp'],
            't_nama_wp' => $post['t_nama_wp'],
            't_rt_wp' => $post['t_rt_wp'],
            't_rw_wp' => $post['t_rw_wp'],
            't_jalan_wp' => $post['t_jalan_wp'],
            't_kelurahan_wp' => $post['t_kelurahan_wp'],
            't_kecamatan_wp' => $post['t_kecamatan_wp'],
            't_kabupaten_wp' => $post['t_kabupaten_wp'],
            't_no_hp_wp' => $post['t_no_hp_wp'],
            'kd_propinsi' => $explodeNop[0],
            'kd_dati2' => $explodeNop[1],
            'kd_kecamatan' => $explodeNop[2],
            'kd_kelurahan' => $explodeNop[3],
            'kd_blok' => $explodeNop[4],
            'no_urut' => $explodeNop[5],
            'kd_jns_op' => $explodeNop[6],
            't_rt_op' => $post['t_rt_op'],
            't_rw_op' => $post['t_rw_op'],
            't_jalan_op' => $post['t_jalan_op'],
            't_kelurahan_op' => $post['t_kelurahan_op'],
            't_kecamatan_op' => $post['t_kecamatan_op'],
            't_jenis_tanah' => $post['t_jenis_tanah'],
            't_luas_tanah' => $post['t_luas_tanah'],
            't_kd_znt' => $post['t_kd_znt'],
            't_jmlh_bangunan' => ($post['t_jmlh_bangunan'] != '' ? $post['t_jmlh_bangunan'] : null),
            't_latitude' => $post['t_latitude'],
            't_longitude' => $post['t_longitude'],
            'created_date' => date("Y-m-d H:i:s"),
            'created_by' => $session['s_iduser'],
            // 't_id_jenis_pelayanan' => $post['t_id_jenis_pelayanan'],
            't_nomor_bidang' => $post['t_nomor_bidang'],
            't_nomor_sertifikat' => $post['t_nomor_sertifikat'],
            't_blok_op' => $post['t_blok_op'],
            't_blok_wp' => $post['t_blok_wp'],
            't_pekerjaan_wp' => $post['t_pekerjaan_wp'],
            't_npwp_wp' => $post['t_npwp_wp'],
            't_kode_pos_wp' => $post['t_kode_pos_wp'],
            // 't_luas_bangunan' => $post['t_luas_bangunan'],
            't_kode_blok_op' => $post['t_kode_blok_op'],
            't_nop_asal' => ($post['t_nomor_bidang'] == 1 ? null : $post['t_nop_asal']),
            't_keterangan' => $post['t_keterangan'],
            // 'thn_pelayanan' => $post['thn_pelayanan'],
            // 'bundel_pelayanan' => $post['bundel_pelayanan'],
            // 'no_urut_pelayanan' => $post['no_urut_pelayanan'],
            // 't_tahun_spop' => $post['t_tahun_spop'],
            // 't_no_urut_spop' => $post['t_no_urut_spop'],
            // 't_no_spop' => $post['t_no_spop'],
            // 't_id_spop' => $post['t_id_spop'],
            // 't_id_permohonan' => $post['t_id_permohonan'],
            't_jmlh_bidang' => $post['t_jmlh_bidang'],
            't_nop' => (($post['t_nomor_bidang'] == 1) ? $post['t_nop'] : null),
            // 't_nop_terdekat' => $post['t_nop_terdekat'],

        ];
        // Debug::dump($data);
        // exit;
        $sql = new Sql($this->adapter);
        if ($id == null) {
            $insert = $sql->insert('t_spt_spop')->values($data);
            $sql->prepareStatementForSqlObject($insert)->execute();

            $idSpop = $this->adapter->getDriver()->getConnection()->getLastGeneratedValue('t_spt_spop_id_seq');
        } else {
            $update = $sql->update('t_spt_spop')->set($data)->where(['id' => $id]);
            $sql->prepareStatementForSqlObject($update)->execute();
            $idSpop = $id;
        }

        // var_dump($post);exit();
        // clear dulu
        // data lspop yg dihapus dari form akan dihapus
        $this->clearDataLspop($idSpop, $post['id_lspop']);

        // simpan lspop klo ada
        if (isset($post['id_lspop']) && count($post['id_lspop']) > 0) {
            for ($i = 0; $i < count($post['id_lspop']); $i++) {
                // ekseskusi satu per satu
                $this->simpanDataLspop($post, $i, $session, $idSpop, $post['id_lspop'][$i]);
            }
        }
    }

    public function simpanDataLspop($post, $key, $session, $idSpop, $idLspop)
    {
        $data = [
            't_idspt' => (int)$post['t_idspt'],
            'id_spop' => (int)$idSpop,
            't_id_spop' => (int)$idSpop,
            't_id_lspop' => (int)$idLspop,
            't_jenis_transaksi' => (int) $post['t_jenis_transaksi'][$key],
            't_jenis_bangunan' => $post['t_jenis_bangunan'][$key],
            't_luas_bangunan' => (int) $post['t_luas_bangunan'][$key], //pneyesuaian form
            't_jumlah_lantai' => (int) $post['t_jumlah_lantai'][$key],
            't_tahun_dibangun' => (int) $post['t_tahun_dibangun'][$key],
            't_tahun_direnovasi' => (int) ($post['t_tahun_direnovasi'][$key] != '' ? $post['t_tahun_direnovasi'][$key] : null),
            't_daya_listrik' => (int) $post['t_daya_listrik'][$key] != 'null' ? $post['t_daya_listrik'][$key] : null,
            't_kondisi_bangunan' => (int) $post['t_kondisi_bangunan'][$key],
            't_konstruksi' => (int) $post['t_konstruksi'][$key],
            't_atap' => (int) $post['t_atap'][$key],
            't_dinding' => (int) $post['t_dinding'][$key],
            't_lantai' => (int) $post['t_lantai'][$key],
            't_langit' => (int) $post['t_langit'][$key],
            't_jmlh_ac_split' => (int) $post['t_jmlh_ac_split'][$key],
            't_jmlh_ac_window' => (int) $post['t_jmlh_ac_window'][$key],
            't_ac_central' => (int) $post['t_ac_central'][$key],
            't_luas_kolam' => (int) $post['t_luas_kolam'][$key],
            't_plester_kolam' => (int) $post['t_plester_kolam'][$key],
            't_luas_perkerasan_halaman' => (int) $post['t_luas_perkerasan_halaman'][$key],
            't_panjang_pagar' => (int) $post['t_panjang_pagar'][$key],
            't_bahan_pagar' => (int) $post['t_bahan_pagar'][$key],
            't_kedalaman_sumur' => (int) $post['t_kedalaman_sumur'][$key],
            'created_date' => date('Y-m-d H:i:s'),
            'created_by' => (int)$session['s_iduser'],
            't_nomor_bangunan' => (int) $post['t_nomor_bangunan'][$key],
            't_jmlh_bangunan' => (int) $post['t_jmlh_bangunan_lspop'][$key], //penyeysuaian fomr untuk simpan sajas
            // 't_tahun_lspop' => $post['t_tahun_lspop'][$key],
            // 't_no_urut_lspop' => $post['t_no_urut_lspop'][$key],

        ];
        // Debug::dump($data);
        // exit;
        // $sql = new Sql($this->adapter);
        // if ((int) $idLspop == 0) {
        //     $insert = $sql->insert('t_spt_lspop')->values($data);
        //     $res = $sql->prepareStatementForSqlObject($insert)->execute();
        // } else {
        //     $update = $sql->update('t_spt_lspop')->set($data)->where(['id' => $idLspop]);
        //     $res = $sql->prepareStatementForSqlObject($update)->execute();
        // }
        try {
            $sql = new Sql($this->adapter);

            if ((int) $idLspop == 0) {
                $insert = $sql->insert('t_spt_lspop')->values($data);
                $res = $sql->prepareStatementForSqlObject($insert)->execute();
            } else {
                $update = $sql->update('t_spt_lspop')->set($data)->where(['id' => $idLspop]);
                $res = $sql->prepareStatementForSqlObject($update)->execute();
            }

            // Check for errors here, if needed
            if ($res->getAffectedRows() === 0) {
                // No rows were affected, handle the error here
            }
        } catch (\Exception $e) {
            // Handle the exception here
            echo "Error: " . $e;
            // Debug::dump($e);
            exit;
        }
    }

    public function clearDataLspop($idSpop, $dataLspop)
    {
        // var_dump($dataLspop);exit();
        // hapus data lspop yg tidak ada pada data post
        // karena sudah dihapuskan dr form
        $sql = new Sql($this->adapter);
        $delete = $sql->delete('t_spt_lspop');
        $where = new Where();
        $where->equalTo('id_spop', $idSpop);

        if ($dataLspop != null) {
            $arrIn = [];
            for ($i = 0; $i < count($dataLspop); $i++) {
                // var_dump($dataLspop[$i]);
                // exit();
                $arrIn[] = (int) $dataLspop[$i];
            }
            $where->notIn('id', $arrIn);
        }

        $delete->where($where);
        // die($sql->buildSqlString($delete));
        $res = $sql->prepareStatementForSqlObject($delete)->execute();
        return $res;
    }

    public function getDataSpopBySpt($idSpt)
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select('t_spt_spop')->where(['t_idspt' => $idSpt]);
        $select->columns([
            '*',
            // 't_bangunan_count' => new Expression("(SELECT COUNT(*) FROM t_spt_lspop WHERE id_spop = t_spt_spop.id)")
        ]);
        $res = $sql->prepareStatementForSqlObject($select)->execute();
        return $res;
    }

    public function getDataSpopByIdSpop($idSpop)
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select('t_spt_spop')->where(['id' => $idSpop]);
        $res = $sql->prepareStatementForSqlObject($select)->execute()->current();
        return $res;
    }

    public function getDataLspopByISpop($idSpop)
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select('t_spt_lspop')->where(['id_spop' => $idSpop]);
        $select->order('t_nomor_bangunan');
        $res = $sql->prepareStatementForSqlObject($select)->execute();
        return $res;
    }

    public function getDataByKodeBayar($kodebayar)
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select(['a' => 't_spt_spop']);
        $select->join(['b' => 't_pembayaranspt'], 'a.t_idspt = b.t_idspt', [], 'LEFT');

        $where = new Where();
        $where->equalTo('b.t_kodebayarbanksppt', $kodebayar);
        $select->where($where);
        $select->order('a.t_nomor_bidang');
        $res = $sql->prepareStatementForSqlObject($select)->execute();

        $arrSpop = [];
        foreach ($res as $key => $value) {
            $getLspop = $this->getDataLspopByISpop($value['id']);
            $resultSet = new ResultSet();
            $resultSet->initialize($getLspop);
            $value['lspop'] = $resultSet->toArray();

            $arrSpop[] = $value;
        }

        return $arrSpop;
    }

    // public function getDataLspopByKodeBayar($kodebayar)
    // {
    //     $sql = new Sql($this->adapter);
    //     $select = $sql->select(['a' => 't_spt_lspop']);
    //     $select->join(['b' => 't_pembayaranspt'], 'a.t_idspt = b.t_idspt', [], 'LEFT');
    //     $where = new Where();
    //     $where->equalTo('b.t_kodebayarbanksppt', $kodebayar);
    //     $select->where($where);
    //     $select->order('a.t_nomor_bangunan');
    //     $res = $sql->prepareStatementForSqlObject($select)->execute();
    //     // return $res;

    //     $resultSet = new ResultSet();
    //     $resultSet->initialize($res);
    //     return $resultSet->toArray();
    // }

    public function getComboTipeSpop()
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select('s_tipe_espop');
        // die($sql->buildSqlString($select));
        $res = $sql->prepareStatementForSqlObject($select)->execute();
        return $res;
    }

    public function saveSpopKonfirmasi($idSpt, $tipeSpop, $session)
    {
        $sql = new Sql($this->adapter);
        $update = $sql->update('t_spt')->set([
            't_id_tipe_espop' => $tipeSpop,
            't_konfirm_espop' => 1,
            't_tgl_konfirm_espop' => date('Y-m-d H:i:s'),
            't_user_konfirm_espop' => $session['s_iduser']

        ]);
        $update->where("t_idspt = '" . $idSpt . "'");

        return $sql->prepareStatementForSqlObject($update)->execute();
    }
}
