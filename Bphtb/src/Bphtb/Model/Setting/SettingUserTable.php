<?php

namespace Bphtb\Model\Setting;

use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Where;
use Zend\Db\ResultSet\ResultSet;

class SettingUserTable extends AbstractTableGateway
{

    protected $table = 's_users';

    public function __construct(Adapter $adapter)
    {
        $this->adapter = $adapter;
        $this->resultSetPrototype = new ResultSet();
        $this->resultSetPrototype->setArrayObjectPrototype(new SettingUserBase());
        $this->initialize();
    }


    public function getGridCount(SettingUserBase $base)
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from($this->table);
        $where = new Where();
        if ($base->s_username != 'undefined')
            $where->literal("$this->table.s_username::text LIKE '%$base->s_username%'");
        if ($base->s_jabatan != 'undefined')
            $where->literal("$this->table.s_jabatan::text LIKE '%$base->s_jabatan%'");
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res->count();
    }

    public function getGridData(SettingUserBase $base, $offset)
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from($this->table);
        $where = new Where();
        if ($base->s_username != 'undefined')
            $where->literal("$this->table.s_username::text LIKE '%$base->s_username%'");
        if ($base->s_jabatan != 'undefined')
            $where->literal("$this->table.s_jabatan::text LIKE '%$base->s_jabatan%'");
        $select->where($where);
        $select->order("s_iduser asc");
        $select->limit($base->rows = (int) $base->rows);
        $select->offset($offset = (int) $offset);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res;
    }

    public function fetchAll()
    {
        $resultSet = $this->select();
        return $resultSet;
    }

    public function getUser(SettingUserBase $sb)
    {
        $rowset = $this->select(array('s_iduser' => $sb->s_iduser));
        $row = $rowset->current();
        return $row;
    }

    public function getUserId($id)
    {
        $rowset = $this->select(array('s_iduser' => $id));
        $row = $rowset->current();
        return $row;
    }

    public function getUserEdit(SettingUserBase $sb)
    {
        $rowset = $this->select(array('s_username' => $sb->s_username, 's_tipe_pejabat' => $sb->s_tipe_pejabat, 's_idpejabat_idnotaris' => $sb->s_idpejabat_idnotaris));
        $row = $rowset->current();
        return $row;
    }

    public function saveData(SettingUserBase $sb)
    {
        $data = array(
            's_username' => $sb->s_username,
            's_password' => md5($sb->s_password),
            's_jabatan' => $sb->s_jabatan,
            's_akses' => $sb->s_akses,
            's_idpejabat_idnotaris' => (int) $sb->s_idpejabat_idnotaris,
            's_tipe_pejabat' => (int) $sb->s_tipe_pejabat,
            's_id_user_espop' => $sb->s_id_user_espop,
        );

        $id = (int) $sb->s_iduser;
        if ($id == 0) {
            $this->insert($data);
        } else {
            $this->update($data, array('s_iduser' => $sb->s_iduser));
        }
        $rowset = $this->select(array(
            's_username' => $sb->s_username,
            's_password' => md5($sb->s_password)
        ));
        $row = $rowset->current();
        return $row;
    }

    public function saveresourcepermission($s_iduser, $s_akses)
    {
        $sql = "INSERT INTO permission_resource (s_iduser, s_idpermission) VALUES (" . $s_iduser . "," . $s_akses . ")";
        $statement = $this->adapter->query($sql);
        return $statement->execute();
    }

    public function deleteResourcePermision($s_iduser)
    {
        $sql = "DELETE FROM permission_resource WHERE s_iduser = " . $s_iduser;
        $statement = $this->adapter->query($sql);
        return $statement->execute();
    }

    public function getRole()
    {
        $sql = "SELECT * FROM role";
        $st = $this->adapter->query($sql);
        $rs = $st->execute();
        foreach ($rs as $key => $value) {
            $ar_role[$value['rid']] = $value['role_name'];
        }
        return $ar_role;
    }

    public function getPejabat()
    {
        $sql = "SELECT * FROM s_pejabat";
        $st = $this->adapter->query($sql);
        return $st->execute();
    }

    public function getNotaris()
    {
        $sql = "SELECT * FROM s_notaris order by s_idnotaris asc";
        $st = $this->adapter->query($sql);
        return $st->execute();
    }

    public function getuserdata($user)
    {
        $sql = new \Zend\Db\Sql\Sql($this->adapter);
        $select = $sql->select();
        $select->from($this->table);
        $select->join(array('role' => 'role'), 'role.rid = s_users.s_akses', array('rid', 'role_name'), 'LEFT');
        $where = new \Zend\Db\Sql\Where();
        $where->equalTo("s_username", $user);
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res->current();
    }

    public function hapusData($id)
    {
        $this->delete(array('s_iduser' => $id));
        $sql = "DELETE FROM permission_resource WHERE s_iduser=$id";
        $st = $this->adapter->query($sql);
        $st->execute();
    }

    public function getPermission($resource_id)
    {
        $sql = new \Zend\Db\Sql\Sql($this->adapter);
        $select = $sql->select();
        $select->from('permission');
        $select->order('alias', 'asc');
        $where = new \Zend\Db\Sql\Where();
        $where->equalTo("resource_id", $resource_id);
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res;
    }

    public function getResourcePermision($id)
    {
        $sql = new \Zend\Db\Sql\Sql($this->adapter);
        $select = $sql->select();
        $select->from('permission_resource');
        $where = new \Zend\Db\Sql\Where();
        $where->equalTo("s_iduser", $id);
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();

        $returnArray = array();
        foreach ($res as $row) {
            $returnArray[] = $row['s_idpermission'];
        }
        return $returnArray;
    }

    public function savepassword(SettingUserBase $sb)
    {
        $data = array(
            's_password' => md5($sb->s_password)
        );
        $this->update($data, array('s_iduser' => $sb->s_iduser));
    }

    public function getDataByUsername($username)
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select($this->table)->where(['s_username' => $username]);
        $res = $sql->prepareStatementForSqlObject($select)->execute()->current();
        return $res;
    }
}
