<?php

namespace Bphtb\Model\Setting;

use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Where;
use Zend\Db\ResultSet\ResultSet;

class TarifNpotkpTable extends AbstractTableGateway {

    protected $table = 's_tarifnpoptkp', $table_ref1 = "s_jenistransaksi";

    public function __construct(Adapter $adapter) {
        $this->adapter = $adapter;
        $this->resultSetPrototype = new ResultSet();
        $this->resultSetPrototype->setArrayObjectPrototype(new TarifNpotkpBase());
        $this->initialize();
    }

    public function savedata(TarifNpotkpBase $kc) {
        $data = array(
            's_idjenistransaksinpotkp' => $kc->s_idjenistransaksinpotkp,
            's_tarifnpotkp' => $kc->s_tarifnpotkp,
//            's_tarifnpotkptambahan' => $kc->s_tarifnpotkptambahan,
            's_dasarhukumnpotkp' => $kc->s_dasarhukumnpotkp,
            's_statusnpotkp' => $kc->s_statusnpotkp,
//            's_tarifnpotkptahunajb1'=>$kc->s_tarifnpotkptahunajb1,
//            's_tarifnpotkptahunajb2'=>$kc->s_tarifnpotkptahunajb2
        );
        $id = (int) $kc->s_idtarifnpotkp;
        if ($id == 0) {
            $this->insert($data);
        } else {
            $this->update($data, array('s_idtarifnpotkp' => $kc->s_idtarifnpotkp));
        }
    }

    public function getGridCount(TarifNpotkpBase $base) {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from($this->table);
        $select->join("$this->table_ref1", "$this->table_ref1.s_idjenistransaksi = $this->table.s_idjenistransaksinpotkp");
        $where = new Where();
        if($base->s_kodejenistransaksi != 'undefined')
            $where->literal("$this->table_ref1.s_kodejenistransaksi::text LIKE '%$base->s_kodejenistransaksi%'");     
        if($base->s_namajenistransaksi != 'undefined')
            $where->literal("$this->table_ref1.s_namajenistransaksi::text LIKE '%$base->s_namajenistransaksi%'");     
        if($base->s_tarifnpotkp != 'undefined')
            $where->literal("$this->table.s_tarifnpotkp::text LIKE '%$base->s_tarifnpotkp%'");
//        if($base->s_tarifnpotkptambahan != 'undefined')
//            $where->literal("$this->table.s_tarifnpotkptambahan::text LIKE '%$base->s_tarifnpotkptambahan%'");     
//        if($base->s_tarifnpotkptahunajb1 != 'undefined')
//            $where->literal("$this->table.s_tarifnpotkptahunajb1::text LIKE '%$base->s_tarifnpotkptahunajb1%'");     
//        if($base->s_tarifnpotkptahunajb2 != 'undefined')
//            $where->literal("$this->table.s_tarifnpotkptahunajb2::text LIKE '%$base->s_tarifnpotkptahunajb2%'");
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res->count();
    }

    public function getGridData(TarifNpotkpBase $base, $offset) {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from($this->table);
        $select->join("$this->table_ref1", "$this->table_ref1.s_idjenistransaksi = $this->table.s_idjenistransaksinpotkp");
        $where = new \Zend\Db\Sql\Where();
        if($base->s_kodejenistransaksi != 'undefined')
            $where->literal("$this->table_ref1.s_kodejenistransaksi::text LIKE '%$base->s_kodejenistransaksi%'");     
        if($base->s_namajenistransaksi != 'undefined')
            $where->literal("$this->table_ref1.s_namajenistransaksi::text LIKE '%$base->s_namajenistransaksi%'");     
        if($base->s_tarifnpotkp != 'undefined')
            $where->literal("$this->table.s_tarifnpotkp::text LIKE '%$base->s_tarifnpotkp%'");
//        if($base->s_tarifnpotkptambahan != 'undefined')
//            $where->literal("$this->table.s_tarifnpotkptambahan::text LIKE '%$base->s_tarifnpotkptambahan%'");     
        if($base->s_tarifnpotkptahunajb1 != 'undefined')
            $where->literal("$this->table.s_tarifnpotkptahunajb1::text LIKE '%$base->s_tarifnpotkptahunajb1%'");     
//        if($base->s_tarifnpotkptahunajb2 != 'undefined')
//            $where->literal("$this->table.s_tarifnpotkptahunajb2::text LIKE '%$base->s_tarifnpotkptahunajb2%'");
        $select->where($where);
        $select->order("s_idjenistransaksi ASC");
        $select->limit($base->rows = (int) $base->rows);
        $select->offset($offset = (int) $offset);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res;
    }

    public function getDataId($id) {
        $rowset = $this->select(array('s_idtarifnpotkp' => $id));
        $row = $rowset->current();
        return $row;
    }

    public function hapusData($id) {
        $this->delete(array('s_idtarifnpotkp' => $id));
    }

    public function comboBox() {
        $resultSet = $this->select();
        return $resultSet;
    }

}
