<?php

namespace Bphtb\Helper\Spop;

class ReffSpopHelper
{
    const JENIS_TANAH_BANGUNAN = ["1", "4"];

    // LOOK UP GROUP
    const JENIS_PEKERJAAN = "08";
    const JENIS_KEPEMILIKAN = "10";
    const JENIS_BUMI = "20";
    const JENIS_KONDISI_UMUM = "21";
    const JENIS_KONSTRUKSI = "22";
    const JENIS_TRANSAKSI_LSPOP = "33";
    const JENIS_KOLAM_RENANG = "39";
    const JENIS_BAHAN_PAGAR = "40";
    const JENIS_ATAP = "41";
    const JENIS_DINDING = "42";
    const JENIS_LANTAI = "43";
    const JENIS_LANGIT = "44";

    // FASILITAS
    const AC_SPLIT = "01";
    const AC_WINDOW = "02";
    const AC_CENTRAL_KANTOR = "03";
    const AC_CENTRAL_KAMAR_HOTEL = "04";
    const AC_CENTRAL_PERTOKOAN = "06";
    const AC_CENTRAL_KAMAR_RUMAH_SAKIT = "07";
    const AC_CENTRAL_APARTEMEN = "09";
    const AC_CENTRAL_BANGUNAN_LAIN = "11";
    const KOLAM_RENANG_DIPLESTER = "12";
    const KOLAM_RENANG_DENGAN_PELAPIS = "13";
    const PERKERASAN_KONSTRUKSI_RINGAN = "14";
    const PERKERASAN_KONSTRUKSI_SEDANG = "15";
    const PERKERASAN_KONSTRUKSI_BERAT = "16";
    const PAGAR_BAJA_BESI = "35";
    const PAGAR_BATA_BATAKO = "36";
    const GENSET = "40";
    const PABX = "41";
    const SUMUR_ARTESIS = "42";
    const LISTRIK = "44";

    // JENIS PELAYANAN
    const PELAYANAN_OP_BARU = 1;
    const PELAYANAN_MUTASI_PENUH = 2;
    const PELAYANAN_MUTASI_SEBAGIAN = 3;
    const PELAYANAN_PEMBETULAN = 4;
    const PELAYANAN_PENGAHPUSAN = 5;
}
