<?php

namespace Bphtb\Model\Pendataan;

use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Sql;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Where;
use Zend\Debug\Debug;

class SSPDBphtbTable extends AbstractTableGateway
{

    protected $table = 't_detailsptbphtb';
    protected $adapter2;

    public function __construct(Adapter $adapter, $adapter3)
    {
        $this->adapter = $adapter;
        $this->adapter2 = $adapter3;
        $this->resultSetPrototype = new ResultSet();
        $this->resultSetPrototype->setArrayObjectPrototype(new SSPDBphtbBase());
        $this->initialize();
    }


    //==================== datagrid daftar pendaftaran
    public function getjumlahdata($sTable, $count, $sWhere)
    {
        $sql = "SELECT " . $count . " FROM " . $sTable . "" . $sWhere;
        $statement = $this->adapter->query($sql);
        $res = $statement->execute()->count();
        return $res;
    }

    public function getCountSelect($select)
    {
        $sql = new Sql($this->adapter);
        $res = $sql->prepareStatementForSqlObject($select)->execute()->count();
        return $res;
    }

    public function semuadatapendaftaran($sTable, $count, $input, $order_default, $order_default_thn, $aColumns, $session, $cekurl)
    {
        $aOrderingRules = array();
        $sLimit = "";
        $sOffset = "";
        if ($input->getPost('iDisplayStart') && $input->getPost('iDisplayLength') != '-1') {
            // $sLimit = " LIMIT " . intval($input->getPost('iDisplayLength')) . " OFFSET " . intval($input->getPost('iDisplayStart'));
            $sLimit = intval($input->getPost('iDisplayLength'));
            $sOffset = intval($input->getPost('iDisplayStart'));
            $no = 1 + intval($input->getPost('iDisplayStart'));
        } else {
            if (intval($input->getPost('iDisplayLength')) >= 1) {
                // $sLimit = " LIMIT " . intval($input->getPost('iDisplayLength')) . " OFFSET " . intval($input->getPost('iDisplayStart'));
                $sLimit = intval($input->getPost('iDisplayLength'));
                $sOffset = intval($input->getPost('iDisplayStart'));
                $no = 1 + intval($input->getPost('iDisplayStart'));
            } else {
                // $sLimit = " LIMIT 10 OFFSET 0";
                $sLimit = 10;
                $sOffset = 0;
                $no = 1;
            }
        }


        $aOrderingRules = array();
        if ($input->getPost('iSortCol_0')) {
            $iSortingCols = intval($input->getPost('iSortingCols'));
            for ($i = 0; $i < $iSortingCols; $i++) {
                if ($input->getPost('bSortable_' . intval($input->getPost('iSortCol_' . $i))) == 'true') {
                    $aOrderingRules[] = " " . $aColumns[intval($input->getPost('iSortCol_' . $i))] . "  "
                        . ($input->getPost('sSortDir_' . $i) === 'asc' ? 'asc' : 'desc');
                }
            }
        }

        if (!empty($aOrderingRules)) {
            // $sOrder = " ORDER BY " . implode(", ", $aOrderingRules);
            $sOrder = implode(", ", $aOrderingRules);
        } else {
            // $sOrder = " ORDER BY " . $order_default_thn . " , " . $order_default . "";
            $sOrder = $order_default_thn . " , " . $order_default . "";
        }

        $iColumnCount = count($aColumns);
        $aFilteringRules = array();
        if ($input->getPost('sSearch') && $input->getPost('sSearch') != "") {
            for ($i = 0; $i < $iColumnCount; $i++) {
                if ($input->getPost('bSearchable_' . $i) && $input->getPost('bSearchable_' . $i) == 'true') {
                    $tanggal = explode('-', $input->getPost('sSearch'));
                    if (count($tanggal) > 1) {
                        if (count($tanggal) > 2) {
                            $tanggalcari = "" . $tanggal[2] . "-" . $tanggal[1] . "-" . $tanggal[0] . "";
                            $aFilteringRules[] = " " . $aColumns[$i] . "::text  ILIKE '%" . $tanggalcari . "%'";
                        } else {
                            $tanggalcari = "" . $tanggal[1] . "-" . $tanggal[0] . "";
                            $aFilteringRules[] = " " . $aColumns[$i] . "::text  ILIKE '%" . $tanggalcari . "%'";
                        }
                    } elseif ($aColumns[$i] == 'jml_pajak_v1') {
                        $aFilteringRules[] = "(
                            CASE
                                WHEN f.p_idpemeriksaan IS NOT NULL THEN f.p_totalspt
                                ELSE a.t_totalspt
                            END 
                        )::text  = '" . $input->getPost('sSearch') . "'";
                    } elseif ($aColumns[$i] == 'status_pendaftaran') {
                        $aFilteringRules[] = "(
                            CASE
                                WHEN (length(translate(a.t_persyaratan::text, '[\"]1234567890'::text, ''::text)) + 1) = j.jmlsyarat THEN 1
                                ELSE 2
                            END
                        )::text  = '" . $input->getPost('sSearch') . "'";
                    } elseif ($aColumns[$i] == 'status_validasi') {
                        $aFilteringRules[] = "(
                            CASE
                                WHEN (length(translate(e.t_verifikasispt::text, '[\"]1234567890'::text, ''::text)) + 1) = j.jmlsyarat AND a.t_kohirketetapanspt IS NOT NULL THEN 1
                                WHEN (length(translate(e.t_verifikasispt::text, '[\"]1234567890'::text, ''::text)) + 1) = j.jmlsyarat THEN 4
                                WHEN (length(translate(e.t_verifikasispt::text, '[\"]1234567890'::text, ''::text)) + 1) IS NULL THEN 3
                                ELSE 2
                            END
                        )::text  = '" . $input->getPost('sSearch') . "'";
                    } else {
                        $aFilteringRules[] = " " . $aColumns[$i] . "::text  ILIKE '%" . $input->getPost('sSearch') . "%'";
                    }
                }
            }
            if (!empty($aFilteringRules)) {
                $aFilteringRules = array('(' . implode(" OR ", $aFilteringRules) . ')');
            }
        }

        for ($i = 0; $i < $iColumnCount; $i++) {
            if ($input->getPost('bSearchable_' . $i) && $input->getPost('bSearchable_' . $i) == 'true' && $input->getPost('sSearch_' . $i) != '') {
                $tanggal = explode('-', $input->getPost('sSearch_' . $i));

                if (count($tanggal) > 1) {
                    if (count($tanggal) > 2) {
                        $tanggalcari = "" . $tanggal[2] . "-" . $tanggal[1] . "-" . $tanggal[0] . "";
                        $aFilteringRules[] = " " . $aColumns[$i] . "::text  ILIKE '%" . $tanggalcari . "%'";
                    } else {
                        $tanggalcari = "" . $tanggal[1] . "-" . $tanggal[0] . "";
                        $aFilteringRules[] = " " . $aColumns[$i] . "::text  ILIKE '%" . $tanggalcari . "%'";
                    }
                } else {
                    if ($aColumns[$i] == 'c.s_idjenistransaksi') {
                        $aFilteringRules[] = " " . $aColumns[$i] . "::text  = '" . $input->getPost('sSearch_' . $i) . "'";
                    } elseif ($aColumns[$i] == 't_idnotarisspt') {
                        $aFilteringRules[] = " " . $aColumns[$i] . "::text  = '" . $input->getPost('sSearch_' . $i) . "'";
                    } elseif ($aColumns[$i] == 't_statusbayarspt') {
                        $aFilteringRules[] = "(
                            CASE
                                WHEN e.t_statusbayarspt::text = 'true'::text THEN 'TRUE'::text
                                ELSE 'FALSE'::text
                            END 
                        )::text  = '" . $input->getPost('sSearch_' . $i) . "'";
                    } elseif ($aColumns[$i] == 'status_pendaftaran') {
                        $aFilteringRules[] = "(
                            CASE
                                WHEN (length(translate(a.t_persyaratan::text, '[\"]1234567890'::text, ''::text)) + 1) = j.jmlsyarat THEN 1
                                ELSE 2
                            END
                        )::text  = '" . $input->getPost('sSearch_' . $i) . "'";
                    } elseif ($aColumns[$i] == 'status_validasi') {
                        $aFilteringRules[] = "(
                            CASE
                                WHEN (length(translate(e.t_verifikasispt::text, '[\"]1234567890'::text, ''::text)) + 1) = j.jmlsyarat AND a.t_kohirketetapanspt IS NOT NULL THEN 1
                                WHEN (length(translate(e.t_verifikasispt::text, '[\"]1234567890'::text, ''::text)) + 1) = j.jmlsyarat THEN 4
                                WHEN (length(translate(e.t_verifikasispt::text, '[\"]1234567890'::text, ''::text)) + 1) IS NULL THEN 3
                                ELSE 2
                            END
                        )::text  = '" . $input->getPost('sSearch_' . $i) . "'";
                    } elseif ($aColumns[$i] == 'jml_pajak_v1') {
                        $aFilteringRules[] = "(
                            CASE
                                WHEN f.p_idpemeriksaan IS NOT NULL THEN f.p_totalspt
                                ELSE a.t_totalspt
                            END 
                        )::text  = '" . $input->getPost('sSearch_' . $i) . "'";
                    } else {
                        $aFilteringRules[] = " " . $aColumns[$i] . "::text  ILIKE '%" . $input->getPost('sSearch_' . $i) . "%'";
                    }
                }
            }
        }

        $s_iduser = $session['s_iduser'];
        $s_tipe_pejabat = $session['s_tipe_pejabat'];

        $sWhere = "";
        if (!empty($aFilteringRules)) {
            $sWhere = implode(" AND ", $aFilteringRules);
        }

        // STATUS VALIDASI 
        // 1 : SELESAI
        // 2 : TIDAK LENGKAP
        // 3 : BELUM
        // 4 : SUDAH BERKAS MENUNGGU VALIDASI

        $sql = new Sql($this->adapter);
        $select = $sql->select(["a" => "t_spt"]);
        $select->columns([
            "t_idspt", "fr_tervalidasidua", "t_kohirspt", "t_kohirketetapanspt",
            't_nopbphtbsppt', "t_konfirm_espop",
            "t_tglprosesspt", "t_totalspt", "t_idsptsebelumnya",
            "jml_syarat_input" => new Expression("(length(translate(a.t_persyaratan::text, '[\"]1234567890'::text, ''::text)) + 1)"),
            "jml_syarat_validasi" => new Expression("(length(translate(e.t_verifikasispt::text, '[\"]1234567890'::text, ''::text)) + 1)"),
            "status_pendaftaran" => new Expression("(
                CASE
                    WHEN (length(translate(a.t_persyaratan::text, '[\"]1234567890'::text, ''::text)) + 1) = j.jmlsyarat THEN 1
                    ELSE 2
                END
            )"),
            "status_validasi" => new Expression("(
                CASE
                    WHEN (length(translate(e.t_verifikasispt::text, '[\"]1234567890'::text, ''::text)) + 1) = j.jmlsyarat AND a.t_kohirketetapanspt IS NOT NULL THEN 1
                    WHEN (length(translate(e.t_verifikasispt::text, '[\"]1234567890'::text, ''::text)) + 1) = j.jmlsyarat THEN 4
                    WHEN (length(translate(e.t_verifikasispt::text, '[\"]1234567890'::text, ''::text)) + 1) IS NULL THEN 3
                    ELSE 2
                END
            )"),
            "status_upload" => new Expression("(
                CASE
                    WHEN (length(translate(a.t_persyaratan_sudah_upload::text, '[\"]1234567890'::text, ''::text)) + 1) = j.jmlsyarat THEN 1
                    ELSE 2
                END
            )"),
            "t_statusbayarspt" => new Expression("(
                CASE
                    WHEN e.t_statusbayarspt::text = 'true'::text THEN 'TRUE'::text
                    ELSE 'FALSE'::text
                END 
            )"),
            "jml_pajak_v1" => new Expression("(
                CASE
                    WHEN f.p_idpemeriksaan IS NOT NULL THEN f.p_totalspt
                    ELSE a.t_totalspt
                END 
            )"),
            "jml_pajak_v2" => new Expression("(
                CASE
                    WHEN f.p_idpemeriksaan IS NOT NULL THEN
                    CASE
                        WHEN a.t_totalspt < f.p_totalspt THEN f.p_totalspt
                        ELSE a.t_totalspt
                    END
                    ELSE a.t_totalspt
                END
            )"), "fr_tervalidasidua",
            "pengajuan_spop" => new Expression("(SELECT COUNT(*) FROM t_spt_spop WHERE t_idspt = a.t_idspt)")
        ]);
        $select->join(["b" => "t_detailsptbphtb"], "b.t_idspt = a.t_idspt", [
            "fr_validasidua", "t_inputbpn", "t_namawppembeli"
        ], "LEFT");
        $select->join(["c" => "s_jenistransaksi"], "c.s_idjenistransaksi = a.t_idjenistransaksi", [
            "s_namajenistransaksi", "s_idjenistransaksi"
        ], "LEFT");
        $select->join(["e" => "t_pembayaranspt"], "e.t_idspt = a.t_idspt", [
            "ntpd" => "t_kodebayarbanksppt", "t_kodebayarbanksppt", "t_verifikasispt",
            "t_ket_verifikasi_berkas"
        ], "LEFT");
        $select->join(["f" => "t_pemeriksaan"], "f.p_idpembayaranspt = e.t_idpembayaranspt", [
            "p_idpemeriksaan"
        ], "LEFT");
        $select->join(["h" => "s_users"], "h.s_iduser = a.t_idnotarisspt", [], "LEFT");
        $select->join(["i" => "s_notaris"], new Expression("i.s_idnotaris::text = h.s_idpejabat_idnotaris::text"), [
            "s_namanotaris"
        ], "LEFT");
        $select->join(["j" => "fr_count_s_persyaratan"], "a.t_idjenistransaksi = j.s_idjenistransaksi", [
            "jml_syarat_sebenarnya" => "jmlsyarat"
        ], "LEFT");
        $select->join(["k" => "t_spt_ptsl"], "a.t_idspt = k.t_idspt", [], "LEFT");

        $where = new Where();
        if ($s_tipe_pejabat == 2) {
            $where->equalTo("a.t_idnotarisspt", $s_iduser);
        }
        // var_dump($sWhere);exit();
        if ($sWhere != "") {
            $where->literal($sWhere);
        }
        $select->where($where);
        // $totaldata = $this->getjumlahdata($sTable, $count, $sWhere);
        $totaldata = $this->getCountSelect($select);
        $iTotal = $totaldata; //$totaldata['COUNT('.$count.')'];

        $select->offset($sOffset);
        $select->limit($sLimit);
        $select->order(new Expression($sOrder));
        // die($sql->getSqlStringForSqlObject($select));

        $rResult = $sql->prepareStatementForSqlObject($select)->execute();

        $output = array(
            "sEcho" => intval($input->getPost('sEcho')),
            "iTotalRecords" => $iTotal,
            "iTotalDisplayRecords" => $iTotal,
            "aaData" => array(),
        );

        foreach ($rResult as $aRow) {
            // $row = array();
            // $spop = '<a href="files/LSPOP_SPOP.pdf"><i class="fa fa-fw fa-print"></i>  Cetak spop </a>';
            // $spop = '<a href="#" onclick="openCetakSPOP(' . $aRow['t_idspt'] . ')" ><i class="fa fa-fw fa-print"></i> SPOP</a>';
            // $ppob = '<a href="#" onclick="openCetakPPOB(' . $aRow['t_idspt'] . ');return false;"><i class="fa fa-fw fa-print"></i> Surat Pengajuan OP</a>';
            // $mutasi = '<a href="#" onclick="openCetakMutasi(' . $aRow['t_idspt'] . '); return false;" ><i class="fa fa-fw fa-print"></i>Surat Pengajuan Mutasi</a>';
            $panggildata = $this->getdatabykodebayarEspop($aRow['t_kodebayarbanksppt']);

            $edit  = ' <a href="pendataan_sspd/edit?t_idspt=' . $aRow['t_idspt'] . '"><i class="fa fa-fw fa-edit"></i> Edit</a>';
            $cetakntpd = '<a href="pendataan_sspd/cetakntpd?t_idspt=' . $aRow['t_idspt'] . '" target="_blink"><i class="fa fa-fw fa-print"></i> NTPD</a>';
            $hapus = '<a style="background-color:blue; color:#fff;" href="#" onclick="hapusall(' . $aRow['t_idspt'] . ');return false;"><i class="fa fa-fw fa-bitbucket"></i> Hapus</a>';
            // $admin_hapus = '<a style="background-color:red;color:#fff;" href="#" onclick="hapusall(' . $aRow['t_idspt'] . ');return false;"><i class="fa fa-fw fa-bitbucket"></i> Hapus Semua</a>';
            $admin_hapus = "";
            $pergantianNotaris = '<a style="background-color: greenyellow; color: black;" href="pendataan_sspd/pergantian-notaris?id=' . $aRow['t_idspt'] . '"><i class="fa fa-fw fa-edit"></i> Pergantian Notaris</a>';
            $jmlpajak = "<span style='float:right;'>" . number_format($aRow['jml_pajak_v1'], 0, ',', '.') . "</span>";

            $lihat = '<a href="pendataan_sspd/viewdata?t_idspt=' . $aRow['t_idspt'] . '"><i class="fa fa-fw fa-eye"></i> Lihat Data</a>';

            $status_upload = (($aRow['status_upload']) == 1) ? '' : '<a target="_blank" href="pendataan_sspd/uploadfilesyarat?t_idspt=' . $aRow['t_idspt'] . '" class="btn btn-danger btn-xs"><i class="fa fa-fw fa-times"></i> UPLOAD</a>';

            // input spop kalau sudah bayar
            // diubah ke waktu cetak sspd
            // untuk tes admin
            $input_pengajuan_spop = '';
            if (!$panggildata) {
                // if (($session['s_namauserrole'] == "Administrator")) {
                // $input_pengajuan_spop = '<a href="pendataan_sspd/inputSpop/' . $aRow['t_idspt'] . '"><i class="fa fa-fw fa-edit"></i> Input SPOP</a>';
                // }
            }

            if ($aRow['status_pendaftaran'] == 1) {
                $status_pendaftaran = '<img title="Syarat Pendaftaran Lengkap" width="20" height="20" src="' . $cekurl . '/public/img/syaratlengkap.png">';
            } else {
                $status_pendaftaran = '<img title="Syarat Pendaftaran Tidak Lengkap" width="20" height="20" src="' . $cekurl . '/public/img/syarattidaklengkap.png">';
            }

            //============ bisa cetak terus walaupun belum divalidasi
            if (!empty($aRow['t_idsptsebelumnya'])) {
                $cetaksspd = '<a href="#" onclick="openCetakSSPD(' . $aRow['t_idspt'] . ', ' . $aRow['t_idsptsebelumnya'] . ');return false;"><i class="fa fa-fw fa-print"></i> SSPD</a>';
            } else {
                $cetaksspd = '<a href="#" onclick="openCetakSSPD(' . $aRow['t_idspt'] . ');return false;"><i class="fa fa-fw fa-print"></i> SSPD</a>';
            }

            if ($aRow['fr_validasidua'] == 1) {
                $warnatr = '<span style="background-color:red;color: #fff;"> &nbsp; <i class="fa fa-fw fa-check-square-o"></i> </span> &nbsp;';
            } elseif ($aRow['fr_tervalidasidua'] == 1) {
                $warnatr = '<span style="background-color:red;color: #fff;"> &nbsp; <i class="fa fa-fw fa-minus-circle"></i> </span> &nbsp;';
            } elseif ($aRow['fr_tervalidasidua'] == 2) {
                $warnatr = '<span style="background-color:blue;color: #fff;"> &nbsp; <i class="fa fa-fw fa-plus-circle"></i> </span> &nbsp;';
            } else {
                $warnatr = '';
            }

            if ($aRow["status_validasi"] == 1) {
                $status_verifikasi = '<img title="Data sudah di Validasi" width="20" height="20" src="' . $cekurl . '/public/img/tervalidasi.png">';
                $cetaksspd = '<a href="#" onclick="openCetakSSPD(' . $aRow['t_idspt'] . ');return false;"><i class="fa fa-fw fa-print"></i> SSPD</a>';
                $hapus = '';
                $edit = '';
            } else if ($aRow["status_validasi"] == 4) {
                $status_verifikasi = '<img title="Berkas Selesai/Menunggu Validasi" width="20" height="20" src="' . $cekurl . '/public/img/syaratlengkap.png">';
                $cetaksspd = '<a href="#" onclick="openCetakSSPD(' . $aRow['t_idspt'] . ');return false;"><i class="fa fa-fw fa-print"></i> SSPD</a>';
                $edit = '';
                $hapus = '';
                $jmlpajak = "";
            } else if ($aRow["status_validasi"] == 3) {
                $status_verifikasi = '<img title="Belum di Validasi" width="20" height="20" src="' . $cekurl . '/public/img/belumdivalidasi.png">';
                $cetaksspd = '';
                $jmlpajak = "";
            } else {
                $status_verifikasi = '<img title="Syarat Validasi Tidak Lengkap" width="20" height="20" src="' . $cekurl . '/public/img/validasi_tidaklengkap.png"><br>' . $aRow['t_ket_verifikasi_berkas'];
                $cetaksspd = '';
                $jmlpajak = "";
                $hapus = '';
            }

            if (($session['s_namauserrole'] != "Administrator")) {
                $pergantianNotaris = "";
            }

            if ($aRow['t_statusbayarspt'] == 'TRUE') {
                $status_bayar = '<a class="btn_fr btn-success btn-sm_fr" style="cursor:default;"><i class="fa fa-fw fa-money"></i> SUDAH</a>';
                $admin_hapus = "";
                $hapus = '';
                $status_upload = '';
                if (!$panggildata) {
                    $cetaksspd = '<a href="pendataan_sspd/inputSpop/' . $aRow['t_idspt'] . '"><i class="fa fa-fw fa-edit"></i> SSPD</a>';
                }
            } else {
                $status_bayar = '<a class="btn_fr btn-danger btn-sm_fr" style="cursor:default;"><i class="fa fa-fw fa-money"></i> BELUM</a>';
                $cetakntpd = '';
                // $ppob = '';
                // $spop = '';
                // $mutasi = '';
            }

            $uploadfile = '<a target="_blank" href="pendataan_sspd/uploadfilesyarat?t_idspt=' . $aRow['t_idspt'] . '"><i class="fa fa-fw fa-upload"></i> Upload File</a>';
            // Debug::dump($input_pengajuan_spop);
            // exit;

            $btn = '
                <div class="dropdown">
                    <button onclick="myFunction(' . $aRow['t_idspt'] . ')" class="dropbtn btn-info dropdown-toggle btn-sm_fr">&nbsp;&nbsp;&nbsp;<span class="caret"></span>&nbsp;&nbsp;&nbsp;</button>
                    <div id="myDropdown' . $aRow['t_idspt'] . '" class="dropdown-content dropdown-menu" style="left:-120px; border-color: blue;">
                                                ' . $cetaksspd . '
                                                ' . $cetakntpd . '
                                                ' . $lihat . '
                                                ' . $edit . '                                           
                                                ' . $uploadfile . '
                                                ' . $hapus . ' 
                                                ' . $admin_hapus . ' 
                                                ' . $pergantianNotaris . '
                                                ' . $input_pengajuan_spop . '
                                                
                    </div>
                  </div>';
            // ' . $spop . '
            // ' . $ppob . '
            // ' . $mutasi . '


            if ($aRow['t_inputbpn'] == true) {
                $t_kohirspt = '<span class="badge" style="background-color:#CC0000;"><a href="pendataan_sspd/viewdata?t_idspt=' . $aRow['t_idspt'] . '">' . $aRow['t_kohirspt'] . '</a></span>';
            } else {
                $t_kohirspt = '<a href="pendataan_sspd/viewdata?t_idspt=' . $aRow['t_idspt'] . '">' . $aRow['t_kohirspt'] . '</a>';
            }

            if (!empty($aRow['t_kohirketetapanspt'])) {
                $novalidasi = $aRow['t_kohirketetapanspt'];
            } else {
                $novalidasi = '';
            }

            $s_tipe_pejabat = $session['s_tipe_pejabat'];

            // pengajuan spop
            $pengajuan_spop = '<button type="button" class="btn btn-xs btn-danger"><i class="fa fa-times"></i> TIDAK</button>';
            if ($panggildata) {
                $pengajuan_spop = '<button type="button" class="btn btn-xs btn-success"><i class="fa fa-check"></i> YA</button><a class="btn btn-xs btn-success" href="#" onclick="openCetakTT(' . $panggildata['t_id_pelayanan'] . ');return false;"><i class="fa fa-fw fa-print"></i> Tanda Terima</a>';
            }

            if ($s_tipe_pejabat == 2) {
                $row = array(
                    "<center>" . $no . "</center>",
                    "<center>" . $warnatr . " " . $t_kohirspt . "<br>" . $status_upload . "</center>",
                    "<center>" . $novalidasi . "</center>",
                    "<center>" . $aRow['s_namajenistransaksi'] . "</center>",
                    "<span style='float:right;'>" . date('d-m-Y', strtotime($aRow['t_tglprosesspt'])) . "</span>",
                    $aRow['t_namawppembeli'],
                    "<span style='float:right;'>" . number_format($aRow['t_totalspt'], 0, ',', '.') . "</span>",
                    "<span style='float:right;'>" . $jmlpajak . "</span>",
                    "<center>" . $status_pendaftaran . "</center>",
                    "<center>" . $status_verifikasi . "</center>",
                    "<center>" . $status_bayar . "</center>",
                    "<center>" . $btn . "</center>",
                    "<center>" . $aRow["t_kodebayarbanksppt"] . "</center>",
                    "<center>" . $pengajuan_spop . "</center>",
                    "<center>" . $aRow['t_nopbphtbsppt'] . "</center>",
                );
            } else {
                $row = array(
                    "<center>" . $no . "</center>",
                    "<center>" . $warnatr . " " . $t_kohirspt . "<br>" . $status_upload . "</center>",
                    "<center>" . $novalidasi . "</center>",
                    "<center>" . $aRow['s_namajenistransaksi'] . "</center>",
                    $aRow['s_namanotaris'],
                    "<span style='float:right;'>" . date('d-m-Y', strtotime($aRow['t_tglprosesspt'])) . "</span>",
                    $aRow['t_namawppembeli'],
                    "<span style='float:right;'>" . number_format($aRow['t_totalspt'], 0, ',', '.') . "</span>",
                    "<span style='float:right;'>" . $jmlpajak . "</span>",
                    "<center>" . $status_pendaftaran . "</center>",
                    "<center>" . $status_verifikasi . "</center>",

                    "<center>" . $status_bayar . "</center>",
                    "<center>" . $btn . "</center>",
                    "<center>" . $aRow["t_kodebayarbanksppt"] . "</center>",
                    "<center>" . $pengajuan_spop . "</center>",
                    "<center>" . $aRow['t_nopbphtbsppt'] . "</center>",
                );
            }
            $output['aaData'][] = $row;
            $no++;
        }

        return $output;
    }
    //==================== end datagrid daftar pendaftaran

    public function getdatabykodebayarEspop($kodebayar)
    {
        $sql2 = "select t_id_pelayanan from t_pelayanan where t_kode_bayar_bphtb = '" . $kodebayar . "' limit 1";

        $opp = $this->adapter2->query($sql2)->execute()->current();
        return $opp;
    }
    public function getDataId($id)
    {
        $rowset = $this->select(function (Select $select) use ($id) {
            $select->where(array(
                't_spt.t_idspt' => $id
            ));
            $select->join('t_spt', 't_detailsptbphtb.t_idspt = t_spt.t_idspt');
            $select->join('s_jenistransaksi', 's_jenistransaksi.s_idjenistransaksi = t_spt.t_idjenistransaksi');
            $select->join('s_jenishaktanah', 's_jenishaktanah.s_idhaktanah = t_spt.t_idjenishaktanah');
        });
        $row = $rowset->current();
        return $row;
    }

    public function viewKelurahan($string = null)
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->columns([
            "s_idkecamatan", "s_kodekecamatan", "s_namakecamatan"
        ]);
        $select->from(["a" => "s_kecamatan"]);
        $select->join(["b" => "s_kelurahan"], "a.s_idkecamatan = b.s_idkecamatan", [
            "s_idkelurahan", "s_kodekelurahan", "s_namakelurahan"
        ], "LEFT");
        if ($string != null) {
            return $select->getSqlString();
        } else {
            return $select;
        }
    }

    public function getDataId_all($id)
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        // $select->columns(array(
        //     "*", "ntpd" => "t_kodebayarbanksppt"
        // ));
        // $select->from('view_sspd_pembayaran');
        $select->columns([
            "t_idspt", "t_tglprosesspt", "t_periodespt", "t_nopbphtbsppt", "t_totalspt",
            "t_nilaitransaksispt", "t_kohirspt", "t_thnsppt", "t_idnotarisspt",
            "t_idsptsebelumnya", "t_kohirketetapanspt", "t_tarif_pembagian_aphb_kali",
            "t_tarif_pembagian_aphb_bagi", "t_idtarifbphtb", "t_persenbphtb",
            "t_tgljatuhtempospt", "t_dasarspt", "t_idjenisdoktanah",
            "t_potongan_waris_hibahwasiat", "t_persyaratan", "t_idjenistransaksi",
            "t_potonganspt", "t_approve_pengurangpembebas",
            "t_nilaipembayaranspt_sebelumnya" => new Expression("(
                CASE WHEN a.t_idsptsebelumnya IS NOT NULL THEN (SELECT x.t_nilaipembayaranspt FROM t_pembayaranspt x WHERE x.t_idspt = a.t_idsptsebelumnya LIMIT 1)
                ELSE NULL END
            )"),
            "status_validasi" => new Expression("(
                CASE
                    WHEN (length(translate(e.t_verifikasispt::text, '[\"]1234567890'::text, ''::text)) + 1) = j.jmlsyarat THEN 1
                    WHEN (length(translate(e.t_verifikasispt::text, '[\"]1234567890'::text, ''::text)) + 1) IS NULL THEN 3
                    ELSE 2
                END
            )"),
        ]);
        $select->from(["a" => "t_spt"]);
        $select->join(["b" => "t_detailsptbphtb"], "a.t_idspt = b.t_idspt", [
            "t_namawppembeli", "t_nikwppembeli", "t_alamatwppembeli", "t_kecamatanwppembeli",
            "t_kelurahanwppembeli", "t_kabkotawppembeli", "t_telponwppembeli",
            "t_kodeposwppembeli", "t_npwpwppembeli", "t_namawppenjual",
            "t_nikwppenjual", "t_alamatwppenjual", "t_kecamatanwppenjual",
            "t_kelurahanwppenjual", "t_kabkotawppenjual", "t_telponwppenjual",
            "t_kodeposwppenjual", "t_npwpwppenjual", "t_luastanah", "t_njoptanah",
            "t_luasbangunan", "t_njopbangunan", "t_totalnjoptanah", "t_totalnjopbangunan",
            "t_grandtotalnjop", "t_nosertifikathaktanah", "t_iddetailsptbphtb",
            "t_kelurahanop", "t_kecamatanop", "t_kabupatenop", "t_rtwppembeli",
            "t_rwwppembeli", "t_alamatop", "t_rtop", "t_rwop", "t_rtwppenjual",
            "t_rwwppenjual", "t_inputbpn", "t_noajbbaru", "t_tglajb", "t_tglajbbaru",
            "t_grandtotalnjop_aphb", "t_namasppt", 't_emailwppembeli', 't_emailwppenjual'
        ], "LEFT");
        $select->join(["c" => "s_jenistransaksi"], "c.s_idjenistransaksi = a.t_idjenistransaksi", [
            "s_namajenistransaksi"
        ], "LEFT");
        $select->join(["d" => "s_jenishaktanah"], "d.s_idhaktanah = a.t_idjenishaktanah", [
            "s_namahaktanah"
        ], "LEFT");
        $select->join(["e" => "t_pembayaranspt"], "e.t_idspt = a.t_idspt", [
            "t_idpembayaranspt", "t_kohirpembayaran", "t_periodepembayaran",
            "t_tanggalpembayaran", "t_idnotaris", "t_nilaipembayaranspt",
            "t_kodebayarspt", "t_tglverifikasispt", "t_verifikasispt",
            "t_tglverifikasiberkas", "t_verifikasiberkas", "t_pejabatverifikasispt",
            "t_statusbayarspt", "t_ketetapanspt", "t_kodebayarbanksppt",
            "ntpd" => "t_kodebayarbanksppt", "t_ket_verifikasi_berkas"
        ], "LEFT");
        $select->join(["f" => "t_pemeriksaan"], "f.p_idpembayaranspt = e.t_idpembayaranspt", [
            "p_idpemeriksaan", "p_idpembayaranspt", "p_luastanah", "p_luasbangunan",
            "p_njoptanah", "p_njopbangunan", "p_totalnjoptanah", "p_totalnjopbangunan",
            "p_grandtotalnjop", "p_nilaitransaksispt", "p_potonganspt",
            "p_totalspt", "p_grandtotalnjop_aphb"
        ], "LEFT");
        $select->join(["g" => "s_users"], "g.s_iduser = a.t_idnotarisspt", [], "LEFT");
        $select->join(["h" => "s_notaris"], new Expression("h.s_idnotaris::text = g.s_idpejabat_idnotaris::text"), [
            "s_namanotaris", "s_idnotaris"
        ], "LEFT");
        $select->join(["i" => "s_users"], new Expression("i.s_iduser = e.t_pejabatverifikasispt"), [], "LEFT");
        $select->join(["j" => "fr_count_s_persyaratan"], "a.t_idjenistransaksi = j.s_idjenistransaksi", [
            "jml_syarat_sebenarnya" => "jmlsyarat"
        ], "LEFT");
        $select->join(["jj" => "s_pejabat"], new Expression("jj.s_idpejabat::text = i.s_idpejabat_idnotaris::text"), [
            "namapejabatverifikasi" => "s_namapejabat"
        ], "LEFT");
        $select->join(["l" => $this->viewKelurahan()], new Expression("substring(a.t_nopbphtbsppt::text, 7, 7) = (l.s_kodekecamatan::text || '.'::text || l.s_kodekelurahan::text)"), [
            "s_kodekecamatan", "s_namakecamatan", "s_kodekelurahan", "s_namakelurahan",
        ], "LEFT");
        $select->join(["p" => "s_jenisdoktanah"], new Expression("a.t_idjenisdoktanah = p.s_iddoktanah"), [
            "s_namadoktanah"
        ], "LEFT");
        $select->join(["q" => "t_spt_ptsl"], new Expression("a.t_idspt = q.t_idspt"), [
            "t_id_ptsl", "t_tgl_ptsl", "t_ket_ptsl"
        ], "LEFT");
        $select->join(["r" => "t_pengurangan"], "a.t_idspt = r.t_idsptpengurangan", [
            "t_totalsptsebelumnya", "t_persenpengurangan", "t_pengurangan", "t_totalpajak"
        ], "LEFT");
        $where = new Where();
        $where->equalTo('a.t_idspt', $id);
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute()->current();
        return $res;
    }


    /*
    public function getDataId($id)
    {
        $rowset = $this->select(function (Select $select) use($id) {
            $select->where(array(
                't_spt.t_idspt' => $id
            ));
            $select->join('t_spt', 't_detailsptbphtb.t_idspt = t_spt.t_idspt');
        });
        $row = $rowset->current();
        return $row;
    }*/

    public function getGridCount(SSPDBphtbBase $base, $id_notaris, $userRole = null)
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from('view_sspd_pembayaran');
        $where = new Where();
        if ($userRole == "Notaris") {
            $where->equalTo('t_idnotarisspt', $id_notaris);
        }
        $kohir = (int) $base->t_kohirspt;
        if ($base->t_kohirspt != 'undefined')
            $where->literal("t_kohirspt::text LIKE '%$kohir%'");
        if ($base->t_periodespt != 'undefined')
            $where->equalTo("t_periodespt", $base->t_periodespt);
        if ($base->t_tglprosesspt != 'undefined')
            $where->equalTo("t_tglprosesspt", date('Y-m-d', strtotime($base->t_tglprosesspt)));
        if ($base->t_namawppembeli != 'undefined')
            $where->literal("t_namawppembeli::text LIKE '%$base->t_namawppembeli%'");
        if ($base->t_totalspt != 'undefined')
            $where->literal("t_totalspt::text LIKE '%$base->t_totalspt%'");
        if ($base->s_namajenistransaksi != 'undefined')
            $where->literal("s_namajenistransaksi::text LIKE '%$base->s_namajenistransaksi%'");

        if ($base->t_statusbayarspt == 'FALSE') {
            $where->isNull("t_statusbayarspt");
        } else {
            if ($base->t_statusbayarspt != 'undefined')
                $where->equalTo("t_statusbayarspt", $base->t_statusbayarspt);
        }
        //        if ($base->t_noajbbaru != 'undefined')
        //            $where->literal("t_noajbbaru::text LIKE '%$base->t_noajbbaru%'");
        //        if ($base->t_tglajbbaru != 'undefined')
        //            $where->equalTo("t_tglajbbaru", date('Y-m-d', strtotime($base->t_tglajbbaru)));
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res->count();
    }

    public function getGridCountPendataanSSPD(SSPDBphtbBase $base, $id_notaris, $s_tipe_pejabat = null)
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from('view_sspd_pembayaran');
        $where = new Where();
        if ($s_tipe_pejabat == 2) {
            $where->equalTo('t_idnotarisspt', $id_notaris);
        }
        $kohir = (int) $base->t_kohirspt;
        if ($base->t_kohirspt != 'undefined')
            $where->literal("t_kohirspt::text LIKE '%$kohir%'");
        if ($base->t_periodespt != 'undefined')
            $where->equalTo("t_periodespt", $base->t_periodespt);
        if ($base->t_tglprosesspt != 'undefined')
            $where->equalTo("t_tglprosesspt", date('Y-m-d', strtotime($base->t_tglprosesspt)));
        if ($base->t_namawppembeli != 'undefined')
            $where->literal("t_namawppembeli::text LIKE '%$base->t_namawppembeli%'");
        if ($base->t_totalspt != 'undefined')
            $where->literal("t_totalspt::text LIKE '%$base->t_totalspt%'");
        if ($base->s_namajenistransaksi != 'undefined')
            $where->literal("s_namajenistransaksi::text LIKE '%$base->s_namajenistransaksi%'");

        if ($base->t_statusbayarspt == 'FALSE') {
            $where->isNull("t_statusbayarspt");
        } else {
            if ($base->t_statusbayarspt != 'undefined')
                $where->equalTo("t_statusbayarspt", $base->t_statusbayarspt);
        }
        //        if ($base->t_noajbbaru != 'undefined')
        //            $where->literal("t_noajbbaru::text LIKE '%$base->t_noajbbaru%'");
        //        if ($base->t_tglajbbaru != 'undefined')
        //            $where->equalTo("t_tglajbbaru", date('Y-m-d', strtotime($base->t_tglajbbaru)));
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res->count();
    }


    public function databelumverifikasiBphtb($sortname, $sortorder, $query, $qtype, $start, $rp)
    {
        if ($query) {
            $where = " WHERE $qtype::text LIKE '%" . $query . "%' and t_ketetapanspt is null";
        } else {
            $where = " WHERE t_ketetapanspt is null";
        }
        $sort = "ORDER BY $sortname $sortorder";

        //$limit = "offset $start limit $rp ";
        $limit = "limit $rp offset $start";



        $sql = "select * from view_sspd_blm_validasi " . @$where . " " . @$sort . " " . @$limit . "  "; // date_part('year', s_tanggaltarifbphtb) = ".date('Y')."";
        $statement = $this->adapter->query($sql);
        return $statement->execute();
    }

    public function getGridDataPendataanSSPD(SSPDBphtbBase $base, $offset, $id_notaris, $s_tipe_pejabat = null)
    {
        // Tambahkan field s_iduser pada view_sspd_semua_pembayaran dari join table user
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from('view_sspd_pembayaran');
        $where = new Where();
        if ($s_tipe_pejabat == 2) {
            $where->equalTo('t_idnotarisspt', $id_notaris);
        }
        $kohir = (int) $base->t_kohirspt;
        $tanggal = date('Y-m-d', strtotime($base->t_tglprosesspt));
        if ($base->t_kohirspt != 'undefined')
            $where->literal("t_kohirspt::text LIKE '%$kohir%'");
        if ($base->t_periodespt != 'undefined')
            $where->equalTo("t_periodespt", $base->t_periodespt);
        if ($base->t_tglprosesspt != 'undefined')
            $where->equalTo("t_tglprosesspt", $tanggal);
        if ($base->t_namawppembeli != 'undefined')
            $where->literal("t_namawppembeli::text LIKE '%$base->t_namawppembeli%'");
        if ($base->t_totalspt != 'undefined')
            $where->literal("t_totalspt::text LIKE '%$base->t_totalspt%'");
        if ($base->s_namajenistransaksi != 'undefined')
            $where->literal("s_namajenistransaksi::text LIKE '%$base->s_namajenistransaksi%'");

        if ($base->t_statusbayarspt == 'FALSE') {
            $where->isNull("t_statusbayarspt");
        } else {
            if ($base->t_statusbayarspt != 'undefined')
                $where->equalTo("t_statusbayarspt", $base->t_statusbayarspt);
        }
        //        if ($base->t_noajbbaru != 'undefined')
        //            $where->literal("t_noajbbaru::text LIKE '%$base->t_noajbbaru%'");
        //        if ($base->t_tglajbbaru != 'undefined')
        //            $where->equalTo("t_tglajbbaru", date('Y-m-d', strtotime($base->t_tglajbbaru)));
        $select->where($where);
        $select->order("t_kohirspt DESC");
        $select->limit($base->rows = (int) $base->rows);
        $select->offset($offset = (int) $offset);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res;
    }

    public function getGridData(SSPDBphtbBase $base, $offset, $id_notaris, $userRole = null)
    {
        // Tambahkan field s_iduser pada view_sspd_semua_pembayaran dari join table user
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from('view_sspd_pembayaran');
        $where = new Where();
        if ($userRole == "Notaris") {
            $where->equalTo('t_idnotarisspt', $id_notaris);
        }
        $kohir = (int) $base->t_kohirspt;
        $tanggal = date('Y-m-d', strtotime($base->t_tglprosesspt));
        if ($base->t_kohirspt != 'undefined')
            $where->literal("t_kohirspt::text LIKE '%$kohir%'");
        if ($base->t_periodespt != 'undefined')
            $where->equalTo("t_periodespt", $base->t_periodespt);
        if ($base->t_tglprosesspt != 'undefined')
            $where->equalTo("t_tglprosesspt", $tanggal);
        if ($base->t_namawppembeli != 'undefined')
            $where->literal("t_namawppembeli::text LIKE '%$base->t_namawppembeli%'");
        if ($base->t_totalspt != 'undefined')
            $where->literal("t_totalspt::text LIKE '%$base->t_totalspt%'");
        if ($base->s_namajenistransaksi != 'undefined')
            $where->literal("s_namajenistransaksi::text LIKE '%$base->s_namajenistransaksi%'");

        if ($base->t_statusbayarspt == 'FALSE') {
            $where->isNull("t_statusbayarspt");
        } else {
            if ($base->t_statusbayarspt != 'undefined')
                $where->equalTo("t_statusbayarspt", $base->t_statusbayarspt);
        }
        //        if ($base->t_noajbbaru != 'undefined')
        //            $where->literal("t_noajbbaru::text LIKE '%$base->t_noajbbaru%'");
        //        if ($base->t_tglajbbaru != 'undefined')
        //            $where->equalTo("t_tglajbbaru", date('Y-m-d', strtotime($base->t_tglajbbaru)));
        $select->where($where);
        $select->order("t_kohirspt DESC");
        $select->limit($base->rows = (int) $base->rows);
        $select->offset($offset = (int) $offset);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res;
    }

    public function getGridCountBlmVerifikasi($query, $qtype, $bulan = null)
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from('view_sspd_blm_validasi');
        $where = new Where();
        if ($qtype && $bulan == null) {
            $where->literal("$qtype::text LIKE '%$query%'");
        }
        if ($bulan != null) {
            $where->literal("EXTRACT(MONTH from t_tglprosesspt) = '" . $bulan . "'");
        }
        $where->literal('t_ketetapanspt is null');
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res->count();
    }

    public function getGridDataBlmVerifikasi($sortname, $sortorder, $query, $qtype, $start, $rp)
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from('view_sspd_blm_validasi');
        $where = new Where();
        $select->order($sortname, $sortorder);
        if ($query) {
            $select->where("$qtype::text LIKE '%$query%'");
        }
        $where->literal('t_ketetapanspt is null');
        $select->where($where);
        $select->limit($rp);
        $select->offset($start);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res;
    }

    public function getGridCountPembayaran($query, $qtype)
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from('view_sspd_sudah_tervalidasi');
        $where = new Where();
        if ($query) {
            $where->literal("$qtype::text LIKE '%$query%' and t_tanggalpembayaran IS NULL");
        } else {
            $where->literal("t_tanggalpembayaran IS NULL");
        }
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res->count();
    }

    public function getGridDataPembayaran($sortname, $sortorder, $query, $qtype, $start, $rp)
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from('view_sspd_sudah_tervalidasi');
        $where = new Where();
        if ($query) {
            $where->literal("$qtype::text LIKE '%$query%' and t_tanggalpembayaran IS NULL");
        } else {
            $where->isNull("t_tanggalpembayaran");
        }
        $select->where($where);
        $select->order($sortname, $sortorder);
        $select->limit($rp);
        $select->offset($start);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res;
    }

    public function getGridCountDS($query, $qtype)
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from('view_pembayarandenda');
        $where = new Where();
        if ($query) {
            $where->literal("$qtype::text LIKE '%$query%'");
        }
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res->count();
    }

    public function getGridDataDS($sortname, $sortorder, $query, $qtype, $start, $rp)
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from('view_pembayarandenda');
        $where = new Where();
        if ($query) {
            $where->literal("$qtype::text LIKE '%$query%'");
        }
        $select->where($where);
        $select->order($sortname, $sortorder);
        $select->limit($rp);
        $select->offset($start);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res;
    }

    public function getGridCountVerifikasi($query, $qtype)
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from('view_sspd_pembayaran');
        $where = new Where();
        if ($query) {
            $where->literal("$qtype::text LIKE '%$query%' and t_tglverifikasispt IS NULL");
        } else {
            $where->literal("t_tglverifikasispt IS NULL");
        }
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res->count();
    }

    public function getGridDataVerifikasi($sortname, $sortorder, $query, $qtype, $start, $rp)
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from('view_sspd_pembayaran');
        $where = new Where();

        if ($query) {
            $where->literal("$qtype::text LIKE '%$query%' and t_tglverifikasispt IS NULL and t_ketetapanspt=1");
        } else {
            $where->literal("t_tglverifikasispt IS NULL and t_ketetapanspt=1");
        }
        $select->where($where);
        $select->order($sortname, $sortorder);
        $select->limit($rp);
        $select->offset($start);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res;
    }

    public function savedatadetail(SSPDBphtbBase $sspddetail, $idspt = null, $session, $t_luastanah, $t_njoptanah, $t_luasbangunan, $t_njopbangunan, $t_totalnjoptanah, $t_totalnjopbangunan, $t_grandtotalnjop, $t_grandtotalnjop_aphb)
    {
        $id = (int) $sspddetail->t_iddetailsptbphtb;
        if (!empty($sspddetail->t_tglajbbaru)) {
            $tglajbbaru = date("Y-m-d", strtotime($sspddetail->t_tglajbbaru));
            $noajbaru = $sspddetail->t_noajbbaru;
        } else {
            $tglajbbaru = null;
            $noajbaru = null;
        }


        //if(!empty($dt->t_grandtotalnjop_aphb)){
        //    $t_grandtotalnjop_aphb = $dt->t_grandtotalnjop_aphb;
        //}else{
        //     $t_grandtotalnjop_aphb = 0;
        //}

        if (!empty($sspddetail->t_njoptanah_sismiop)) {
            $t_njoptanah_sismiop = $sspddetail->t_njoptanah_sismiop;
        } else {
            $t_njoptanah_sismiop = 0;
        }

        if (!empty($sspddetail->t_njopbangunan_sismiop)) {
            $t_njopbangunan_sismiop = $sspddetail->t_njopbangunan_sismiop;
        } else {
            $t_njopbangunan_sismiop = 0;
        }

        if (!empty($sspddetail->t_luastanah_sismiop)) {
            $t_luastanah_sismiop = $sspddetail->t_luastanah_sismiop;
        } else {
            $t_luastanah_sismiop = 0;
        }

        if (!empty($sspddetail->t_luasbangunan_sismiop)) {
            $t_luasbangunan_sismiop = $sspddetail->t_luasbangunan_sismiop;
        } else {
            $t_luasbangunan_sismiop = 0;
        }

        $data = array(
            't_idspt' => $idspt,
            // Data Wajib Pajak
            't_namawppembeli' => $sspddetail->t_namawppembeli,
            't_nikwppembeli' => $sspddetail->t_nikwppembeli,
            't_npwpwppembeli' => $sspddetail->t_npwpwppembeli,
            't_alamatwppembeli' => $sspddetail->t_alamatwppembeli,
            't_rtwppembeli' => $sspddetail->t_rtwppembeli,
            't_rwwppembeli' => $sspddetail->t_rwwppembeli,
            't_kecamatanwppembeli' => $sspddetail->t_kecamatanwppembeli,
            't_kelurahanwppembeli' => $sspddetail->t_kelurahanwppembeli,
            't_kabkotawppembeli' => $sspddetail->t_kabkotawppembeli,
            't_telponwppembeli' => $sspddetail->t_telponwppembeli,
            't_kodeposwppembeli' => $sspddetail->t_kodeposwppembeli,
            // Data Objek Pajak
            't_namasppt' => $sspddetail->t_namasppt,
            't_alamatop' => $sspddetail->t_alamatop,
            't_rtop' => $sspddetail->t_rtop,
            't_rwop' => $sspddetail->t_rwop,
            't_kelurahanop' => $sspddetail->t_kelurahanop,
            't_kecamatanop' => $sspddetail->t_kecamatanop,
            't_kabupatenop' => $sspddetail->t_kabupatenop,
            't_luastanah' => $t_luastanah, //str_ireplace(".", "", $sspddetail->t_luastanah),
            't_njoptanah' => $t_njoptanah, //str_ireplace(".", "", $sspddetail->t_njoptanah),
            't_totalnjoptanah' => $t_totalnjoptanah, //str_ireplace(".", "", $sspddetail->t_totalnjoptanah),
            't_luasbangunan' => $t_luasbangunan, //str_ireplace(".", "", $sspddetail->t_luasbangunan),
            't_njopbangunan' => $t_njopbangunan, //str_ireplace(".", "", $sspddetail->t_njopbangunan),
            't_totalnjopbangunan' => $t_totalnjopbangunan, //str_ireplace(".", "", $sspddetail->t_totalnjopbangunan),
            't_grandtotalnjop' => $t_grandtotalnjop, //str_ireplace(".", "", $sspddetail->t_grandtotalnjop),
            't_nosertifikathaktanah' => $sspddetail->t_nosertifikathaktanah,

            't_ketwaris' => $sspddetail->t_ketwaris,
            't_terbukti' => $sspddetail->t_terbukti,
            't_tglajb' => date('Y-m-d', strtotime($sspddetail->t_tglajb)),
            't_namasppt' => $sspddetail->t_namasppt,
            //'t_tglajbbaru' => $tglajbbaru,
            //'t_noajbbaru' => $noajbaru,
            // Data Penjual
            't_namawppenjual' => $sspddetail->t_namawppenjual,
            't_nikwppenjual' => $sspddetail->t_nikwppenjual,
            't_npwpwppenjual' => $sspddetail->t_npwpwppenjual,
            't_alamatwppenjual' => $sspddetail->t_alamatwppenjual,
            't_kelurahanwppenjual' => $sspddetail->t_kelurahanwppenjual,
            't_kecamatanwppenjual' => $sspddetail->t_kecamatanwppenjual,
            't_kabkotawppenjual' => $sspddetail->t_kabkotawppenjual,
            't_telponwppenjual' => $sspddetail->t_telponwppenjual,
            't_kodeposwppenjual' => $sspddetail->t_kodeposwppenjual,
            't_rtwppenjual' => $sspddetail->t_rtwppenjual,
            't_rwwppenjual' => $sspddetail->t_rwwppenjual,

            't_luastanah_sismiop' => $t_luastanah_sismiop, //$sspddetail->t_luastanah_sismiop,
            't_luasbangunan_sismiop' => $t_luasbangunan_sismiop, //$sspddetail->t_luasbangunan_sismiop,
            't_njoptanah_sismiop' => $t_njoptanah_sismiop, //$sspddetail->t_njoptanah_sismiop,
            't_njopbangunan_sismiop' => $t_njopbangunan_sismiop, //$sspddetail->t_njopbangunan_sismiop,
            't_grandtotalnjop_aphb' => $t_grandtotalnjop_aphb, //str_ireplace(".", "", $t_grandtotalnjop_aphb)
            't_emailwppembeli' => $sspddetail->t_emailwppembeli,
            't_emailwppenjual' => $sspddetail->t_emailwppenjual,
        );
        //$datahistlog = array(
        //    'hislog_opr_id' => $session['s_iduser'],
        //    'hislog_opr_user' => $session['s_username'] . "-" . $session['s_jabatan'],
        //    'hislog_opr_nama' => $session['s_username'],
        //    'hislog_time' => date("Y-m-d h:i:s")
        //);
        if ($id == 0) {
            $this->insert($data);
            //$datahistlog['hislog_idspt'] = $idspt;
            //$datahistlog['hislog_action'] = "SIMPAN DATA PENDAFTARAN - " . $sspddetail->t_namawppembeli . "-" . $sspddetail->t_nikwppembeli;
            //$tabel_histlog = new TableGateway('history_log', $this->adapter);
            //$tabel_histlog->insert($datahistlog);
        } else {
            $data['t_idspt'] = $sspddetail->t_idspt;
            $this->update($data, array(
                't_iddetailsptbphtb' => $sspddetail->t_iddetailsptbphtb
            ));
            //$datahistlog['hislog_idspt'] = $sspddetail->t_idspt;
            //$datahistlog['hislog_action'] = "UPDATE DATA PENDAFTARAN - " . $sspddetail->t_namawppembeli . "-" . $sspddetail->t_nikwppembeli;
            //$tabel_histlog = new TableGateway('history_log', $this->adapter);
            //$tabel_histlog->insert($datahistlog);
        }
    }


    public function savedatadetail_validasikedua(SSPDBphtbBase $sspddetail, $idspt = null, $input, $session, $t_luastanah, $t_njoptanah, $t_luasbangunan, $t_njopbangunan, $t_totalnjoptanah, $t_totalnjopbangunan, $t_grandtotalnjop, $t_grandtotalnjop_aphb)
    {
        $id = (int) $sspddetail->t_iddetailsptbphtb;
        if (!empty($sspddetail->t_tglajbbaru)) {
            $tglajbbaru = date("Y-m-d", strtotime($sspddetail->t_tglajbbaru));
            $noajbaru = $sspddetail->t_noajbbaru;
        } else {
            $tglajbbaru = null;
            $noajbaru = null;
        }


        //if(!empty($dt->t_grandtotalnjop_aphb)){
        //    $t_grandtotalnjop_aphb = $dt->t_grandtotalnjop_aphb;
        //}else{
        //     $t_grandtotalnjop_aphb = 0;
        //}

        $data = array(
            't_idspt' => $idspt,
            // Data Wajib Pajak
            't_namawppembeli' => $sspddetail->t_namawppembeli,
            't_nikwppembeli' => $sspddetail->t_nikwppembeli,
            't_npwpwppembeli' => $sspddetail->t_npwpwppembeli,
            't_alamatwppembeli' => $sspddetail->t_alamatwppembeli,
            't_rtwppembeli' => $sspddetail->t_rtwppembeli,
            't_rwwppembeli' => $sspddetail->t_rwwppembeli,
            't_kecamatanwppembeli' => $sspddetail->t_kecamatanwppembeli,
            't_kelurahanwppembeli' => $sspddetail->t_kelurahanwppembeli,
            't_kabkotawppembeli' => $sspddetail->t_kabkotawppembeli,
            't_telponwppembeli' => $sspddetail->t_telponwppembeli,
            't_kodeposwppembeli' => $sspddetail->t_kodeposwppembeli,
            // Data Objek Pajak
            't_namasppt' => $sspddetail->t_namasppt,
            't_alamatop' => $sspddetail->t_alamatop,
            't_rtop' => $sspddetail->t_rtop,
            't_rwop' => $sspddetail->t_rwop,
            't_kelurahanop' => $sspddetail->t_kelurahanop,
            't_kecamatanop' => $sspddetail->t_kecamatanop,
            't_kabupatenop' => $sspddetail->t_kabupatenop,
            't_luastanah' => $t_luastanah, //str_ireplace(".", "", $sspddetail->t_luastanah),
            't_njoptanah' => $t_njoptanah, //str_ireplace(".", "", $sspddetail->t_njoptanah),
            't_totalnjoptanah' => $t_totalnjoptanah, //str_ireplace(".", "", $sspddetail->t_totalnjoptanah),
            't_luasbangunan' => $t_luasbangunan, //str_ireplace(".", "", $sspddetail->t_luasbangunan),
            't_njopbangunan' => $t_njopbangunan, //str_ireplace(".", "", $sspddetail->t_njopbangunan),
            't_totalnjopbangunan' => $t_totalnjopbangunan, //str_ireplace(".", "", $sspddetail->t_totalnjopbangunan),
            't_grandtotalnjop' => $t_grandtotalnjop, //str_ireplace(".", "", $sspddetail->t_grandtotalnjop),
            't_nosertifikathaktanah' => $sspddetail->t_nosertifikathaktanah,

            't_ketwaris' => $sspddetail->t_ketwaris,
            't_terbukti' => $sspddetail->t_terbukti,
            't_tglajb' => date('Y-m-d', strtotime($sspddetail->t_tglajb)),

            't_tglajbbaru' => $tglajbbaru,
            't_noajbbaru' => $noajbaru,
            // Data Penjual
            't_namawppenjual' => $sspddetail->t_namawppenjual,
            't_nikwppenjual' => $sspddetail->t_nikwppenjual,
            't_npwpwppenjual' => $sspddetail->t_npwpwppenjual,
            't_alamatwppenjual' => $sspddetail->t_alamatwppenjual,
            't_kelurahanwppenjual' => $sspddetail->t_kelurahanwppenjual,
            't_kecamatanwppenjual' => $sspddetail->t_kecamatanwppenjual,
            't_kabkotawppenjual' => $sspddetail->t_kabkotawppenjual,
            't_telponwppenjual' => $sspddetail->t_telponwppenjual,
            't_kodeposwppenjual' => $sspddetail->t_kodeposwppenjual,
            't_rtwppenjual' => $sspddetail->t_rtwppenjual,
            't_rwwppenjual' => $sspddetail->t_rwwppenjual,

            't_luastanah_sismiop' => $sspddetail->t_luastanah_sismiop,
            't_luasbangunan_sismiop' => $sspddetail->t_luasbangunan_sismiop,
            't_njoptanah_sismiop' => $sspddetail->t_njoptanah_sismiop,
            't_njopbangunan_sismiop' => $sspddetail->t_njopbangunan_sismiop,
            't_grandtotalnjop_aphb' => $t_grandtotalnjop_aphb, //str_ireplace(".", "", $t_grandtotalnjop_aphb)
            'fr_validasidua' => 1
        );

        $this->insert($data);
    }


    //    public function savedatadetail(SSPDBphtbBase $sspddetail, $idspt = null) {
    //        $id = (int) $sspddetail->t_iddetailsptbphtb;
    //        $data = array(
    //            't_idspt' => $idspt,
    //            // Data Wajib Pajak
    //            't_namawppembeli' => $sspddetail->t_namawppembeli,
    //            't_nikwppembeli' => $sspddetail->t_nikwppembeli,
    //            't_npwpwppembeli' => $sspddetail->t_npwpwppembeli,
    //            't_alamatwppembeli' => $sspddetail->t_alamatwppembeli,
    //            't_rtwppembeli' => $sspddetail->t_rtwppembeli,
    //            't_rwwppembeli' => $sspddetail->t_rwwppembeli,
    //            't_kecamatanwppembeli' => $sspddetail->t_kecamatanwppembeli,
    //            't_kelurahanwppembeli' => $sspddetail->t_kelurahanwppembeli,
    //            't_kabkotawppembeli' => $sspddetail->t_kabkotawppembeli,
    //            't_telponwppembeli' => $sspddetail->t_telponwppembeli,
    //            't_kodeposwppembeli' => $sspddetail->t_kodeposwppembeli,
    //            // Data Objek Pajak
    //            't_namasppt' => $sspddetail->t_namasppt,
    //            't_alamatop' => $sspddetail->t_alamatop,
    //            't_rtop' => $sspddetail->t_rtop,
    //            't_rwop' => $sspddetail->t_rwop,
    //            't_kelurahanop' => $sspddetail->t_kelurahanop,
    //            't_kecamatanop' => $sspddetail->t_kecamatanop,
    //            't_kabupatenop' => $sspddetail->t_kabupatenop,
    //            't_luastanah' => str_ireplace(".", "", $sspddetail->t_luastanah),
    //            't_njoptanah' => str_ireplace(".", "", $sspddetail->t_njoptanah),
    //            't_totalnjoptanah' => str_ireplace(".", "", $sspddetail->t_totalnjoptanah),
    //            't_luasbangunan' => str_ireplace(".", "", $sspddetail->t_luasbangunan),
    //            't_njopbangunan' => str_ireplace(".", "", $sspddetail->t_njopbangunan),
    //            't_totalnjopbangunan' => str_ireplace(".", "", $sspddetail->t_totalnjopbangunan),
    //            't_grandtotalnjop' => str_ireplace(".", "", $sspddetail->t_grandtotalnjop),
    //            't_nosertifikathaktanah' => $sspddetail->t_nosertifikathaktanah,
    //            't_ketwaris' => $sspddetail->t_ketwaris,
    //            't_terbukti' => $sspddetail->t_terbukti,
    //            't_tglajb' => date('Y-m-d', strtotime($sspddetail->t_tglajb)),
    //            't_namasppt' => $sspddetail->t_namasppt,
    //            // Data Penjual
    //            't_namawppenjual' => $sspddetail->t_namawppenjual,
    //            't_nikwppenjual' => $sspddetail->t_nikwppenjual,
    //            't_npwpwppenjual' => $sspddetail->t_npwpwppenjual,
    //            't_alamatwppenjual' => $sspddetail->t_alamatwppenjual,
    //            't_kelurahanwppenjual' => $sspddetail->t_kelurahanwppenjual,
    //            't_kecamatanwppenjual' => $sspddetail->t_kecamatanwppenjual,
    //            't_kabkotawppenjual' => $sspddetail->t_kabkotawppenjual,
    //            't_telponwppenjual' => $sspddetail->t_telponwppenjual,
    //            't_kodeposwppenjual' => $sspddetail->t_kodeposwppenjual,
    //            't_rtwppenjual' => $sspddetail->t_rtwppenjual,
    //            't_rwwppenjual' => $sspddetail->t_rwwppenjual
    //        );
    //        if ($id == 0) {
    //            $this->insert($data);
    //        } else {
    //            $data['t_idspt'] = $sspddetail->t_idspt;
    //            $this->update($data, array(
    //                't_iddetailsptbphtb' => $sspddetail->t_iddetailsptbphtb
    //            ));
    //        }
    //    }

    public function savedatadetailbpn(SSPDBphtbBase $sspddetail, $idspt, $t_luastanahbpn, $t_luasbangunanbpn)
    {
        $data = array(
            't_idspt' => $idspt,
            // Data Wajib Pajak
            't_namawppembeli' => $sspddetail->t_namawppembeli,
            't_nikwppembeli' => $sspddetail->t_nikwppembeli,
            't_npwpwppembeli' => $sspddetail->t_npwpwppembeli,
            't_alamatwppembeli' => $sspddetail->t_alamatwppembeli,
            't_rtwppembeli' => $sspddetail->t_rtwppembeli,
            't_rwwppembeli' => $sspddetail->t_rwwppembeli,
            't_kecamatanwppembeli' => $sspddetail->t_kecamatanwppembeli,
            't_kelurahanwppembeli' => $sspddetail->t_kelurahanwppembeli,
            't_kabkotawppembeli' => $sspddetail->t_kabkotawppembeli,
            't_telponwppembeli' => $sspddetail->t_telponwppembeli,
            't_kodeposwppembeli' => $sspddetail->t_kodeposwppembeli,
            // Data Objek Pajak
            't_namasppt' => $sspddetail->t_namasppt,
            't_alamatop' => $sspddetail->t_alamatop,
            't_rtop' => $sspddetail->t_rtop,
            't_rwop' => $sspddetail->t_rwop,
            't_kelurahanop' => $sspddetail->t_kelurahanop,
            't_kecamatanop' => $sspddetail->t_kecamatanop,
            't_kabupatenop' => $sspddetail->t_kabupatenop,
            't_luastanah' => $t_luastanahbpn,
            't_njoptanah' => str_ireplace(".", "", $sspddetail->t_njoptanah),
            't_totalnjoptanah' => str_ireplace(".", "", $sspddetail->t_totalnjoptanah),
            't_luasbangunan' => $t_luasbangunanbpn,
            't_njopbangunan' => str_ireplace(".", "", $sspddetail->t_njopbangunan),
            't_totalnjopbangunan' => str_ireplace(".", "", $sspddetail->t_totalnjopbangunan),
            't_grandtotalnjop' => str_ireplace(".", "", $sspddetail->t_grandtotalnjop),
            't_nosertifikathaktanah' => $sspddetail->t_nosertifikathaktanah,
            't_ketwaris' => $sspddetail->t_ketwaris,
            't_terbukti' => $sspddetail->t_terbukti,
            't_tglajb' => date('Y-m-d', strtotime($sspddetail->t_tglajb)),
            't_namasppt' => $sspddetail->t_namasppt,
            // Data Penjual
            't_namawppenjual' => $sspddetail->t_namawppenjual,
            't_nikwppenjual' => $sspddetail->t_nikwppenjual,
            't_npwpwppenjual' => $sspddetail->t_npwpwppenjual,
            't_alamatwppenjual' => $sspddetail->t_alamatwppenjual,
            't_kelurahanwppenjual' => $sspddetail->t_kelurahanwppenjual,
            't_kecamatanwppenjual' => $sspddetail->t_kecamatanwppenjual,
            't_kabkotawppenjual' => $sspddetail->t_kabkotawppenjual,
            't_telponwppenjual' => $sspddetail->t_telponwppenjual,
            't_kodeposwppenjual' => $sspddetail->t_kodeposwppenjual,
            't_rtwppenjual' => $sspddetail->t_rtwppenjual,
            't_rwwppenjual' => $sspddetail->t_rwwppenjual,
            't_inputbpn' => true
        );
        $this->insert($data);
    }

    public function savedatatglajbbaru(SSPDBphtbBase $sspddetail)
    {
        $data = array(
            't_tglajbbaru' => date('Y-m-d', strtotime($sspddetail->t_tglajbbaru)),
            't_noajbbaru' => $sspddetail->t_noajbbaru
        );
        $this->update($data, array(
            't_iddetailsptbphtb' => $sspddetail->t_iddetailsptbphtb
        ));
    }

    public function getPendataanSspdBphtb(SSPDBphtbBase $ss)
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from(["a" => $this->view_sspd_pembayaran()]);
        $where = new \Zend\Db\Sql\Where();
        $where->equalTo("a.t_idspt", $ss->t_idspt);
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res->current();
    }

    public function view_sspd_pembayaran($string = null)
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->columns([
            "t_idspt", "t_tglprosesspt", "t_periodespt", "t_nopbphtbsppt", "t_totalspt",
            "t_nilaitransaksispt", "t_kohirspt", "t_potonganspt", "t_idjenistransaksi",
            "t_idjenishaktanah", "t_thnsppt", "t_persyaratan", "t_idnotarisspt",
            "t_idsptsebelumnya", "t_kohirketetapanspt",
            "t_tarif_pembagian_aphb_kali", "t_tarif_pembagian_aphb_bagi",
            "t_idtarifbphtb", "t_persenbphtb",
            "t_tgljatuhtempospt", "t_dasarspt", "t_idjenisdoktanah",
            "t_potongan_waris_hibahwasiat", "t_latitudeobjek", "t_longitudeobjek"
        ]);
        $select->from(["a" => "t_spt"]);
        $select->join(["b" => "t_detailsptbphtb"], "a.t_idspt = b.t_idspt", [
            "t_namawppembeli", "t_nikwppembeli", "t_alamatwppembeli",
            "t_kecamatanwppembeli", "t_kelurahanwppembeli", "t_kabkotawppembeli",
            "t_telponwppembeli", "t_kodeposwppembeli", "t_npwpwppembeli", "t_namawppenjual",
            "t_nikwppenjual", "t_alamatwppenjual", "t_kecamatanwppenjual",
            "t_kelurahanwppenjual", "t_kabkotawppenjual",  "t_telponwppenjual",
            "t_kodeposwppenjual", "t_npwpwppenjual", "t_luastanah", "t_njoptanah",
            "t_luasbangunan", "t_njopbangunan", "t_totalnjoptanah",
            "t_totalnjopbangunan", "t_grandtotalnjop", "t_nosertifikathaktanah",
            "t_iddetailsptbphtb", "t_kelurahanop", "t_kecamatanop",
            "t_kabupatenop", "t_rtwppembeli", "t_rwwppembeli", "t_alamatop",
            "t_rtop", "t_rwop", "t_rtwppenjual", "t_rwwppenjual", "t_inputbpn",
            "t_noajbbaru", "t_tglajbbaru", "t_grandtotalnjop_aphb",
            "t_tglajb", "t_namasppt",
            't_emailwppembeli', 't_emailwppenjual'
        ], "LEFT");
        $select->join(["c" => "s_jenistransaksi"], "c.s_idjenistransaksi = a.t_idjenistransaksi", [
            "s_namajenistransaksi"
        ], "LEFT");
        $select->join(["d" => "s_jenishaktanah"], "d.s_idhaktanah = a.t_idjenishaktanah", [
            "s_namahaktanah"
        ], "LEFT");
        $select->join(["e" => "t_pembayaranspt"], "e.t_idspt = a.t_idspt", [
            "t_idpembayaranspt", "t_kohirpembayaran", "t_periodepembayaran",
            "t_tanggalpembayaran", "t_idnotaris", "t_nilaipembayaranspt",
            "t_kodebayarspt", "t_tglverifikasispt", "t_verifikasispt",
            "t_tglverifikasiberkas", "t_verifikasiberkas", "t_pejabatverifikasispt",
            "t_statusbayarspt", "t_ketetapanspt", "t_kodebayarbanksppt",
            "s_id_format_pesan"
        ], "LEFT");
        $select->join(["f" => "t_pemeriksaan"], "f.p_idpembayaranspt = e.t_idpembayaranspt", [
            "p_idpemeriksaan", "p_idpembayaranspt", "p_luastanah", "p_luasbangunan",
            "p_njoptanah", "p_njopbangunan", "p_totalnjoptanah", "p_totalnjopbangunan",
            "p_grandtotalnjop", "p_nilaitransaksispt", "p_potonganspt", "p_totalspt",
            "p_grandtotalnjop_aphb"
        ], "LEFT");
        $select->join(["g" => "s_users"], "g.s_iduser = a.t_idnotarisspt", [], "LEFT");
        $select->join(["h" => "s_notaris"], new Expression("h.s_idnotaris::text = g.s_idpejabat_idnotaris::text"), [
            "s_namanotaris", "s_idnotaris"
        ], "LEFT");
        $select->join(["i" => "s_users"], new Expression("i.s_iduser = e.t_pejabatverifikasispt"), [], "LEFT");
        $select->join(["j" => "s_pejabat"], new Expression("j.s_idpejabat::text = i.s_idpejabat_idnotaris::text"), [
            "namapejabatverifikasi" => "s_namapejabat",
        ], "LEFT");
        $select->join(["k" => "s_kecamatan"], new Expression("substring(a.t_nopbphtbsppt::text, 7, 3) = (k.s_kodekecamatan::text || '.'::text)"), [
            "s_kodekecamatan", "s_namakecamatan",
        ], "LEFT");
        $select->join(["l" => "s_kelurahan"], new Expression("k.s_idkecamatan = l.s_idkecamatan AND substring(a.t_nopbphtbsppt::text, 11, 3) = l.s_kodekelurahan::TEXT"), [
            "s_kodekelurahan", "s_namakelurahan",
        ], "LEFT");
        $select->join(["p" => "s_jenisdoktanah"], "a.t_idjenisdoktanah = p.s_iddoktanah", [
            "s_namadoktanah"
        ], "LEFT");
        if ($string == null) {
            return $select;
        } else {
            return $select->getSqlString();
        }
    }

    public function getPendataanDS($id)
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from("view_pembayarandenda");
        $where = new \Zend\Db\Sql\Where();
        $where->equalTo("t_idds", (int) $id);
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res->current();
    }

    public function getSkpdkbBphtb(SSPDBphtbBase $ss)
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from("view_sspd_validasi");
        $where = new \Zend\Db\Sql\Where();
        $where->equalTo("t_idspt", $ss->t_idspt);
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res->current();
    }

    public function hapusData(SSPDBphtbBase $kb)
    {
        $this->delete(array(
            't_idspt' => $kb->t_idspt
        ));
    }

    public function hapusall($idspt)
    {


        $dataspt = "select t_idsptsebelumnya from t_spt where t_idspt = " . $idspt . "";
        $id_spt_sebelumnya = $this->adapter->query($dataspt)->execute()->current();

        if (!empty($id_spt_sebelumnya['t_idsptsebelumnya'])) {

            $update = "update t_spt set fr_tervalidasidua =3 where t_idspt= " . $id_spt_sebelumnya['t_idsptsebelumnya'] . "";
            $this->adapter->query($update)->execute();
        } else {
        }

        // hapus file t_spt
        $input = "DELETE FROM t_spt where t_idspt= " . $idspt . "";
        $this->adapter->query($input)->execute();

        // hapus data t_detailsptbphtb   
        $this->delete(array(
            't_idspt' => $idspt
        ));

        // cari data id_pembayaran
        $caridata_idpembayaran = "select t_idpembayaranspt from t_pembayaranspt where t_idspt = " . $idspt . "";
        $id_pembayaran = $this->adapter->query($caridata_idpembayaran)->execute()->current();

        if (!empty($id_pembayaran)) {
            $hapusdatapemeriksaan = "DELETE FROM t_pemeriksaan where p_idpembayaranspt = " . $id_pembayaran['t_idpembayaranspt'] . "";
            $this->adapter->query($hapusdatapemeriksaan)->execute();
        }




        // hapus data t_pembayaranspt
        $input2 = "DELETE FROM t_pembayaranspt where t_idspt= " . $idspt . "";
        $this->adapter->query($input2)->execute();
        //echo "Data Sukses Dihapus";
    }

    public function hapusall2($idspt)
    {

        // hapus file t_spt
        $input = "DELETE FROM t_spt where t_idspt= " . $idspt . "";
        $this->adapter->query($input)->execute();

        // hapus data t_detailsptbphtb   
        $this->delete(array(
            't_idspt' => $idspt
        ));

        // cari data id_pembayaran
        $caridata_idpembayaran = "select t_idpembayaranspt from t_pembayaranspt where t_idspt = " . $idspt . "";
        $id_pembayaran = $this->adapter->query($caridata_idpembayaran)->execute()->current();

        $hapusdatapemeriksaan = "DELETE FROM t_pemeriksaan where p_idpembayaranspt = " . $id_pembayaran['t_idpembayaranspt'] . "";
        $this->adapter->query($hapusdatapemeriksaan)->execute();


        // hapus data t_pembayaranspt
        $input2 = "DELETE FROM t_pembayaranspt where t_idspt= " . $idspt . "";
        $this->adapter->query($input2)->execute();
        //echo "Data Sukses Dihapus";
    }

    public function hapusDataWaris($id)
    {
        $t_penerimawaris = new \Zend\Db\TableGateway\TableGateway('t_penerimawaris', $this->adapter);
        $t_penerimawaris->delete(array(
            't_idspt' => $id
        ));
    }

    // belum valid
    public function temukanDataHistory(SSPDBphtbBase $kb)
    {
        $sql = "select * from view_sspd a
                left join t_pembayaranspt b on b.t_idspt = a.t_idspt
                left join t_penerimawaris c on c.t_idspt = a.t_idspt
                where a.t_nikwppembeli='" . $kb->t_nikwppembeli . "' or c.t_nikpenerima ='" . $kb->t_nikwppembeli . "'
                and a.t_periodespt='" . date('Y', strtotime($kb->t_tglprosesspt)) . "' order by a.t_idspt desc";



        $statement = $this->adapter->query($sql);

        //var_dump($sql);
        //exit();

        return $statement->execute()->current();
    }

    // belum valid
    public function temukanDataHistory2(SSPDBphtbBase $kb)
    {
        $sql = "select * from view_sspd a
                left join t_pembayaranspt b on b.t_idspt = a.t_idspt
                where a.t_nikwppembeli='" . $kb->t_nikwppembeli . "' 
                and a.t_periodespt='" . date('Y', strtotime($kb->t_tglprosesspt)) . "'";
        $statement = $this->adapter->query($sql);
        //var_dump($sql);
        //exit();

        return $statement->execute();
    }

    public function temukanDataNPOPTKP(SSPDBphtbBase $kb)
    {
        $sql = "select * from s_tarifnpoptkp
                where s_idjenistransaksinpotkp='" . $kb->t_idjenistransaksi . "'";
        $statement = $this->adapter->query($sql);
        return $statement->execute()->current();
    }

    public function temukanDataTarifBphtb()
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select("s_tarifbphtb")->where("date_part('year'::text, s_tanggaltarifbphtb) = " . date('Y') . "");
        $res = $sql->prepareStatementForSqlObject($select)->execute()->current();

        if (!$res) {
            $select = $sql->select("s_tarifbphtb")->where([
                "s_statustarifbphtb" => 2
            ])->order(new Expression("s_tanggaltarifbphtb DESC"));
            $res = $sql->prepareStatementForSqlObject($select)->execute()->current();
        }
        return $res;
    }

    public function temukanDataTarifBphtb_tahun($tahun)
    {
        //$sql = "select * from s_tarifbphtb where s_idtarifbphtb=2";
        $sql = "select * from s_tarifbphtb where date_part('year'::text, s_tanggaltarifbphtb) = " . $tahun . "";
        $statement = $this->adapter->query($sql);
        return $statement->execute()->current();
    }

    public function temukanPersyaratan($kb)
    {
        $sql = "select * from s_persyaratan where s_idjenistransaksi =$kb->t_idjenistransaksi order by s_idpersyaratan";
        $statement = $this->adapter->query($sql);
        return $statement->execute();
    }

    // Belum valid
    public function gethistorybphtb($nikpembeli, $idspt, $periode)
    {
        $sql = "select * from view_sspd a
                left join t_pembayaranspt b on b.t_idspt = a.t_idspt 
                where a.t_nikwppembeli='$nikpembeli' and a.t_idspt not in ($idspt)
                and a.t_periodespt='" . $periode . "' order by a.t_idspt";
        $statement = $this->adapter->query($sql);
        return $statement->execute();
    }

    public function savedata_koreksiluas($id, $t_luastanahbpn, $t_luasbangunanbpn)
    {
        $data = array(
            't_luastanahbpn' => $t_luastanahbpn,
            't_luasbangunanbpn' => $t_luasbangunanbpn
        );
        $update = $this->update($data, array(
            't_iddetailsptbphtb' => $id
        ));
        return $update;
    }

    public function savedata_sertifikatbaru($id, $t_nosertifikatbaru, $t_tglsertifikatbaru)
    {
        $data = array(
            't_nosertifikatbaru' => $t_nosertifikatbaru,
            't_tglsertifikatbaru' => date('Y-m-d', strtotime($t_tglsertifikatbaru))
        );
        $update = $this->update($data, array(
            't_iddetailsptbphtb' => $id
        ));
        return $update;
    }

    public function getSptid($t_iddetailsptbphtb)
    {
        $rowset = $this->select(array(
            't_iddetailsptbphtb' => $t_iddetailsptbphtb
        ));
        $row = $rowset->current();
        return $row;
    }

    public function getDataPemeriksaan($id)
    {
        $sql = "select * from view_sspd_semua_pembayaran where t_idspt=$id";
        $statement = $this->adapter->query($sql);
        return $statement->execute()->current();
    }

    public function getDataPemeriksaan1($id)
    {
        $sql = "select * from view_sspd_semua_pembayaran where t_iddetailsptbphtb=$id";
        $statement = $this->adapter->query($sql);
        return $statement->execute()->current();
    }

    public function getDataIdBpn($id)
    {
        $rowset = $this->select(function (Select $select) use ($id) {
            $select->where(array(
                't_spt.t_idspt' => $id
            ));
            $select->join('t_spt', 't_detailsptbphtb.t_idspt = t_spt.t_idspt');
            $select->join('s_jenistransaksi', 't_spt.t_idjenistransaksi = s_jenistransaksi.s_idjenistransaksi');
            $select->join('s_jenishaktanah', 't_spt.t_idjenishaktanah = s_jenishaktanah.s_idhaktanah');
            $select->join('t_pembayaranspt', 't_pembayaranspt.t_idspt = t_spt.t_idspt');
            $datapemeriksaan = $this->getDataPemeriksaan($id);
            if ($datapemeriksaan['p_idpemeriksaan'] != null) {
                $select->join('t_pemeriksaan', 't_pemeriksaan.p_idpembayaranspt = t_pembayaranspt.t_idpembayaranspt');
            }
        });
        $row = $rowset->current();
        return $row;
    }

    public function getSemuaData($t_iddetailsptbphtb)
    {
        $rowset = $this->select(function (Select $select) use ($t_iddetailsptbphtb) {
            $select->where(array(
                't_detailsptbphtb.t_iddetailsptbphtb' => $t_iddetailsptbphtb
            ));
            $select->join('t_spt', 't_detailsptbphtb.t_idspt = t_spt.t_idspt');
            $select->join('s_jenistransaksi', 't_spt.t_idjenistransaksi = s_jenistransaksi.s_idjenistransaksi');
            $select->join('s_jenishaktanah', 't_spt.t_idjenishaktanah = s_jenishaktanah.s_idhaktanah');
            $select->join('t_pembayaranspt', 't_pembayaranspt.t_idspt = t_spt.t_idspt');
            $datapemeriksaan = $this->getDataPemeriksaan1($t_iddetailsptbphtb);
            if ($datapemeriksaan['p_idpemeriksaan'] != null) {
                $select->join('t_pemeriksaan', 't_pemeriksaan.p_idpembayaranspt = t_pembayaranspt.t_idpembayaranspt');
            }
        });
        $row = $rowset->current();
        return $row;
    }

    public function jumlahsyarat($id_syarat)
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from('s_persyaratan');
        $where = new Where();
        $where->literal("s_idjenistransaksi = '$id_syarat'");
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res->count();
    }

    public function getGridCountSismiop(SSPDBphtbBase $base)
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from('view_terbit_ajb');
        $where = new Where();
        $kohir = (int) $base->t_kohirspt;
        if ($base->t_kohirspt != 'undefined')
            $where->literal("t_kohirspt::text LIKE '%$kohir%'");
        if ($base->t_periodespt != 'undefined')
            $where->equalTo("t_periodespt", $base->t_periodespt);
        if ($base->t_tglprosesspt != 'undefined')
            $where->equalTo("t_tglprosesspt", date('Y-m-d', strtotime($base->t_tglprosesspt)));
        if ($base->t_namawppembeli != 'undefined')
            $where->literal("t_namawppembeli::text LIKE '%$base->t_namawppembeli%'");
        if ($base->t_totalspt != 'undefined')
            $where->literal("t_totalspt::text LIKE '%$base->t_totalspt%'");
        if ($base->s_namajenistransaksi != 'undefined')
            $where->literal("s_namajenistransaksi::text LIKE '%$base->s_namajenistransaksi%'");
        if ($base->t_noajbbaru != 'undefined')
            $where->literal("t_noajbbaru::text LIKE '%$base->t_noajbbaru%'");
        if ($base->t_tglajbbaru != 'undefined')
            $where->equalTo("t_tglajbbaru", date('Y-m-d', strtotime($base->t_tglajbbaru)));
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res->count();
    }

    public function getGridDataSismiop(SSPDBphtbBase $base, $offset)
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from('view_terbit_ajb');
        $where = new Where();
        $kohir = (int) $base->t_kohirspt;
        if ($base->t_kohirspt != 'undefined')
            $where->literal("t_kohirspt::text LIKE '%$kohir%'");
        if ($base->t_periodespt != 'undefined')
            $where->equalTo("t_periodespt", $base->t_periodespt);
        if ($base->t_tglprosesspt != 'undefined')
            $where->equalTo("t_tglprosesspt", date('Y-m-d', strtotime($base->t_tglprosesspt)));
        if ($base->t_namawppembeli != 'undefined')
            $where->literal("t_namawppembeli::text LIKE '%$base->t_namawppembeli%'");
        if ($base->t_totalspt != 'undefined')
            $where->literal("t_totalspt::text LIKE '%$base->t_totalspt%'");
        if ($base->s_namajenistransaksi != 'undefined')
            $where->literal("s_namajenistransaksi::text LIKE '%$base->s_namajenistransaksi%'");
        if ($base->t_noajbbaru != 'undefined')
            $where->literal("t_noajbbaru::text LIKE '%$base->t_noajbbaru%'");
        if ($base->t_tglajbbaru != 'undefined')
            $where->equalTo("t_tglajbbaru", date('Y-m-d', strtotime($base->t_tglajbbaru)));
        $select->where($where);
        $select->limit($base->rows = (int) $base->rows);
        $select->offset($offset = (int) $offset);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res;
    }

    public function getJumlahPendataan(SSPDBphtbBase $base, $id_notaris, $userRole = null)
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from('view_sspd_pembayaran');
        $where = new Where();
        if ($userRole == "Notaris") {
            $where->equalTo('t_idnotarisspt', $id_notaris);
        }
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res->count();
    }

    public function getcaritransaksimonitoringtgl($tglcari)
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from('view_sspd_pembayaran');
        $where = new Where();
        $where->equalTo('t_tglprosesspt', $tglcari);
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res;
    }

    public function getcaritransaksimonitoringnop($nopcari)
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from('view_sspd_pembayaran');
        $where = new Where();
        $where->literal("t_nopbphtbsppt::text LIKE '%$nopcari%'");
        $select->where($where);
        $select->order("t_tanggalpembayaran DESC");
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res;
    }

    public function getcaridetailmonitoring($idsptnya)
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from('view_sspd_pembayaran');
        $where = new Where();
        $where->equalTo('t_idspt', (int) $idsptnya);
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute()->current();
        return $res;
    }

    public function savePergantianNotaris($post, $session)
    {
        $sql = new Sql($this->adapter);

        $data = [
            "t_tgl_log" => date("Y-m-d H:i:s"),
            "t_iduser" => $session["s_iduser"],
            "t_idspt" => $post["t_idspt"],
            "t_idnotarisspt_lama" => $post["t_idnotarisspt_lama"],
            "t_idnotarisspt_baru" => $post["t_idnotarisspt_baru"]
        ];

        $id = (int) $post["t_id_log"];
        $res = false;
        if ($id == 0) {
            $insert = $sql->insert("t_pergantian_notaris_log")->values($data);
            $res = $sql->prepareStatementForSqlObject($insert)->execute();
        }

        if ($res) {
            $res = false;
            $update = $sql->update("t_spt")->set([
                "t_idnotarisspt" => $post["t_idnotarisspt_baru"]
            ])->where(["t_idspt" => $post["t_idspt"]]);
            $res = $sql->prepareStatementForSqlObject($update)->execute();
        }

        return $res;
    }
}
