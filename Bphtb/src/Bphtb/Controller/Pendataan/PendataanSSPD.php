<?php

namespace Bphtb\Controller\Pendataan;

use ArrayObject;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Math\Rand;
use Bphtb\Form\Pendataan\SSPDFrm;
use Bphtb\Form\Spop\SpopFrm;
use Bphtb\Form\Spop\SpopMutasiFrm;
use Bphtb\Helper\Spop\ReffSpopHelper;
use Bphtb\Helper\Spop\TipeEspopHelper;
use Bphtb\Helper\UploadHelper;
use mPDF;
use Zend\Debug\Debug;

class PendataanSSPD extends AbstractActionController
{

    protected $tbl_sspd, $tbl_jenistransaksi, $tbl_haktanah, $tbl_nop;
    protected $tbl_spt, $tbl_pendataan, $tbl_notaris, $tbl_persyaratan, $tbl_doktanah, $tbl_sspdwaris;
    protected $routeMatch, $tbl_pemda, $tbl_pejabat;
    protected $options;
    // PHP File Upload error message codes:
    // http://php.net/manual/en/features.file-upload.errors.php
    protected $error_messages = array(
        1 => 'The uploaded file exceeds the upload_max_filesize directive in php.ini',
        2 => 'The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form',
        3 => 'The uploaded file was only partially uploaded',
        4 => 'No file was uploaded',
        6 => 'Missing a temporary folder',
        7 => 'Failed to write file to disk',
        8 => 'A PHP extension stopped the file upload',
        'post_max_size' => 'The uploaded file exceeds the post_max_size directive in php.ini',
        'max_file_size' => 'Ukuran file terlalu besar',
        'min_file_size' => 'File is too small',
        'accept_file_types' => 'Filetype not allowed',
        'max_number_of_files' => 'Maximum number of files exceeded',
        'max_width' => 'Image exceeds maximum width',
        'min_width' => 'Image requires a minimum width',
        'max_height' => 'Image exceeds maximum height',
        'min_height' => 'Image requires a minimum height',
        'abort' => 'File upload aborted',
        'image_resize' => 'Failed to resize image'
    );
    protected $image_objects = array();

    public function cekurl()
    {
        $basePath = $this->getRequest()->getBasePath();
        $uri = new \Zend\Uri\Uri($this->getRequest()->getUri());
        $uri->setPath($basePath);
        $uri->setQuery(array());
        $uri->setFragment('');

        return $uri->getScheme() . '://' . $uri->getHost() . ':' . $_SERVER['SERVER_PORT'] . '' . $uri->getPath();
    }

    public function indexAction()
    {
        $ar_pemda = $this->getPemda()->getdata();
        $session = $this->getServiceLocator()->get('EtaxService')->getStorage()->read();
        $data_mengetahuilengkap_sspd = $this->getTblPejabat()->getdata();

        // if (($session['s_namauserrole'] == "Administrator") || ($session['s_namauserrole'] == "Pegawai") || ($session['s_tipe_pejabat'] == 1)) {
        //     $show_hide_combo_notaris = "";
        //     $idnotarisdatacari = 1;
        // } elseif ($session['s_tipe_pejabat'] == 2) {
        //     //if ($session['s_namauserrole'] == "Notaris") {
        //     $show_hide_combo_notaris = "display:none;";
        //     $idnotarisdatacari = 2;
        // } else {
        //     $show_hide_combo_notaris = "";
        //     $idnotarisdatacari = "";
        // }

        if ($session["s_akses"] == 3) {
            $show_hide_combo_notaris = "display:none;";
            $idnotarisdatacari = 2;
        } else {
            $show_hide_combo_notaris = "";
            $idnotarisdatacari = 1;
        }

        $panggildata = $this->getServiceLocator()->get("JenisTransaksiBphtbTable");
        $datajenistransaksi = $panggildata->comboBox();

        $ar_notaris = $this->getServiceLocator()->get('NotarisBphtbTable')->getdataCombo();

        $form = new \Bphtb\Form\Pendataan\SSPDFrm();
        $frmpendaftaran = new SSPDFrm(null, null, null, null, $this->populateComboNotaris(), null, $this->populateComboNotaris(), null);
        $btnPendaftaran = $this->getServiceLocator()->get("ButtonTable")->getDataButtonId(1);

        $view = new ViewModel(
            [
                'form' => $form,
                'data_mengetahuilengkap_sspd' => $data_mengetahuilengkap_sspd,
                'frm' => $frmpendaftaran,
                'data_mengetahuilengkap_sspd' => $data_mengetahuilengkap_sspd,
                'datajenistransaksi' => $datajenistransaksi,
                'idnotarisdatacari' => $idnotarisdatacari,
                'datanotaris' => $ar_notaris,
                'show_hide_combo_notaris' => $show_hide_combo_notaris,
                'btnPendaftaran' => $btnPendaftaran,
            ]
        );
        $data = array(
            'menu_pendataan' => 'active',
            'role_id' => $session['s_akses'],
            'data_pemda' => $ar_pemda,
            'aturgambar' => 1,
            'username' => $session['s_username']
        );
        $this->layout()->setVariables($data);
        return $view;
    }

    // ====================== CETAK SPOP / SPOP
    public function indexspopAction()
    {
        $ar_pemda = $this->getPemda()->getdata();

        $sm = $this->getServiceLocator();
        $this->caridata_spop_lspop = $sm->get('SPPTTable');

        $kd_propinsi = $this->caridata_spop_lspop->cek_kd_propinsi();
        $kd_dati2 = $this->caridata_spop_lspop->cek_kd_dati();

        $view = new ViewModel(array(
            'kd_propinsi' => $kd_propinsi['KD_PROPINSI'],
            'kd_dati2' => $kd_dati2['KD_DATI2']
        ));
        $session = $this->getServiceLocator()->get('EtaxService')->getStorage()->read();
        $data = array(
            'menu_pendataan' => 'active',
            'role_id' => $session['s_akses'],
            'data_pemda' => $ar_pemda,
            'aturgambar' => 1,
            'username' => $session['s_username']
        );
        $this->layout()->setVariables($data);
        return $view;
    }

    public function dataGridKecamatanAction()
    {
        $session = $this->getServiceLocator()->get('EtaxService')->getStorage()->read();
        $s_iduser = $session['s_iduser'];
        $s_tipe_pejabat = $session['s_tipe_pejabat'];

        $sTable = 'REF_KECAMATAN';
        $count = 'KD_PROPINSI';

        $input = $this->getRequest();
        $order_default = " KD_PROPINSI ASC";
        $aColumns = array('KD_PROPINSI', 'KD_PROPINSI', 'KD_DATI2', 'KD_KECAMATAN', 'NM_KECAMATAN');

        $panggildata = $this->getServiceLocator()->get("SPPTTable");

        $rResult = $panggildata->ref_kecamatan($sTable, $count, $input, $order_default, $aColumns);


        return $this->getResponse()->setContent(\Zend\Json\Json::encode($rResult));
    }

    public function dataGridKelurahanAction()
    {
        $session = $this->getServiceLocator()->get('EtaxService')->getStorage()->read();
        $s_iduser = $session['s_iduser'];
        $s_tipe_pejabat = $session['s_tipe_pejabat'];

        $sTable = 'REF_KELURAHAN';
        $count = 'KD_PROPINSI';

        $input = $this->getRequest();
        $order_default = " c.KD_PROPINSI ASC";
        $aColumns = array('KD_PROPINSI', 'KD_KECAMATAN', 'NM_KECAMATAN', 'KD_KELURAHAN', 'NM_KELURAHAN');

        $panggildata = $this->getServiceLocator()->get("SPPTTable");

        $rResult = $panggildata->ref_kelurahan($sTable, $count, $input, $order_default, $aColumns);


        return $this->getResponse()->setContent(\Zend\Json\Json::encode($rResult));
    }

    public function cekminmaxopAction()
    {

        $req = $this->getRequest();
        $res = $this->getResponse();
        if ($req->isGet()) {
            //================== Model\Pendataan\SPPTTable
            $nopcari = $req->getQuery()->get('nop');

            $sm = $this->getServiceLocator();
            $this->caridata_spop_lspop = $sm->get('SPPTTable');

            $data_minmax = $this->caridata_spop_lspop->cek_min_max_op($nopcari);

            $view = new ViewModel(array(
                'data_minmax' => $data_minmax
            ));
            $data = array(
                'nilai' => '3'
            );
            $this->layout()->setVariables($data);
            return $view;
        }
        return $res;
    }

    public function cetakspoplspopAction()
    {
        $req = $this->getRequest();
        $data_get = $req->getQuery();

        $nopcari = $req->getQuery()->get('nop');
        $nourut1 = $req->getQuery()->get('nourut1');
        $nourut2 = $req->getQuery()->get('nourut2');

        $sm = $this->getServiceLocator();
        $this->caridata_spop_lspop = $sm->get('SPPTTable');

        $ar_pemda = $this->getPemda()->getdata();

        $data_spop_lspop_count = $this->caridata_spop_lspop->cetakdataspop_lspop_count($nopcari, $nourut1, $nourut2);
        $data_spop_lspop = $this->caridata_spop_lspop->cetakdataspop_lspop($nopcari, $nourut1, $nourut2); //temukanDataInfoop($nopcari, $tahuncari); 
        $session = $this->getServiceLocator()->get('EtaxService')->getStorage()->read();

        $view = new ViewModel(array(
            'data_spop_lspop' => $data_spop_lspop,
            'ar_pemda' => $ar_pemda,
            'nama_login' => $session['s_iduser'],
            'cetak' => $data_get->cetak,
            'jml_data' => $data_spop_lspop_count,
            'namafile' => $nopcari . '_nourut_' . $nourut1 . '_sd_' . $nourut2
        ));
        $data = array(
            'nilai' => '3' //no layout
        );
        $this->layout()->setVariables($data);
        return $view;
    }

    // ========================= END CETAK SPOP / LSPOP
    public function dataGridAction()
    {
        $session = $this->getServiceLocator()->get('EtaxService')->getStorage()->read();
        $s_iduser = $session['s_iduser'];
        $s_tipe_pejabat = $session['s_tipe_pejabat'];

        $sTable = 'fr_pendaftaran_v5';
        $count = 't_idspt';

        $input = $this->getRequest();
        $order_default = " t_kohirspt DESC";
        $order_default_thn = " t_tglprosesspt DESC";
        if ($s_tipe_pejabat == 2) {
            $aColumns = array(
                'a.t_idspt', 't_kohirspt', 't_kohirketetapanspt',
                'c.s_idjenistransaksi', 't_tglprosesspt', 't_namawppembeli', 't_totalspt',
                'jml_pajak_v1', 'status_pendaftaran', 'status_validasi', 't_statusbayarspt',
                'a.t_idspt', 'e.t_kodebayarbanksppt', 'a.t_idspt', 'a.t_nopbphtbsppt'
            );
        } else {
            $aColumns = array(
                'a.t_idspt', 't_kohirspt', 't_kohirketetapanspt',
                'c.s_idjenistransaksi', 't_idnotarisspt', 't_tglprosesspt', 't_namawppembeli',
                't_totalspt', 'jml_pajak_v1', 'status_pendaftaran', 'status_validasi',
                't_statusbayarspt', 'a.t_idspt', 'e.t_kodebayarbanksppt', 'a.t_idspt',
                'a.t_nopbphtbsppt'
            );
        }

        $panggildata = $this->getServiceLocator()->get("SSPDBphtbTable");
        $rResult = $panggildata->semuadatapendaftaran($sTable, $count, $input, $order_default, $order_default_thn, $aColumns, $session, $this->cekurl());
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($rResult));
    }

    public function dataGrid2Action()
    {
        $allParams = (array) $this->getEvent()->getRouteMatch()->getParams();
        $session = $this->getServiceLocator()->get('EtaxService')->getStorage()->read();
        $base = new \Bphtb\Model\Pendataan\SSPDBphtbBase();
        $base->exchangeArray($allParams);
        if ($base->direction == 2)
            $base->page = $base->page + 1;
        if ($base->direction == 1)
            $base->page = $base->page - 1;
        if ($base->page <= 0)
            $base->page = 1;
        $page = $base->page;
        $limit = $base->rows;
        //============= Model\Pendataan\SSPDBphtbTable
        $count = $this->getTblSSPDBphtb()->getGridCountPendataanSSPD($base, $session['s_iduser'], $session['s_tipe_pejabat']); //getGridCount //$session['s_namauserrole']
        if ($count > 0 && $limit > 0) {
            $total_pages = ceil($count / $limit);
        } else {
            $total_pages = 0;
        }

        if ($page > $total_pages)
            $page = $total_pages;
        $start = $limit * $page - $limit;
        if ($start < 0)
            $start = 0;

        //============ Model\Pendataan\SSPDBphtbTable
        $data = $this->getTblSSPDBphtb()->getGridDataPendataanSSPD($base, $start, $session['s_iduser'], $session['s_tipe_pejabat']); // getGridData $session['s_namauserrole']
        $s = "";
        foreach ($data as $row) {
            $s .= "<tr>";
            if ($row['t_inputbpn'] == true) {
                $s .= "<td> <span class='badge' style='background-color:#CC0000;'>" . str_pad($row['t_kohirspt'], 4, '0', STR_PAD_LEFT) . " </span> </td>";
            } else {
                $s .= "<td>" . str_pad($row['t_kohirspt'], 4, '0', STR_PAD_LEFT) . "</td>";
            }
            $s .= "<td>" . $row['t_periodespt'] . "</td>";
            $s .= "<td>" . date('d-m-Y', strtotime($row['t_tglprosesspt'])) . "</td>";
            $s .= "<td>" . $row['t_namawppembeli'] . "</td>";
            if (!empty($row['p_idpemeriksaan'])) {
                $s .= "<td><div style='text-align:right'>" . number_format($row['p_totalspt'], 0, ',', '.') . "</div></td>";
            } else {
                $s .= "<td><div style='text-align:right'>" . number_format($row['t_totalspt'], 0, ',', '.') . "</div></td>";
            }
            $s .= "<td>" . $row['s_namajenistransaksi'] . "</td>";
            $result_array_syarat = \Zend\Json\Json::decode($row['t_persyaratan']);
            $jml_syarat = count($result_array_syarat);
            $result_array_syarat_verifikasi = \Zend\Json\Json::decode($row['t_verifikasispt']);

            $jml_syarat_verifikasi = count($result_array_syarat_verifikasi);
            $cektabelpersyaratan = $this->getTblJenTran()->jumlahsyarat($row['t_idjenistransaksi']);

            if ($cektabelpersyaratan == $jml_syarat) {
                $s .= "<td>Lengkap</td>";
            } else {
                $s .= "<td>Belum Lengkap</td>";
            }
            if (($cektabelpersyaratan == $jml_syarat) && ($cektabelpersyaratan == $jml_syarat_verifikasi)) {
                $status_verifikasi = "Tervalidasi";
                if ($row['t_statusbayarspt'] == true) {
                    $edit = "<td><a href='pendataan_sspd/viewdata?t_idspt= $row[t_idspt]' class='btn btn-primary btn-sm btn-flat' style='width:50px'>Lihat</a></td>";
                } else {
                    $edit = "<td><a href='pendataan_sspd/viewdata?t_idspt= $row[t_idspt]' class='btn btn-primary btn-sm btn-flat' style='width:50px'>Lihat</a> </td>"; //<a href='pendataan_sspd/edit?t_idspt= $row[t_idspt]' class='btn btn-warning btn-sm btn-flat' style='width:50px'>Edit</a>
                }
            } else {
                if (empty($row['t_verifikasispt'])) {
                    $status_verifikasi = "";
                    $edit = "<td><a href='pendataan_sspd/viewdata?t_idspt= $row[t_idspt]' class='btn btn-primary btn-sm btn-flat' style='width:50px'>Lihat</a> <a href='pendataan_sspd/edit?t_idspt= $row[t_idspt]' class='btn btn-warning btn-sm btn-flat' style='width:50px'>Edit</a></td>";
                } else {
                    $status_verifikasi = "Belum Lengkap";
                    if ($row['t_inputbpn'] == true) {
                        $edit = "<td><a href='pendataan_sspd/viewdata?t_idspt= $row[t_idspt]' class='btn btn-primary btn-sm btn-flat' style='width:50px'>Lihat</a></td>";
                    } else {
                        $edit = "<td><a href='pendataan_sspd/viewdata?t_idspt= $row[t_idspt]' class='btn btn-primary btn-sm btn-flat' style='width:50px'>Lihat</a> <a href='pendataan_sspd/edit?t_idspt=$row[t_idspt]' class='btn btn-warning btn-sm btn-flat' style='width:50px'>Edit</a> <a href='#' onclick='hapus(" . $row['t_idspt'] . ");return false; 'class='btn btn-danger btn-sm btn-flat' style='width:50px'>Hapus</a></td>";
                    }
                }
            }



            $s .= "<td>" . $status_verifikasi . "</td>";
            if ($row['t_statusbayarspt'] == true) {
                $status_bayar = "Sudah Dibayar";
            } else {
                $status_bayar = "Belum Dibayar";
            }
            $s .= "<td>" . $status_bayar . "</td>";
            $cetaksurat_sspd = " <a href='#' onclick='openCetakSSPD(" . $row['t_idspt'] . ");return false;' class='btn btn-success btn-sm btn-flat' style='width:80px'>SSPD</a>";
            //$s .= "<td><a href='pendataan_sspd/cetaksspdbphtb?&action=cetaksspd&t_idspt=$row[t_idspt]' target='_blank' class='btn btn-success btn-sm btn-flat'>SSPD</a></td>";
            $s .= "<td>" . $cetaksurat_sspd . "</td>";
            $s .= "" . $edit . " ";
            if (($session['s_namauserrole'] == "Administrator")) {
                $s .= "<td><a href='#' onclick='hapusall(" . $row['t_idspt'] . ");return false; 'class='btn btn-danger btn-sm btn-flat' style='width:50px'>Hapus Semua</a></td>";
            } else {
            }

            $s .= "<tr>";
        }
        $data_render = array(
            "grid" => $s,
            "rows" => $base->rows,
            "count" => $count,
            "page" => $page,
            "start" => $start,
            "total_halaman" => $total_pages
        );
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($data_render));
    }

    public function tambahAction()
    {
        $ar_pemda = $this->getPemda()->getdata();
        $session = $this->getServiceLocator()->get('EtaxService')->getStorage()->read();
        $string = Rand::getString(6, '1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ', true);
        $show_hide_combo_notaris = "display:none;";

        if (($session['s_namauserrole'] == "Administrator") || ($session['s_namauserrole'] == "Pegawai") || ($session['s_tipe_pejabat'] == 1)) {
            $show_hide_combo_notaris = "display:inherit;";
            if ($session['s_namauserrole'] == "Administrator") {
                $notaris = 3;
            } else {
                $notaris = 2;
            }
        } elseif ($session['s_tipe_pejabat'] == 2) {
            //if ($session['s_namauserrole'] == "Notaris") {

            $show_hide_combo_notaris = "display:none;";
            $notaris = 1;
        } else {
            $show_hide_combo_notaris = "display:none;";
            $notaris = '';
        }

        $req = $this->getRequest();
        $datane = $req->getPost();
        if (!empty($datane['t_idspt'])) {
            $dataki = $this->getTblSSPDBphtb()->getDataId($datane['t_idspt']);
            $frm = new SSPDFrm($this->populateComboJenisTransaksi(), $this->populateComboHakTanah(), $this->populateComboDokTanah(), $string, $this->populateComboNotaris(), $this->populatePersyaratanId($datane['t_idjenistransaksi']), null);
        } else {
            $frm = new SSPDFrm($this->populateComboJenisTransaksi(), $this->populateComboHakTanah(), $this->populateComboDokTanah(), $string, $this->populateComboNotaris(), null, null);
        }
        $data_tarif = $this->getTblSSPDBphtb()->temukanDataTarifBPHTB();
        if ($req->isPost()) {
            $kb1 = new \Bphtb\Model\Pendataan\SPTBase();
            $kb = new \Bphtb\Model\Pendataan\SSPDBphtbBase();
            $frm->setInputFilter($kb1->getInputFilter());
            $frm->setInputFilter($kb->getInputFilter($session['s_namauserrole']));
            $post = $req->getPost();
            $frm->setData($post);
            if ($frm->isValid()) {
                $kb1->exchangeArray($frm->getData());
                $kb->exchangeArray($frm->getData());
                $this->getTblSPT()->persyaratan($post);
                asort($kb->t_persyaratan);
                $kb->t_persyaratan = implode(",", $kb->t_persyaratan);
                $kb->t_persyaratan = explode(",", $kb->t_persyaratan);
                if (($session['s_namauserrole'] == "Administrator") || ($session['s_namauserrole'] == "Pegawai") || ($session['s_tipe_pejabat'] == 1)) {
                    $idnotaris = $kb->t_idnotarisspt;
                    $idpendaftar = $session['s_iduser'];
                } else
                    //if ($session['s_namauserrole'] == "Notaris") {
                    if ($session['s_tipe_pejabat'] == 2) {
                        $idnotaris = $session['s_iduser'];
                        $idpendaftar = $session['s_iduser'];
                    }

                //======== eksekusi simpan di Model\Pendataan\SPTTable
                sleep(3);
                //=================== hitung lagi biar aman coy
                $post["t_luastanah"] = str_ireplace(".", "", $post["t_luastanah"]);
                $post["t_luasbangunan"] = str_ireplace(".", "", $post["t_luasbangunan"]);
                $perhitungan = $this->ControllerHelper()->hitungBphtb($post);

                $t_totalspt = $perhitungan["t_totalspt"];
                $t_potonganspt = $perhitungan["t_potonganspt"];
                $t_potongan_waris_hibahwasiat = $perhitungan["t_potongan_waris_hibahwasiat"];
                $t_luastanah = $perhitungan["t_luastanah"];
                $t_njoptanah = $perhitungan["t_njoptanah"];
                $t_luasbangunan = $perhitungan["t_luasbangunan"];
                $t_njopbangunan = $perhitungan["t_njopbangunan"];
                $t_totalnjoptanah = $perhitungan["t_totalnjoptanah"];
                $t_totalnjopbangunan = $perhitungan["t_totalnjopbangunan"];
                $t_grandtotalnjop = $perhitungan["t_grandtotalnjop"];
                $t_grandtotalnjop_aphb = $perhitungan["t_grandtotalnjop_aphb"];

                //=================== hitung lagi biar aman coy
                $data = $this->getTblSPT()->savedata($kb1, $kb->t_nopbphtbsppt, $kb->t_kodebayarbanksppt, $kb->t_idjenistransaksi, $kb->t_idjenishaktanah, $t_totalspt, $kb->t_nilaitransaksispt, $t_potonganspt, $idnotaris, $idpendaftar, $post, $t_potongan_waris_hibahwasiat);
                if (gettype($data) == 'object') {
                    $this->getTblSSPDBphtb()->savedatadetail($kb, $data->t_idspt, $session, $t_luastanah, $t_njoptanah, $t_luasbangunan, $t_njopbangunan, $t_totalnjoptanah, $t_totalnjopbangunan, $t_grandtotalnjop, $t_grandtotalnjop_aphb);
                    //$idsptnya = $data->t_idspt;
                } else {
                    $this->getTblSSPDBphtb()->savedatadetail($kb, $data['t_idspt'], $session, $t_luastanah, $t_njoptanah, $t_luasbangunan, $t_njopbangunan, $t_totalnjoptanah, $t_totalnjopbangunan, $t_grandtotalnjop, $t_grandtotalnjop_aphb);
                    //$idsptnya = $data['t_idspt'];
                }
                return $this->redirect()->toRoute('pendataan_sspd');
            } else {
                // var_dump($frm->getMessages());exit();
                if (!empty($datane['t_idspt'])) {
                    $frm->bind($dataki);
                    $frm->get("t_persyaratan")->setValue(\Zend\Json\Json::decode($dataki->t_persyaratan));
                }
            }
        }

        $view = new ViewModel(array(
            'frm' => $frm,
            'tarifbphtb' => $data_tarif['s_tarifbphtb'],
            'show_hide_combo_notaris' => $show_hide_combo_notaris,
            'idtarifbphtb' => $data_tarif['s_idtarifbphtb'],
            'notaris' => $notaris,
            's_namauserrole' => $session['s_tipe_pejabat']
        ));

        $data = array(
            'menu_pendataan' => 'active',
            'role_id' => $session['s_akses'],
            'data_pemda' => $ar_pemda,
            'aturgambar' => 2,
            'username' => $session['s_username']
        );
        $this->layout()->setVariables($data);
        return $view;
    }

    // Edit Permohonan BPHTB
    // Lokasi : Permohonan BPHTB
    public function editAction()
    {
        $ar_pemda = $this->getPemda()->getdata();
        $session = $this->getServiceLocator()
            ->get('EtaxService')
            ->getStorage()
            ->read();
        if (($session['s_namauserrole'] == "Administrator") || ($session['s_namauserrole'] == "Pegawai") || ($session['s_tipe_pejabat'] == 1)) {
            $show_hide_combo_notaris = "display:inherit;";
            $notaris = 2;
        } elseif ($session['s_tipe_pejabat'] == 2) {
            //if ($session['s_namauserrole'] == "Notaris") {
            $show_hide_combo_notaris = "display:none;";
            $notaris = 1;
        }
        $string = Rand::getString(6, 'abcdefghijklmnopqrstuvwxyz-1234567890_ABCDEFGHIJKLMNOPQRSTUVWXYZ', true);
        $frm = new \Bphtb\Form\Pendataan\SSPDFrm($this->populateComboJenisTransaksi(), $this->populateComboHakTanah(), $this->populateComboDokTanah(), $string, $this->populateComboNotaris());
        $req = $this->getRequest();
        if ($req->isGet()) {
            $id = (int) $req->getQuery()->get('t_idspt');
            $data = $this->getTblSSPDBphtb()->getDataId($id);
            $datahistory = $this->getTblSSPDBphtb()->gethistorybphtb($data->t_nikwppembeli, $data->t_idspt, $data->t_periodespt);
            if ($data->t_idjenistransaksi == 5) {
                $show_hide_penerima_waris = "display:inherit;";
            } else {
                $show_hide_penerima_waris = "display:none;";
            }
            $data_penerimawaris = $this->getTblWaris()->CariPenerimaWaris($data->t_idspt);
            $data->t_tglprosesspt = date('d-m-Y', strtotime($data->t_tglprosesspt));
            $data->t_tglajb = date('d-m-Y', strtotime($data->t_tglajb));
            $t_luastanah = str_ireplace('.', '', $data->t_luastanah);
            $t_luasbangunan = str_ireplace('.', '', $data->t_luasbangunan);
            $data->t_luastanah = number_format(($t_luastanah / 100), 0, ',', '.');
            $data->t_luasbangunan = number_format(($t_luasbangunan / 100), 0, ',', '.');
            $frm2 = new \Bphtb\Form\Pendataan\SSPDFrm($this->populateComboJenisTransaksi(), $this->populateComboHakTanah(), $this->populateComboDokTanah(), $string, $this->populateComboNotaris(), $this->populatePersyaratanId($data->t_idjenistransaksi));
            $frm2->bind($data);
            $frm2->get("t_terbukti")->setValue(\Zend\Json\Json::decode($data->t_terbukti));
            $frm2->get("t_persyaratan")->setValue(\Zend\Json\Json::decode($data->t_persyaratan));

            $data_tarif = $this->getTblSSPDBphtb()->temukanDataTarifBphtb_tahun($data->t_periodespt);
        }

        //========== panggil Model\Pendataan\SSPDBphtbTable
        $fr_tarif = $data->t_persenbphtb;
        $tahunproses = date('Y', strtotime($data->t_tglprosesspt));
        $ceknik = $this->getTblSPT()->ceknik($data->t_nikwppembeli, $tahunproses);
        if (!empty($ceknik['t_idspt'])) {
            $idceknikidspt = $ceknik['t_idspt'];
        } else {
            $idceknikidspt = '';
        }

        $view = new ViewModel(array(
            'frm' => $frm2,
            'data' => $data->t_terbukti,
            'show_hide_combo_notaris' => $show_hide_combo_notaris,
            'datahistory' => $datahistory,
            'show_hide_penerima_waris' => $show_hide_penerima_waris,
            'data_penerimawaris' => $data_penerimawaris,
            'idtarifbphtb' => $data_tarif['s_idtarifbphtb'],
            'notaris' => $notaris,
            't_tarif_pembagian_aphb_kali' => $data->t_tarif_pembagian_aphb_kali,
            't_tarif_pembagian_aphb_bagi' => $data->t_tarif_pembagian_aphb_bagi,
            't_grandtotalnjop_aphb' => $data->t_grandtotalnjop_aphb,
            'tarifbphtbsekarang' => $fr_tarif,
            'idceknikidspt' => $idceknikidspt,
            't_potongan_waris_hibahwasiat' => $data->t_potongan_waris_hibahwasiat
        ));

        $data = array(
            'menu_pendataan' => 'active',
            'role_id' => $session['s_akses'],
            'data_pemda' => $ar_pemda,
            'aturgambar' => 2,
            'username' => $session['s_username']
        );
        $this->layout()->setVariables($data);
        return $view;
    }

    public function viewdataAction()
    {
        $ar_pemda = $this->getPemda()->getdata();
        $session = $this->getServiceLocator()
            ->get('EtaxService')
            ->getStorage()
            ->read();
        $req = $this->getRequest();
        $data = [];
        $perhitungan = [];
        if ($req->isGet()) {
            $id = (int) $req->getQuery()->get('t_idspt');
            $data = $this->getTblSSPDBphtb()->getDataId_all($id);
            $data['t_tglprosesspt'] = date('d-m-Y', strtotime($data['t_tglprosesspt']));
            $data['t_tglajb'] = date('d-m-Y', strtotime($data['t_tglajb']));
            $perhitungan = $this->ControllerHelper()->hitungBphtbCetakan($data);
        }
        $view = new ViewModel(array(
            'datasspd' => $data,
            'perhitungan' => $perhitungan
        ));

        $data = array(
            'menu_pendataan' => 'active',
            'role_id' => $session['s_akses'],
            'data_pemda' => $ar_pemda,
            'aturgambar' => 2,
            'username' => $session['s_username']
        );
        $this->layout()->setVariables($data);
        return $view;
    }

    public function viewdata2Action()
    {
        $ar_pemda = $this->getPemda()->getdata();
        $session = $this->getServiceLocator()
            ->get('EtaxService')
            ->getStorage()
            ->read();
        $req = $this->getRequest();
        if ($req->isGet()) {
            $id = (int) $req->getQuery()->get('t_idspt');
            $data = $this->getTblSSPDBphtb()->getDataId($id);
            $data->t_tglprosesspt = date('d-m-Y', strtotime($data->t_tglprosesspt));
            $data->t_tglajb = date('d-m-Y', strtotime($data->t_tglajb));
            $t_luastanah = str_ireplace('.', '', $data->t_luastanah);
            $t_luasbangunan = str_ireplace('.', '', $data->t_luasbangunan);
            $data->t_luastanah = number_format(($t_luastanah / 100), 0, ',', '.');
            $data->t_luasbangunan = number_format(($t_luasbangunan / 100), 0, ',', '.');
        }
        $view = new ViewModel(array(
            'datasspd' => $data
        ));

        $data = array(
            'menu_pendataan' => 'active',
            'role_id' => $session['s_akses'],
            'data_pemda' => $ar_pemda,
            'aturgambar' => 2,
            'username' => $session['s_username']
        );
        $this->layout()->setVariables($data);
        return $view;
    }

    // Hapus Permohonan
    // Lokasi : index Pendataan
    public function HapusAction()
    {
        $frm = new \Bphtb\Form\Pendataan\SSPDFrm($this->populateComboJenisTransaksi(), $this->populateComboHakTanah());
        $req = $this->getRequest();
        $res = $this->getResponse();
        if ($req->isPost()) {
            $kb = new \Bphtb\Model\Pendataan\SSPDBphtbBase();
            $kb1 = new \Bphtb\Model\Pendataan\SPTBase();
            $frm->setInputFilter($kb->getInputFilter());
            $frm->setInputFilter($kb1->getInputFilter());
            $frm->setData($req->getPost());
            if (!$frm->isValid()) {
                $idspt = $req->getPost();
                $kb->exchangeArray($frm->getData());
                $kb1->exchangeArray($frm->getData());
                //=============== Model\Pendataan\SSPDBphtbTable
                $this->getTblSSPDBphtb()->hapusData($kb);
                $this->getTblSpt()->hapusDataSpt($kb1);
                $this->getTblSSPDBphtb()->hapusDataWaris($idspt->t_idspt);
            }
        }
        return $res;
    }

    public function hapusallAction()
    {
        $this->getTblSSPDBphtb()->hapusall($this->params("page"));
        return $this->getResponse();
    }

    // Mencari History Transaksi BPHTB
    // Lokasi : Tambah dan Edit Permohonan BPHTB
    public function historybphtbAction()
    {
        $frm = new \Bphtb\Form\Pendataan\SSPDFrm($this->populateComboJenisTransaksi(), $this->populateComboHakTanah(), $this->populateComboDokTanah());
        $req = $this->getRequest();
        $res = $this->getResponse();
        if ($req->isPost()) {
            $ex = new \Bphtb\Model\Pendataan\SSPDBphtbBase();
            $frm->setData($req->getPost());
            if (!$frm->isValid()) {
                $ex->exchangeArray($frm->getData());
                $posnya = $req->getPost();
                if (!empty($posnya->t_nikwppembeli)) {

                    $data = $this->getTblSSPDBphtb()->temukanDataHistory($ex);
                    if ($posnya->t_nikwppembeli == $data['t_nikpenerima']) { // ki kanggo waris jo paijo
                        $data['t_namawppembeli'] = $data['t_namapenerima'];
                        $data['t_alamatpembeli'] = $data['t_alamatpenerima'];
                        $data['t_npwpwppembeli'] = '';
                        $data['t_rtwppembeli'] = '';
                        $data['t_rwwppembeli'] = '';
                        $data['t_kelurahanwppembeli'] = '';
                        $data['t_kecamatanwppembeli'] = '';
                        $data['t_kabkotawppembeli'] = '';
                        $data['t_kodeposwppembeli'] = '';
                        $data['t_telponwppembeli'] = '';
                    }
                    $data_table = $this->getTblSSPDBphtb()->temukanDataHistory2($ex);
                    $html = "<table class='table table-striped'>";
                    $html .= "<tr>";
                    $html .= "<th>NOP</th>";
                    $html .= "<th>NPOP</th>";
                    $html .= "<th>NPOPTKP</th>";
                    $html .= "<th>NPOPKP</th>";
                    $html .= "<th>Tanggal Bayar</th>";
                    $html .= "<th>Jumlah Pembayaran</th>";
                    $html .= "</tr>";
                    foreach ($data_table as $row) {
                        $npopkp = $row['t_totalspt'] * 100 / 5;
                        if ($row['t_nilaitransaksispt'] > $row['t_grandtotalnjop']) {
                            $npop = $row['t_nilaitransaksispt'];
                        } else {
                            $npop = $row['t_grandtotalnjop'];
                        }
                        $html .= "<tr>";
                        $html .= "<td> " . $row['t_nopbphtbsppt'] . "-" . $row['t_thnsppt'] . " </td>";
                        $html .= "<td> " . number_format($npop, 0, ',', '.') . " </td>";
                        $html .= "<td> " . number_format($row['t_potonganspt'], 0, ',', '.') . " </td>";
                        $html .= "<td> " . number_format($npopkp, 0, ',', '.') . " </td>";
                        if (empty($row['t_tanggalpembayaran'])) {
                            $html .= "<td></td>";
                        } else {
                            $html .= "<td> " . date('d-m-Y', strtotime($row['t_tanggalpembayaran'])) . " </td>";
                        }
                        $html .= "<td> " . number_format($row['t_nilaipembayaranspt'], 0, ',', '.') . " </td>";
                        $html .= "</tr>";
                    }
                    $html .= "</table>";

                    $data['raw_return'] = $html;
                } else {
                    $data = array();
                }

                $res->setContent(\Zend\Json\Json::encode($data));
            }
        }
        return $res;
    }

    // Mencari Nilai NOP dari database PBB
    // Lokasi : Permohonan SSPD
    public function datanopAction()
    {
        $frm = new \Bphtb\Form\Pendataan\SSPDFrm($this->populateComboJenisTransaksi(), $this->populateComboHakTanah());
        $req = $this->getRequest();
        $res = $this->getResponse();
        if ($req->isPost()) {
            $ex = new \Bphtb\Model\Pendataan\SPPTBase();
            $frm->setData($req->getPost());
            if (!$frm->isValid()) {
                $ex->exchangeArray($frm->getData());
                //$formatnop = str_ireplace('.', '', $ex->t_nopbphtbsppt);
                //$ex->t_nopbphtbsppt = $formatnop;
                $data = $this->getTblSPPT()->temukanData($ex);
                $res->setContent(\Zend\Json\Json::encode($data));
            }
        }
        return $res;
    }

    public function TampilPersyaratanAction()
    {
        $frm = new \Bphtb\Form\Pendataan\SSPDFrm($this->populateComboJenisTransaksi(), $this->populateComboHakTanah(), $this->populateComboDokTanah());
        $req = $this->getRequest();
        $res = $this->getResponse();
        if ($req->isPost()) {
            $ex = new \Bphtb\Model\Pendataan\SSPDBphtbBase();
            $frm->setData($req->getPost());
            if (!$frm->isValid()) {
                $ex->exchangeArray($frm->getData());
                $data_table = $this->getTblSSPDBphtb()->temukanPersyaratan($req->getPost());
                $countpersyaratan = count($data_table);
                $i = 1;
                $html = "<div>";
                $html .= "<div class='col-sm-12'>
                              <div class='form-group'>
                                 <span  class='col-sm-2'></span>
                                    <div class='col-sm-3'>
                                        <input type='checkbox' id='CheckAll' name='CheckAll' onClick='modify_boxes($countpersyaratan)'> <span style='color:green'>Centang Semua</span>
                                    </div>
                              </div>
                          </div>";
                foreach ($data_table as $row) {
                    $html .= "<div class='col-sm-12'>
                              <div class='form-group'><label class='col-sm-2 control-label'></label>";
                    $html .= "<div class='col-sm-10'>";
                    $html .= "<input id='t_persyaratan' name='t_persyaratan[]' type='checkbox' value='" . $row['s_idpersyaratan'] . "'>  ";
                    $html .= $row['s_namapersyaratan'];
                    $html .= "</div>";
                    $html .= "</div>";
                    $html .= "</div>";
                    $i++;
                }
                $html .= "</div>";

                $data['raw_return'] = $html;

                $res->setContent(\Zend\Json\Json::encode($data));
            }
        }
        return $res;
    }

    // ============================ cetak data pendaftaran
    public function cetakdatapendaftaranAction()
    {
        $session = $this->getServiceLocator()->get('EtaxService')->getStorage()->read();
        $sm = $this->getServiceLocator();
        $this->tbl_sspd = $sm->get("SSPDTable");
        $req = $this->getRequest();
        if ($req->isGet()) {
            $data_get = $req->getQuery();
            // var_dump($data_get);exit();
            $getNamaNotaris = "SEMUA";
            if ($session['s_tipe_pejabat'] == 2) {
                $user = $this->getServiceLocator()->get("UserTable")->getUserId($session["s_iduser"]);
                $getNamaNotaris = $this->getServiceLocator()->get("NotarisBphtbTable")->getNamaNotaris($user->s_idpejabat_idnotaris)->current()["coalesce"];
            } else {
                if ($data_get["t_idnotarisspt"] != "") {
                    $user = $this->getServiceLocator()->get("UserTable")->getUserId($data_get["t_idnotarisspt"]);
                    $getNamaNotaris = $this->getServiceLocator()->get("NotarisBphtbTable")->getNamaNotaris($user->s_idpejabat_idnotaris)->current()["coalesce"];
                }
            }

            //========================= Model\Pencetakan\SSPDTable
            $ar_DataVerifikasi = $this->tbl_sspd->getDataPendaftaran($data_get->periode_spt, $data_get->tgl_verifikasi1, $data_get->tgl_verifikasi2, $session['s_iduser'], $data_get->t_idnotarisspt, $session['s_tipe_pejabat'], $data_get);
            $ar_tglcetak = $data_get->tgl_cetak;
            $ar_periodespt = $data_get->periode_spt;

            $sm = $this->getServiceLocator();
            $this->tbl_pemda = $sm->get("PemdaTable");
            $ar_pemda = $this->tbl_pemda->getdata();

            $view = new ViewModel(array(
                'data_Verifikasi' => $ar_DataVerifikasi,
                'tgl_cetak' => $ar_tglcetak,
                'periode_spt' => $ar_periodespt,
                'tgl_verifikasi1' => $data_get->tgl_verifikasi1,
                'tgl_verifikasi2' => $data_get->tgl_verifikasi2,
                'nama_login' => $session['s_namauserrole'],
                'nama_notaris' => $getNamaNotaris,
                'ar_pemda' => $ar_pemda,
                'cetak' => $data_get->cetak
            ));
            $data = array(
                'nilai' => '3' //no layout
            );
            $this->layout()->setVariables($data);
            return $view;
        }
    }

    public function cetakdatapendaftaranpernotarisAction()
    {
        $session = $this->getServiceLocator()
            ->get('EtaxService')
            ->getStorage()
            ->read();

        $sm = $this->getServiceLocator();
        $this->tbl_sspd = $sm->get("SSPDTable");

        $req = $this->getRequest();
        if ($req->isGet()) {
            $data_get = $req->getQuery();

            //========================= Model\Pencetakan\SSPDTable
            $ar_DataVerifikasi = $this->tbl_sspd->getDataPendaftaran($data_get->periode_spt, $data_get->tgl_verifikasi1, $data_get->tgl_verifikasi2, $session['s_iduser'], $data_get->t_idnotarisspt, $session['s_tipe_pejabat']);
            $ar_tglcetak = $data_get->tgl_cetak;
            $ar_periodespt = $data_get->periode_spt;
            $sk = $this->getServiceLocator();
            $this->tbl_notaris = $sk->get("NotarisBphtbTable");
            $ar_notaris = $this->tbl_notaris->getNamaNotaris($data_get->t_idnotarisspt);

            $sm = $this->getServiceLocator();
            $this->tbl_pemda = $sm->get("PemdaTable");
            $ar_pemda = $this->tbl_pemda->getdata();

            /* $pdf = new \DOMPDFModule\View\Model\PdfModel();
              $pdf->setOption('filename', 'DataPendaftaran.pdf');
              $pdf->setOption('paperSize', 'legal');
              $pdf->setOption('paperOrientation', 'landscape');
              $pdf->setVariables(array(
              'data_Verifikasi' => $ar_DataVerifikasi,
              'tgl_cetak' => $ar_tglcetak,
              'periode_spt' => $ar_periodespt,
              'tgl_verifikasi1' => $data_get->tgl_verifikasi1,
              'tgl_verifikasi2' => $data_get->tgl_verifikasi2,
              'nama_login' => $session['s_namauserrole'],
              'ar_pemda' => $ar_pemda
              ));
              return $pdf; */
            $view = new ViewModel(array(
                'data_Verifikasi' => $ar_DataVerifikasi,
                'tgl_cetak' => $ar_tglcetak,
                'periode_spt' => $ar_periodespt,
                'tgl_verifikasi1' => $data_get->tgl_verifikasi1,
                'tgl_verifikasi2' => $data_get->tgl_verifikasi2,
                'nama_login' => $session['s_namauserrole'],
                'nama_notaris' => $ar_notaris,
                'ar_pemda' => $ar_pemda,
                'cetak' => $data_get->cetak
            ));
            $data = array(
                'nilai' => '3' //no layout
            );
            $this->layout()->setVariables($data);
            return $view;
        }
    }

    public function formcetakdatapendaftaranAction()
    {
        $ar_pemda = $this->getPemda()->getdata();
        $session = $this->getServiceLocator()
            ->get('EtaxService')
            ->getStorage()
            ->read();
        $show_hide_combo_notaris = "display:none;";
        $frm = new SSPDFrm(null, null, null, null, $this->populateComboNotaris());
        if (($session['s_namauserrole'] == "Administrator") || ($session['s_namauserrole'] == "Pegawai") || ($session['s_tipe_pejabat'] == 1)) {
            $show_hide_combo_notaris = "";
        } elseif ($session['s_tipe_pejabat'] == 2) {
            //if ($session['s_namauserrole'] == "Notaris") {
            $show_hide_combo_notaris = "display:none;";
        }
        $view = new ViewModel(array(
            'frm' => $frm,
            'show_hide_combo_notaris' => $show_hide_combo_notaris
        ));
        $data = array(
            'menu_pendataan' => 'active',
            'role_id' => $session['s_akses'],
            'data_pemda' => $ar_pemda,
            'aturgambar' => 2,
            'username' => $session['s_username']
        );
        $this->layout()->setVariables($data);
        return $view;
    }

    public function formcetakdatapendaftaranpernotarisAction()
    {
        $ar_pemda = $this->getPemda()->getdata();
        $session = $this->getServiceLocator()
            ->get('EtaxService')
            ->getStorage()
            ->read();
        $show_hide_combo_notaris = "display:none;";
        $frm = new SSPDFrm(null, null, null, null, $this->populateComboNotaris());
        if (($session['s_namauserrole'] == "Administrator") || ($session['s_namauserrole'] == "Pegawai") || ($session['s_tipe_pejabat'] == 1)) {
            $show_hide_combo_notaris = "";
        } elseif ($session['s_tipe_pejabat'] == 2) {
            //if ($session['s_namauserrole'] == "Notaris") {
            $show_hide_combo_notaris = "display:none;";
        }
        $view = new ViewModel(array(
            'frm' => $frm,
            'show_hide_combo_notaris' => $show_hide_combo_notaris
        ));
        $data = array(
            'menu_pendataan' => 'active',
            'role_id' => $session['s_akses'],
            'data_pemda' => $ar_pemda,
            'aturgambar' => 2,
            'username' => $session['s_username']
        );
        $this->layout()->setVariables($data);
        return $view;
    }

    // ============================ end cetak data pendaftaran
    // ============================ cetak data status berkas
    public function formcetakdatastatusberkasAction()
    {
        $ar_pemda = $this->getPemda()->getdata();
        $session = $this->getServiceLocator()
            ->get('EtaxService')
            ->getStorage()
            ->read();
        $show_hide_combo_notaris = "display:none;";
        $frm = new SSPDFrm(null, null, null, null, $this->populateComboNotaris());
        if (($session['s_namauserrole'] == "Administrator") || ($session['s_namauserrole'] == "Pegawai") || ($session['s_tipe_pejabat'] == 1)) {
            $show_hide_combo_notaris = "";
        } elseif ($session['s_tipe_pejabat'] == 2) {
            //if ($session['s_namauserrole'] == "Notaris") {
            $show_hide_combo_notaris = "display:none;";
        }
        $view = new ViewModel(array(
            'frm' => $frm,
            'show_hide_combo_notaris' => $show_hide_combo_notaris
        ));
        $data = array(
            'menu_pendataan' => 'active',
            'role_id' => $session['s_akses'],
            'data_pemda' => $ar_pemda,
            'aturgambar' => 2,
            'username' => $session['s_username']
        );
        $this->layout()->setVariables($data);
        return $view;
    }

    public function cetakdatastatusberkasAction()
    {
        $session = $this->getServiceLocator()
            ->get('EtaxService')
            ->getStorage()
            ->read();
        $sm = $this->getServiceLocator();
        $this->tbl_sspd = $sm->get("SSPDTable");
        $this->tbl_sspdsyarat = $sm->get("SSPDBphtbTable");
        $req = $this->getRequest();
        if ($req->isGet()) {
            $data_get = $req->getQuery();

            //========================= Model\Pencetakan\SSPDTable
            $ar_DataVerifikasi = $this->tbl_sspd->getDataStatusBerkas($data_get->periode_spt, $data_get->tgl_cetakstatusberkas1, $data_get->tgl_cetakstatusberkas2, $session['s_iduser'], $data_get->t_idnotarisspt, $session['s_tipe_pejabat']);
            $ar_tglcetak = $data_get->tgl_cetak;
            $ar_periodespt = $data_get->periode_spt;
            $this->tbl_pemda = $sm->get("PemdaTable");
            $ar_pemda = $this->tbl_pemda->getdata();
            $datahasil = array();
            foreach ($ar_DataVerifikasi as $key => $v) {
                // array_push($datahasil, $v);
                $result_array_syarat = \Zend\Json\Json::decode($v['t_persyaratan']);
                $jml_syarat = count($result_array_syarat);
                $result_array_verifikasi = \Zend\Json\Json::decode($v['t_verifikasispt']);
                $jml_verifikasi = count($result_array_verifikasi);
                $cektabelpersyaratan = $this->tbl_sspdsyarat->jumlahsyarat($v['t_idjenistransaksi']);
                if ($jml_syarat == $cektabelpersyaratan) {
                    $v['ceksyarat'] = 1;
                } else {
                    $v['ceksyarat'] = 2;
                }

                if ($jml_syarat == $jml_verifikasi) {
                    $v['cekverifikasi'] = 1;
                } else {
                    if (empty($v['t_verifikasispt'])) {
                        $v['cekverifikasi'] = 3;
                    } else {
                        $v['cekverifikasi'] = 2;
                    }
                }
                array_push($datahasil, $v);
            }
            /* $pdf = new \DOMPDFModule\View\Model\PdfModel();
              $pdf->setOption('filename', 'DataPendaftaran.pdf');
              $pdf->setOption('paperSize', 'legal');
              $pdf->setOption('paperOrientation', 'landscape');
              $pdf->setVariables(array(
              'data_Verifikasi' => $datahasil,
              'tgl_cetak' => $ar_tglcetak,
              'periode_spt' => $ar_periodespt,
              'tgl_verifikasi1' => $data_get->tgl_cetakstatusberkas1,
              'tgl_verifikasi2' => $data_get->tgl_cetakstatusberkas2,
              'nama_login' => $session['s_namauserrole'],
              'ar_pemda' => $ar_pemda
              ));
              return $pdf; */
            $view = new ViewModel(array(
                'data_Verifikasi' => $datahasil,
                'tgl_cetak' => $ar_tglcetak,
                'periode_spt' => $ar_periodespt,
                'tgl_verifikasi1' => $data_get->tgl_cetakstatusberkas1,
                'tgl_verifikasi2' => $data_get->tgl_cetakstatusberkas2,
                'nama_login' => $session['s_namauserrole'],
                'ar_pemda' => $ar_pemda,
                'cetak' => $data_get->cetak
            ));
            $data1 = array(
                'nilai' => '3' //no layout
            );
            $this->layout()->setVariables($data1);
            return $view;
        }
    }

    // Semua Penghitungan BPHTB dari 13 Transaksi
    public function hitungBphtbAction()
    {
        $frm = new \Bphtb\Form\Pendataan\SSPDFrm($this->populateComboJenisTransaksi(), $this->populateComboHakTanah(), $this->populateComboDokTanah());
        $req = $this->getRequest();
        $res = $this->getResponse();
        if ($req->isPost()) {
            $ex = new \Bphtb\Model\Pendataan\SSPDBphtbBase();
            $post = $req->getPost();
            $frm->setData($post);
            if (!$frm->isValid()) {
                $ex->exchangeArray($frm->getData());

                if (!empty($ex->t_idjenistransaksi)) {
                    $post["t_luastanah"] = str_ireplace(".", "", $post["t_luastanah"]);
                    $post["t_luasbangunan"] = str_ireplace(".", "", $post["t_luasbangunan"]);
                    $data = $this->ControllerHelper()->hitungBphtb($post);
                } else {
                    $data = array();
                }

                $res->setContent(\Zend\Json\Json::encode($data));
            }
        }
        return $res;
    }

    // Menghitung nilai njop pbb
    public function hitungnjopAction()
    {
        $frm = new \Bphtb\Form\Pendataan\SSPDFrm($this->populateComboJenisTransaksi(), $this->populateComboHakTanah(), $this->populateComboDokTanah());
        $req = $this->getRequest();
        $res = $this->getResponse();
        if ($req->isPost()) {
            $ex = new \Bphtb\Model\Pendataan\SSPDBphtbBase();
            $frm->setData($req->getPost());
            if (!$frm->isValid()) {
                $ex->exchangeArray($frm->getData());
                $t_luastanah = str_ireplace(".", "", $ex->t_luastanah);
                $t_njoptanah = str_ireplace(".", "", $ex->t_njoptanah);
                $t_luasbangunan = str_ireplace(".", "", $ex->t_luasbangunan);
                $t_njopbangunan = str_ireplace(".", "", $ex->t_njopbangunan);

                $t_totalnjoptanah = $t_luastanah * $t_njoptanah;
                $t_totalnjopbangunan = $t_luasbangunan * $t_njopbangunan;
                $t_grandtotalnjop = $t_totalnjoptanah + $t_totalnjopbangunan;
                $data = array(
                    "t_totalnjoptanah" => $t_totalnjoptanah,
                    "t_totalnjopbangunan" => $t_totalnjopbangunan,
                    "t_grandtotalnjop" => $t_grandtotalnjop
                );
                if ($ex->t_idjenistransaksi != 1 || $ex->t_idjenistransaksi != 8) {
                    $data["t_nilaitransaksispt"] = $t_grandtotalnjop;
                }
                $res->setContent(\Zend\Json\Json::encode($data));
            }
        }
        return $res;
    }

    public function cetakpermohonanpenelitianAction()
    {
        $frm = new \Bphtb\Form\Pencetakan\SSPDFrm();
        $req = $this->getRequest();
        if ($req->isGet()) {
            $base = new \Bphtb\Model\Pencetakan\SSPDBase();
            $frm->setData($req->getQuery());
            if ($frm->isValid()) {
                $base->exchangeArray($frm->getData());
                $data_get = $req->getQuery();
                $ar_sspd = $this->getSSPD()->getdatapenelitian($data_get);
                $ar_pemda = $this->getPemda()->getdata();
            }
        }
        $pdf = new \DOMPDFModule\View\Model\PdfModel();
        $pdf->setOption('filename', 'PermohonanPenelitian.pdf');
        $pdf->setOption('paperSize', 'A4');
        $pdf->setOption('paperOrientation', 'potrait');
        $pdf->setVariables(array(
            'data_sspd' => $ar_sspd,
            'data_pemda' => $ar_pemda,
            'tgl_cetak' => $data_get->tgl_cetak_penelitian
        ));
        return $pdf;
    }

    public function cetaksspdbphtbAction()
    {
        $session = $this->getServiceLocator()->get('EtaxService')->getStorage()->read();
        $frm = new \Bphtb\Form\Pencetakan\SSPDFrm();
        $req = $this->getRequest();
        if ($req->isGet()) {
            $base = new \Bphtb\Model\Pencetakan\SSPDBase();
            $frm->setData($req->getQuery());
            // $ar_sebelum = "";
            $perhitungan = [];
            if ($frm->isValid()) {
                $base->exchangeArray($frm->getData());
                $app_path = $this->getServiceLocator()->get('app_path') . "/";
                $data_get = $req->getQuery();


                $ar_sspd = $this->getSSPD()->getviewcetakssp($data_get->t_idspt);

                // APABILA BELUM DIBAYAR AKAN MUNCUL KODEBAAYR
                // && $session['s_akses'] == 3
                // sebelumnya khusus notaris, sekarang untuk semua
                if ($ar_sspd[0]["t_tanggalpembayaran"] == null) {
                    return $this->redirect()->toRoute('pendataan_sspd', [
                        'action' => 'cetakkodebayar'
                    ], ['query' => [
                        'no_spt1' => $ar_sspd[0]['t_kohirspt'],
                        'periode_spt' => $ar_sspd[0]['t_periodespt']
                    ]]);
                }

                // APABILA BELUM KONFIRM ESPOP
                // akan dibawa ke form konfirm espop
                if ($ar_sspd[0]['t_konfirm_espop'] == null) {
                    // return $this->redirect()->toRoute('pendataan_sspd', [
                    //   'action' => 'input-spop',
                    //  'page' => $ar_sspd[0]['t_idspt']
                    // ]);
                }

                // APABILA SUDAH TTE
                $dataTte = $this->getServiceLocator()->get("PenandatangananTable")->getDataByIdSpt($data_get->t_idspt);
                if ($dataTte) {
                    return $this->redirect()->toRoute("dokumen", ["action" => "download", "id" => $dataTte["guid"]]);
                }

                if ($data_get->action == 'cetaksspd') { // nyetak sspd dari tombol cetak dalam tabel grid
                    //=============== Model\Pencetakan\SSPDTable

                    $perhitungan = $this->ControllerHelper()->hitungBphtbCetakan($ar_sspd[0]);
                    $dataidsptsebelum = $this->getSSPD()->getdataidsptsebelumnya($data_get->t_idspt);
                    // if (!empty($dataidsptsebelum['t_idsptsebelumnya'])) { // jika ada spt sebelumnya yang dikoreksi bpn
                    //     $ar_sebelum = $this->getSSPD()->getdatassspdsebelumnya($dataidsptsebelum['t_idsptsebelumnya']);
                    // }
                    $ar_tglcetak = date('d-m-Y');
                } else { // nyetak sspd yang dari pop up modal
                    $ar_sspd = $this->getSSPD()->getdatasspd($base);
                    $ar_tglcetak = $base->tgl_cetak;
                }
                $ar_pemda = $this->getPemda()->getdata();

                $ar_Mengetahui = (array) $this->getTblPejabat()->getdataid($data_get->mengetahuibphtbsspd);
            }
        }
        // $pdf = new \DOMPDFModule\View\Model\PdfModel();
        // // $pdf->setOption('filename', 'SSPD.pdf');
        // $pdf->setOption('paperSize', 'legal');
        // $pdf->setOption('paperOrientation', 'potrait');
        // $pdf->setVariables(array(
        //     'app_path' => $app_path,
        //     'data_sspd' => $ar_sspd,
        //     'data_pemda' => $ar_pemda,
        //     'tgl_cetak' => $ar_tglcetak,
        //     // 'ar_sebelum' => $ar_sebelum,
        // 'dataidsptsebelum' => $dataidsptsebelum,
        //     'data_mengetahui' => $ar_Mengetahui,
        //     'cetakversi' => $data_get->cetakversi,
        //     'perhitungan' => $perhitungan,
        // ));
        // return $pdf;
        // Debug::dump($ar_sspd);
        // exit;
        $array = [
            'app_path' => $app_path,
            'data_sspd' => $ar_sspd,
            'data_pemda' => $ar_pemda,
            'tgl_cetak' => $ar_tglcetak,
            'data_mengetahui' => $ar_Mengetahui,
            'cetakversi' => $data_get->cetakversi,
            'perhitungan' => $perhitungan,
            'cekurl' => $this->cekurl(),
            'dataidsptsebelum' => $dataidsptsebelum,
        ];

        require_once 'public/cetakan/MPDF57/mpdf.php';
        $mpdf = new mPDF('utf-8', 'Legal', 0, '', 5, 5, 5, 5, 5, 5, '');
        $html = $this->CetakSSPD()->cetakSSPD(1, $array);
        // 1 DOKUMEN TANPA ESIGN
        // 2 DOKUMEN DENGAN ESIGN
        $mpdf->WriteHTML($html);
        return $mpdf->Output();
    }

    public function cetaktandaterimaAction()
    {
        $session = $this->getServiceLocator()->get('EtaxService')->getStorage()->read();
        $frm = new \Bphtb\Form\Pencetakan\SSPDFrm();
        $req = $this->getRequest();
        // if ($req->isGet()) {
        $base = new \Bphtb\Model\Pencetakan\SSPDBase();
        $request = $req->getQuery();
        $ar_pemda = $this->getPemda()->getdata();
        $pelayanan = $this->getServiceLocator()->get("EspopTable")->getdatabyidpelayanan($request['t_idspt']);
        $wpLama = $this->getServiceLocator()->get("EspopTable")->getdataWpLama($request['t_idspt']);
        $opLama = $this->getServiceLocator()->get("EspopTable")->getdataOpLama($request['t_idspt']);
        $wpBaru = $this->getServiceLocator()->get("EspopTable")->getdataWpBaru($request['t_idspt']);

        if (in_array($pelayanan['t_id_jenis_pelayanan'], [2, 3])) {

            foreach ($wpBaru as $key => $v) {
                $opBaru = $this->getServiceLocator()->get("EspopTable")->getdataOpBaru($v['t_id_op']);
                $luas = $this->getServiceLocator()->get("EspopTable")->getdataBangunan($v['t_id_op']);

                $databaru = [
                    't_id_wp' => $v['t_id_wp'],
                    't_id_pelayanan' => $v['t_id_pelayanan'],
                    't_nop' => $v['t_nop'],
                    't_nama_wp' => $v['t_nama_wp'],
                    't_nik_wp' => $v['t_nik_wp'],
                    't_jalan_wp' => $v['t_jalan_wp'],
                    't_rt_wp' => $v['t_rt_wp'],
                    't_rw_wp' => $v['t_rw_wp'],
                    't_kelurahan_wp' => $v['t_kelurahan_wp'],
                    't_kecamatan_wp' => $v['t_kecamatan_wp'],
                    't_kabupaten_wp' => $v['t_kabupaten_wp'],
                    't_no_hp_wp' => $v['t_no_hp_wp'],
                    't_npwpd' => $v['t_npwpd'],
                    't_email' => $v['t_email'],
                    'created_by' => $v['created_by'],
                    't_blok_kav_wp' => $v['t_blok_kav_wp'],
                    't_id_op' => $v['t_id_op'],
                    'kd_propinsi' => $opBaru['kd_propinsi'],
                    'kd_dati2' => $opBaru['kd_dati2'],
                    'kd_kecamatan' => $opBaru['kd_kecamatan'],
                    'kd_kelurahan' => $opBaru['kd_kelurahan'],
                    'kd_blok' => $opBaru['kd_blok'],
                    'no_urut' => $opBaru['no_urut'],
                    'kd_jns_op' => $opBaru['kd_jns_op'],
                    't_jalan_op' => $opBaru['t_jalan_op'],
                    't_rt_op' => $opBaru['t_rt_op'],
                    't_rw_op' => $opBaru['t_rw_op'],
                    't_kelurahan_op' => $opBaru['t_kelurahan_op'],
                    't_kecamatan_op' => $opBaru['t_kecamatan_op'],
                    't_luas_tanah' => $opBaru['t_luas_tanah'],
                    't_luas_bangunan' => $opBaru['t_luas_bangunan'],
                    't_jenis_tanah' => $opBaru['t_jenis_tanah'],
                    't_kode_lookup_item' => $opBaru['t_kode_lookup_item'],
                    't_latitude' => $opBaru['t_latitude'],
                    't_longitude' => $opBaru['t_longitude'],
                    'created_by' => $opBaru['created_by'],
                    'created_at' => $opBaru['created_at'],
                    'updated_at' => $opBaru['updated_at'],
                    't_blok_kav' => $opBaru['t_blok_kav'],
                    't_kode_znt' => $opBaru['t_kode_znt'],
                    't_nomor_op' => $opBaru['t_nomor_op'],
                    't_nop_asal' => $opBaru['t_nop_asal'],
                    'jns_bumi' => $opBaru['jns_bumi'],
                    'luas_bng' => $luas['t_luas']
                ];
                $data[] = $databaru;
            }

            $dataLama = [
                't_id_wp_lama' => $wpLama['t_id_wp_lama'],
                't_id_pelayanan' => $wpLama['t_id_pelayanan'],
                't_nik_wp' => $wpLama['t_nik_wp'],
                't_nama_wp' => $wpLama['t_nama_wp'],
                't_jalan_wp' => $wpLama['t_jalan_wp'],
                't_rt_wp' => $wpLama['t_rt_wp'],
                't_rw_wp' => $wpLama['t_rw_wp'],
                't_kelurahan_wp' => $wpLama['t_kelurahan_wp'],
                't_kecamatan_wp' => $wpLama['t_kecamatan_wp'],
                't_kabupaten_wp' => $wpLama['t_kabupaten_wp'],
                't_no_hp_wp' => $wpLama['t_no_hp_wp'],
                't_blok_kav_wp' => $wpLama['t_blok_kav_wp'],
                'kd_propinsi' => $opLama['kd_propinsi'],
                'kd_dati2' => $opLama['kd_dati2'],
                'kd_kecamatan' => $opLama['kd_kecamatan'],
                'kd_kelurahan' => $opLama['kd_kelurahan'],
                'kd_blok' => $opLama['kd_blok'],
                'no_urut' => $opLama['no_urut'],
                'kd_jns_op' => $opLama['kd_jns_op'],
                't_jalan_op' => $opLama['t_jalan_op'],
                't_rt_op' => $opLama['t_rt_op'],
                't_rw_op' => $opLama['t_rw_op'],
                't_kelurahan_op' => $opLama['t_kelurahan_op'],
                't_kecamatan_op' => $opLama['t_kecamatan_op'],
                't_luas_tanah' => $opLama['t_luas_tanah'],
                't_luas_bangunan' => $opLama['t_luas_bangunan'],
                'created_by' => $opLama['created_by'],
                't_blok_kav' => $opLama['t_blok_kav'],
            ];
        } else {
            $dataLama = [
                't_id_wp_lama' => null,
                't_id_pelayanan' => null,
                't_nik_wp' => null,
                't_nama_wp' => null,
                't_jalan_wp' => null,
                't_rt_wp' => null,
                't_rw_wp' => null,
                't_kelurahan_wp' => null,
                't_kecamatan_wp' => null,
                't_kabupaten_wp' => null,
                't_no_hp_wp' => null,
                't_blok_kav_wp' => null,
                'kd_propinsi' => null,
                'kd_dati2' => null,
                'kd_kecamatan' => null,
                'kd_kelurahan' => null,
                'kd_blok' => null,
                'no_urut' => null,
                'kd_jns_op' => null,
                't_jalan_op' => null,
                't_rt_op' => null,
                't_rw_op' => null,
                't_kelurahan_op' => null,
                't_kecamatan_op' => null,
                't_luas_tanah' => null,
                't_luas_bangunan' => null,
                'created_by' => null,
                't_blok_kav' => null,
            ];
            $data = [[
                't_id_wp' => null,
                't_id_pelayanan' => null,
                't_nop' => null,
                't_nama_wp' => null,
                't_nik_wp' => null,
                't_jalan_wp' => null,
                't_rt_wp' => null,
                't_rw_wp' => null,
                't_kelurahan_wp' => null,
                't_kecamatan_wp' => null,
                't_kabupaten_wp' => null,
                't_no_hp_wp' => null,
                't_npwpd' => null,
                't_email' => null,
                'created_by' => null,
                't_blok_kav_wp' => null,
                't_id_op' => null,
                'kd_propinsi' => null,
                'kd_dati2' => null,
                'kd_kecamatan' => null,
                'kd_kelurahan' => null,
                'kd_blok' => null,
                'no_urut' => null,
                'kd_jns_op' => null,
                't_jalan_op' => null,
                't_rt_op' => null,
                't_rw_op' => null,
                't_kelurahan_op' => null,
                't_kecamatan_op' => null,
                't_luas_tanah' => null,
                't_luas_bangunan' => null,
                't_jenis_tanah' => null,
                't_kode_lookup_item' => null,
                't_latitude' => null,
                't_longitude' => null,
                'created_by' => null,
                'created_at' => null,
                'updated_at' => null,
                't_blok_kav' => null,
                't_kode_znt' => null,
                't_nomor_op' => null,
                't_nop_asal' => null,
                'jns_bumi' => null,
            ]];
        }

        // $ar_sebelum = "";


        // $ar_Mengetahui = (array) $this->getTblPejabat()->getdataid($data_get->mengetahuibphtbsspd);
        // }

        $array = [
            'data_pemda' => $ar_pemda,
            'cetakversi' => $request['cetakversi'],
            'pelayanan' => $pelayanan,
            'dataLama' => $dataLama,
            'dataBaru' => $data,
            'wpLama' => $wpLama,
            'wpBaru' => $wpBaru,
        ];
        // debug::dump($pelayanan);
        // debug::dump($dataLama);
        // debug::dump($data);
        // debug::dump($wpLama);
        // debug::dump($wpBaru);
        // exit;

        // require_once 'public/cetakan/MPDF57/mpdf.php';
        // $mpdf = new mPDF('utf-8', 'Legal', 0, '', 5, 5, 5, 5, 5, 5, '');
        // $html = $this->CetakSSPD()->cetakSSPD(1, $array);
        // $mpdf->WriteHTML($html);
        $pdf = new \DOMPDFModule\View\Model\PdfModel();
        // $pdf->setOption('filename', 'KodeBayar');
        $pdf->setOption('paperSize', 'A4');
        $pdf->setOption('paperOrientation', 'potrait');
        $pdf->setVariables(array(
            'data_pemda' => $ar_pemda,
            'cetakversi' => $request['cetakversi'],
            'pelayanan' => $pelayanan,
            'dataLama' => $dataLama,
            'dataBaru' => $data,
            'wpLama' => $wpLama,
            'wpBaru' => $wpBaru,
        ));
        return $pdf;
        // return $mpdf->Output();
    }

    public function cetakkodebayarAction()
    {
        // $session = $this->getServiceLocator()->get('EtaxService')->getStorage()->read();
        $frm = new \Bphtb\Form\Pencetakan\SSPDFrm();
        $req = $this->getRequest();
        if ($req->isGet()) {
            $base = new \Bphtb\Model\Pencetakan\SSPDBase();
            $frm->setData($req->getQuery());
            if ($frm->isValid()) {
                $base->exchangeArray($frm->getData());
                $data_get = $req->getQuery();

                //=============== Model\Pencetakan\SSPDTable
                $ar_sebelum = "";
                $ar_sspd = $this->getSSPD()->ambildatakodebayardaripendaftaran($data_get);

                $dataidsptsebelum = $this->getSSPD()->getdataidsptsebelumnya($ar_sspd['t_idspt']);
                if (!empty($dataidsptsebelum['t_idsptsebelumnya'])) { // jika ada spt sebelumnya yang dikoreksi bpn
                    $ar_sebelum = $this->getSSPD()->getdatassspdsebelumnya($dataidsptsebelum['t_idsptsebelumnya']);
                }
                // Debug::dump($ar_sebelum);
                // exit;

                $ar_tglcetak = $base->tgl_cetak;
                $ar_pemda = $this->getPemda()->getdata();
            }
        }
        
        $pdf = new \DOMPDFModule\View\Model\PdfModel();
        // $pdf->setOption('filename', 'KodeBayar');
        $pdf->setOption('paperSize', 'legal');
        $pdf->setOption('paperOrientation', 'potrait');
        $pdf->setVariables(array(
            'data_sspd' => $ar_sspd,
            'tgl_cetak' => $ar_tglcetak,
            'data_pemda' => $ar_pemda,
            'ar_sebelum' => $ar_sebelum
        ));
        return $pdf;
    }

    public function cetaksspdbphtbtextonlyAction()
    {
        $frm = new \Bphtb\Form\Pencetakan\SSPDFrm();
        $req = $this->getRequest();
        if ($req->isGet()) {
            $base = new \Bphtb\Model\Pencetakan\SSPDBase();
            $frm->setData($req->getQuery());
            $ar_sebelum = "";
            if ($frm->isValid()) {
                $base->exchangeArray($frm->getData());
                $app_path = $this->getServiceLocator()->get('app_path') . "/";
                $data_get = $req->getQuery();
                if ($data_get->action == 'cetaksspd') { // nyetak sspd dari tombol cetak dalam tabel grid
                    //=============== Model\Pencetakan\SSPDTable
                    $ar_sspd = $this->getSSPD()->getviewcetakssp($data_get->t_idspt);
                    $dataidsptsebelum = $this->getSSPD()->getdataidsptsebelumnya($data_get->t_idspt);
                    if (!empty($dataidsptsebelum['t_idsptsebelumnya'])) { // jika ada spt sebelumnya yang dikoreksi bpn
                        $ar_sebelum = $this->getSSPD()->getdatassspdsebelumnya($dataidsptsebelum['t_idsptsebelumnya']);
                    }
                    $ar_tglcetak = date('d-m-Y');
                } else { // nyetak sspd yang dari pop up modal
                    $ar_sspd = $this->getSSPD()->getdatasspd($base);
                    $ar_tglcetak = $base->tgl_cetak;
                }
                $ar_pemda = $this->getPemda()->getdata();

                $ar_Mengetahui = $this->getTblPejabat()->getdataid($data_get->mengetahuibphtbsspd);
            }
        }
        $pdf = new \DOMPDFModule\View\Model\PdfModel();
        $pdf->setOption('filename', 'SSPD[TextOnly].pdf');
        $pdf->setOption('paperSize', 'legal');
        $pdf->setOption('paperOrientation', 'potrait');
        $pdf->setVariables(array(
            'app_path' => $app_path,
            'data_sspd' => $ar_sspd,
            'data_pemda' => $ar_pemda,
            'tgl_cetak' => $ar_tglcetak,
            'ar_sebelum' => $ar_sebelum,
            'dataidsptsebelum' => $dataidsptsebelum,
            'data_mengetahui' => $ar_Mengetahui,
            'cetakversi' => $data_get->cetakversi
        ));
        return $pdf;
    }

    public function cetakpengajuanobjekpajakAction()
    {
        $frm = new \Bphtb\Form\Pencetakan\SSPDFrm();
        $req = $this->getRequest();
        if ($req->isGet()) {
            $base = new \Bphtb\Model\Pencetakan\SSPDBase();
            $frm->setData($req->getQuery());
            $ar_sebelum = "";
            if ($frm->isValid()) {
                $base->exchangeArray($frm->getData());
                $app_path = $this->getServiceLocator()->get('app_path') . "/";
                $data_get = $req->getQuery();
                if ($data_get->action == 'cetaksspd_ppob') { // nyetak sspd dari tombol cetak dalam tabel grid
                    //=============== Model\Pencetakan\SSPDTable
                    $ar_sspd = $this->getSSPD()->getviewcetakssp_ppob($data_get->t_idspt);
                    $ar_tglcetak = date('d-m-Y');
                } else { // nyetak sspd yang dari pop up modal
                    $ar_sspd = $this->getSSPD()->getdatasspd_ppob($base);
                    $ar_tglcetak = $base->tgl_cetak;
                }

                $ar_pemda = $this->getPemda()->getdata();
            }
        }
        $pdf = new \DOMPDFModule\View\Model\PdfModel();
        $pdf->setOption('filename', 'PengajuanObjekPajak.pdf');
        $pdf->setOption('paperSize', 'A4');
        $pdf->setOption('paperOrientation', 'potrait');
        $pdf->setVariables(array(
            'app_path' => $app_path,
            'data_sspd' => $ar_sspd,
            'data_pemda' => $ar_pemda,
            'tgl_cetak' => $ar_tglcetak,
            'ar_sebelum' => $ar_sebelum,
            'cetakversi' => $data_get->cetakversi
        ));
        return $pdf;
    }

    public function cetakmutasiseluruhnyaAction()
    {
        $frm = new \Bphtb\Form\Pencetakan\SSPDFrm();
        $req = $this->getRequest();
        if ($req->isGet()) {
            $base = new \Bphtb\Model\Pencetakan\SSPDBase();
            $frm->setData($req->getQuery());
            if ($frm->isValid()) {
                $base->exchangeArray($frm->getData());
                $app_path = $this->getServiceLocator()->get('app_path') . "/";
                $data_get = $req->getQuery();
                if ($data_get->action == 'cetaksspd_mutasi') { // nyetak sspd dari tombol cetak dalam tabel grid
                    //=============== Model\Pencetakan\SSPDTable
                    $ar_sspd = $this->getSSPD()->getviewcetakssp_mutasi($data_get->t_idspt);
                    $ar_tglcetak = date('d-m-Y');
                } else { // nyetak sspd yang dari pop up modal
                    $ar_sspd = $this->getSSPD()->getdatasspd_mutasi($base);
                    $ar_tglcetak = $base->tgl_cetak;
                }
                $ar_pemda = $this->getPemda()->getdata();
            }
        }

        $pdf = new \DOMPDFModule\View\Model\PdfModel();
        $pdf->setOption('filename', 'MutasiSeluruhnya.pdf');
        $pdf->setOption('paperSize', 'A4');
        $pdf->setOption('paperOrientation', 'potrait');
        $pdf->setVariables(array(
            'app_path' => $app_path,
            'data_sspd' => $ar_sspd,
            'data_pemda' => $ar_pemda,
            'tgl_cetak' => $ar_tglcetak,
            'cetakversi' => $data_get->cetakversi
        ));

        return $pdf;
    }

    public function cetakspopAction()
    {
        $frm = new \Bphtb\Form\Pencetakan\SSPDFrm();
        $req = $this->getRequest();
        if ($req->isGet()) {
            $base = new \Bphtb\Model\Pencetakan\SSPDBase();
            $frm->setData($req->getQuery());
            if ($frm->isValid()) {
                $base->exchangeArray($frm->getData());
                $app_path = $this->getServiceLocator()->get('app_path') . "/";
                $data_get = $req->getQuery();
                if ($data_get->action == 'cetaksspd_spop') { // nyetak sspd dari tombol cetak dalam tabel grid
                    //=============== Model\Pencetakan\SSPDTable
                    $ar_sspd = $this->getSSPD()->getviewcetakssp_spop($data_get->t_idspt);
                    $ar_tglcetak = date('d-m-Y');
                } else { // nyetak sspd yang dari pop up modal
                    $ar_sspd = $this->getSSPD()->getdatasspd_spop($base);
                    $ar_tglcetak = $base->tgl_cetak;
                }
                $ar_pemda = $this->getPemda()->getdata();
            }
        }

        $pdf = new \DOMPDFModule\View\Model\PdfModel();
        $pdf->setOption('filename', 'SPOP.pdf');
        $pdf->setOption('paperSize', 'legal');
        $pdf->setOption('paperOrientation', 'potrait');
        $pdf->setVariables(array(
            'app_path' => $app_path,
            'data_sspd' => $ar_sspd,
            'data_pemda' => $ar_pemda,
            'tgl_cetak' => $ar_tglcetak,
        ));

        return $pdf;
    }

    public function cetakbuktipenerimaanAction()
    {
        $session = $this->getServiceLocator()->get('EtaxService')->getStorage()->read();
        $frm = new \Bphtb\Form\Pencetakan\SSPDFrm();
        $req = $this->getRequest();
        if ($req->isGet()) {
            $base = new \Bphtb\Model\Pencetakan\SSPDBase();
            $frm->setData($req->getQuery());
            if ($frm->isValid()) {
                $base->exchangeArray($frm->getData());
                // $app_path = $this->getServiceLocator()->get('app_path') . "/";
                $data_get = $req->getQuery();
                // if ($data_get->action == 'cetakbuktipenerimaan') {
                // $ar_sspd = $this->getSSPD()->getdatassspd($data_get->t_idspt);
                // $ar_tglcetak = date('d-m-Y');
                // } else {
                //=============== Model\Pencetakan\SSPDTable
                $ar_sspd = $this->getSSPD()->getdatasspdbuktipenerimaan($base, $session['s_iduser'], $session['s_tipe_pejabat']); //getdatasspd
                $ar_tglcetak = $base->tgl_cetak_bukti;
                // $hasildata = $this->getSSPD()->getdatasspd($base);
                // }
                $ar_pemda = $this->getPemda()->getdata();
            }
        }
        /*
         * $testarray = array();
         * foreach ($hasildata as $row) {
         * $combosyarat = $this->getTblPersyaratan()->comboBox($row['t_idjenistransaksi']);
         * array_push($testarray, $combosyarat);
         * }
         */
        $pdf = new \DOMPDFModule\View\Model\PdfModel();
        $pdf->setOption('filename', 'BuktiPenerimaan.pdf');
        $pdf->setOption('paperSize', 'A4');
        $pdf->setOption('paperOrientation', 'potrait');
        $pdf->setVariables(array(
            // 'app_path' => $app_path,
            'data_sspd' => $ar_sspd,
            'data_pemda' => $ar_pemda
        ));
        // 'testarray' => $testarray,
        // 'combosyarat' => $combosyarat,

        return $pdf;
    }

    // coba
    public function cetakcobaAction()
    {
        $session = $this->getServiceLocator()->get('EtaxService')->getStorage()->read();
        $frm = new \Bphtb\Form\Pencetakan\COBAFrm();
        $req = $this->getRequest();
        if ($req->isGet()) {
            $base = new \Bphtb\Model\Pencetakan\COBABase();
            $frm->setData($req->getQuery());
            if ($frm->isValid()) {
                $base->exchangeArray($frm->getData());

                $data_get = $req->getQuery();


                //=============== Model\Pencetakan\SSPDTable
                $ar_sspd = $this->getCOBA()->getdatasspdcoba($base, $session['s_iduser'], $session['s_tipe_pejabat']); //getdatasspd
                $ar_tglcetak = $base->tgl_cetak_bukti;

                $ar_pemda = $this->getPemda()->getdata();
            }
        }

        $pdf = new \DOMPDFModule\View\Model\PdfModel();
        $pdf->setOption('filename', 'cetakcoba.pdf');
        $pdf->setOption('paperSize', 'A4');
        $pdf->setOption('paperOrientation', 'potrait');
        $pdf->setVariables(array(
            'data_sspd' => $ar_sspd,
            'data_pemda' => $ar_pemda
        ));

        return $pdf;
    }

    // end coba
    //======================= upload file syarat
    public function uploadfilesyaratAction()
    {
        $ar_pemda = $this->getPemda()->getdata();
        $session = $this->getServiceLocator()
            ->get('EtaxService')
            ->getStorage()
            ->read();
        $req = $this->getRequest();
        if ($req->isGet()) {
            $id = (int) $req->getQuery()->get('t_idspt');
            $data = $this->getTblSSPDBphtb()->getDataId_all($id);
            $data['t_tglprosesspt'] = date('d-m-Y', strtotime($data['t_tglprosesspt']));
            $data['t_tglajb'] = date('d-m-Y', strtotime($data['t_tglajb']));
            $perhitungan = $this->ControllerHelper()->hitungBphtbCetakan($data);
            //=========== persyaratan
            $combosyarat = $this->getTblPersyaratan()->syaratfileupload($data['t_idjenistransaksi'], $data['t_idspt']);
        }
        $view = new ViewModel(array(
            'datasspd' => $data,
            'perhitungan' => $perhitungan,
            'combosyarat' => $combosyarat
        ));

        $data = array(
            'menu_pendataan' => 'active',
            'role_id' => $session['s_akses'],
            'data_pemda' => $ar_pemda,
            'aturgambar' => 2,
            'username' => $session['s_username']
        );
        $this->layout()->setVariables($data);
        return $view;
    }

    public function uploadjenisyaratAction()
    {
        $ar_pemda = $this->getPemda()->getdata();
        $session = $this->getServiceLocator()
            ->get('EtaxService')
            ->getStorage()
            ->read();
        $req = $this->getRequest();
        if ($req->isGet()) {
            $id = (int) $req->getQuery()->get('t_idspt');
            $data = $this->getTblSSPDBphtb()->getDataId_all($id);
            $data['t_tglprosesspt'] = date('d-m-Y', strtotime($data['t_tglprosesspt']));
            $data['t_tglajb'] = date('d-m-Y', strtotime($data['t_tglajb']));
            $t_luastanah = str_ireplace('.', '', $data['t_luastanah']);
            $t_luasbangunan = str_ireplace('.', '', $data['t_luasbangunan']);
            $data['t_luastanah'] = number_format(($t_luastanah / 100), 0, ',', '.');
            $data['t_luasbangunan'] = number_format(($t_luasbangunan / 100), 0, ',', '.');

            //=========== persyaratan
            $combosyarat = $this->getTblPersyaratan()->syaratfileupload($data['t_idjenistransaksi'], $data['t_idspt']);
            $idyyarat = $req->getQuery()->get('syarat');
            $namasyarat = $this->getTblPersyaratan()->getDataId($idyyarat);
        }
        $view = new ViewModel(array(
            'datasspd' => $data,
            //'combosyarat' => $combosyarat,
            'idspt' => $req->getQuery()->get('t_idspt'),
            'idsyarat' => $req->getQuery()->get('syarat'),
            'namasyarat' => $namasyarat,
        ));


        $data = array(
            'menu_pendataan' => 'active',
            'role_id' => $session['s_akses'],
            'data_pemda' => $ar_pemda,
            'aturgambar' => 2,
            'username' => $session['s_username'],
            //'nilai' => '3' //no layout
        );
        $this->layout()->setVariables($data);
        return $view;
    }

    public function uploadfilesyarat2Action()
    {
        $ar_pemda = $this->getPemda()->getdata();
        $session = $this->getServiceLocator()
            ->get('EtaxService')
            ->getStorage()
            ->read();
        if (($session['s_namauserrole'] == "Administrator") || ($session['s_namauserrole'] == "Pegawai") || ($session['s_tipe_pejabat'] == 1)) {
            $show_hide_combo_notaris = "display:inherit;";
            $notaris = 2;
        } elseif ($session['s_tipe_pejabat'] == 2) {
            //if ($session['s_namauserrole'] == "Notaris") {
            $show_hide_combo_notaris = "display:none;";
            $notaris = 1;
        }
        $string = Rand::getString(6, 'abcdefghijklmnopqrstuvwxyz-1234567890_ABCDEFGHIJKLMNOPQRSTUVWXYZ', true);
        $frm = new \Bphtb\Form\Pendataan\SSPDFrmUpload($this->populateComboJenisTransaksi(), $this->populateComboHakTanah(), $this->populateComboDokTanah(), $string, $this->populateComboNotaris());
        $req = $this->getRequest();
        if ($req->isGet()) {
            $id = (int) $req->getQuery()->get('t_idspt');
            $data = $this->getTblSSPDBphtb()->getDataId($id);
            $datahistory = $this->getTblSSPDBphtb()->gethistorybphtb($data->t_nikwppembeli, $data->t_idspt, $data->t_periodespt);
            if ($data->t_idjenistransaksi == 5) {
                $show_hide_penerima_waris = "display:inherit;";
            } else {
                $show_hide_penerima_waris = "display:none;";
            }
            $data_penerimawaris = $this->getTblWaris()->CariPenerimaWaris($data->t_idspt);
            $data->t_tglprosesspt = date('d-m-Y', strtotime($data->t_tglprosesspt));
            $data->t_tglajb = date('d-m-Y', strtotime($data->t_tglajb));
            $t_luastanah = str_ireplace('.', '', $data->t_luastanah);
            $t_luasbangunan = str_ireplace('.', '', $data->t_luasbangunan);
            $data->t_luastanah = number_format(($t_luastanah / 100), 0, ',', '.');
            $data->t_luasbangunan = number_format(($t_luasbangunan / 100), 0, ',', '.');
            $frm2 = new \Bphtb\Form\Pendataan\SSPDFrmUpload($this->populateComboJenisTransaksi(), $this->populateComboHakTanah(), $this->populateComboDokTanah(), $string, $this->populateComboNotaris(), $this->populatePersyaratanId($data->t_idjenistransaksi));
            $frm2->bind($data);
            $frm2->get("t_terbukti")->setValue(\Zend\Json\Json::decode($data->t_terbukti));
            $frm2->get("t_persyaratan")->setValue(\Zend\Json\Json::decode($data->t_persyaratan));

            $data_tarif = $this->getTblSSPDBphtb()->temukanDataTarifBphtb_tahun($data->t_periodespt);


            $combosyarat = $this->getTblPersyaratan()->syaratfileupload($data->t_idjenistransaksi, $data->t_idspt);
        }




        //========== panggil Model\Pendataan\SSPDBphtbTable


        $view = new ViewModel(array(
            'frm' => $frm2,
            'data' => $data->t_terbukti,
            'show_hide_combo_notaris' => $show_hide_combo_notaris,
            'datahistory' => $datahistory,
            'show_hide_penerima_waris' => $show_hide_penerima_waris,
            'data_penerimawaris' => $data_penerimawaris,
            'idtarifbphtb' => $data_tarif['s_idtarifbphtb'],
            'notaris' => $notaris,
            'datasspd' => $data,
            'combosyarat' => $combosyarat
        ));

        $data = array(
            'menu_pendataan' => 'active',
            'role_id' => $session['s_akses'],
            'data_pemda' => $ar_pemda,
            'aturgambar' => 2,
            'username' => $session['s_username']
        );
        $this->layout()->setVariables($data);
        return $view;
    }

    public function uploadsyaratpostAction()
    {
        $session = $this->getServiceLocator()->get('EtaxService')->getStorage()->read();
        $req = $this->getRequest();

        $helper = new UploadHelper();
        $validasi = $helper->validateAndUploadFile($req->getFiles()['files'][0]);
        if ($validasi['status'] == false) {
            echo $validasi['message'];
            exit;
        }
        //if ($req->isGet()) {
        $id = (int) $req->getQuery()->get('idspt');
        $idsyarat = (int) $req->getQuery()->get('idsyarat');
        if (!empty($id)) {

            $datacari = $this->getTblSSPDBphtb()->getDataId_all($id);
            $letak_dir1 = "public";
            $letak_dir2 = "upload";
            $letak_dir3 = "filetransaksi";
            $letak_dir4 = $datacari['t_periodespt'];
            $letak_dir5 = str_replace(' ', '', strtolower($datacari['s_namajenistransaksi']));
            $letak_dir6 = $datacari['t_idspt'];
            $letak_dir7 = $req->getQuery()->get('idsyarat');

            $letak_dir = $letak_dir1 . '/' . $letak_dir2 . '/' . $letak_dir3 . '/' . $letak_dir4 . '/' . $letak_dir5 . '/' . $letak_dir6 . '/' . $letak_dir7 . '/';
            $view = new ViewModel(array(
                'get' => $req,
                'letakfile' => $letak_dir,
                'idspt' => $id,
                'idsyarat' => $idsyarat,
                'datasspd' => $datacari
            ));

            $options2 = array(
                'delete_type' => 'POST',
            );

            $this->simpanfilepost($options2, $letak_dir, $id, $idsyarat, $datacari);

            // UPDATE SYARAT YG SUDAH DI UPLOAD
            $this->getServiceLocator()->get("SPTTable")->updateSptSyaratYgSudahUpload($datacari['t_idspt']);
        } else {
            $view = new ViewModel(array(
                'get' => $req,
            ));
        }

        $data = array(
            'menu_pendataan' => 'active',
            'role_id' => $session['s_akses'],
            'aturgambar' => 2,
            'username' => $session['s_username'],
            'nilai' => '3' //no layout
        );
        $this->layout()->setVariables($data);
        return $view;
    }

    // public function uploadsyaratpost2Action()
    // {
    //     //$session = new \Zend\Session\Container('user_session');
    //     $session = $this->getServiceLocator()->get('EtaxService')->getStorage()->read();

    //     $req = $this->getRequest();
    //     $newFile = "";


    //     $iddetailspt = $req->getPost('t_iddetailsptbphtb');

    //     if (!empty($iddetailspt)) {
    //         $post = array_merge_recursive($req->getPost()->toArray(), $req->getFiles()->toArray());
    //         $httpadapter = new \Zend\File\Transfer\Adapter\Http();
    //         $httpadapter->setDestination('public/upload/');
    //         //if ($httpadapter->receive($post["uploadsyarat"]["name"])) {
    //         //    $newFile = $httpadapter->getFileName();
    //         //}

    //         $sm = $this->getServiceLocator();
    //         $tbl_upload = $sm->get("UploadTable");

    //         //var_dump($newFile);
    //         //exit();
    //         //$this->tbl_upload->savedata($base, $newFile);

    //         for ($i = 1; $i <= (int) ($req->getPost('hdnLine')); $i++) {
    //             if ($req->getPost("fileUpload" . $i) != "") { //["name"]
    //                 $letak_dir1 = "public";
    //                 $letak_dir2 = "upload";
    //                 $letak_dir3 = "filetransaksi";
    //                 $letak_dir4 = $req->getPost('foldertahun');
    //                 $letak_dir5 = $req->getPost('foldertransaksi');
    //                 $letak_dir6 = $req->getPost('folderidspt');

    //                 $letak_dir = $letak_dir1 . '/' . $letak_dir2 . '/' . $letak_dir3 . '/' . $letak_dir4 . '/' . $letak_dir5 . '/' . $letak_dir6 . '/';
    //                 //mkdir("kampret/module/asem/src/anjir/Controller", 0777, true);

    //                 if (is_dir($letak_dir1) == false) {
    //                     mkdir("" . $letak_dir1 . "", 0777, true);  // Create directory if it does not exist
    //                 }
    //                 if (is_dir($letak_dir1 . '/' . $letak_dir2) == false) {
    //                     mkdir("" . $letak_dir1 . "/" . $letak_dir2, 0777, true);  // Create directory if it does not exist
    //                 }
    //                 if (is_dir($letak_dir1 . '/' . $letak_dir2 . '/' . $letak_dir3) == false) {
    //                     mkdir("" . $letak_dir1 . "/" . $letak_dir2 . "/" . $letak_dir3 . "", 0777, true);  // Create directory if it does not exist
    //                 }

    //                 if (is_dir($letak_dir1 . '/' . $letak_dir2 . '/' . $letak_dir3 . '/' . $letak_dir4) == false) {
    //                     mkdir("" . $letak_dir1 . "/" . $letak_dir2 . "/" . $letak_dir3 . "/" . $letak_dir4 . "", 0777, true);  // Create directory if it does not exist
    //                 }

    //                 if (is_dir($letak_dir1 . '/' . $letak_dir2 . '/' . $letak_dir3 . '/' . $letak_dir4) == false) {
    //                     mkdir("" . $letak_dir1 . "/" . $letak_dir2 . "/" . $letak_dir3 . "/" . $letak_dir4 . "", 0777, true);  // Create directory if it does not exist
    //                 }

    //                 if (is_dir($letak_dir1 . '/' . $letak_dir2 . '/' . $letak_dir3 . '/' . $letak_dir4 . '/' . $letak_dir5) == false) {
    //                     mkdir("" . $letak_dir1 . "/" . $letak_dir2 . "/" . $letak_dir3 . "/" . $letak_dir4 . "/" . $letak_dir5, 0777, true);  // Create directory if it does not exist
    //                 }

    //                 if (is_dir($letak_dir1 . '/' . $letak_dir2 . '/' . $letak_dir3 . '/' . $letak_dir4 . '/' . $letak_dir5 . '/' . $letak_dir6) == false) {
    //                     mkdir("" . $letak_dir1 . "/" . $letak_dir2 . "/" . $letak_dir3 . "/" . $letak_dir4 . "/" . $letak_dir5 . "/" . $letak_dir6, 0777, true);  // Create directory if it does not exist
    //                 }

    //                 if (move_uploaded_file($_FILES["fileUpload" . $i]["tmp_name"], "" . $letak_dir . "" . $_FILES["fileUpload" . $i]["name"])) {
    //                     echo $req->getPost("fileUpload" . $i);
    //                     chmod($letak_dir . "" . $_FILES["fileUpload" . $i]["name"], 0777, true);
    //                     $tbl_upload->savedata_syarat($req->getPost('idjenistransaksi'), $req->getPost('id_detailspt' . $i), $_FILES["fileUpload" . $i]["name"], $req->getPost('keterangan_file' . $i), $req->getPost('id_syarat' . $i), $letak_dir, $req->getPost('folderidspt'), $req->getPost('folderiddetailspt'));
    //                     //$strSQL = "INSERT INTO gallery ";
    //                     //$strSQL .="(GalleryName,Picture) VALUES ('".$_POST["txtGalleryName".$i]."','".$_FILES["fileUpload".$i]["name"]."')";
    //                     //mysql_query($strSQL);
    //                     //echo "Copy/Upload ".$_FILES["fileUpload".$i]["name"]." completed.<br>";
    //                 }
    //             }
    //         }
    //         //return $this->redirect()->toRoute('pendataan_sspd');
    //     }
    // }

    //======================= end upload file syarat

    function populatePersyaratanId($id)
    {
        $data = $this->getTblPersyaratan()->getDataIdTransaksis($id);
        $selectData = array();
        foreach ($data as $row) {
            $selectData[$row->s_idpersyaratan] = $row->s_namapersyaratan;
        }
        return $selectData;
    }

    public function populateComboJenisTransaksi()
    {
        $data = $this->getTblJenTran()->comboBox();
        $selectData = array();
        foreach ($data as $row) {
            $selectData[$row["s_idjenistransaksi"]] = $row["s_namajenistransaksi"];
        }
        return $selectData;
    }

    public function populateComboHakTanah()
    {
        $data = $this->getTblHakTan()->comboBox();
        $selectData = array();
        foreach ($data as $row) {
            $selectData[$row->s_idhaktanah] = $row->s_namahaktanah;
        }
        return $selectData;
    }

    public function populateComboDokTanah()
    {
        $data = $this->getTblDokTan()->comboBox();
        $selectData = array();
        foreach ($data as $row) {
            $selectData[$row->s_iddoktanah] = $row->s_namadoktanah;
        }
        return $selectData;
    }

    private function populateComboNotaris()
    {
        $data = $this->getNotaris()->getdataCombo();
        $selectData = array();
        foreach ($data as $row) {
            $selectData[$row['s_iduser']] = $row['s_namanotaris'];
        }
        return $selectData;
    }

    public function getSSPD()
    {
        if (!$this->tbl_sspd) {
            $sm = $this->getServiceLocator();
            $this->tbl_sspd = $sm->get("SSPDTable");
        }
        return $this->tbl_sspd;
    }

    // function coba
    public function getCOBA()
    {
        if (!$this->tbl_sspd) {
            $sm = $this->getServiceLocator();
            $this->tbl_sspd = $sm->get("SSPDTable");
        }
        return $this->tbl_sspd;
        //
    }

    public function getTblSSPDBphtb()
    {
        if (!$this->tbl_sspd) {
            $sm = $this->getServiceLocator();
            $this->tbl_sspd = $sm->get("SSPDBphtbTable");
        }
        return $this->tbl_sspd;
    }

    public function getTblNotaris()
    {
        if (!$this->tbl_notaris) {
            $sm = $this->getServiceLocator();
            $this->tbl_notaris = $sm->get("NotarisBphtbTable");
        }
        return $this->tbl_notaris;
    }

    public function getTblWaris()
    {
        if (!$this->tbl_sspdwaris) {
            $sm = $this->getServiceLocator();
            $this->tbl_sspdwaris = $sm->get("SPTWarisTable");
        }
        return $this->tbl_sspdwaris;
    }

    public function getTblSpt()
    {
        if (!$this->tbl_spt) {
            $sm = $this->getServiceLocator();
            $this->tbl_spt = $sm->get("SPTTable");
        }
        return $this->tbl_spt;
    }

    public function getTblSPPT()
    {
        if (!$this->tbl_nop) {
            $sm = $this->getServiceLocator();
            $this->tbl_nop = $sm->get('SPPTTable');
        }
        return $this->tbl_nop;
    }

    public function getPemda()
    {
        if (!$this->tbl_pemda) {
            $sm = $this->getServiceLocator();
            $this->tbl_pemda = $sm->get("PemdaTable");
        }
        return $this->tbl_pemda;
    }

    public function getTblPejabat()
    {
        if (!$this->tbl_pejabat) {
            $sm = $this->getServiceLocator();
            $this->tbl_pejabat = $sm->get("PejabatBphtbTable");
        }
        return $this->tbl_pejabat;
    }

    public function getNotaris()
    {
        if (!$this->tbl_notaris) {
            $sm = $this->getServiceLocator();
            $this->tbl_notaris = $sm->get('NotarisBphtbTable');
        }
        return $this->tbl_notaris;
    }

    public function getTblPersyaratan()
    {
        if (!$this->tbl_persyaratan) {
            $sm = $this->getServiceLocator();
            $this->tbl_persyaratan = $sm->get('PersyaratanTable');
        }
        return $this->tbl_persyaratan;
    }

    public function getTblJenTran()
    {
        if (!$this->tbl_jenistransaksi) {
            $sm = $this->getServiceLocator();
            $this->tbl_jenistransaksi = $sm->get('JenisTransaksiBphtbTable');
        }
        return $this->tbl_jenistransaksi;
    }

    public function getTblHakTan()
    {
        if (!$this->tbl_haktanah) {
            $sm = $this->getServiceLocator();
            $this->tbl_haktanah = $sm->get('HakTanahTable');
        }
        return $this->tbl_haktanah;
    }

    public function getTblDokTan()
    {
        if (!$this->tbl_doktanah) {
            $sm = $this->getServiceLocator();
            $this->tbl_doktanah = $sm->get('DokTanahTable');
        }
        return $this->tbl_doktanah;
    }

    //==================================================================================== class upload
    //public function __construct($options = null, $lokasifile = null, $idspt = null, $idsyarat = null, $datasspd = null, $initialize = true, $error_messages = null) {
    public function simpanfilepost($options = null, $lokasifile = null, $idspt = null, $idsyarat = null, $datasspd = null, $initialize = true, $error_messages = null)
    {

        //global $lokasifile
        //echo kampret();
        //$this->MenuHelper()->SimpanFileUpload('5', '12886', 'DSC03494.jpg', 'waris','26','public/upload/filetransaksi/2016/waris/13053/26/','13053','12886');

        $this->lokasifile = $lokasifile;
        $this->idspt = $idspt;
        $this->idsyarat = $idsyarat;
        $this->idjenistransaksi = $datasspd['t_idjenistransaksi'];
        $this->t_iddetailsptbphtb = $datasspd['t_iddetailsptbphtb'];
        $this->foldertahun = $datasspd['t_periodespt'];
        $this->s_namajenistransaksi = str_replace(' ', '', strtolower($datasspd['s_namajenistransaksi']));
        $this->folderidspt = $idspt;
        $this->folderiddetailspt = $datasspd['t_iddetailsptbphtb'];

        $this->response = array();
        $this->options = array(
            'script_url' => $this->get_full_url() . '/' . $this->basename($this->get_server_var('SCRIPT_NAME')),
            'upload_dir' => dirname($this->get_server_var('SCRIPT_FILENAME')) . '/' . $lokasifile . '', //'.$this->letakfile.' 
            'upload_url' => $this->get_full_url() . '/' . $lokasifile . '', //
            'input_stream' => 'php://input',
            'user_dirs' => false,
            'mkdir_mode' => 0755,
            'param_name' => 'files',
            // Set the following option to 'POST', if your server does not support
            // DELETE requests. This is a parameter sent to the client:
            'delete_type' => 'DELETE',
            'access_control_allow_origin' => '*',
            'access_control_allow_credentials' => false,
            'access_control_allow_methods' => array(
                'OPTIONS',
                'HEAD',
                'GET',
                'POST',
                'PUT',
                'PATCH',
                'DELETE'
            ),
            'access_control_allow_headers' => array(
                'Content-Type',
                'Content-Range',
                'Content-Disposition'
            ),
            'redirect_allow_target' => '/^' . preg_quote(
                parse_url($this->get_server_var('HTTP_REFERER'), PHP_URL_SCHEME)
                    . '://'
                    . parse_url($this->get_server_var('HTTP_REFERER'), PHP_URL_HOST)
                    . '/', // Trailing slash to not match subdomains by mistake
                '/' // preg_quote delimiter param
            ) . '/',
            // Enable to provide file downloads via GET requests to the PHP script:
            //     1. Set to 1 to download files via readfile method through PHP
            //     2. Set to 2 to send a X-Sendfile header for lighttpd/Apache
            //     3. Set to 3 to send a X-Accel-Redirect header for nginx
            // If set to 2 or 3, adjust the upload_url option to the base path of
            // the redirect parameter, e.g. '/files/'.
            'download_via_php' => false,
            // Read files in chunks to avoid memory limits when download_via_php
            // is enabled, set to 0 to disable chunked reading of files:
            'readfile_chunk_size' => 10 * 1024 * 1024, // 10 MiB
            // Defines which files can be displayed inline when downloaded:
            'inline_file_types' => '/\.(gif|jpe?g|png|pdf)$/i',
            // Defines which files (based on their names) are accepted for upload:
            // 'accept_file_types' => '/.+$/i',
            'accept_file_types' => '/\.(gif|jpe?g|png|pdf)$/i',
            // The php.ini settings upload_max_filesize and post_max_size
            // take precedence over the following max_file_size setting:
            'max_file_size' => 2000000,
            'min_file_size' => 1,
            // The maximum number of files for the upload directory:
            'max_number_of_files' => null,
            // Defines which files are handled as image files:
            'image_file_types' => '/\.(gif|jpe?g|png)$/i',
            // Use exif_imagetype on all files to correct file extensions:
            'correct_image_extensions' => false,
            // Image resolution restrictions:
            'max_width' => null,
            'max_height' => null,
            'min_width' => 1,
            'min_height' => 1,
            // Set the following option to false to enable resumable uploads:
            'discard_aborted_uploads' => true,
            // Set to 0 to use the GD library to scale and orient images,
            // set to 1 to use imagick (if installed, falls back to GD),
            // set to 2 to use the ImageMagick convert binary directly:
            'image_library' => 1,
            // Uncomment the following to define an array of resource limits
            // for imagick:
            // Command or path for to the ImageMagick convert binary:
            'convert_bin' => 'convert',
            // Uncomment the following to add parameters in front of each
            // ImageMagick convert call (the limit constraints seem only
            // to have an effect if put in front):
            // Command or path for to the ImageMagick identify binary:
            'identify_bin' => 'identify',
            'image_versions' => array(
                // The empty image version key defines options for the original image:
                '' => array(
                    // Automatically rotate images based on EXIF meta data:
                    'auto_orient' => true
                ),
                // Uncomment the following to create medium sized images:
                'thumbnail' => array(
                    // Uncomment the following to use a defined directory for the thumbnails
                    // instead of a subdirectory based on the version identifier.
                    // Make sure that this directory doesn't allow execution of files if you
                    // don't pose any restrictions on the type of uploaded files, e.g. by
                    // copying the .htaccess file from the files directory for Apache:
                    //'upload_dir' => dirname($this->get_server_var('SCRIPT_FILENAME')).'/thumb/',
                    //'upload_url' => $this->get_full_url().'/thumb/',
                    // Uncomment the following to force the max
                    // dimensions and e.g. create square thumbnails:
                    //'crop' => true,
                    'max_width' => 80,
                    'max_height' => 80
                )
            ),
            'print_response' => true
        );
        if ($options) {
            $this->options = $options + $this->options;
        }
        if ($error_messages) {
            $this->error_messages = $error_messages + $this->error_messages;
        }
        if ($initialize) {
            $this->initialize();
        }
    }

    protected function initialize()
    {
        /* $this->db = new mysqli(
          $this->options['db_host'],
          $this->options['db_user'],
          $this->options['db_pass'],
          $this->options['db_name']
          ); */
        //parent::initialize();
        //============ pindah kene
        switch ($this->get_server_var('REQUEST_METHOD')) {
            case 'OPTIONS':
            case 'HEAD':
                $this->head();
                break;
            case 'GET':
                $this->get($this->options['print_response']);
                break;
            case 'PATCH':
            case 'PUT':
            case 'POST':
                $this->post($this->options['print_response']);
                break;
            case 'DELETE':
                $this->delete($this->options['print_response']);
                break;
            default:
                $this->header('HTTP/1.1 405 Method Not Allowed');
        }

        //============ end pindah kene
        //$this->db->close();
    }

    protected function get_file_object($file_name)
    {
        if ($this->is_valid_file_object($file_name)) {
            $file = new \stdClass();
            $file->name = $file_name;
            $file->size = $this->get_file_size(
                $this->get_upload_path($file_name)
            );
            $file->url = $this->get_download_url($file->name);
            foreach ($this->options['image_versions'] as $version => $options) {
                if (!empty($version)) {
                    if (is_file($this->get_upload_path($file_name, $version))) {
                        $file->{$version . 'Url'} = $this->get_download_url(
                            $file->name,
                            $version
                        );
                    }
                }
            }
            $this->set_additional_file_properties($file);
            return $file;
        }
        return null;
    }

    protected function get_file_objects($iteration_method = 'get_file_object')
    {
        $upload_dir = $this->get_upload_path();
        if (!is_dir($upload_dir)) {
            return array();
        }
        return array_values(array_filter(array_map(
            array($this, $iteration_method),
            scandir($upload_dir)
        )));
    }

    protected function count_file_objects()
    {
        return count($this->get_file_objects('is_valid_file_object'));
    }

    protected function get_error_message($error)
    {
        return isset($this->error_messages[$error]) ?
            $this->error_messages[$error] : $error;
    }

    public function get_config_bytes($val)
    {
        $val = trim($val);
        $last = strtolower($val[strlen($val) - 1]);
        $val = (int) $val;
        switch ($last) {
            case 'g':
                $val *= 1024;
            case 'm':
                $val *= 1024;
            case 'k':
                $val *= 1024;
        }
        return $this->fix_integer_overflow($val);
    }

    protected function validate($uploaded_file, $file, $error, $index)
    {
        if ($error) {
            $file->error = $this->get_error_message($error);
            return false;
        }
        $content_length = $this->fix_integer_overflow(
            (int) $this->get_server_var('CONTENT_LENGTH')
        );
        $post_max_size = $this->get_config_bytes(ini_get('post_max_size'));
        if ($post_max_size && ($content_length > $post_max_size)) {
            $file->error = $this->get_error_message('post_max_size');
            return false;
        }
        if (!preg_match($this->options['accept_file_types'], $file->name)) {
            $file->error = $this->get_error_message('accept_file_types');
            return false;
        }
        if ($uploaded_file && is_uploaded_file($uploaded_file)) {
            $file_size = $this->get_file_size($uploaded_file);
        } else {
            $file_size = $content_length;
        }
        if (
            $this->options['max_file_size'] && ($file_size > $this->options['max_file_size'] ||
                $file->size > $this->options['max_file_size'])
        ) {
            $file->error = $this->get_error_message('max_file_size');
            return false;
        }
        if (
            $this->options['min_file_size'] &&
            $file_size < $this->options['min_file_size']
        ) {
            $file->error = $this->get_error_message('min_file_size');
            return false;
        }
        if (
            is_int($this->options['max_number_of_files']) &&
            ($this->count_file_objects() >= $this->options['max_number_of_files']) &&
            // Ignore additional chunks of existing files:
            !is_file($this->get_upload_path($file->name))
        ) {
            $file->error = $this->get_error_message('max_number_of_files');
            return false;
        }
        $max_width = @$this->options['max_width'];
        $max_height = @$this->options['max_height'];
        $min_width = @$this->options['min_width'];
        $min_height = @$this->options['min_height'];
        if (($max_width || $max_height || $min_width || $min_height) && preg_match($this->options['image_file_types'], $file->name)) {
            list($img_width, $img_height) = $this->get_image_size($uploaded_file);

            // If we are auto rotating the image by default, do the checks on
            // the correct orientation
            if (
                @$this->options['image_versions']['']['auto_orient'] &&
                function_exists('exif_read_data') &&
                ($exif = @exif_read_data($uploaded_file)) &&
                (((int) @$exif['Orientation']) >= 5)
            ) {
                $tmp = $img_width;
                $img_width = $img_height;
                $img_height = $tmp;
                unset($tmp);
            }
        }
        if (!empty($img_width)) {
            if ($max_width && $img_width > $max_width) {
                $file->error = $this->get_error_message('max_width');
                return false;
            }
            if ($max_height && $img_height > $max_height) {
                $file->error = $this->get_error_message('max_height');
                return false;
            }
            if ($min_width && $img_width < $min_width) {
                $file->error = $this->get_error_message('min_width');
                return false;
            }
            if ($min_height && $img_height < $min_height) {
                $file->error = $this->get_error_message('min_height');
                return false;
            }
        }
        return true;
    }

    protected function upcount_name_callback($matches)
    {
        $index = isset($matches[1]) ? ((int) $matches[1]) + 1 : 1;
        $ext = isset($matches[2]) ? $matches[2] : '';
        return ' (' . $index . ')' . $ext;
    }

    protected function upcount_name($name)
    {
        return preg_replace_callback(
            '/(?:(?: \(([\d]+)\))?(\.[^.]+))?$/',
            array($this, 'upcount_name_callback'),
            $name,
            1
        );
    }

    protected function get_unique_filename(
        $file_path,
        $name,
        $size,
        $type,
        $error,
        $index,
        $content_range
    ) {
        while (is_dir($this->get_upload_path($name))) {
            $name = $this->upcount_name($name);
        }
        // Keep an existing filename if this is part of a chunked upload:
        $uploaded_bytes = $this->fix_integer_overflow((int) $content_range[1]);
        while (is_file($this->get_upload_path($name))) {
            if ($uploaded_bytes === $this->get_file_size(
                $this->get_upload_path($name)
            )) {
                break;
            }
            $name = $this->upcount_name($name);
        }
        return $name;
    }

    protected function fix_file_extension(
        $file_path,
        $name,
        $size,
        $type,
        $error,
        $index,
        $content_range
    ) {
        // Add missing file extension for known image types:
        if (
            strpos($name, '.') === false &&
            preg_match('/^image\/(gif|jpe?g|png)/', $type, $matches)
        ) {
            $name .= '.' . $matches[1];
        }
        if (
            $this->options['correct_image_extensions'] &&
            function_exists('exif_imagetype')
        ) {
            switch (@exif_imagetype($file_path)) {
                case IMAGETYPE_JPEG:
                    $extensions = array('jpg', 'jpeg');
                    break;
                case IMAGETYPE_PNG:
                    $extensions = array('png');
                    break;
                case IMAGETYPE_GIF:
                    $extensions = array('gif');
                    break;
            }
            // Adjust incorrect image file extensions:
            if (!empty($extensions)) {
                $parts = explode('.', $name);
                $extIndex = count($parts) - 1;
                $ext = strtolower(@$parts[$extIndex]);
                if (!in_array($ext, $extensions)) {
                    $parts[$extIndex] = $extensions[0];
                    $name = implode('.', $parts);
                }
            }
        }
        return $name;
    }

    //=========================== pindah kene

    protected function get_full_url()
    {
        $https = !empty($_SERVER['HTTPS']) && strcasecmp($_SERVER['HTTPS'], 'on') === 0 ||
            !empty($_SERVER['HTTP_X_FORWARDED_PROTO']) &&
            strcasecmp($_SERVER['HTTP_X_FORWARDED_PROTO'], 'https') === 0;
        return ($https ? 'https://' : 'http://') .
            (!empty($_SERVER['REMOTE_USER']) ? $_SERVER['REMOTE_USER'] . '@' : '') .
            (isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : ($_SERVER['SERVER_NAME'] .
                ($https && $_SERVER['SERVER_PORT'] === 443 ||
                    $_SERVER['SERVER_PORT'] === 80 ? '' : ':' . $_SERVER['SERVER_PORT']))) .
            substr($_SERVER['SCRIPT_NAME'], 0, strrpos($_SERVER['SCRIPT_NAME'], '/'));
    }

    protected function get_upload_path($file_name = null, $version = null)
    {
        $file_name = $file_name ? $file_name : '';
        if (empty($version)) {
            $version_path = '';
        } else {
            $version_dir = @$this->options['image_versions'][$version]['upload_dir'];
            if ($version_dir) {
                return $version_dir . $this->get_user_path() . $file_name;
            }
            $version_path = $version . '/';
        }
        return $this->options['upload_dir'] . $this->get_user_path()
            . $version_path . $file_name;
    }

    protected function get_user_id()
    {
        @session_start();
        return session_id();
    }

    protected function get_user_path()
    {
        if ($this->options['user_dirs']) {
            return $this->get_user_id() . '/';
        }
        return '';
    }

    protected function get_query_separator($url)
    {
        return strpos($url, '?') === false ? '?' : '&';
    }

    protected function get_download_url($file_name, $version = null, $direct = false)
    {
        if (!$direct && $this->options['download_via_php']) {
            $url = $this->options['script_url']
                . $this->get_query_separator($this->options['script_url'])
                . $this->get_singular_param_name()
                . '=' . rawurlencode($file_name);
            if ($version) {
                $url .= '&version=' . rawurlencode($version);
            }
            return $url . '&download=1';
        }
        if (empty($version)) {
            $version_path = '';
        } else {
            $version_url = @$this->options['image_versions'][$version]['upload_url'];
            if ($version_url) {
                return $version_url . $this->get_user_path() . rawurlencode($file_name);
            }
            $version_path = rawurlencode($version) . '/';
        }
        return $this->options['upload_url'] . $this->get_user_path()
            . $version_path . rawurlencode($file_name);
    }

    protected function is_valid_file_object($file_name)
    {
        $file_path = $this->get_upload_path($file_name);
        if (is_file($file_path) && $file_name[0] !== '.') {
            return true;
        }
        return false;
    }

    protected function trim_file_name(
        $file_path,
        $name,
        $size,
        $type,
        $error,
        $index,
        $content_range
    ) {
        // Remove path information and dots around the filename, to prevent uploading
        // into different directories or replacing hidden system files.
        // Also remove control characters and spaces (\x00..\x20) around the filename:
        $name = trim($this->basename(stripslashes($name)), ".\x00..\x20");
        // Use a timestamp for empty filenames:
        if (!$name) {
            $name = str_replace('.', '-', microtime(true));
        }
        return $name;
    }

    protected function get_file_name(
        $file_path,
        $name,
        $size,
        $type,
        $error,
        $index,
        $content_range
    ) {
        $name = $this->trim_file_name(
            $file_path,
            $name,
            $size,
            $type,
            $error,
            $index,
            $content_range
        );
        return $this->get_unique_filename(
            $file_path,
            $this->fix_file_extension(
                $file_path,
                $name,
                $size,
                $type,
                $error,
                $index,
                $content_range
            ),
            $size,
            $type,
            $error,
            $index,
            $content_range
        );
    }

    protected function get_scaled_image_file_paths($file_name, $version)
    {
        $file_path = $this->get_upload_path($file_name);
        if (!empty($version)) {
            $version_dir = $this->get_upload_path(null, $version);
            if (!is_dir($version_dir)) {
                mkdir($version_dir, $this->options['mkdir_mode'], true);
            }
            $new_file_path = $version_dir . '/' . $file_name;
        } else {
            $new_file_path = $file_path;
        }
        return array($file_path, $new_file_path);
    }

    // Fix for overflowing signed 32 bit integers,
    // works for sizes up to 2^32-1 bytes (4 GiB - 1):
    protected function fix_integer_overflow($size)
    {
        if ($size < 0) {
            $size += 2.0 * (PHP_INT_MAX + 1);
        }
        return $size;
    }

    protected function get_file_size($file_path, $clear_stat_cache = false)
    {
        if ($clear_stat_cache) {
            if (version_compare(PHP_VERSION, '5.3.0') >= 0) {
                clearstatcache(true, $file_path);
            } else {
                clearstatcache();
            }
        }
        return $this->fix_integer_overflow(filesize($file_path));
    }

    protected function gd_get_image_object($file_path, $func, $no_cache = false)
    {
        if (empty($this->image_objects[$file_path]) || $no_cache) {
            $this->gd_destroy_image_object($file_path);
            $this->image_objects[$file_path] = $func($file_path);
        }
        return $this->image_objects[$file_path];
    }

    protected function gd_set_image_object($file_path, $image)
    {
        $this->gd_destroy_image_object($file_path);
        $this->image_objects[$file_path] = $image;
    }

    protected function gd_destroy_image_object($file_path)
    {
        $image = (isset($this->image_objects[$file_path])) ? $this->image_objects[$file_path] : null;
        return $image && imagedestroy($image);
    }

    protected function gd_imageflip($image, $mode)
    {
        if (function_exists('imageflip')) {
            return imageflip($image, $mode);
        }
        $new_width = $src_width = imagesx($image);
        $new_height = $src_height = imagesy($image);
        $new_img = imagecreatetruecolor($new_width, $new_height);
        $src_x = 0;
        $src_y = 0;
        switch ($mode) {
            case '1': // flip on the horizontal axis
                $src_y = $new_height - 1;
                $src_height = -$new_height;
                break;
            case '2': // flip on the vertical axis
                $src_x = $new_width - 1;
                $src_width = -$new_width;
                break;
            case '3': // flip on both axes
                $src_y = $new_height - 1;
                $src_height = -$new_height;
                $src_x = $new_width - 1;
                $src_width = -$new_width;
                break;
            default:
                return $image;
        }
        imagecopyresampled(
            $new_img,
            $image,
            0,
            0,
            $src_x,
            $src_y,
            $new_width,
            $new_height,
            $src_width,
            $src_height
        );
        return $new_img;
    }

    protected function gd_orient_image($file_path, $src_img)
    {
        if (!function_exists('exif_read_data')) {
            return false;
        }
        $exif = @exif_read_data($file_path);
        if ($exif === false) {
            return false;
        }
        $orientation = (int) @$exif['Orientation'];
        if ($orientation < 2 || $orientation > 8) {
            return false;
        }
        switch ($orientation) {
            case 2:
                $new_img = $this->gd_imageflip(
                    $src_img,
                    defined('IMG_FLIP_VERTICAL') ? IMG_FLIP_VERTICAL : 2
                );
                break;
            case 3:
                $new_img = imagerotate($src_img, 180, 0);
                break;
            case 4:
                $new_img = $this->gd_imageflip(
                    $src_img,
                    defined('IMG_FLIP_HORIZONTAL') ? IMG_FLIP_HORIZONTAL : 1
                );
                break;
            case 5:
                $tmp_img = $this->gd_imageflip(
                    $src_img,
                    defined('IMG_FLIP_HORIZONTAL') ? IMG_FLIP_HORIZONTAL : 1
                );
                $new_img = imagerotate($tmp_img, 270, 0);
                imagedestroy($tmp_img);
                break;
            case 6:
                $new_img = imagerotate($src_img, 270, 0);
                break;
            case 7:
                $tmp_img = $this->gd_imageflip(
                    $src_img,
                    defined('IMG_FLIP_VERTICAL') ? IMG_FLIP_VERTICAL : 2
                );
                $new_img = imagerotate($tmp_img, 270, 0);
                imagedestroy($tmp_img);
                break;
            case 8:
                $new_img = imagerotate($src_img, 90, 0);
                break;
            default:
                return false;
        }
        $this->gd_set_image_object($file_path, $new_img);
        return true;
    }

    protected function gd_create_scaled_image($file_name, $version, $options)
    {
        if (!function_exists('imagecreatetruecolor')) {
            error_log('Function not found: imagecreatetruecolor');
            return false;
        }
        list($file_path, $new_file_path) = $this->get_scaled_image_file_paths($file_name, $version);
        $type = strtolower(substr(strrchr($file_name, '.'), 1));
        switch ($type) {
            case 'jpg':
            case 'jpeg':
                $src_func = 'imagecreatefromjpeg';
                $write_func = 'imagejpeg';
                $image_quality = isset($options['jpeg_quality']) ?
                    $options['jpeg_quality'] : 75;
                break;
            case 'gif':
                $src_func = 'imagecreatefromgif';
                $write_func = 'imagegif';
                $image_quality = null;
                break;
            case 'png':
                $src_func = 'imagecreatefrompng';
                $write_func = 'imagepng';
                $image_quality = isset($options['png_quality']) ?
                    $options['png_quality'] : 9;
                break;
            default:
                return false;
        }
        $src_img = $this->gd_get_image_object(
            $file_path,
            $src_func,
            !empty($options['no_cache'])
        );
        $image_oriented = false;
        if (!empty($options['auto_orient']) && $this->gd_orient_image(
            $file_path,
            $src_img
        )) {
            $image_oriented = true;
            $src_img = $this->gd_get_image_object(
                $file_path,
                $src_func
            );
        }
        $max_width = $img_width = imagesx($src_img);
        $max_height = $img_height = imagesy($src_img);
        if (!empty($options['max_width'])) {
            $max_width = $options['max_width'];
        }
        if (!empty($options['max_height'])) {
            $max_height = $options['max_height'];
        }
        $scale = min(
            $max_width / $img_width,
            $max_height / $img_height
        );
        if ($scale >= 1) {
            if ($image_oriented) {
                return $write_func($src_img, $new_file_path, $image_quality);
            }
            if ($file_path !== $new_file_path) {
                return copy($file_path, $new_file_path);
            }
            return true;
        }
        if (empty($options['crop'])) {
            $new_width = $img_width * $scale;
            $new_height = $img_height * $scale;
            $dst_x = 0;
            $dst_y = 0;
            $new_img = imagecreatetruecolor($new_width, $new_height);
        } else {
            if (($img_width / $img_height) >= ($max_width / $max_height)) {
                $new_width = $img_width / ($img_height / $max_height);
                $new_height = $max_height;
            } else {
                $new_width = $max_width;
                $new_height = $img_height / ($img_width / $max_width);
            }
            $dst_x = 0 - ($new_width - $max_width) / 2;
            $dst_y = 0 - ($new_height - $max_height) / 2;
            $new_img = imagecreatetruecolor($max_width, $max_height);
        }
        // Handle transparency in GIF and PNG images:
        switch ($type) {
            case 'gif':
            case 'png':
                imagecolortransparent($new_img, imagecolorallocate($new_img, 0, 0, 0));
            case 'png':
                imagealphablending($new_img, false);
                imagesavealpha($new_img, true);
                break;
        }
        $success = imagecopyresampled(
            $new_img,
            $src_img,
            $dst_x,
            $dst_y,
            0,
            0,
            $new_width,
            $new_height,
            $img_width,
            $img_height
        ) && $write_func($new_img, $new_file_path, $image_quality);
        $this->gd_set_image_object($file_path, $new_img);
        return $success;
    }

    protected function imagick_get_image_object($file_path, $no_cache = false)
    {
        if (empty($this->image_objects[$file_path]) || $no_cache) {
            $this->imagick_destroy_image_object($file_path);
            $image = new \Imagick();
            if (!empty($this->options['imagick_resource_limits'])) {
                foreach ($this->options['imagick_resource_limits'] as $type => $limit) {
                    $image->setResourceLimit($type, $limit);
                }
            }
            $image->readImage($file_path);
            $this->image_objects[$file_path] = $image;
        }
        return $this->image_objects[$file_path];
    }

    protected function imagick_set_image_object($file_path, $image)
    {
        $this->imagick_destroy_image_object($file_path);
        $this->image_objects[$file_path] = $image;
    }

    protected function imagick_destroy_image_object($file_path)
    {
        $image = (isset($this->image_objects[$file_path])) ? $this->image_objects[$file_path] : null;
        return $image && $image->destroy();
    }

    protected function imagick_orient_image($image)
    {
        $orientation = $image->getImageOrientation();
        $background = new \ImagickPixel('none');
        switch ($orientation) {
            case \imagick::ORIENTATION_TOPRIGHT: // 2
                $image->flopImage(); // horizontal flop around y-axis
                break;
            case \imagick::ORIENTATION_BOTTOMRIGHT: // 3
                $image->rotateImage($background, 180);
                break;
            case \imagick::ORIENTATION_BOTTOMLEFT: // 4
                $image->flipImage(); // vertical flip around x-axis
                break;
            case \imagick::ORIENTATION_LEFTTOP: // 5
                $image->flopImage(); // horizontal flop around y-axis
                $image->rotateImage($background, 270);
                break;
            case \imagick::ORIENTATION_RIGHTTOP: // 6
                $image->rotateImage($background, 90);
                break;
            case \imagick::ORIENTATION_RIGHTBOTTOM: // 7
                $image->flipImage(); // vertical flip around x-axis
                $image->rotateImage($background, 270);
                break;
            case \imagick::ORIENTATION_LEFTBOTTOM: // 8
                $image->rotateImage($background, 270);
                break;
            default:
                return false;
        }
        $image->setImageOrientation(\imagick::ORIENTATION_TOPLEFT); // 1
        return true;
    }

    protected function imagick_create_scaled_image($file_name, $version, $options)
    {
        list($file_path, $new_file_path) = $this->get_scaled_image_file_paths($file_name, $version);
        $image = $this->imagick_get_image_object(
            $file_path,
            !empty($options['crop']) || !empty($options['no_cache'])
        );
        if ($image->getImageFormat() === 'GIF') {
            // Handle animated GIFs:
            $images = $image->coalesceImages();
            foreach ($images as $frame) {
                $image = $frame;
                $this->imagick_set_image_object($file_name, $image);
                break;
            }
        }
        $image_oriented = false;
        if (!empty($options['auto_orient'])) {
            $image_oriented = $this->imagick_orient_image($image);
        }
        $new_width = $max_width = $img_width = $image->getImageWidth();
        $new_height = $max_height = $img_height = $image->getImageHeight();
        if (!empty($options['max_width'])) {
            $new_width = $max_width = $options['max_width'];
        }
        if (!empty($options['max_height'])) {
            $new_height = $max_height = $options['max_height'];
        }
        if (!($image_oriented || $max_width < $img_width || $max_height < $img_height)) {
            if ($file_path !== $new_file_path) {
                return copy($file_path, $new_file_path);
            }
            return true;
        }
        $crop = !empty($options['crop']);
        if ($crop) {
            $x = 0;
            $y = 0;
            if (($img_width / $img_height) >= ($max_width / $max_height)) {
                $new_width = 0; // Enables proportional scaling based on max_height
                $x = ($img_width / ($img_height / $max_height) - $max_width) / 2;
            } else {
                $new_height = 0; // Enables proportional scaling based on max_width
                $y = ($img_height / ($img_width / $max_width) - $max_height) / 2;
            }
        }
        $success = $image->resizeImage(
            $new_width,
            $new_height,
            isset($options['filter']) ? $options['filter'] : \imagick::FILTER_LANCZOS,
            isset($options['blur']) ? $options['blur'] : 1,
            $new_width && $new_height // fit image into constraints if not to be cropped
        );
        if ($success && $crop) {
            $success = $image->cropImage(
                $max_width,
                $max_height,
                $x,
                $y
            );
            if ($success) {
                $success = $image->setImagePage($max_width, $max_height, 0, 0);
            }
        }
        $type = strtolower(substr(strrchr($file_name, '.'), 1));
        switch ($type) {
            case 'jpg':
            case 'jpeg':
                if (!empty($options['jpeg_quality'])) {
                    $image->setImageCompression(\imagick::COMPRESSION_JPEG);
                    $image->setImageCompressionQuality($options['jpeg_quality']);
                }
                break;
        }
        if (!empty($options['strip'])) {
            $image->stripImage();
        }
        return $success && $image->writeImage($new_file_path);
    }

    protected function imagemagick_create_scaled_image($file_name, $version, $options)
    {
        list($file_path, $new_file_path) = $this->get_scaled_image_file_paths($file_name, $version);
        $resize = @$options['max_width']
            . (empty($options['max_height']) ? '' : 'X' . $options['max_height']);
        if (!$resize && empty($options['auto_orient'])) {
            if ($file_path !== $new_file_path) {
                return copy($file_path, $new_file_path);
            }
            return true;
        }
        $cmd = $this->options['convert_bin'];
        if (!empty($this->options['convert_params'])) {
            $cmd .= ' ' . $this->options['convert_params'];
        }
        $cmd .= ' ' . escapeshellarg($file_path);
        if (!empty($options['auto_orient'])) {
            $cmd .= ' -auto-orient';
        }
        if ($resize) {
            // Handle animated GIFs:
            $cmd .= ' -coalesce';
            if (empty($options['crop'])) {
                $cmd .= ' -resize ' . escapeshellarg($resize . '>');
            } else {
                $cmd .= ' -resize ' . escapeshellarg($resize . '^');
                $cmd .= ' -gravity center';
                $cmd .= ' -crop ' . escapeshellarg($resize . '+0+0');
            }
            // Make sure the page dimensions are correct (fixes offsets of animated GIFs):
            $cmd .= ' +repage';
        }
        if (!empty($options['convert_params'])) {
            $cmd .= ' ' . $options['convert_params'];
        }
        $cmd .= ' ' . escapeshellarg($new_file_path);
        exec($cmd, $output, $error);
        if ($error) {
            error_log(implode('\n', $output));
            return false;
        }
        return true;
    }

    protected function get_image_size($file_path)
    {
        if ($this->options['image_library']) {
            if (extension_loaded('imagick')) {
                $image = new \Imagick();
                try {
                    if (@$image->pingImage($file_path)) {
                        $dimensions = array($image->getImageWidth(), $image->getImageHeight());
                        $image->destroy();
                        return $dimensions;
                    }
                    return false;
                } catch (\Exception $e) {
                    error_log($e->getMessage());
                }
            }
            if ($this->options['image_library'] === 2) {
                $cmd = $this->options['identify_bin'];
                $cmd .= ' -ping ' . escapeshellarg($file_path);
                exec($cmd, $output, $error);
                if (!$error && !empty($output)) {
                    // image.jpg JPEG 1920x1080 1920x1080+0+0 8-bit sRGB 465KB 0.000u 0:00.000
                    $infos = preg_split('/\s+/', substr($output[0], strlen($file_path)));
                    $dimensions = preg_split('/x/', $infos[2]);
                    return $dimensions;
                }
                return false;
            }
        }
        if (!function_exists('getimagesize')) {
            error_log('Function not found: getimagesize');
            return false;
        }
        return @getimagesize($file_path);
    }

    protected function create_scaled_image($file_name, $version, $options)
    {
        if ($this->options['image_library'] === 2) {
            return $this->imagemagick_create_scaled_image($file_name, $version, $options);
        }
        if ($this->options['image_library'] && extension_loaded('imagick')) {
            return $this->imagick_create_scaled_image($file_name, $version, $options);
        }
        return $this->gd_create_scaled_image($file_name, $version, $options);
    }

    protected function destroy_image_object($file_path)
    {
        if ($this->options['image_library'] && extension_loaded('imagick')) {
            return $this->imagick_destroy_image_object($file_path);
        }
    }

    protected function is_valid_image_file($file_path)
    {
        if (!preg_match($this->options['image_file_types'], $file_path)) {
            return false;
        }
        if (function_exists('exif_imagetype')) {
            return @exif_imagetype($file_path);
        }
        $image_info = $this->get_image_size($file_path);
        return $image_info && $image_info[0] && $image_info[1];
    }

    protected function handle_image_file($file_path, $file)
    {
        $failed_versions = array();
        foreach ($this->options['image_versions'] as $version => $options) {
            if ($this->create_scaled_image($file->name, $version, $options)) {
                if (!empty($version)) {
                    $file->{$version . 'Url'} = $this->get_download_url(
                        $file->name,
                        $version
                    );
                } else {
                    $file->size = $this->get_file_size($file_path, true);
                }
            } else {
                $failed_versions[] = $version ? $version : 'original';
            }
        }
        if (count($failed_versions)) {
            $file->error = $this->get_error_message('image_resize')
                . ' (' . implode($failed_versions, ', ') . ')';
        }
        // Free memory:
        $this->destroy_image_object($file_path);
    }

    protected function readfile($file_path)
    {
        $file_size = $this->get_file_size($file_path);
        $chunk_size = $this->options['readfile_chunk_size'];
        if ($chunk_size && $file_size > $chunk_size) {
            $handle = fopen($file_path, 'rb');
            while (!feof($handle)) {
                echo fread($handle, $chunk_size);
                @ob_flush();
                @flush();
            }
            fclose($handle);
            return $file_size;
        }
        return readfile($file_path);
    }

    protected function body($str)
    {
        echo $str;
    }

    protected function header($str)
    {
        header($str);
    }

    protected function get_upload_data($id)
    {
        return @$_FILES[$id];
    }

    protected function get_post_param($id)
    {
        return @$_POST[$id];
    }

    protected function get_query_param($id)
    {
        return @$_GET[$id];
    }

    protected function get_server_var($id)
    {
        return @$_SERVER[$id];
    }

    protected function get_version_param()
    {
        return $this->basename(stripslashes($this->get_query_param('version')));
    }

    protected function get_singular_param_name()
    {
        return substr($this->options['param_name'], 0, -1);
    }

    protected function get_file_name_param()
    {
        $name = $this->get_singular_param_name();
        return $this->basename(stripslashes($this->get_query_param($name)));
    }

    protected function get_file_names_params()
    {
        $params = $this->get_query_param($this->options['param_name']);
        if (!$params) {
            return null;
        }
        foreach ($params as $key => $value) {
            $params[$key] = $this->basename(stripslashes($value));
        }
        return $params;
    }

    protected function get_file_type($file_path)
    {
        switch (strtolower(pathinfo($file_path, PATHINFO_EXTENSION))) {
            case 'jpeg':
            case 'jpg':
                return 'image/jpeg';
            case 'png':
                return 'image/png';
            case 'gif':
                return 'image/gif';
            default:
                return '';
        }
    }

    protected function download()
    {
        switch ($this->options['download_via_php']) {
            case 1:
                $redirect_header = null;
                break;
            case 2:
                $redirect_header = 'X-Sendfile';
                break;
            case 3:
                $redirect_header = 'X-Accel-Redirect';
                break;
            default:
                return $this->header('HTTP/1.1 403 Forbidden');
        }
        $file_name = $this->get_file_name_param();
        if (!$this->is_valid_file_object($file_name)) {
            return $this->header('HTTP/1.1 404 Not Found');
        }
        if ($redirect_header) {
            return $this->header(
                $redirect_header . ': ' . $this->get_download_url(
                    $file_name,
                    $this->get_version_param(),
                    true
                )
            );
        }
        $file_path = $this->get_upload_path($file_name, $this->get_version_param());
        // Prevent browsers from MIME-sniffing the content-type:
        $this->header('X-Content-Type-Options: nosniff');
        if (!preg_match($this->options['inline_file_types'], $file_name)) {
            $this->header('Content-Type: application/octet-stream');
            $this->header('Content-Disposition: attachment; filename="' . $file_name . '"');
        } else {
            $this->header('Content-Type: ' . $this->get_file_type($file_path));
            $this->header('Content-Disposition: inline; filename="' . $file_name . '"');
        }
        $this->header('Content-Length: ' . $this->get_file_size($file_path));
        $this->header('Last-Modified: ' . gmdate('D, d M Y H:i:s T', filemtime($file_path)));
        $this->readfile($file_path);
    }

    protected function send_content_type_header()
    {
        $this->header('Vary: Accept');
        if (strpos($this->get_server_var('HTTP_ACCEPT'), 'application/json') !== false) {
            $this->header('Content-type: application/json');
        } else {
            $this->header('Content-type: text/plain');
        }
    }

    protected function send_access_control_headers()
    {
        $this->header('Access-Control-Allow-Origin: ' . $this->options['access_control_allow_origin']);
        $this->header('Access-Control-Allow-Credentials: '
            . ($this->options['access_control_allow_credentials'] ? 'true' : 'false'));
        $this->header('Access-Control-Allow-Methods: '
            . implode(', ', $this->options['access_control_allow_methods']));
        $this->header('Access-Control-Allow-Headers: '
            . implode(', ', $this->options['access_control_allow_headers']));
    }

    public function generate_response($content, $print_response = true)
    {
        $this->response = $content;
        if ($print_response) {
            $json = json_encode($content);
            $redirect = stripslashes($this->get_post_param('redirect'));
            if ($redirect && preg_match($this->options['redirect_allow_target'], $redirect)) {
                $this->header('Location: ' . sprintf($redirect, rawurlencode($json)));
                return;
            }
            $this->head();
            if ($this->get_server_var('HTTP_CONTENT_RANGE')) {
                $files = isset($content[$this->options['param_name']]) ?
                    $content[$this->options['param_name']] : null;
                if ($files && is_array($files) && is_object($files[0]) && $files[0]->size) {
                    $this->header('Range: 0-' . ($this->fix_integer_overflow((int) $files[0]->size) - 1));
                }
            }
            $this->body($json);
        }
        return $content;
    }

    public function get_response()
    {
        return $this->response;
    }

    public function head()
    {
        $this->header('Pragma: no-cache');
        $this->header('Cache-Control: no-store, no-cache, must-revalidate');
        $this->header('Content-Disposition: inline; filename="files.json"');
        // Prevent Internet Explorer from MIME-sniffing the content-type:
        $this->header('X-Content-Type-Options: nosniff');
        if ($this->options['access_control_allow_origin']) {
            $this->send_access_control_headers();
        }
        $this->send_content_type_header();
    }

    public function get($print_response = true)
    {
        if ($print_response && $this->get_query_param('download')) {
            return $this->download();
        }
        $file_name = $this->get_file_name_param();
        if ($file_name) {
            $response = array(
                $this->get_singular_param_name() => $this->get_file_object($file_name)
            );
        } else {
            $response = array(
                $this->options['param_name'] => $this->get_file_objects()
            );
        }
        return $this->generate_response($response, $print_response);
    }

    public function post($print_response = true)
    {
        if ($this->get_query_param('_method') === 'DELETE') {
            return $this->delete($print_response);
        }
        $upload = $this->get_upload_data($this->options['param_name']);
        // Parse the Content-Disposition header, if available:
        $content_disposition_header = $this->get_server_var('HTTP_CONTENT_DISPOSITION');
        $file_name = $content_disposition_header ?
            rawurldecode(preg_replace(
                '/(^[^"]+")|("$)/',
                '',
                $content_disposition_header
            )) : null;
        // Parse the Content-Range header, which has the following form:
        // Content-Range: bytes 0-524287/2000000
        $content_range_header = $this->get_server_var('HTTP_CONTENT_RANGE');
        $content_range = $content_range_header ?
            preg_split('/[^0-9]+/', $content_range_header) : null;
        $size = $content_range ? $content_range[3] : null;
        $files = array();
        if ($upload) {
            if (is_array($upload['tmp_name'])) {
                // param_name is an array identifier like "files[]",
                // $upload is a multi-dimensional array:
                foreach ($upload['tmp_name'] as $index => $value) {
                    $files[] = $this->handle_file_upload(
                        $upload['tmp_name'][$index],
                        $file_name ? $file_name : $upload['name'][$index],
                        $size ? $size : $upload['size'][$index],
                        $upload['type'][$index],
                        $upload['error'][$index],
                        $index,
                        $content_range
                    );
                }
            } else {
                // param_name is a single object identifier like "file",
                // $upload is a one-dimensional array:
                $files[] = $this->handle_file_upload(
                    isset($upload['tmp_name']) ? $upload['tmp_name'] : null,
                    $file_name ? $file_name : (isset($upload['name']) ?
                        $upload['name'] : null),
                    $size ? $size : (isset($upload['size']) ?
                        $upload['size'] : $this->get_server_var('CONTENT_LENGTH')),
                    isset($upload['type']) ?
                        $upload['type'] : $this->get_server_var('CONTENT_TYPE'),
                    isset($upload['error']) ? $upload['error'] : null,
                    null,
                    $content_range
                );
            }
        }
        $response = array($this->options['param_name'] => $files);
        return $this->generate_response($response, $print_response);
    }

    protected function basename($filepath, $suffix = null)
    {
        $splited = preg_split('/\//', rtrim($filepath, '/ '));
        return substr(basename('X' . $splited[count($splited) - 1], $suffix), 1);
    }

    //=========================== end pindah kene

    protected function handle_form_data($file, $index)
    {
        $file->title = @$_REQUEST['title'][$index];
        $file->description = @$_REQUEST['nama_transaksi'][$index];
    }

    protected function handle_file_upload_ori(
        $uploaded_file,
        $name,
        $size,
        $type,
        $error,
        $index = null,
        $content_range = null
    ) {
        $file = new \stdClass();
        $file->name = $this->get_file_name(
            $uploaded_file,
            $name,
            $size,
            $type,
            $error,
            $index,
            $content_range
        );
        $file->size = $this->fix_integer_overflow((int) $size);
        $file->type = $type;
        if ($this->validate($uploaded_file, $file, $error, $index)) {
            $this->handle_form_data($file, $index);
            $upload_dir = $this->get_upload_path();
            if (!is_dir($upload_dir)) {
                mkdir($upload_dir, $this->options['mkdir_mode'], true);
            }
            $file_path = $this->get_upload_path($file->name);
            $append_file = $content_range && is_file($file_path) &&
                $file->size > $this->get_file_size($file_path);
            if ($uploaded_file && is_uploaded_file($uploaded_file)) {
                // multipart/formdata uploads (POST method uploads)
                if ($append_file) {
                    file_put_contents(
                        $file_path,
                        fopen($uploaded_file, 'r'),
                        FILE_APPEND
                    );
                } else {
                    move_uploaded_file($uploaded_file, $file_path);
                }
            } else {
                // Non-multipart uploads (PUT method support)
                file_put_contents(
                    $file_path,
                    fopen($this->options['input_stream'], 'r'),
                    $append_file ? FILE_APPEND : 0
                );
            }
            $file_size = $this->get_file_size($file_path, $append_file);
            if ($file_size === $file->size) {
                $file->url = $this->get_download_url($file->name);
                if ($this->is_valid_image_file($file_path)) {
                    $this->handle_image_file($file_path, $file);
                }
            } else {
                $file->size = $file_size;
                if (!$content_range && $this->options['discard_aborted_uploads']) {
                    unlink($file_path);
                    $file->error = $this->get_error_message('abort');
                }
            }
            $this->set_additional_file_properties($file);
        }
        return $file;
    }

    protected function handle_file_upload($uploaded_file, $name, $size, $type, $error, $index = null, $content_range = null)
    {
        //$file = parent::handle_file_upload(
        //    $uploaded_file, $name, $size, $type, $error, $index, $content_range
        //);

        $sm = $this->getServiceLocator();
        $tbl_upload = $sm->get("UploadTable");

        $file = $this->handle_file_upload_ori($uploaded_file, $name, $size, $type, $error, $index, $content_range);
        if (empty($file->error)) {

            /* $sql = 'INSERT INTO `'.$this->options['db_table']
              .'` (`name`, `size`, `type`, `title`, `description`)'
              .' VALUES (?, ?, ?, ?, ?)';
              $query = $this->db->prepare($sql);
              $query->bind_param(
              'sisss',
              $file->name,
              $file->size,
              $file->type,
              $file->title,
              $file->description
              );
              $query->execute();
              $file->id = $this->db->insert_id; */
            //echo $this->idjenistransaksi.' <br>'.$this->t_iddetailsptbphtb.' <br>'.$name.' <br>'.$this->s_namajenistransaksi.' <br>'.$this->idsyarat.' <br>'.$this->lokasifile.' <br>'.$this->idspt.' <br>'.$this->t_iddetailsptbphtb;
            //$file->id = 1;
            //lemparpostsimpan($this->idjenistransaksi, $this->t_iddetailsptbphtb, $name, $this->s_namajenistransaksi, $this->idsyarat, $this->lokasifile, $this->idspt, $this->t_iddetailsptbphtb);
            $tbl_upload->savedata_syarat($this->idjenistransaksi, $this->t_iddetailsptbphtb, $name, $this->s_namajenistransaksi, $this->idsyarat, $this->lokasifile, $this->idspt, $this->t_iddetailsptbphtb);
        }
        return $file;
    }

    protected function set_additional_file_properties($file)
    {
        //parent::set_additional_file_properties($file);
        //================== delete e pindah kene
        //$file->deleteUrl = $this->options['script_url']
        //    .$this->get_query_separator($this->options['script_url'])
        //    .$this->get_singular_param_name()
        //    .'='.rawurlencode($file->name);
        $file->deleteUrl = 'uploadsyaratpost?' . $this->get_singular_param_name() . '=' . rawurlencode($file->name);
        $file->deleteType = $this->options['delete_type'];
        if ($file->deleteType !== 'DELETE') {
            $file->deleteUrl .= '&_method=DELETE&idspt=' . $this->idspt . '&idsyarat=' . $this->idsyarat . '';
        }
        if ($this->options['access_control_allow_credentials']) {
            $file->deleteWithCredentials = true;
        }
        //================== end delete e pindah kene


        if ($_SERVER['REQUEST_METHOD'] === 'GET') {
            /* $sql = 'SELECT `id`, `type`, `title`, `description` FROM `'
              .$this->options['db_table'].'` WHERE `name`=? ';
              $query = $this->db->prepare($sql);
              $query->bind_param('s', $file->name);
              $query->execute();
              $query->bind_result(
              $id,
              $type,
              $title,
              $description
              );
              while ($query->fetch()) {
              $file->id = $id;
              $file->type = $type;
              $file->title = $title;
              $file->description = $description;
              } */
        }
    }

    public function delete_ori($print_response = true)
    {
        //echo $this->lokasifile;
        $file_names = $this->get_file_names_params();
        if (empty($file_names)) {
            $file_names = array($this->get_file_name_param());
        }
        $response = array();
        foreach ($file_names as $file_name) {
            $file_path = $this->get_upload_path($file_name); //'public/upload/filetransaksi/2016/jualbeli/13051/1/'
            //public/upload/filetransaksi/2016/jualbeli/13051/1/
            $success = is_file($file_path) && $file_name[0] !== '.' && unlink($file_path);
            //$success = unlink($file_path);
            if ($success) {
                foreach ($this->options['image_versions'] as $version => $options) {
                    if (!empty($version)) {
                        $file = $this->get_upload_path($file_name, $version);
                        if (is_file($file)) {
                            unlink($file);
                        }
                    }
                }
            }
            $response[$file_name] = $success;
        }
        return $this->generate_response($response, $print_response);
    }

    public function delete($print_response = true)
    {
        $sm = $this->getServiceLocator();
        $tbl_upload = $sm->get("UploadTable");

        //$response = parent::delete(false);
        $response = $this->delete_ori(false); //parent::delete(false);
        //unlink('public/upload/filetransaksi/2016/jualbeli/13051/1/kampret2.pdf');
        foreach ($response as $name => $deleted) {
            if ($deleted) {
                /* sql = 'DELETE FROM `'
                  .$this->options['db_table'].'` WHERE `name`=?';
                  $query = $this->db->prepare($sql);
                  $query->bind_param('s', $name);
                  $query->execute(); */
                $tbl_upload->hapusdata_syarat($name, $this->idspt, $this->t_iddetailsptbphtb, $this->idsyarat);
            }
        }
        return $this->generate_response($response, $print_response);
    }

    //==================================================================================== end class upload
    // ======================================mbuh
    public function tambahspoplspopv2Action()
    {
        $session = $this->getServiceLocator()
            ->get('EtaxService')
            ->getStorage()
            ->read();
        $frm = new \Bphtb\Form\Pendataan\SPOPSptFrm();
        $ar_pemda = $this->getPemda()->getdata();
        $req = $this->getRequest();

        $view = new ViewModel(array(
            'frm' => $frm,
        ));

        $data = array(
            'menu_pendataan' => 'active',
            'role_id' => $session['s_akses'],
            'data_pemda' => $ar_pemda,
            'aturgambar' => 2,
            'username' => $session['s_username']
        );
        $this->layout()->setVariables($data);
        return $view;
    }

    // ======================================mbuh

    public function cetakntpdAction()
    {
        $session = $this->getServiceLocator()->get('EtaxService')->getStorage()->read();
        $req = $this->getRequest();
        $data_get = $req->getQuery();
        $ar_sspd = $this->getTblSSPDBphtb()->getDataId_all($data_get->t_idspt);
        $ar_sspd['ntpd'] = $ar_sspd['t_kodebayarbanksppt'];
        $ar_pemda = $this->getPemda()->getdata();
        $pdf = new \DOMPDFModule\View\Model\PdfModel();
        $pdf->setOption('paperSize', 'A4');
        $pdf->setOption('paperOrientation', 'potrait');
        // Debug::dump($ar_sspd);
        // exit;
        $pdf->setVariables(array(
            'data_sspd' => $ar_sspd,
            'data_pemda' => $ar_pemda
        ));
        return $pdf;
    }

    //============================================ class untuk upload

    public function pergantianNotarisAction()
    {
        $ar_pemda = $this->getPemda()->getdata();
        $session = $this->getServiceLocator()->get('EtaxService')->getStorage()->read();
        $req = $this->getRequest();

        if ($req->isGet()) {
            $id = $req->getQuery("id");
            $data = $this->getTblSSPDBphtb()->getDataId_all($id);
        }
        if ($req->isPost()) {
            $post = $req->getPost();
            $this->getServiceLocator()->get("SSPDBphtbTable")->savePergantianNotaris($post, $session);
            // var_dump($post);exit();
            return $this->redirect()->toRoute("pendataan_sspd");
        }

        $view = new ViewModel([
            "data" => $data,
            "dataNotaris" => $this->getServiceLocator()->get('NotarisBphtbTable')->getdataCombo()
        ]);

        $data = array(
            'menu_pendataan' => 'active',
            'role_id' => $session['s_akses'],
            'data_pemda' => $ar_pemda,
            'aturgambar' => 2,
            'username' => $session['s_username']
        );
        $this->layout()->setVariables($data);
        return $view;
    }

    public function hapusFileSyaratAction()
    {
        $req = $this->getRequest();
        $code = "00";
        if ($req->isPost()) {
            $post = $req->getPost();
            $data = $this->getServiceLocator()->get("PersyaratanTable")->getDataPesyaratanUploadByIdFileSyarat($post["id"]);
            if ($data) {
                $letak_file = $data["letak_file"];
                $nama_file = $data["nama_file"];
                if (file_exists($letak_file . $nama_file)) {
                    unlink($letak_file . $nama_file);
                }

                $namafile = $data["nama_file"];
                $idspt = $data["t_idspt"];
                $t_iddetailsptbphtb = $data["id_detailspt"];
                $idsyarat = $data["s_idpersyaratan"];
                $this->getServiceLocator()->get("UploadTable")->hapusdata_syarat($namafile, $idspt, $t_iddetailsptbphtb, $idsyarat);

                // UPDATE SYARAT YG SUDAH DI UPLOAD
                $this->getServiceLocator()->get("SPTTable")->updateSptSyaratYgSudahUpload($idspt);

                $code = "01";
            }
        }
        return  $this->getResponse()->setContent($code);
    }

    public function tesAction()
    {
        $idPermohonan = $this->getServiceLocator()->get('EspopTable')->tes();
        Debug::dump($idPermohonan);
        exit;
    }

    public function inputSpopAction()
    {
        $allParams = (array) $this->getEvent()->getRouteMatch()->getParams();

        $idSpt = $allParams['page'];
        $session = $this->getServiceLocator()->get('EtaxService')->getStorage()->read();
        $comboEspop = $this->getServiceLocator()->get('InputSpopTable')->getComboTipeSpop();
        $dataSpop = $this->getServiceLocator()->get('InputSpopTable')->getDataSpopBySpt($idSpt);

        $view = new ViewModel([
            'idSpt' => $idSpt,
            'dataSpop' => $dataSpop,
            'comboEspop' => $comboEspop
        ]);
        $view->setTemplate('bphtb/pendataan-sspd/spop/input-spop.phtml');

        $ar_pemda = $this->getPemda()->getdata();
        $data = array(
            'menu_pendataan' => 'active',
            'role_id' => $session['s_akses'],
            'data_pemda' => $ar_pemda,
            'aturgambar' => 2,
            'username' => $session['s_username']
        );
        $this->layout()->setVariables($data);
        return $view;
    }

    public function getReffSpopTable()
    {
        return $this->getServiceLocator()->get('ReffSpopTable');
    }

    public function tambahSpopAction()
    {
        $req = $this->getRequest();

        $session = $this->getServiceLocator()->get('EtaxService')->getStorage()->read();
        $form = new SpopFrm($this->getReffSpopTable()->getLookUpItemByKodeGroup(ReffSpopHelper::JENIS_KEPEMILIKAN), $this->getReffSpopTable()->getLookUpItemByKodeGroup(ReffSpopHelper::JENIS_PEKERJAAN), null, null, null, $this->getReffSpopTable()->getLookUpItemByKodeGroup(ReffSpopHelper::JENIS_BUMI));

        if ($req->isPost()) {
            $post = $req->getPost();
            $form->setData($post);
            if ($form->isValid()) {
                if (in_array($post['t_jenis_tanah'], ReffSpopHelper::JENIS_TANAH_BANGUNAN) && ($post['t_jmlh_bangunan'] == '' || $post['t_jmlh_bangunan'] <= 0 || count($post['id_lspop']) <= 0)) {
                    $this->flashMessenger()->addMessage(array('error' => 'Data Bangunan Harus Diisi'));
                } else {
                    $id = (int) $post['id'];
                    $this->getServiceLocator()->get('InputSpopTable')->simpanDataSPop($post, $session, $id);

                    $this->flashMessenger()->addMessage(array('success' => 'Data berhasil disimpan'));
                }
            } else {
                $this->flashMessenger()->addMessage(array('error' => 'Form isian tidak sesuai'));
            }

            return $this->redirect()->toRoute('pendataan_sspd', ['action' => 'inputSpop', 'page' => $post['t_idspt']]);
        }

        $allParams = (array) $this->getEvent()->getRouteMatch()->getParams();
        $idSpt = $allParams['page'];

        $dataSpt = $this->getServiceLocator()->get('InputSpopTable')->getDataSptByIdSpt($idSpt);

        $dataSpt['t_nik_wp'] = $dataSpt['t_nikwppembeli'];
        $dataSpt['t_npwp_wp'] = $dataSpt['t_npwpwppembeli'];
        $dataSpt['t_nama_wp'] = $dataSpt['t_namawppembeli'];
        $dataSpt['t_rt_wp'] = $dataSpt['t_rtwppembeli'];
        $dataSpt['t_rw_wp'] = $dataSpt['t_rwwppembeli'];
        $dataSpt['t_jalan_wp'] = $dataSpt['t_alamatwppembeli'];
        $dataSpt['t_kelurahan_wp'] = $dataSpt['t_kelurahanwppembeli'];
        $dataSpt['t_kecamatan_wp'] = $dataSpt['t_kecamatanwppembeli'];
        $dataSpt['t_kabupaten_wp'] = $dataSpt['t_kabkotawppembeli'];
        $dataSpt['t_no_hp_wp'] = $dataSpt['t_telponwppembeli'];
        $dataSpt['t_kode_pos_wp'] = $dataSpt['t_kodeposwppembeli'];
        $dataSpt['t_nop'] = $dataSpt['t_nopbphtbsppt'];
        $dataSpt['t_rt_op'] = $dataSpt['t_rtop'];
        $dataSpt['t_rw_op'] = $dataSpt['t_rwop'];
        $dataSpt['t_jalan_op'] = $dataSpt['t_alamatop'];

        $dataSpt['t_nomor_sertifikat'] = $dataSpt['t_nosertifikathaktanah'];

        $explodeNop = explode('.', $dataSpt['t_nopbphtbsppt']);
        // var_dump($explodeNop);exit();
        $kodeKecamatan = $explodeNop[2];
        $kodeKelurahan = $explodeNop[3];
        $kodeBlok = $explodeNop[4];

        $dataSpt['t_kecamatan_op'] = $kodeKecamatan;
        $dataSpt['t_kelurahan_op'] = $kodeKelurahan;
        $dataSpt['t_kode_blok_op'] = $kodeBlok;

        $dataSpt['t_luas_tanah'] = $dataSpt['p_idpemeriksaan'] != null ? round($dataSpt['p_luastanah']) : round($dataSpt['t_luastanah']);

        $form = new SpopFrm($this->getReffSpopTable()->getLookUpItemByKodeGroup(ReffSpopHelper::JENIS_KEPEMILIKAN), $this->getReffSpopTable()->getLookUpItemByKodeGroup(ReffSpopHelper::JENIS_PEKERJAAN), $this->getReffSpopTable()->getComboBlokSelected($kodeKecamatan, $kodeKelurahan, $kodeBlok), $this->getReffSpopTable()->getComboKelurahanByKodeKecamatanSelected($kodeKecamatan, $kodeKelurahan), $this->getReffSpopTable()->getComboKecamatanSelected($kodeKecamatan), $this->getReffSpopTable()->getLookUpItemByKodeGroup(ReffSpopHelper::JENIS_BUMI));

        $form->bind(new ArrayObject($dataSpt));

        $view = new ViewModel([
            'idSpt' => $idSpt,
            'form' => $form
        ]);
        $view->setTemplate('bphtb/pendataan-sspd/spop/form-spop.phtml');

        $ar_pemda = $this->getPemda()->getdata();
        $data = array(
            'menu_pendataan' => 'active',
            'role_id' => $session['s_akses'],
            'data_pemda' => $ar_pemda,
            'aturgambar' => 2,
            'username' => $session['s_username']
        );
        $this->layout()->setVariables($data);
        return $view;
    }

    public function editSpopAction()
    {
        $allParams = (array) $this->getEvent()->getRouteMatch()->getParams();

        $idSpop = $allParams['page'];
        $session = $this->getServiceLocator()->get('EtaxService')->getStorage()->read();
        $dataSpop = $this->getServiceLocator()->get('InputSpopTable')->getDataSpopByIdSpop($idSpop);

        $form = new SpopFrm($this->getReffSpopTable()->getLookUpItemByKodeGroup(ReffSpopHelper::JENIS_KEPEMILIKAN), $this->getReffSpopTable()->getLookUpItemByKodeGroup(ReffSpopHelper::JENIS_PEKERJAAN), $this->getReffSpopTable()->getComboBlokSelected($dataSpop['kd_kecamatan'], $dataSpop['kd_kelurahan'], $dataSpop['kd_blok']), $this->getReffSpopTable()->getComboKelurahanByKodeKecamatanSelected($dataSpop['kd_kecamatan'], $dataSpop['kd_kelurahan']), $this->getReffSpopTable()->getComboKecamatanSelected($dataSpop['kd_kecamatan']), $this->getReffSpopTable()->getLookUpItemByKodeGroup(ReffSpopHelper::JENIS_BUMI));

        $form->bind(new ArrayObject($dataSpop));

        $dataLspop = $this->getServiceLocator()->get('InputSpopTable')->getDataLspopByISpop($dataSpop['id']);

        $view = new ViewModel([
            'idSpt' => $dataSpop['t_idspt'],
            'form' => $form,
            'dataLspop' => $dataLspop
        ]);

        $view->setTemplate('bphtb/pendataan-sspd/spop/form-spop.phtml');

        $ar_pemda = $this->getPemda()->getdata();
        $data = array(
            'menu_pendataan' => 'active',
            'role_id' => $session['s_akses'],
            'data_pemda' => $ar_pemda,
            'aturgambar' => 2,
            'username' => $session['s_username']
        );
        $this->layout()->setVariables($data);
        return $view;
    }

    // spop mutasi
    public function tambahSpopMutasiAction()
    {
        $req = $this->getRequest();

        $session = $this->getServiceLocator()->get('EtaxService')->getStorage()->read();
        $form = new SpopMutasiFrm();

        if ($req->isPost()) {
            $post = $req->getPost();

            $form->setData($post);
            if ($form->isValid()) {
                if (in_array($post['t_jenis_tanah'], ReffSpopHelper::JENIS_TANAH_BANGUNAN) && ($post['t_jmlh_bangunan'] == '' || $post['t_jmlh_bangunan'] <= 0 || count($post['id_lspop']) <= 0)) {
                    $this->flashMessenger()->addMessage(array('error' => 'Data Bangunan Harus Diisi'));
                } else {
                    $id = (int) $post['id'];
                    $this->getServiceLocator()->get('InputSpopTable')->simpanDataSPopMutasi($post, $session, $id);

                    $this->flashMessenger()->addMessage(array('success' => 'Data berhasil disimpan'));
                }
            } else {
                $this->flashMessenger()->addMessage(array('error' => 'Form isian tidak sesuai'));
            }

            // $dasa['idSpt'] = $post['t_idspt'];
            // $dasa['tipeSpop'] = TipeEspopHelper::PEMECAHAN;

            return $this->redirect()->toRoute('pendataan_sspd', ['action' => 'simpanKonfirmasiMutasiSpop', 'page' => $post['t_idspt']]);
        }

        $allParams = (array) $this->getEvent()->getRouteMatch()->getParams();
        $idSpt = $allParams['page'];

        $dataSpt = $this->getServiceLocator()->get('InputSpopTable')->getDataSptByIdSpt($idSpt);
        // Debug::dump($dataSpt);
        // exit;
        $dataSpt['t_nik_wp'] = $dataSpt['t_nikwppembeli'];
        $dataSpt['t_npwp_wp'] = $dataSpt['t_npwpwppembeli'];
        $dataSpt['t_nama_wp'] = $dataSpt['t_namawppembeli'];
        $dataSpt['t_rt_wp'] = $dataSpt['t_rtwppembeli'];
        $dataSpt['t_rw_wp'] = $dataSpt['t_rwwppembeli'];
        $dataSpt['t_jalan_wp'] = $dataSpt['t_alamatwppembeli'];
        $dataSpt['t_kelurahan_wp'] = $dataSpt['t_kelurahanwppembeli'];
        $dataSpt['t_kecamatan_wp'] = $dataSpt['t_kecamatanwppembeli'];
        $dataSpt['t_kabupaten_wp'] = $dataSpt['t_kabkotawppembeli'];
        $dataSpt['t_no_hp_wp'] = $dataSpt['t_telponwppembeli'];
        $dataSpt['t_kode_pos_wp'] = $dataSpt['t_kodeposwppembeli'];

        $dataSpt['t_rt_op'] = $dataSpt['t_rtop'];
        $dataSpt['t_rw_op'] = $dataSpt['t_rwop'];
        $dataSpt['t_jalan_op'] = $dataSpt['t_alamatop'];

        $dataSpt['t_nomor_sertifikat'] = $dataSpt['t_nosertifikathaktanah'];

        $dataSpt['t_nop'] = $dataSpt['t_nopbphtbsppt']; //nop induk
        $dataSpt['t_nop_asal'] = $dataSpt['t_nopbphtbsppt']; //nop asal

        $explodeNop = explode('.', $dataSpt['t_nopbphtbsppt']);
        $kodeKecamatan = $explodeNop[2];
        $kodeKelurahan = $explodeNop[3];
        $kodeBlok = $explodeNop[4];

        $dataSpt['t_kecamatan_op'] = $kodeKecamatan;
        $dataSpt['t_kelurahan_op'] = $kodeKelurahan;
        $dataSpt['t_kode_blok_op'] = $kodeBlok;

        $dataObjekBumi = $this->getServiceLocator()->get('SPPTTable')->getDataObjekBumi($dataSpt['t_nop_asal']);
        $dataSpt['t_kd_znt'] = $dataObjekBumi['KD_ZNT'];
        // var_dump($dataSpt);
        // exit();

        $dataSpt['t_luas_tanah'] = $dataSpt['p_idpemeriksaan'] != null ? round($dataSpt['p_luastanah']) : round($dataSpt['t_luastanah']);

        $form = new SpopMutasiFrm(
            $this->getReffSpopTable()->getLookUpItemByKodeGroup(ReffSpopHelper::JENIS_KEPEMILIKAN),
            $this->getReffSpopTable()->getLookUpItemByKodeGroup(ReffSpopHelper::JENIS_PEKERJAAN),
            $this->getReffSpopTable()->getComboBlokSelected($kodeKecamatan, $kodeKelurahan, $kodeBlok),
            $this->getReffSpopTable()->getComboKelurahanByKodeKecamatanSelected($kodeKecamatan, $kodeKelurahan),
            $this->getReffSpopTable()->getComboKecamatanSelected($kodeKecamatan),
            $this->getReffSpopTable()->getLookUpItemByKodeGroup(ReffSpopHelper::JENIS_BUMI)
        );

        $form->bind(new ArrayObject($dataSpt));

        $view = new ViewModel([
            'idSpt' => $idSpt,
            'form' => $form
        ]);
        $view->setTemplate('bphtb/pendataan-sspd/spop/form-spop-mutasi.phtml');

        $ar_pemda = $this->getPemda()->getdata();
        $data = array(
            'menu_pendataan' => 'active',
            'role_id' => $session['s_akses'],
            'data_pemda' => $ar_pemda,
            'aturgambar' => 2,
            'username' => $session['s_username']
        );
        $this->layout()->setVariables($data);
        return $view;
    }

    // spop mutasi edit
    public function editSpopMutasiAction()
    {
        $allParams = (array) $this->getEvent()->getRouteMatch()->getParams();

        $idSpop = $allParams['page'];
        $session = $this->getServiceLocator()->get('EtaxService')->getStorage()->read();
        $dataSpop = $this->getServiceLocator()->get('InputSpopTable')->getDataSpopByIdSpop($idSpop);
        // var_dump($dataSpop);exit();
        if ($dataSpop['t_nomor_bidang'] == 1) {
            $dataSpop['t_nop_asal'] = $dataSpop['t_nop'];
        } else {
            $dataSpop['t_nop'] = $dataSpop['t_nop_asal'];
        }

        $form = new SpopMutasiFrm(
            $this->getReffSpopTable()->getLookUpItemByKodeGroup(ReffSpopHelper::JENIS_KEPEMILIKAN),
            $this->getReffSpopTable()->getLookUpItemByKodeGroup(ReffSpopHelper::JENIS_PEKERJAAN),
            $this->getReffSpopTable()->getComboBlokSelected($dataSpop['kd_kecamatan'], $dataSpop['kd_kelurahan'], $dataSpop['kd_blok']),
            $this->getReffSpopTable()->getComboKelurahanByKodeKecamatanSelected($dataSpop['kd_kecamatan'], $dataSpop['kd_kelurahan']),
            $this->getReffSpopTable()->getComboKecamatanSelected($dataSpop['kd_kecamatan']),
            $this->getReffSpopTable()->getLookUpItemByKodeGroup(ReffSpopHelper::JENIS_BUMI),
            $this->getReffSpopTable()->getDataZntSelectedEdit($dataSpop['t_kecamatan_op'], $dataSpop['t_kelurahan_op'], $dataSpop['t_kode_blok_op'], date('Y'), $dataSpop['t_kd_znt'])
        );

        $form->bind(new ArrayObject($dataSpop));

        $dataLspop = $this->getServiceLocator()->get('InputSpopTable')->getDataLspopByISpop($dataSpop['id']);

        $view = new ViewModel([
            'idSpt' => $dataSpop['t_idspt'],
            'form' => $form,
            'dataLspop' => $dataLspop
        ]);

        $view->setTemplate('bphtb/pendataan-sspd/spop/form-spop-mutasi.phtml');

        $ar_pemda = $this->getPemda()->getdata();
        $data = array(
            'menu_pendataan' => 'active',
            'role_id' => $session['s_akses'],
            'data_pemda' => $ar_pemda,
            'aturgambar' => 2,
            'username' => $session['s_username']
        );
        $this->layout()->setVariables($data);
        return $view;
    }

    public function getDataObjekNopAction()
    {
        // $req = $this->getRequest();
        $allParams = (array) $this->getEvent()->getRouteMatch()->getParams();
        // var_dump($allParams);
        // exit();

        $nop = $allParams['page'];
        $dataObjek = $this->getServiceLocator()->get('SPPTTable')->getDataObjek($nop);
        // var_dump($dataObjek);exit();
        $dataObjekSubjek = $this->getServiceLocator()->get('SPPTTable')->getDataObjekSubjek($dataObjek['SUBJEK_PAJAK_ID']);
        $dataObjekBumi = $this->getServiceLocator()->get('SPPTTable')->getDataObjekBumi($nop);
        // var_dump($dataObjekBumi);exit();
        $dataObjekBangunan = $this->getServiceLocator()->get('SPPTTable')->getDataObjekBangunan($nop);
        $dataZnt = $this->getReffSpopTable()->getDataZntSelected($dataObjek['KD_KECAMATAN'], $dataObjek['KD_KELURAHAN'], $dataObjek['KD_BLOK'], date('Y'), $dataObjekBumi['KD_ZNT']);
        // var_dump($dataZnt);exit();

        return $this->getResponse()->setContent(json_encode([
            'dataObjek' => $dataObjek,
            'dataObjekSubjek' => $dataObjekSubjek,
            'dataObjekBumi' => $dataObjekBumi,
            'dataObjekBangunan' => $dataObjekBangunan,
            'dataZnt' => $dataZnt,
        ]));
    }

    public function simpanKonfirmasiSpopAction()
    {
        $req = $this->getRequest();
        $post = $req->getPost();
        $session = $this->getServiceLocator()->get('EtaxService')->getStorage()->read();

        // if ($session['s_id_user_espop'] == null) {
        //     $this->flashMessenger()->addMessage(array('error' => 'Belum terdaftar pada espop, mohon hubungi admin kantor'));
        //     return $this->redirect()->toRoute('pendataan_sspd', [
        //         'action' => 'input-spop',
        //         'page' => $post['idSpt']
        //     ]);
        // }

        if ($post['tipeSpop'] == TipeEspopHelper::PEMECAHAN) {
            return $this->redirect()->toRoute('pendataan_sspd', ['action' => 'tambahSpopMutasi', 'page' => $post['idSpt']]);
        }

        if ($req->isPost()) {
            try {
                $idSpt = $post['idSpt'];

                $tipeSpop = $post['tipeSpop'];

                $dataSpt = $this->getServiceLocator()->get('SSPDBphtbTable')->getDataId_all($idSpt);

                $dataSpop = $this->getServiceLocator()->get('InputSpopTable')->getDataSpopBySpt($idSpt);

                $idPermohonan = $this->getServiceLocator()->get('EspopTable')->simpanPermohonan($dataSpt, $session, $tipeSpop, $dataSpop);
                $pbb = $this->getServiceLocator()->get('ReffSpopTable')->getDataOpWpKecKel($dataSpt);
                // debug::dump($pbb);
                // exit;

                switch ($tipeSpop) {
                    case TipeEspopHelper::BALIK_NAMA:

                        $as = $this->getServiceLocator()->get('EspopTable')->simpanop($dataSpt, $session, $tipeSpop, $dataSpop, $idPermohonan, $pbb);

                        $this->getServiceLocator()->get('EspopTable')->simpanwp($dataSpt, $session, $tipeSpop, $dataSpop, $idPermohonan, $as);
                        $this->getServiceLocator()->get('EspopTable')->simpanwplama($dataSpt, $session, $tipeSpop, $dataSpop, $idPermohonan, $pbb);
                        $this->getServiceLocator()->get('EspopTable')->simpanoplama($dataSpt, $session, $tipeSpop, $dataSpop, $idPermohonan, $pbb);
                        break;
                    case TipeEspopHelper::PEMECAHAN:

                        // $this->getServiceLocator()->get('EspopTable')->simpanSpopPemecahan($dataSpt, $tipeSpop, $session, $idPermohonan, $dataSpop);
                        $a = $this->getServiceLocator()->get('EspopTable')->simpanopPecah($dataSpt, $session, $tipeSpop, $dataSpop, $idPermohonan, $pbb, $dataSpop);
                        $this->getServiceLocator()->get('EspopTable')->simpanwp($dataSpt, $session, $tipeSpop, $dataSpop, $idPermohonan, $a);
                        $this->getServiceLocator()->get('EspopTable')->simpanwplama($dataSpt, $session, $tipeSpop, $dataSpop, $idPermohonan, $pbb);
                        $this->getServiceLocator()->get('EspopTable')->simpanoplama($dataSpt, $session, $tipeSpop, $dataSpop, $idPermohonan, $pbb);
                        break;

                    default:
                        break;
                }

                // verifikasi -> validasi
                $this->getServiceLocator()->get("EspopTable")->simpanVerifikasi($dataSpt, $session, $idPermohonan);

                // Debug::dump("");
                // exit;

                // simpan spt konfirm spop
                $this->getServiceLocator()->get('InputSpopTable')->saveSpopKonfirmasi($dataSpt['t_idspt'], $tipeSpop, $session);

                $this->flashMessenger()->addMessage(array('success' => 'Data SPOP sudah tersampaikan, terima kasih'));
                return $this->redirect()->toRoute('pendataan_sspd');
            } catch (\Throwable $th) {
                echo $th;
                exit;
            }
        }
    }

    public function simpanKonfirmasiMutasiSpopAction()
    {

        $session = $this->getServiceLocator()->get('EtaxService')->getStorage()->read();
        $req = $this->getRequest();
        $post = $req->getPost();
        $routeMatch = $this->getEvent()->getRouteMatch();

        $post['idSpt'] = $routeMatch->getParam('page', 0);

        // if ($session['s_id_user_espop'] == null) {
        //     $this->flashMessenger()->addMessage(array('error' => 'Belum terdaftar pada espop, mohon hubungi admin kantor'));
        //     return $this->redirect()->toRoute('pendataan_sspd', [
        //         'action' => 'input-spop',
        //         'page' => $post['idSpt']
        //     ]);
        // }

        // if ($post['tipeSpop'] == TipeEspopHelper::PEMECAHAN) {
        //     return $this->redirect()->toRoute('pendataan_sspd', ['action' => 'tambahSpopMutasi', 'page' => $post['idSpt']]);
        // }

        // if ($req->isPost()) {
        try {
            $idSpt = $post['idSpt'];

            $tipeSpop = 2;

            $dataSpt = $this->getServiceLocator()->get('SSPDBphtbTable')->getDataId_all($idSpt);

            $dataSpop = $this->getServiceLocator()->get('InputSpopTable')->getDataSpopBySpt($idSpt);

            $idPermohonan = $this->getServiceLocator()->get('EspopTable')->simpanPermohonan($dataSpt, $session, $tipeSpop, $dataSpop);
            $pbb = $this->getServiceLocator()->get('ReffSpopTable')->getDataOpWpKecKel($dataSpt);
            // debug::dump($pbb);
            // exit;

            // switch ($tipeSpop) {
            //     case TipeEspopHelper::BALIK_NAMA:

            //         $this->getServiceLocator()->get('EspopTable')->simpanwp($dataSpt, $session, $tipeSpop, $dataSpop, $idPermohonan);
            //         $this->getServiceLocator()->get('EspopTable')->simpanop($dataSpt, $session, $tipeSpop, $dataSpop, $idPermohonan, $pbb);
            //         $this->getServiceLocator()->get('EspopTable')->simpanwplama($dataSpt, $session, $tipeSpop, $dataSpop, $idPermohonan, $pbb);
            //         $this->getServiceLocator()->get('EspopTable')->simpanoplama($dataSpt, $session, $tipeSpop, $dataSpop, $idPermohonan, $pbb);
            //         break;
            //     case TipeEspopHelper::PEMECAHAN:

            // $this->getServiceLocator()->get('EspopTable')->simpanSpopPemecahan($dataSpt, $tipeSpop, $session, $idPermohonan, $dataSpop);
            $a = $this->getServiceLocator()->get('EspopTable')->simpanopPecah($dataSpt, $session, $tipeSpop, $dataSpop, $idPermohonan, $pbb, $dataSpop);
            $this->getServiceLocator()->get('EspopTable')->simpanwp($dataSpt, $session, $tipeSpop, $dataSpop, $idPermohonan, $a);
            $this->getServiceLocator()->get('EspopTable')->simpanwplama($dataSpt, $session, $tipeSpop, $dataSpop, $idPermohonan, $pbb);
            $this->getServiceLocator()->get('EspopTable')->simpanoplama($dataSpt, $session, $tipeSpop, $dataSpop, $idPermohonan, $pbb);
            //         break;

            //     default:
            //         break;
            // }

            // verifikasi -> validasi
            $this->getServiceLocator()->get("EspopTable")->simpanVerifikasi($dataSpt, $session, $idPermohonan);

            // Debug::dump("");
            // exit;

            // simpan spt konfirm spop
            $this->getServiceLocator()->get('InputSpopTable')->saveSpopKonfirmasi($dataSpt['t_idspt'], $tipeSpop, $session);

            $this->flashMessenger()->addMessage(array('success' => 'Data SPOP sudah tersampaikan, terima kasih'));
            return $this->redirect()->toRoute('pendataan_sspd');
        } catch (\Throwable $th) {
            echo $th;
            exit;
        }
        // }
        // }   
    }
}
