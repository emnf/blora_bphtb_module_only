<?php

namespace Bphtb\Controller\Pengurangan;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Math\Rand;

class Pengurangan extends AbstractActionController {

    protected $tbl_pemda, $tbl_pembayaran, $tbl_pendataan, $tbl_jenistransaksi, $tbl_haktanah, $tbl_doktanah, $tbl_sspdbphtb, $tbl_sspd, $tbl_notaris, $tbl_persyaratan, $tbl_spt, $tbl_verifikasi, $tbl_sspdwaris, $tbl_pengurangan;

    public function cekurl() {
        $basePath = $this->getRequest()->getBasePath();
        $uri = new \Zend\Uri\Uri($this->getRequest()->getUri());
        $uri->setPath($basePath);
        $uri->setQuery(array());
        $uri->setFragment('');
        return $uri->getScheme() . '://' . $uri->getHost() . '' . $uri->getPath(); //:'.$_SERVER['SERVER_PORT'].'
    }

    public function indexAction() {
        $session = $this->getServiceLocator()->get('EtaxService')->getStorage()->read();
        $ar_pemda = $this->getPemda()->getdata();
        $form = new \Bphtb\Form\Pendataan\SSPDFrm();

        $panggildata = $this->getServiceLocator()->get("JenisTransaksiBphtbTable");
        $datajenistransaksi = $panggildata->comboBox();

        $view = new ViewModel(array(
            "form" => $form,
            'datajenistransaksi' => $datajenistransaksi
        ));
        $data = array(
            'menu_pengurangan' => 'active',
            'role_id' => $session['s_akses'],
            'data_pemda' => $ar_pemda,
            'aturgambar' => 1,
            'username' => $session['s_username'],
            'session' => $session,
        );
        $this->layout()->setVariables($data);
        return $view;
    }

    public function dataGridAction() {
        $session = $this->getServiceLocator()->get('EtaxService')->getStorage()->read();
        $s_iduser = $session['s_iduser'];
        $s_tipe_pejabat = $session['s_tipe_pejabat'];

        $sTable = 'view_pengurangan';
        $count = 't_idspt';

        $input = $this->getRequest();
        $order_default = " t_kohirspt DESC";
//        $aColumns = array('t_idspt', 't_kohirspt', 't_nopbphtbsppt', 's_idjenistransaksi', 't_tglverifikasispt', 't_namawppembeli', 't_nilaipembayaranspt', 't_statusbayarspt', 'status_validasi', 'luas_tanah', 'luas_bangunan','t_nosertifikatbaru', 't_tglsertifikatbaru', 'fr_luas_tanah_bpn', 'fr_luas_bangunan_bpn', 'p_luastanah', 'p_luasbangunan', 't_luastanah', 't_luasbangunan', 's_namajenistransaksi', 't_persyaratan', 't_verifikasispt', 't_idjenistransaksi', 't_inputbpn', 't_periodespt', 'status_pendaftaran','t_idpembayaranspt', 'p_totalspt', 'p_idpemeriksaan','t_totalspt', 't_ketetapanspt', 't_iddetailsptbphtb', 't_luastanahbpn', 't_luasbangunanbpn'); 
        $aColumns = array('t_idspt', 't_kohirspt', 't_kohirketetapanspt', 's_idjenistransaksi', 't_idnotarisspt', 't_tglprosesspt', 't_namawppembeli', 'jml_pajak_v1', 't_statusbayarspt', 'status_pendaftaran', 'status_validasi', 's_namajenistransaksi', 't_persyaratan', 't_verifikasispt', 't_idjenistransaksi', 't_inputbpn', 't_periodespt', 't_totalspt', 's_namanotaris', 'fr_tervalidasidua', 'fr_validasidua', 't_idsptsebelumnya', 't_idpengurangan', 't_approve_pengurangan');

        $panggildata = $this->getServiceLocator()->get("PenguranganTable");
        $rResult = $panggildata->semuadatapengurangan($sTable, $count, $input, $order_default, $aColumns, $session, $this->cekurl());
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($rResult));
    }

    public function tambahAction() {
        $ar_pemda = $this->getPemda()->getdata();
        $session = $this->getServiceLocator()->get('EtaxService')->getStorage()->read();
        $string = Rand::getString(6, '1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ', true);
        $req = $this->getRequest();
        $datane = $req->getPost();
        $post = $req->getPost();
        if (!empty($datane['t_idspt'])) {
            $dataki = $this->getTblSSPDBphtb()->getDataId($datane['t_idspt']);
            $frm = new \Bphtb\Form\Pendataan\SSPDFrm($this->populateComboJenisTransaksi(), $this->populateComboHakTanah(), $this->populateComboDokTanah(), $string, $this->populateComboNotaris(), $this->populatePersyaratanId($datane['t_idjenistransaksi']), null);
        } else {
            $frm = new \Bphtb\Form\Pendataan\SSPDFrm($this->populateComboJenisTransaksi(), $this->populateComboHakTanah(), $this->populateComboDokTanah(), $string, $this->populateComboNotaris(), null, null);
        }
        $data_tarif = $this->getTblSSPDBphtb()->temukanDataTarifBPHTB();
        if ($req->isPost()) {
            $kb = new \Bphtb\Model\Pendataan\SSPDBphtbBase();
            $frm->setInputFilter($kb->getInputFilter($session['s_namauserrole']));
            $frm->setData($req->getPost());
            
            // 1. update data t_spt
            $this->getTblPengurangan()->update_t_spt($post);
            //update t_pembayaranspt
            $this->getTblPengurangan()->update_pembayaran($post);
            // 2. simpan data t_pengurangan
            $this->getTblPengurangan()->simpan_t_pengurangan($post, $session);
            return $this->redirect()->toRoute('pengurangan');
        } else {
            if (!empty($datane['t_idspt'])) {
                $frm->bind($dataki);
                $frm->get("t_persyaratan")->setValue(\Zend\Json\Json::decode($dataki->t_persyaratan));
            }
        }

        $view = new ViewModel(array(
            'frm' => $frm,
            'tarifbphtb' => $data_tarif['s_tarifbphtb'],
            'idtarifbphtb' => $data_tarif['s_idtarifbphtb'],
            's_namauserrole' => $session['s_tipe_pejabat']
        ));

        $data = array(
            'menu_pengurangan' => 'active',
            'role_id' => $session['s_akses'],
            'data_pemda' => $ar_pemda,
            'aturgambar' => 2,
            'username' => $session['s_username'],
            'session' => $session,
        );
        $this->layout()->setVariables($data);
        return $view;
    }

    public function inputpenguranganAction() {
        $ar_pemda = $this->getPemda()->getdata();
        $session = $this->getServiceLocator()->get('EtaxService')->getStorage()->read();
        if (($session['s_namauserrole'] == "Administrator") || ($session['s_namauserrole'] == "Pegawai") || ($session['s_tipe_pejabat'] == 1)) {
            $show_hide_combo_notaris = "display:inherit;";
            $notaris = 2;
        } elseif ($session['s_tipe_pejabat'] == 2) {
            $show_hide_combo_notaris = "display:none;";
            $notaris = 1;
        }
        $string = Rand::getString(6, 'abcdefghijklmnopqrstuvwxyz-1234567890_ABCDEFGHIJKLMNOPQRSTUVWXYZ', true);
        $frm = new \Bphtb\Form\Pendataan\SSPDFrm($this->populateComboJenisTransaksi(), $this->populateComboHakTanah(), $this->populateComboDokTanah(), $string, $this->populateComboNotaris());
        $req = $this->getRequest();
        if ($req->isGet()) {
            $id = (int) $req->getQuery()->get('t_idspt');
            $data = $this->getTblSSPDBphtb()->getDataId($id);
            $datahistory = $this->getTblSSPDBphtb()->gethistorybphtb($data->t_nikwppembeli, $data->t_idspt, $data->t_periodespt);
            if ($data->t_idjenistransaksi == 5) {
                $show_hide_penerima_waris = "display:inherit;";
            } else {
                $show_hide_penerima_waris = "display:none;";
            }
            $data_penerimawaris = $this->getTblWaris()->CariPenerimaWaris($data->t_idspt);
            $data->t_tglprosesspt = date('d-m-Y', strtotime($data->t_tglprosesspt));
            $data->t_tglajb = date('d-m-Y', strtotime($data->t_tglajb));
            $t_luastanah = str_ireplace('.', '', $data->t_luastanah);
            $t_luasbangunan = str_ireplace('.', '', $data->t_luasbangunan);
            $data->t_luastanah = number_format(($t_luastanah / 100), 0, ',', '.');
            $data->t_luasbangunan = number_format(($t_luasbangunan / 100), 0, ',', '.');
            $frm2 = new \Bphtb\Form\Pendataan\SSPDFrm($this->populateComboJenisTransaksi(), $this->populateComboHakTanah(), $this->populateComboDokTanah(), $string, $this->populateComboNotaris(), $this->populatePersyaratanId($data->t_idjenistransaksi));
            $frm2->bind($data);
            $frm2->get("t_terbukti")->setValue(\Zend\Json\Json::decode($data->t_terbukti));
            $frm2->get("t_persyaratan")->setValue(\Zend\Json\Json::decode($data->t_persyaratan));
            $data_tarif = $this->getTblSSPDBphtb()->temukanDataTarifBphtb_tahun($data->t_periodespt);
        }

        //========== panggil Model\Pendataan\SSPDBphtbTable
        $fr_tarif = $data->t_persenbphtb;
        $tahunproses = date('Y', strtotime($data->t_tglprosesspt));

        $datatransaksi = $this->getTblSSPDBphtb()->getDataId_all($id);

        $view = new ViewModel(array(
            'frm' => $frm2,
            'data' => $data->t_terbukti,
            'datasspd' => $datatransaksi,
            'show_hide_combo_notaris' => $show_hide_combo_notaris,
            'datahistory' => $datahistory,
            'show_hide_penerima_waris' => $show_hide_penerima_waris,
            'data_penerimawaris' => $data_penerimawaris,
            'idtarifbphtb' => $data_tarif['s_idtarifbphtb'],
            'notaris' => $notaris,
            't_tarif_pembagian_aphb_kali' => $data->t_tarif_pembagian_aphb_kali,
            't_tarif_pembagian_aphb_bagi' => $data->t_tarif_pembagian_aphb_bagi,
            't_grandtotalnjop_aphb' => $data->t_grandtotalnjop_aphb,
            'tarifbphtbsekarang' => $fr_tarif,
            't_potongan_waris_hibahwasiat' => $data->t_potongan_waris_hibahwasiat
        ));

        $data = array(
            'menu_pengurangan' => 'active',
            'role_id' => $session['s_akses'],
            'data_pemda' => $ar_pemda,
            'aturgambar' => 2,
            'username' => $session['s_username'],
            'session' => $session,
        );
        $this->layout()->setVariables($data);
        return $view;
    }

    public function cetakdaftarpenguranganAction() {
        $req = $this->getRequest();
        $data_get = $req->getQuery();
        $data_pengurangan = $this->getTblPengurangan()->getDataHasilPengurangan($data_get->tgl_pengurangan1, $data_get->tgl_pengurangan2);
//        $ar_Mengetahui = $this->getTblPejabat()->getdataid($data_get->mengetahui);
//        $ar_Pejabat = $this->getTblPejabat()->getdataid($data_get->pejabat);
        $ar_pemda = $this->getPemda()->getdata();
        $session = $this->getServiceLocator()->get('EtaxService')->getStorage()->read();
        $view = new ViewModel(array(
            'data_pengurangan' => $data_pengurangan,
//                'data_mengetahui' => $ar_Mengetahui,
//                'data_pejabat' => $ar_Pejabat,
            'tgl_cetak' => date('d-m-Y'),
            'ar_pemda' => $ar_pemda,
            'nama_login' => $session['s_iduser'],
            'tgl_pengurangan1' => $data_get->tgl_pengurangan1,
            'tgl_pengurangan2' => $data_get->tgl_pengurangan2,
            'cetak' => $data_get->cetak
        ));
        $data = array(
            'nilai' => '3', //no layout
            'session' => $session,
        );
        $this->layout()->setVariables($data);
        return $view;
    }

    public function hitungpenguranganAction() {
        $req = $this->getRequest();
        $post = $req->getPost();
        $res = $this->getResponse();
        // $t_pengurangan = ($post->t_pengurangan > str_ireplace(".","",$post->t_totalspt)) ? $post->t_totalspt : $post->t_pengurangan;
        $presentase = $post->t_persenpengurangan;
        $totalspt = str_ireplace(".", "", $post ->t_totalspt); 
        
        $t_hasilpresentase = ($presentase * $totalspt) / 100;
        
        $t_totalpajak = $totalspt - $t_hasilpresentase;
        //var_dump($t_totalpajak);exit();
        $data = array(
            "t_persenpengurangan" => $presentase,
            "t_pengurangan" => number_format($t_hasilpresentase, 0, ",", "."),
            "t_totalpajak" => number_format($t_totalpajak, 0, ",", ".")
        );
        $res->setContent(\Zend\Json\Json::encode($data));
        return $res;
    }

    public function cetakskpenguranganAction() {
        $session = $this->getServiceLocator()->get('EtaxService')->getStorage()->read();
        $frm = new \Bphtb\Form\Pencetakan\SSPDFrm();
        $req = $this->getRequest();
        if ($req->isGet()) {
            $base = new \Bphtb\Model\Pencetakan\SSPDBase();
            $frm->setData($req->getQuery());
            if ($frm->isValid()) {
                $base->exchangeArray($frm->getData());
                $data_get = $req->getQuery();
                //=============== Model\Pencetakan\SSPDTable
                $datapengurangan = $this->getSSPD()->ambildatapengurangan($data_get);
                $ar_tglcetak = $base->tgl_cetak;
                $ar_pemda = $this->getPemda()->getdata();
            }
        }
        $pdf = new \DOMPDFModule\View\Model\PdfModel();
        $pdf->setOption('filename', 'SKPengurangan');
        $pdf->setOption('paperSize', 'legal');
        $pdf->setOption('paperOrientation', 'potrait');
        $pdf->setVariables(array(
            'datapengurangan' => $datapengurangan,
            'tgl_cetak' => $ar_tglcetak,
            'data_pemda' => $ar_pemda
        ));
        return $pdf;
    }

    public function populateComboJenisTransaksi() {
        $data = $this->getTblJenTran()->comboBox();
        $selectData = array();
        foreach ($data as $row) {
            $selectData[$row["s_idjenistransaksi"]] = $row["s_namajenistransaksi"];
        }
        return $selectData;
    }

    public function getTblJenTran() {
        if (!$this->tbl_jenistransaksi) {
            $sm = $this->getServiceLocator();
            $this->tbl_jenistransaksi = $sm->get('JenisTransaksiBphtbTable');
        }
        return $this->tbl_jenistransaksi;
    }

    public function getTblPembayaran() {
        if (!$this->tbl_pembayaran) {
            $sm = $this->getServiceLocator();
            $this->tbl_pembayaran = $sm->get('PembayaranSptTable');
        }
        return $this->tbl_pembayaran;
    }

    public function populateComboHakTanah() {
        $data = $this->getTblHakTan()->comboBox();
        $selectData = array();
        foreach ($data as $row) {
            $selectData[$row->s_idhaktanah] = $row->s_namahaktanah;
        }
        return $selectData;
    }

    public function populateComboDokTanah() {
        $data = $this->getTblDokTan()->comboBox();
        $selectData = array();
        foreach ($data as $row) {
            $selectData[$row->s_iddoktanah] = $row->s_namadoktanah;
        }
        return $selectData;
    }

    private function populateComboNotaris() {
        $data = $this->getNotaris()->getdataCombo();
        $selectData = array();
        foreach ($data as $row) {
            $selectData[$row['s_iduser']] = $row['s_namanotaris'];
        }
        return $selectData;
    }

    function populatePersyaratanId($id) {
        $data = $this->getTblPersyaratan()->getDataIdTransaksis($id);
        $selectData = array();
        foreach ($data as $row) {
            $selectData[$row->s_idpersyaratan] = $row->s_namapersyaratan;
        }
        return $selectData;
    }

    public function getTblHakTan() {
        if (!$this->tbl_haktanah) {
            $sm = $this->getServiceLocator();
            $this->tbl_haktanah = $sm->get('HakTanahTable');
        }
        return $this->tbl_haktanah;
    }

    public function getNotaris() {
        if (!$this->tbl_notaris) {
            $sm = $this->getServiceLocator();
            $this->tbl_notaris = $sm->get('NotarisBphtbTable');
        }
        return $this->tbl_notaris;
    }

    public function getTblSSPDBphtb() {
        if (!$this->tbl_sspdbphtb) {
            $sm = $this->getServiceLocator();
            $this->tbl_sspdbphtb = $sm->get("SSPDBphtbTable");
        }
        return $this->tbl_sspdbphtb;
    }

    public function getTblPengurangan() {
        if (!$this->tbl_pengurangan) {
            $sm = $this->getServiceLocator();
            $this->tbl_pengurangan = $sm->get("PenguranganTable");
        }
        return $this->tbl_pengurangan;
    }

    public function getTblVerifikasi() {
        if (!$this->tbl_verifikasi) {
            $sm = $this->getServiceLocator();
            $this->tbl_verifikasi = $sm->get("VerifikasiSptTable");
        }
        return $this->tbl_verifikasi;
    }

    public function getTblPersyaratan() {
        if (!$this->tbl_persyaratan) {
            $sm = $this->getServiceLocator();
            $this->tbl_persyaratan = $sm->get('PersyaratanTable');
        }
        return $this->tbl_persyaratan;
    }

    public function getTbl() {
        if (!$this->tbl_spt) {
            $sm = $this->getServiceLocator();
            $this->tbl_spt = $sm->get("SPTTable");
        }
        return $this->tbl_spt;
    }

    public function getTblDokTan() {
        if (!$this->tbl_doktanah) {
            $sm = $this->getServiceLocator();
            $this->tbl_doktanah = $sm->get('DokTanahTable');
        }
        return $this->tbl_doktanah;
    }

    public function getPemda() {
        if (!$this->tbl_pemda) {
            $sm = $this->getServiceLocator();
            $this->tbl_pemda = $sm->get("PemdaTable");
        }
        return $this->tbl_pemda;
    }

    public function getTableSSPD() {
        if (!$this->tbl_sspd) {
            $sm = $this->getServiceLocator();
            $this->tbl_sspd = $sm->get("SSPDTable");
        }
        return $this->tbl_sspd;
    }

    public function getTblWaris() {
        if (!$this->tbl_sspdwaris) {
            $sm = $this->getServiceLocator();
            $this->tbl_sspdwaris = $sm->get("SPTWarisTable");
        }
        return $this->tbl_sspdwaris;
    }

    public function getTblSpt() {
        if (!$this->tbl_spt) {
            $sm = $this->getServiceLocator();
            $this->tbl_spt = $sm->get("SPTTable");
        }
        return $this->tbl_spt;
    }

    public function getSSPD() {
        if (!$this->tbl_sspd) {
            $sm = $this->getServiceLocator();
            $this->tbl_sspd = $sm->get("SSPDTable");
        }
        return $this->tbl_sspd;
    }

}
