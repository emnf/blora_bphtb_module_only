<?php

namespace Bphtb\Controller\Setting;

use Zend\View\Model\ViewModel;

class SettingButton extends \Zend\Mvc\Controller\AbstractActionController
{
    public function indexAction()
    {
        $session = $this->getServiceLocator()->get('EtaxService')->getStorage()->read();
        $req = $this->getRequest();
        if($req->isPost()){
            $post = $req->getPost();
            // var_dump($post);exit();
            $this->getServiceLocator()->get("ButtonTable")->simpan($post);
            return $this->getResponse();
        }
        $ar_pemda = $this->getServiceLocator()->get("PemdaTable")->getdata();
        $data = $this->getServiceLocator()->get("ButtonTable")->getData();
        $view = new ViewModel([
            'data' => $data,
        ]);
        $datane = array(
            'side_setting_button' => 'active',
            'role_id' => $session['s_akses'],
            'data_pemda' => $ar_pemda,
            'aturgambar' => 1,
            'username' => $session['s_username'],
        );
        $this->layout()->setVariables($datane);
        return $view;
    }
}
