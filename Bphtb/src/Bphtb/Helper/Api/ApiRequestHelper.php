<?php

namespace Bphtb\Helper\Api;

class ApiRequestHelper
{
    public static function formatRequest($request, $method, $tipe = null, $allParams = null)
    {
        $error = 0;
        $message = [];

        if ($request->getMethod() != $method) {
            $error++;
            $message[] = ["405" => "Method Not Allowed"];
        }

        if (!$request->getHeaders("Content-Type") || $request->getHeaders("Content-Type")->getFieldValue() != "application/json") {
            $error = $error + 1;
            $message[] = ["400" => "Content-Type invalid"];
        }

        if (!$request->getHeaders("Authorization")) {
            $error = $error + 1;
            $message[] = ["400" => "Authorization not found"];
        }

        if ($tipe == ApiConstHelper::TYPE_KODEBAYAR && empty($allParams['id'])) {
            $error = $error + 1;
            $message[] = ["400" => "Kodebayar not found"];
        }

        return [
            "error" => $error,
            "message" => $message
        ];
    }
}
