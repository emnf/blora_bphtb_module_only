<?php

namespace Bphtb\Model\Pendataan;

use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Sql;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Where;
use Zend\Db\TableGateway\TableGateway;
use Zend\Debug\Debug;
use Zend\Math\Rand;

class SPTTable extends AbstractTableGateway
{

    protected $table = 't_spt';

    public function __construct(Adapter $adapter)
    {
        $this->adapter = $adapter;
        $this->resultSetPrototype = new ResultSet();
        $this->resultSetPrototype->setArrayObjectPrototype(new SPTBase());
        $this->initialize();
    }

    public function savedata_sismiop($idspt, $iduser)
    {
        $data = array(
            //            
            't_input_sismiop' => 1,
            't_iduser_sismiop' => $iduser,
            't_tglproses_sismiop' => date('Y-m-d')
        );
        $tabelspt = new TableGateway('t_spt', $this->adapter);
        $tabelspt->update($data, array('t_idspt' => $idspt));
    }

    //============ ini buat ambil tarif bphtb sekarang
    public function gettarifbphtbsekarang()
    {
        $sql = "select *
                from s_tarifbphtb
                where s_statustarifbphtb=2";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        return $res->current();
    }

    public function gettarifbphtbspt($id)
    {
        $sql = "select *
                from t_spt
                where t_idspt=" . $id . "";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        return $res->current();
    }

    public function ceknik($nik, $tglproses)
    {

        $sql = "SELECT a.t_idspt, a.t_tglprosesspt, a.t_periodespt, a.t_nopbphtbsppt, 
    a.t_kodebayarbanksppt, a.t_totalspt, a.t_nilaitransaksispt, a.t_kohirspt, 
    a.t_potonganspt, a.t_idjenistransaksi, a.t_idjenishaktanah, a.t_thnsppt, 
    b.t_namawppembeli, b.t_nikwppembeli, b.t_npwpwppembeli, b.t_alamatwppembeli, 
    b.t_rtwppembeli, b.t_rwwppembeli, b.t_kecamatanwppembeli, 
    b.t_kelurahanwppembeli, b.t_telponwppembeli, b.t_kodeposwppembeli, 
    b.t_namawppenjual, b.t_nikwppenjual, b.t_npwpwppenjual, b.t_alamatwppenjual, 
    b.t_kelurahanwppenjual, b.t_kecamatanwppenjual, b.t_kabkotawppenjual, 
    b.t_telponwppenjual, b.t_kodeposwppenjual, b.t_alamatop, b.t_rtop, b.t_rwop, 
    b.t_luastanah, b.t_njoptanah, b.t_totalnjoptanah, b.t_luasbangunan, 
    b.t_njopbangunan, b.t_totalnjopbangunan, b.t_grandtotalnjop, 
    b.t_nosertifikathaktanah, b.t_iddetailsptbphtb, b.t_kelurahanop, 
    b.t_kecamatanop, c.s_namajenistransaksi, d.s_namahaktanah, a.t_idnotarisspt, 
    b.t_kabkotawppembeli, a.t_persyaratan, b.t_kabupatenop, a.t_idsptsebelumnya, 
    b.t_inputbpn, a.t_kohirketetapanspt, b.t_rtwppenjual, b.t_rwwppenjual, 
    a.t_idtarifbphtb, a.t_persenbphtb
   FROM t_spt a
   LEFT JOIN t_detailsptbphtb b ON b.t_idspt = a.t_idspt
   LEFT JOIN s_jenistransaksi c ON c.s_idjenistransaksi = a.t_idjenistransaksi
   LEFT JOIN s_jenishaktanah d ON d.s_idhaktanah = a.t_idjenishaktanah
    where b.t_nikwppembeli='" . $nik . "' and a.t_periodespt='" . $tglproses . "' order by a.t_idspt asc";

        /*$sql = "select * from view_sspd a
                left join t_pembayaranspt b on b.t_idspt = a.t_idspt
                where 
                a.t_nikwppembeli='".$nik."' 
                and a.t_periodespt='".$tglproses."' order by a.t_idspt asc";*/

        //var_dump($sql);
        //exit();

        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        return $res->current();
    }

    public function cekpotongannpoptkp($id)
    {
        $sql = "select cast(s_tarifnpotkp AS numeric) from s_tarifnpoptkp
                where s_idjenistransaksinpotkp='" . $id . "'";
        $statement = $this->adapter->query($sql);
        return $statement->execute()->current();
    }

    //============ end ini buat ambil tarif bphtb sekarang


    public function savedata(SPTBase $spt, $nop, $kodebayarbankop, $s_idjenistransaksi, $s_idhaktanah, $bphtbterhutangop, $hargatransaksiop, $potongan, $idnotaris, $idpendaftar, $dt, $t_potongan_waris_hibahwasiat)
    {
        $id = (int) $spt->t_idspt;

        $tarifbphtbsekarang = $this->gettarifbphtbsekarang();

        if (!empty($dt->t_tarif_pembagian_aphb_kali)) {
            $t_tarif_pembagian_aphb_kali = $dt->t_tarif_pembagian_aphb_kali;
        } else {
            $t_tarif_pembagian_aphb_kali = 0;
        }

        if (!empty($dt->t_tarif_pembagian_aphb_bagi)) {
            $t_tarif_pembagian_aphb_bagi = $dt->t_tarif_pembagian_aphb_bagi;
        } else {
            $t_tarif_pembagian_aphb_bagi = 0;
        }

        if ($id == 0) {
            $dataa = $this->getKohirFsave($spt);

            $data = array(
                't_kohirspt' => $dataa['t_kohirspt'] + 1,
                't_kodedaftarspt' =>  str_replace('-', '', date('y')) . '.' . $idnotaris . '.' . str_replace('-', '', date('m')) . '.' . str_pad($dataa['t_kohirspt'] + 1, 4, '0', STR_PAD_LEFT),
                't_tglprosesspt' => date('Y-m-d', strtotime($spt->t_tglprosesspt)),
                't_periodespt' => date('Y', strtotime($spt->t_tglprosesspt)),
                't_idnotarisspt' => $idnotaris,
                't_tgljatuhtempospt' => $spt->t_tgljatuhtempospt,
                't_nopbphtbsppt' => $nop,
                't_kodebayarbanksppt' => $kodebayarbankop,
                't_idjenistransaksi' => $s_idjenistransaksi,
                't_idjenishaktanah' => $s_idhaktanah,
                't_dasarspt' => $spt->t_dasarspt,
                't_totalspt' => str_ireplace(".", "", $bphtbterhutangop),
                't_nilaitransaksispt' => str_ireplace(".", "", $hargatransaksiop),
                't_potonganspt' => str_ireplace(".", "", $potongan),
                't_thnsppt' => $spt->t_thnsppt,
                't_persyaratan' => \Zend\Json\Json::encode($spt->t_persyaratan),
                't_idjenisdoktanah' => $spt->t_idjenisdoktanah,
                't_pejabatpendaftaranspt' => $idpendaftar,
                't_tarif_pembagian_aphb_kali' => $t_tarif_pembagian_aphb_kali,
                't_tarif_pembagian_aphb_bagi' => $t_tarif_pembagian_aphb_bagi,
                't_idtarifbphtb' => $tarifbphtbsekarang['s_idtarifbphtb'], //$spt->t_idtarifbphtb,
                't_persenbphtb' => $tarifbphtbsekarang['s_tarifbphtb'],
                't_potongan_waris_hibahwasiat' => $t_potongan_waris_hibahwasiat,
                't_jenispembelian' => $spt->t_jenispembelian,
                't_latitudeobjek' => $spt->t_latitudeobjek,
                't_longitudeobjek' => $spt->t_longitudeobjek,
            );
            $this->insert($data);
            $rowset1 = $this->select(array('t_kohirspt' => $data['t_kohirspt'], 't_periodespt' => date('Y', strtotime($spt->t_tglprosesspt))));
            $row1 = $rowset1->current();
            return $row1;
        } else {
            $data = array(
                't_idnotarisspt' => $idnotaris,
                't_tgljatuhtempospt' => $spt->t_tgljatuhtempospt,
                't_nopbphtbsppt' => $nop,
                't_kodebayarbanksppt' => $kodebayarbankop,
                't_idjenistransaksi' => $s_idjenistransaksi,
                't_idjenishaktanah' => $s_idhaktanah,
                't_dasarspt' => $spt->t_dasarspt,
                't_totalspt' => str_ireplace(".", "", $bphtbterhutangop),
                't_nilaitransaksispt' => str_ireplace(".", "", $hargatransaksiop),
                't_potonganspt' => str_ireplace(".", "", $potongan),
                't_thnsppt' => $spt->t_thnsppt,
                't_persyaratan' => \Zend\Json\Json::encode($spt->t_persyaratan),
                't_tarif_pembagian_aphb_kali' => $t_tarif_pembagian_aphb_kali,
                't_tarif_pembagian_aphb_bagi' => $t_tarif_pembagian_aphb_bagi,
                't_potongan_waris_hibahwasiat' => $t_potongan_waris_hibahwasiat,
                't_latitudeobjek' => $spt->t_latitudeobjek,
                't_longitudeobjek' => $spt->t_longitudeobjek,
            );
            $this->update($data, array('t_idspt' => $spt->t_idspt));
            $rowset = $this->select(array('t_kohirspt' => $spt->t_kohirspt, 't_periodespt' => date('Y', strtotime($spt->t_tglprosesspt))));
            $row = $rowset->current();
            return $row;
        }
    }

    //============== function savedata ini digunakan pada controller PendataanSSPD = tambahAction, 
    public function savedata2(SPTBase $spt, SSPDBphtbBase $sspddetail, $idnotaris, $idpendaftar, $dt)
    {

        $delay = Rand::getString(1, '123456789', true);
        sleep($delay);

        $id = (int) $spt->t_idspt;
        $dataa = $this->getKohirFSave($spt);
        //        $noterakir = str_pad($dataa['t_kohirspt'],7,5);
        //        $nextno ++;


        $data = array(
            't_kohirspt' => $dataa['t_kohirspt'] + 1,
            't_kodedaftarspt' =>  str_replace('-', '', date('y')) . '.' . $idnotaris . '.' . str_replace('-', '', date('m')) . '.' . str_pad($dataa['t_kohirspt'] + 1, 4, '0', STR_PAD_LEFT),
            't_tglprosesspt' => date('Y-m-d', strtotime($spt->t_tglprosesspt)),
            't_periodespt' => date('Y', strtotime($spt->t_tglprosesspt)),
            't_idnotarisspt' => $idnotaris,
            't_tgljatuhtempospt' => $spt->t_tgljatuhtempospt,
            't_nopbphtbsppt' => $sspddetail->t_nopbphtbsppt,
            't_kodebayarbanksppt' => $sspddetail->t_kodebayarbanksppt,
            't_idjenistransaksi' => $sspddetail->t_idjenistransaksi,
            't_idjenishaktanah' => $sspddetail->t_idjenishaktanah,
            't_dasarspt' => $spt->t_dasarspt,
            't_totalspt' => str_ireplace(".", "", $sspddetail->t_totalspt),
            't_nilaitransaksispt' => str_ireplace(".", "", $sspddetail->t_nilaitransaksispt),
            't_potonganspt' => str_ireplace(".", "", $sspddetail->t_potonganspt),
            't_thnsppt' => $spt->t_thnsppt,
            't_persyaratan' => \Zend\Json\Json::encode($spt->t_persyaratan),
            't_idjenisdoktanah' => $spt->t_idjenisdoktanah,
            't_pejabatpendaftaranspt' => $idpendaftar,
            't_idtarifbphtb' => $spt->t_idtarifbphtb
            // Detail spt =================================================================
        );
        $datadetail = array(
            //            'idspt' => $idspt,
            // Data Wajib Pajak
            't_namawppembeli' => $sspddetail->t_namawppembeli,
            't_nikwppembeli' => $sspddetail->t_nikwppembeli,
            't_npwpwppembeli' => $sspddetail->t_npwpwppembeli,
            't_alamatwppembeli' => $sspddetail->t_alamatwppembeli,
            't_rtwppembeli' => $sspddetail->t_rtwppembeli,
            't_rwwppembeli' => $sspddetail->t_rwwppembeli,
            't_kecamatanwppembeli' => $sspddetail->t_kecamatanwppembeli,
            't_kelurahanwppembeli' => $sspddetail->t_kelurahanwppembeli,
            't_kabkotawppembeli' => $sspddetail->t_kabkotawppembeli,
            't_telponwppembeli' => $sspddetail->t_telponwppembeli,
            't_kodeposwppembeli' => $sspddetail->t_kodeposwppembeli,
            // Data Objek Pajak
            't_namasppt' => $sspddetail->t_namasppt,
            't_alamatop' => $sspddetail->t_alamatop,
            't_rtop' => $sspddetail->t_rtop,
            't_rwop' => $sspddetail->t_rwop,
            't_kelurahanop' => $sspddetail->t_kelurahanop,
            't_kecamatanop' => $sspddetail->t_kecamatanop,
            't_kabupatenop' => $sspddetail->t_kabupatenop,
            't_luastanah' => str_ireplace(".", "", $sspddetail->t_luastanah),
            't_njoptanah' => str_ireplace(".", "", $sspddetail->t_njoptanah),
            't_totalnjoptanah' => str_ireplace(".", "", $sspddetail->t_totalnjoptanah),
            't_luasbangunan' => str_ireplace(".", "", $sspddetail->t_luasbangunan),
            't_njopbangunan' => str_ireplace(".", "", $sspddetail->t_njopbangunan),
            't_totalnjopbangunan' => str_ireplace(".", "", $sspddetail->t_totalnjopbangunan),
            't_grandtotalnjop' => str_ireplace(".", "", $sspddetail->t_grandtotalnjop),
            't_nosertifikathaktanah' => $sspddetail->t_nosertifikathaktanah,
            't_ketwaris' => $sspddetail->t_ketwaris,
            't_terbukti' => $sspddetail->t_terbukti,
            't_tglajb' => date('Y-m-d', strtotime($sspddetail->t_tglajb)),
            't_namasppt' => $sspddetail->t_namasppt,
            // Data Penjual
            't_namawppenjual' => $sspddetail->t_namawppenjual,
            't_nikwppenjual' => $sspddetail->t_nikwppenjual,
            't_npwpwppenjual' => $sspddetail->t_npwpwppenjual,
            't_alamatwppenjual' => $sspddetail->t_alamatwppenjual,
            't_kelurahanwppenjual' => $sspddetail->t_kelurahanwppenjual,
            't_kecamatanwppenjual' => $sspddetail->t_kecamatanwppenjual,
            't_kabkotawppenjual' => $sspddetail->t_kabkotawppenjual,
            't_telponwppenjual' => $sspddetail->t_telponwppenjual,
            't_kodeposwppenjual' => $sspddetail->t_kodeposwppenjual,
            't_rtwppenjual' => $sspddetail->t_rtwppenjual,
            't_rwwppenjual' => $sspddetail->t_rwwppenjual,

            't_luastanah_sismiop' => $sspddetail->t_luastanah_sismiop,
            't_luasbangunan_sismiop' => $sspddetail->t_luasbangunan_sismiop,
            't_njoptanah_sismiop' => $sspddetail->t_njoptanah_sismiop,
            't_njopbangunan_sismiop' => $sspddetail->t_njopbangunan_sismiop
        );
        if ($id == 0) {
            $tabelspt = new TableGateway('t_spt', $this->adapter);
            $tabelspt->insert($data);

            $delay2 = Rand::getString(1, '123456789', true);
            sleep($delay2);

            $rowset = $tabelspt->select(array('t_kohirspt' => $data['t_kohirspt'], 't_periodespt' => date('Y', strtotime($spt->t_tglprosesspt))));
            $row = $rowset->current();
            $datadetail['t_idspt'] = $row['t_idspt'];
            $tabeldetail = new TableGateway('t_detailsptbphtb', $this->adapter);
            $tabeldetail->insert($datadetail);
        } else {
            $data['t_kohirspt'] =  $spt->t_kohirspt;
            $tabelspt = new TableGateway('t_spt', $this->adapter);
            $tabelspt->update($data, array('t_idspt' => $spt->t_idspt));
            $tabeldetail = new TableGateway('t_detailsptbphtb', $this->adapter);
            $tabeldetail->update($datadetail, array('t_iddetailsptbphtb' => $sspddetail->t_iddetailsptbphtb));
        }
    }

    public function persyaratan($dt)
    {
        //        $data = "";
        //        foreach ($dt->persyaratann as $row) {
        //            $data .= "|" . $row;
        //        }
        //        return $data;
    }

    public function getKohirFsave(SPTBase $spt)
    {
        $sql = "select max(t_kohirspt) as t_kohirspt 
                from t_spt
                where t_periodespt='" . date('Y') . "'"; //, strtotime($spt->t_tglprosesspt)    
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        return $res->current();
    }

    public function hapusDataSpt(SPTBase $kb)
    {
        $this->delete(array('t_idspt' => $kb->t_idspt));
    }

    public function savedatavalidasikedua(SPTBase $spt, $idpendaftar, $input, $t_potonganspt, $t_totalspt, $aphb_kali, $aphb_bagi, $fr_statusvalidasi, $t_potongan_waris_hibahwasiat)
    {

        $tarifbphtbsekarang = $this->gettarifbphtbsekarang();

        $dataa = $this->getKohirFSave($spt);
        $data = array(
            't_kohirspt' => $dataa['t_kohirspt'] + 1,
            't_kodedaftarspt' =>  str_replace('-', '', date('y')) . '.' . $idnotaris . '.' . str_replace('-', '', date('m')) . '.' . str_pad($dataa['t_kohirspt'] + 1, 4, '0', STR_PAD_LEFT),
            't_tglprosesspt' => date('Y-m-d'),
            't_periodespt' => date('Y'),
            't_idnotarisspt' => $spt->t_idnotarisspt,

            't_tgljatuhtempospt' => $spt->t_tgljatuhtempospt,
            't_nopbphtbsppt' => $spt->t_nopbphtbsppt,
            't_kodebayarbanksppt' => '28' . ($dataa['t_kohirspt'] + 1) . date('Y') . '',
            't_idjenistransaksi' => $spt->t_idjenistransaksi,
            't_idjenishaktanah' => $spt->t_idjenishaktanah,

            't_dasarspt' => $spt->t_dasarspt,
            't_totalspt' => str_ireplace(".", "", $t_totalspt),
            't_nilaitransaksispt' => str_ireplace(".", "", $input->getPost('p_nilaitransaksispt')),
            't_potonganspt' => str_ireplace(".", "", $t_potonganspt),
            't_thnsppt' => $spt->t_thnsppt,
            't_persyaratan' => $spt->t_persyaratan,
            't_idjenisdoktanah' => $spt->t_idjenisdoktanah,
            't_idsptsebelumnya' => $spt->t_idspt,

            't_pejabatpendaftaranspt' => $idpendaftar,
            't_tarif_pembagian_aphb_kali' => $aphb_kali,
            't_tarif_pembagian_aphb_bagi' => $aphb_bagi,
            't_idtarifbphtb' => $spt->t_idtarifbphtb, //$spt->t_idtarifbphtb,
            't_persenbphtb' => $spt->t_persenbphtb,
            't_potongan_waris_hibahwasiat' => $t_potongan_waris_hibahwasiat

        );
        $this->insert($data);

        $dataupdate = array('fr_tervalidasidua' => $fr_statusvalidasi);
        $this->update($dataupdate, array('t_idspt' => $spt->t_idspt));

        $rowset1 = $this->select(array('t_kohirspt' => $data['t_kohirspt'], 't_periodespt' => date('Y')));
        $row1 = $rowset1->current();
        return $row1;
    }

    public function savedatabpn(SPTBase $spt)
    {
        $total = (int) $spt->t_totalspt;
        $dataa = $this->getKohirFSave($spt);
        $data = array(
            't_kohirspt' => $dataa['t_kohirspt'] + 1,
            't_kodedaftarspt' =>  str_replace('-', '', date('y')) . '.' . $idnotaris . '.' . str_replace('-', '', date('m')) . '.' . str_pad($dataa['t_kohirspt'] + 1, 4, '0', STR_PAD_LEFT),
            't_tglprosesspt' => date('Y-m-d'),
            't_periodespt' => date('Y'),
            't_idnotarisspt' => $spt->t_idnotarisspt,
            't_tgljatuhtempospt' => $spt->t_tgljatuhtempospt,
            't_nopbphtbsppt' => $spt->t_nopbphtbsppt,
            't_kodebayarbanksppt' => $spt->t_kodebayarbanksppt,
            't_idjenistransaksi' => $spt->t_idjenistransaksi,
            't_idjenishaktanah' => $spt->t_idjenishaktanah,
            't_dasarspt' => $spt->t_dasarspt,
            't_totalspt' => $total,
            't_nilaitransaksispt' => $spt->t_nilaitransaksispt,
            't_potonganspt' => $spt->t_potonganspt,
            't_thnsppt' => $spt->t_thnsppt,
            't_persyaratan' => $spt->t_persyaratan,
            't_idjenisdoktanah' => $spt->t_idjenisdoktanah,
            't_idsptsebelumnya' => $spt->t_idspt
        );
        $this->insert($data);
        $rowset1 = $this->select(array('t_kohirspt' => $data['t_kohirspt'], 't_periodespt' => date('Y', strtotime($spt->t_tglprosesspt))));
        $row1 = $rowset1->current();
        return $row1;
    }

    public function getSptid($t_idspt)
    {
        $rowset = $this->select(array('t_idspt' => $t_idspt));
        $row = $rowset->current();
        return $row;
    }
    public function getmaxkohir()
    {
        //$sql = "select max(t_kohirketetapanspt) as t_kohirketetapanspt from t_spt";
        $sql = "SELECT COALESCE(max(t_kohirspt)+1,1) as t_kohirspt FROM t_spt WHERE t_periodespt::text='" . date('Y') . "'";
        $statement = $this->adapter->query($sql);
        return $statement->execute()->current();
    }

    public function getDataHistoriNOP($nop, $tahun)
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from(['a' => 't_spt']);
        $select->columns([
            't_idspt', 't_idsptsebelumnya', 't_tglprosesspt', 't_periodespt', 't_nopbphtbsppt', 't_nilaitransaksispt',
            't_totalspt',
            't_thnsppt', 'fr_tervalidasidua',
            'njoptanahtransaksi' => new Expression('(COALESCE( ROUND( ( b.t_grandtotalnjop - b.t_totalnjopbangunan ) :: NUMERIC / NULLIF ( b.t_luastanah, 0 :: NUMERIC ) ), 0))'),
            'njoptanah' => new Expression('(COALESCE( ROUND( ( a.t_nilaitransaksispt - b.t_totalnjopbangunan ) :: NUMERIC / NULLIF ( b.t_luastanah, 0 :: NUMERIC ) ), 2))'),
        ]);
        $select->join(['b' => 't_detailsptbphtb'], new Expression('a.t_idspt = b.t_idspt'), [
            't_luastanah', 't_njoptanah', 't_luasbangunan', 't_njopbangunan', 't_totalnjoptanah',
            't_totalnjopbangunan', 't_grandtotalnjop', 't_kelurahanop', 't_kecamatanop', 't_alamatop',
            't_rtop', 't_rwop', 't_kabupatenop',
        ], 'LEFT');
        $select->join(['c' => 't_pembayaranspt'], new Expression('a.t_idspt = c.t_idspt'), [
            't_idpembayaranspt'
        ], 'LEFT');
        $select->join(['f' => 's_jenistransaksi'], new Expression('a.t_idjenistransaksi = f.s_idjenistransaksi'), [
            's_namajenistransaksi', 's_kodejenistransaksi'
        ], 'LEFT');
        $where = new Where();
        $where->literal('SUBSTRING(a.t_nopbphtbsppt, 1, 17) = \'' . substr($nop, 0, 17) . '\'');
        // $where->literal('a.t_thnsppt = \''. $tahun .'\'');
        $where->literal('a.t_thnsppt BETWEEN \'' . ($tahun - 4) . '\' AND \'' . $tahun . '\'');
        // yg ditampilkan hanya data spt terbaru
        // spt sebelum skpkb/skpdn diabaikan
        $where->literal('NOT EXISTS(SELECT * FROM t_spt WHERE t_idsptsebelumnya is not null ANd t_idsptsebelumnya = a.t_idspt)');
        $select->where($where);
        $selectRow = $sql->select(array('x' => $select));
        $selectRow->order('t_thnsppt DESC, njoptanah DESC');
        $res = $sql->prepareStatementForSqlObject($selectRow)->execute();
        return $res;
    }

    public function updateSptSyaratYgSudahUpload($idspt)
    {
        $sql = new Sql($this->adapter);

        $select1 = $sql->select('t_spt')->where(['t_idspt' => $idspt]);
        $res1 = $sql->prepareStatementForSqlObject($select1)->execute()->current();

        $select = $sql->select("t_filesyarat");
        $select->columns([
            "t_persyaratan_sudah_upload" => new Expression("(
                '[\"'|| array_to_string(array_agg(DISTINCT s_idpersyaratan), '\",\"') || '\"]'
            )")
        ]);
        $select->where(["t_idspt" => $idspt, 's_idjenistransaksi' => $res1['t_idjenistransaksi']]);
        $res = $sql->prepareStatementForSqlObject($select)->execute()->current();

        if ($res) {
            $update = $sql->update("t_spt");
            $update->set([
                "t_persyaratan_sudah_upload" => $res["t_persyaratan_sudah_upload"]
            ]);
            $update->where(["t_idspt" => $idspt]);
            $res = $sql->prepareStatementForSqlObject($update)->execute();
        }
    }

    public function updatekordinat($data)
    {
        $kordinat = array(
            't_latitudeobjek' => $data['t_latitudeobjek'],
            't_longitudeobjek' => $data['t_longitudeobjek']

        );

        $t_detailsptbphtb = new TableGateway('t_spt', $this->adapter);
        $t_detailsptbphtb->update($kordinat, array(
            't_idspt' => $data['t_idspt']
        ));
    }
}
