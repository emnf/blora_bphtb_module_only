<?php

namespace Bphtb\Controller\Api;

use Bphtb\Helper\Api\ApiConstHelper;
use Bphtb\Helper\Api\ApiRequestHelper;
use Bphtb\Helper\Api\ApiResponseHelper;
use Zend\Mvc\Controller\AbstractActionController;

class ApiController extends AbstractActionController
{
    public function cekurl()
    {
        $basePath = $this->getRequest()->getBasePath();
        $uri = new \Zend\Uri\Uri($this->getRequest()->getUri());
        $uri->setPath($basePath);
        $uri->setQuery(array());
        $uri->setFragment('');

        return $uri->getScheme() . '://' . $uri->getHost() . ':' . $_SERVER['SERVER_PORT'] . '' . $uri->getPath();
    }

    public function indexAction()
    {

        $request = $this->getRequest();
        $formatRequest = ApiRequestHelper::formatRequest($request, ApiConstHelper::METHOD_GET);

        $resp_data = null;
        $resp_code = ApiConstHelper::CODE_ERROR;
        $resp_message = ApiConstHelper::MESSAGE_ERROR;
        $username = null;

        if ($formatRequest["error"] == 0) {
            $authorization = $request->getHeaders("Authorization")->getFieldValue();
            $checkUser = $this->ApiLoginHelper()->checkUserLogin($authorization);

            // var_dump($checkUser);exit();
            if ($checkUser['resp_code'] != ApiConstHelper::CODE_SUCCESS) {
                $resp_code = $checkUser['resp_code'];
                $resp_message = $checkUser['resp_message'];
            } else {
                $resp_code = ApiConstHelper::CODE_SUCCESS;
                $resp_message = ApiConstHelper::MESSAGE_SUCCESS;
                $resp_data = [
                    "Title" => "Dokumentasi Api",
                    "Api Service" => [
                        'GET DATA BPHTB BY KODEBAYAR' => [
                            'url' => $this->cekurl() . '/api/get-data-by-kodebayar/{kodebayar}',
                            "headers" => [
                                "Content-Type" => "application/json",
                                "Authorization" => "xxx"
                            ],
                        ]
                    ]
                ];
            }
        } else {
            $resp_data = $formatRequest['message'];
        }

        $response = $this->getResponse();
        $response->getHeaders()->addHeaderLine('Content-Type', 'application/json; charset=UTF-8;');
        $this->layout()->setTerminal(true);

        return ApiResponseHelper::formatResponse($response, $resp_data, $resp_code, $resp_message, 200, $username);
    }

    public function getDataByKodebayarAction()
    {
        $request = $this->getRequest();
        $allParams = (array) $this->getEvent()->getRouteMatch()->getParams();
        $formatRequest = ApiRequestHelper::formatRequest($request, ApiConstHelper::METHOD_GET, ApiConstHelper::TYPE_KODEBAYAR, $allParams);

        $resp_data = null;
        $resp_code = ApiConstHelper::CODE_ERROR;
        $resp_message = ApiConstHelper::MESSAGE_ERROR;
        $username = null;

        if ($formatRequest["error"] == 0) {
            $authorization = $request->getHeaders("Authorization")->getFieldValue();
            $checkUser = $this->ApiLoginHelper()->checkUserLogin($authorization);
            // var_dump($checkUser);exit();
            if ($checkUser['resp_code'] != ApiConstHelper::CODE_SUCCESS) {
                $resp_code = $checkUser['resp_code'];
                $resp_message = $checkUser['resp_message'];
            } else {
                $kodeBayar = $allParams['id'];
                $data = $this->getServiceLocator()->get('PembayaranSptTable')->getDataByKodeBayar($kodeBayar);

                if ($data == false) {
                    $resp_data = null;
                    $resp_code = ApiConstHelper::CODE_DATA_NOT_FOUND;
                    $resp_message = ApiConstHelper::MESSAGE_DATA_NOT_FOUND;
                } else {
                    $resp_data = $this->setData($data);
                    $resp_code = ApiConstHelper::CODE_SUCCESS;
                    $resp_message = ApiConstHelper::MESSAGE_SUCCESS;
                }
            }
        } else {
            $resp_data = $formatRequest['message'];
        }

        $response = $this->getResponse();
        $response->getHeaders()->addHeaderLine('Content-Type', 'application/json; charset=UTF-8;');
        $this->layout()->setTerminal(true);

        return ApiResponseHelper::formatResponse($response, $resp_data, $resp_code, $resp_message, 200, $username);
    }

    // public function getDataSpopByKodebayarAction()
    // {
    //     $request = $this->getRequest();
    //     $allParams = (array) $this->getEvent()->getRouteMatch()->getParams();
    //     $formatRequest = ApiRequestHelper::formatRequest($request, ApiConstHelper::METHOD_GET, ApiConstHelper::TYPE_KODEBAYAR, $allParams);

    //     $resp_data = null;
    //     $resp_code = ApiConstHelper::CODE_ERROR;
    //     $resp_message = ApiConstHelper::MESSAGE_ERROR;
    //     $username = null;

    //     if ($formatRequest["error"] == 0) {
    //         $authorization = $request->getHeaders("Authorization")->getFieldValue();
    //         $checkUser = $this->ApiLoginHelper()->checkUserLogin($authorization);
    //         // var_dump($checkUser);exit();
    //         if ($checkUser['resp_code'] != ApiConstHelper::CODE_SUCCESS) {
    //             $resp_code = $checkUser['resp_code'];
    //             $resp_message = $checkUser['resp_message'];
    //         } else {
    //             $kodeBayar = $allParams['id'];
    //             $data = $this->getServiceLocator()->get('InputSpopTable')->getDataByKodeBayar($kodeBayar);
    //             // var_dump($data);exit();

    //             if ($data == false) {
    //                 $resp_data = null;
    //                 $resp_code = ApiConstHelper::CODE_DATA_NOT_FOUND;
    //                 $resp_message = ApiConstHelper::MESSAGE_DATA_NOT_FOUND;
    //             } else {
    //                 $resp_data = $this->setData($data);
    //                 $resp_code = ApiConstHelper::CODE_SUCCESS;
    //                 $resp_message = ApiConstHelper::MESSAGE_SUCCESS;
    //             }
    //         }
    //     } else {
    //         $resp_data = $formatRequest['message'];
    //     }

    //     $response = $this->getResponse();
    //     $response->getHeaders()->addHeaderLine('Content-Type', 'application/json; charset=UTF-8;');
    //     $this->layout()->setTerminal(true);

    //     return ApiResponseHelper::formatResponse($response, $resp_data, $resp_code, $resp_message, 200, $username);
    // }

    public function setData($data)
    {
        // var_dump($data);exit();

        $dataReturn['bphtb'] = $data;
        $dataReturn['spop'] = null;
        // $dataReturn['lspop'] = null;
        $getSpop = $this->getServiceLocator()->get('InputSpopTable')->getDataByKodeBayar($data['t_kodebayarbanksppt']);
        // $getLspop = $this->getServiceLocator()->get('InputSpopTable')->getDataLspopByKodeBayar($data['t_kodebayarbanksppt']);
        if ($getSpop) {
            $dataReturn['spop'] = $getSpop;
        }
        // if ($getSpop) {
        //     $dataReturn['lspop'] = $getLspop;
        // }
        return $dataReturn;
    }
}
