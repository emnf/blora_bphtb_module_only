<?php
namespace Bphtb\Model\Pendataan;

use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Where;
use Zend\Db\ResultSet\ResultSet;

class UploadTable extends AbstractTableGateway {
    
    protected $table = "t_detailsptbphtb";
    
    public function __construct(Adapter $adapter) {
        $this->adapter = $adapter;
        $this->resultSetPrototype = new ResultSet();
        $this->resultSetPrototype->setArrayObjectPrototype(new \Bphtb\Model\Pendataan\UploadBase());
        $this->initialize();
    }

    public function getdata() {
        $rowset = $this->select();
        $row = $rowset->current();
        return $row;
    }
    
    public function savedata(\Bphtb\Model\Pendataan\UploadBase $db,$path){        
        $data = array("uploadsyarat"=>$db->uploadsyarat["name"]);
        //if(!empty($path)){
        //    $data["s_logo"] = $path;
        //}
        //$id = (int) $db->s_idpemda;
        //if($id == 0){
        //    $this->insert($data);
        //}else{
            $this->update($data,array("t_iddetailsptbphtb"=>$db->t_iddetailsptbphtb));
           
            //var_dump($db->uploadsyarat);
            exit();
        //}
    }
    
    public function savedata_syarat($idjenistransaksi, $id_detailspt, $file, $keteranganfile, $idsyarat, $letakfile, $idspt, $folderiddetailspt){        
        //$data = array("uploadsyarat"=>$db->uploadsyarat["name"]);
        
            $sql = "INSERT INTO t_filesyarat (letak_file, nama_file, id_detailspt,  tgl_upload, keterangan_file, s_idpersyaratan, t_idspt, s_idjenistransaksi) VALUES ('".$letakfile."', '".$file."',  " . $folderiddetailspt . ", '".date('Y-m-d')."', '".$keteranganfile."', '".$idsyarat."', $idspt, $idjenistransaksi)";
        
            //var_dump($sql);
            //exit();
            
            $statement = $this->adapter->query($sql);
        return $statement->execute();
          
         //   exit();
       
    }
    
    public function hapusdata_syarat($namafile, $idspt, $t_iddetailsptbphtb, $idsyarat){   
        $sql = "delete from t_filesyarat where nama_file='".$namafile."' AND t_idspt::text='".$idspt."' AND id_detailspt::text = '".$t_iddetailsptbphtb."' AND s_idpersyaratan::text='".$idsyarat."' ";

        $statement = $this->adapter->query($sql);
        return $statement->execute();
          
    }
    
    public function savedata_filetransaksilama($idjenistransaksi, $id_detailspt, $file, $keteranganfile, $letakfile, $idspt, $folderiddetailspt){        
        //$data = array("uploadsyarat"=>$db->uploadsyarat["name"]);
        
        $cekdata = "SELECT * FROM t_uploadtransaksilama where t_idspt = ".$idspt." ";
        $cek = $this->adapter->query($cekdata);
        $hasilcek = $cek->execute();
        
        if(count($hasilcek) >= 1){
            @unlink ("".$letakfile."/".$file."");
            echo 'File sudah di upload';
        }else{
        
            $sql = "INSERT INTO t_uploadtransaksilama (letak_file, nama_file, id_detailspt,  tgl_upload, keterangan_file, t_idspt, s_idjenistransaksi) VALUES ('".$letakfile."', '".$file."',  " . $folderiddetailspt . ", '".date('Y-m-d')."', '".$keteranganfile."', $idspt, $idjenistransaksi)";
            $statement = $this->adapter->query($sql);
        
            chmod("".$letakfile."", 0777, true);
        
            return $statement->execute();
        }
        
        //var_dump($statement);
        //exit();
        
        
          
            exit();
       
    }
    
    public function cek_filetransaksilama($idspt){        
        
        $cekdata = "SELECT * FROM t_uploadtransaksilama where t_idspt = ".$idspt." ";
        $cek = $this->adapter->query($cekdata);
        $hasilcek = $cek->execute()->current();
        return $hasilcek;
        
    }
    
    public function updatedata_filetransaksilama($letakfile, $file, $id_filetransaksilama){        
        //$data = array("uploadsyarat"=>$db->uploadsyarat["name"]);
        
        $cekdata = "SELECT * FROM t_uploadtransaksilama where id_filetransaksilama = ".$id_filetransaksilama." ";
        $cek = $this->adapter->query($cekdata);
        $hasilcek = $cek->execute()->current();
        
        
            @unlink ("".$hasilcek['letak_file']."/".$hasilcek['nama_file']."");
            
        
            $sql = "UPDATE t_uploadtransaksilama SET letak_file = '".$letakfile."', nama_file = '".$file."' WHERE id_filetransaksilama = ".$id_filetransaksilama."";
            $statement = $this->adapter->query($sql);
        
            chmod("".$letakfile."", 0777, true);
        
            return $statement->execute();
       
       
    }
    
    
    
}