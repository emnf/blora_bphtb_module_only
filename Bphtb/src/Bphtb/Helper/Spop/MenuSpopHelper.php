<?php

namespace Bphtb\Helper\Spop;

use Bphtb\Model\Spop\ReffSpopTable;
use Zend\View\Helper\AbstractHelper;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class MenuSpopHelper extends AbstractHelper implements ServiceLocatorAwareInterface
{

    protected $tbl;

    public function __invoke()
    {
        return $this;
    }

    public function setServiceLocator(ServiceLocatorInterface $serviceLocator)
    {
        $this->serviceLocator = $serviceLocator;
        return $this;
    }

    public function getServiceLocator()
    {
        return $this->serviceLocator;
    }

    public function gettbl($tbl_service)
    {
        $this->tbl = $this->getServiceLocator()->getServiceLocator()->get($tbl_service);
        return $this->tbl;
    }

    public function getComboJenisTransaksi()
    {
        return $this->gettbl('ReffSpopTable')->getLookUpItemByKodeGroup(ReffSpopHelper::JENIS_TRANSAKSI_LSPOP);
    }
    
    public function getComboKondisiBangunan()
    {
        return $this->gettbl('ReffSpopTable')->getLookUpItemByKodeGroup(ReffSpopHelper::JENIS_KONDISI_UMUM);
    }

    public function getComboKontruksi()
    {
        return $this->gettbl('ReffSpopTable')->getLookUpItemByKodeGroup(ReffSpopHelper::JENIS_KONSTRUKSI);
    }

    public function getComboAtap()
    {
        return $this->gettbl('ReffSpopTable')->getLookUpItemByKodeGroup(ReffSpopHelper::JENIS_ATAP);
    }
    
    public function getComboDinding()
    {
        return $this->gettbl('ReffSpopTable')->getLookUpItemByKodeGroup(ReffSpopHelper::JENIS_DINDING);
    }
    
    public function getComboLantai()
    {
        return $this->gettbl('ReffSpopTable')->getLookUpItemByKodeGroup(ReffSpopHelper::JENIS_LANTAI);
    }
    
    public function getComboLangit()
    {
        return $this->gettbl('ReffSpopTable')->getLookUpItemByKodeGroup(ReffSpopHelper::JENIS_LANGIT);
    }

    public function getComboJenisKolamRenang()
    {
        return $this->gettbl('ReffSpopTable')->getLookUpItemByKodeGroup(ReffSpopHelper::JENIS_KOLAM_RENANG);
    }
    
    public function getComboBahanPagar()
    {
        return $this->gettbl('ReffSpopTable')->getLookUpItemByKodeGroup(ReffSpopHelper::JENIS_BAHAN_PAGAR);
    }
    
    public function getComboJenisBangunan()
    {
        return $this->gettbl('ReffSpopTable')->getJenisBangunan();
    }
}
