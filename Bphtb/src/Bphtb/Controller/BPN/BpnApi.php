<?php

namespace Bphtb\Controller\BPN;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class BpnApi extends AbstractActionController
{

    protected $DataSertifikatTable;
    protected $PemdaTable;

    public function cekurl()
    {
        $basePath = $this->getRequest()->getBasePath();
        $uri = new \Zend\Uri\Uri($this->getRequest()->getUri());
        $uri->setPath($basePath);
        $uri->setQuery(array());
        $uri->setFragment('');

        return $uri->getScheme() . '://' . $uri->getHost() . '' . $uri->getPath(); //:'.$_SERVER['SERVER_PORT'].'

    }

    public function formceksertifikatAction()
    {
        $session = $this->getServiceLocator()->get('EtaxService')->getStorage()->read();
        $ar_pemda = $this->getPemdaTable()->getdata();
        $view = new ViewModel(array());
        $data = array(
            'menu_bpn_api' => 'active',
            'menu_bpn_api_formceksertifikat' => 'active',
            'role_id' => $session['s_akses'],
            'data_pemda' => $ar_pemda,
            'aturgambar' => 1,
            'username' => $session['s_username']
        );
        $this->layout()->setVariables($data);
        return $view;
    }

    public function ceksertifikatAction()
    {
        $response = $this->getResponse();
        $response->getHeaders()->addHeaderLine('Content-Type', 'application/json');
        $response->sendHeaders();
        $body = $this->getRequest()->getContent();
        $json = json_decode($body, true);
        $tgltransaksi = $json['t_tglsertifikat'];

        $bpn_users = $this->getDataSertifikatTable()->getUserBpn();

        $curl = curl_init();
        curl_setopt_array($curl, array(
            // CURLOPT_URL => "https://services.atrbpn.go.id/BpnApiService/api/bphtb/getDataBPN",
            CURLOPT_URL => "https://services.atrbpn.go.id/BpnApiService/api/bphtb/getDataATRBPN",
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "{
            \n\t\"USERNAME\":\"" . $bpn_users['s_username'] . "\",
            \n\t\"PASSWORD\":\"" . $bpn_users['s_password'] . "\",
            \n\t\"TANGGAL\":\"" . $tgltransaksi . "\"\n}",
            CURLOPT_HTTPHEADER => array(
                "Content-Type: application/json"
            ),
        ));

        $response = curl_exec($curl);
        curl_close($curl);

        $response = json_decode($response, true);
        $semuadata = $response['result'];

        $totaldata = count($semuadata);

        $hasil_data = array();

        if ($totaldata > 0) {
            $respon_code = '01';
            $respons_message = 'success';

            foreach ($semuadata as $data) {
                $hasil_data[] = $data;
            }
        } else {
            $respon_code = '03';
            $respons_message = 'Tidak Ada Data';
        }

        $data_render = array(
            "result" => $hasil_data,
            "total_data" => $totaldata,
            "responsecode" => $respon_code,
            "responsmessage" => $respons_message
        );
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($data_render));
    }

    public function simpandatasertifikatAction()
    {
        $session = $this->getServiceLocator()->get('EtaxService')->getStorage()->read();
        $response = $this->getResponse();
        $response->getHeaders()->addHeaderLine('Content-Type', 'application/json');
        $response->sendHeaders();
        $body = $this->getRequest()->getContent();
        $json = json_decode($body, true);
        $tgltransaksi = $json['t_tglsertifikat'];

        $bpn_users = $this->getDataSertifikatTable()->getUserBpn();


        $curl = curl_init();
        curl_setopt_array($curl, array(
            // CURLOPT_URL => "https://services.atrbpn.go.id/BpnApiService/api/bphtb/getDataBPN",
            CURLOPT_URL => "https://services.atrbpn.go.id/BpnApiService/api/bphtb/getDataATRBPN",
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "{
            \n\t\"USERNAME\":\"" . $bpn_users['s_username'] . "\",
            \n\t\"PASSWORD\":\"" . $bpn_users['s_password'] . "\",
            \n\t\"TANGGAL\":\"" . $tgltransaksi . "\"\n}",
            CURLOPT_HTTPHEADER => array(
                "Content-Type: application/json"
            ),
        ));

        $response = curl_exec($curl);
        curl_close($curl);

        $response = json_decode($response, true);
        $semuadata = $response['result'];
        $totaldata = count($semuadata);
        // var_dump($totaldata);exit();
        if ($totaldata > 0) {
            $respon_code = '01';
            $respons_message = 'success';

            foreach ($semuadata as $data) {
                // var_dump($data);exit();
                // if (!empty($data['AKTAID'])) {
                //     $simpan_data = $this->getDataSertifikatTable()->simpandatasertifikatbpn($tgltransaksi, $data, $session);
                // }
                $simpan_data = $this->getDataSertifikatTable()->simpandatasertifikatbpn($tgltransaksi, $data, $session);
            }
        } else {
            $respon_code = '03';
            $respons_message = 'Tidak Ada Data';
        }

        $data_render = array(
            "total_data" => $totaldata,
            "responsecode" => $respon_code,
            "responsmessage" => $respons_message
        );

        return $this->getResponse()->setContent(\Zend\Json\Json::encode($data_render));
    }

    public function datasertifikatbpnAction()
    {
        $ar_pemda = $this->getPemdaTable()->getdata();
        $session = $this->getServiceLocator()->get('EtaxService')->getStorage()->read();
        $view = new ViewModel(array());
        $data = array(
            'menu_bpn_api' => 'active',
            'menu_bpn_api_datasertifikatbpn' => 'active',
            'role_id' => $session['s_akses'],
            'data_pemda' => $ar_pemda,
            'aturgambar' => 2,
            'username' => $session['s_username']
        );
        $this->layout()->setVariables($data);
        return $view;
    }


    public function dataGriddatasertifikatAction()
    {
        $session = $this->getServiceLocator()->get('EtaxService')->getStorage()->read();
        $allParams = (array) $this->getEvent()->getRouteMatch()->getParams();
        $input = $this->getRequest();
        $aColumns = array('t_datasertifikat_bpn.t_idsertifikat', 't_datasertifikat_bpn.aktaid', 't_datasertifikat_bpn.tgl_akta', 't_datasertifikat_bpn.nop',  't_datasertifikat_bpn.nib', 't_datasertifikat_bpn.nik', 't_datasertifikat_bpn.npwp', 't_datasertifikat_bpn.nama_wp', 't_datasertifikat_bpn.alamat_op', 't_datasertifikat_bpn.kelurahan_op', 't_datasertifikat_bpn.kecamatan_op', 't_datasertifikat_bpn.kota_op', 't_datasertifikat_bpn.luastanah_op', 't_datasertifikat_bpn.luasbangunan_op', 't_datasertifikat_bpn.ppat', 't_datasertifikat_bpn.no_sertipikat', 't_datasertifikat_bpn.no_akta');

        $panggildata = $this->getDataSertifikatTable();

        $rResult = $panggildata->semuadatasertifikatbpn($input, $aColumns, $session, $this->cekurl(), $allParams);
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($rResult));
    }


    public function cetakdatasertifikatAction()
    {
        $session = $this->getServiceLocator()->get('EtaxService')->getStorage()->read();
        $ar_pemda = $this->getPemdaTable()->getdata();
        $req = $this->getRequest();
        if ($req->isGet()) {
            $data_get = $req->getQuery();
            $cek_data = $this->getDataSertifikatTable()->cekdatasertifikatbpn($data_get);
            $view = new ViewModel(array(
                'cek_data' => $cek_data,
                'data_get' => $data_get,
                'ar_pemda' => $ar_pemda,
            ));
            $data1 = array(
                'nilai' => '3',
                'session' => $session,
            );
            $this->layout()->setVariables($data1);
            return $view;
        }
    }

    public function getDataSertifikatTable()
    {
        if (!$this->DataSertifikatTable) {
            $sm = $this->getServiceLocator();
            $this->DataSertifikatTable = $sm->get('DataSertifikatTable');
        }
        return $this->DataSertifikatTable;
    }

    public function getPemdaTable()
    {
        if (!$this->PemdaTable) {
            $sm = $this->getServiceLocator();
            $this->PemdaTable = $sm->get('PemdaTable');
        }
        return $this->PemdaTable;
    }
}
