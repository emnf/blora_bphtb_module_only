<?php

namespace Bphtb\Controller\Setting;

use Bphtb\Form\Setting\FormatPesanFrm;
use Bphtb\Model\Setting\FormatPesanBase;
use Zend\Db\Sql\Sql;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class SettingFormatPesan extends AbstractActionController {

    public function indexAction() {
        $session = $this->getServiceLocator()->get('EtaxService')->getStorage()->read();
        $ar_pemda = $this->getServiceLocator()->get('PemdaTable')->getdata();
        $view = new ViewModel();
        $data = array(
            'side_setting_format_pesan' => 'active',
            'role_id' => $session['s_akses'],
            'data_pemda' => $ar_pemda,
            'aturgambar' => 1,
            'username' => $session['s_username']
        );
        $this->layout()->setVariables($data);
        return $view;
    }

    public function dataGridAction() {
        $post = $this->getRequest()->getQuery();
        $base = new FormatPesanBase();
        $base->exchangeArray($post);
        if ($base->direction == 2){
            $base->page = $base->page + 1;
        }
        if ($base->direction == 1){
            $base->page = $base->page - 1;
        }
        if ($base->page <= 0){
            $base->page = 1;
        }
        $data = $this->getServiceLocator()->get('FormatPesanTable')->getGridData($base, $post);
        $s = "";
        foreach ($data["res"] as $row) {
            $s .= "<tr>";
            $s .= "<td>" . $row['s_ket_jns_pesan'] . "</td>";
            $s .= "<td>" . $row['s_isian_pesan'] . "</td>";
            if($row["s_id_format_pesan"] != 3){
                $s .= "<td><center><a href='setting_hak_tanah/edit?id=$row[s_id_format_pesan]' class='btn btn-warning btn-sm btn-flat' style='width:100px'>Edit</a> <a href='#' onclick='hapus(" . $row['s_id_format_pesan'] . ");return false;' class='btn btn-danger btn-sm btn-flat' style='width:100px'>Hapus</a></center></td>";
            }else{
                $s .= "<td></td>";
            }
            $s .= "</tr>";
        }
        $data_render = array(
            "grid" => $s,
            "rows" => $base->rows,
            "count" => $data["count"],
            "page" => $data["page"],
            "start" => $data["start"],
            "total_halaman" => $data["total_pages"]
        );
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($data_render));
    }

    public function tambahAction() {
        $session = $this->getServiceLocator()->get('EtaxService')->getStorage()->read();
        $ar_pemda = $this->getServiceLocator()->get("PemdaTable")->getdata();
        $form = new FormatPesanFrm();
        $post = $this->getRequest()->getPost();
        if ($this->getRequest()->isPost()) {
            $base = new FormatPesanBase();
            $form->setInputFilter($base->getInputFilter());
            $form->setData($post);
            if ($form->isValid()) {
                $base->exchangeArray($form->getData());
                $this->getServiceLocator()->get('FormatPesanTable')->simpan($post);
                return $this->redirect()->toRoute('setting_format_pesan');
            }else{
                var_dump($form->getMessages());exit();
            }
        }
        $view = new \Zend\View\Model\ViewModel(array("frm" => $form));
        $data = array(
            'side_setting_format_pesan' => 'active',
            'role_id' => $session['s_akses'],
            'data_pemda' => $ar_pemda,
            'aturgambar' => 1,
            'username' => $session['s_username']
        );
        $this->layout()->setVariables($data);
        return $view;
    }

    public function hapusAction()
    {
        $allParams = (array) $this->getEvent()->getRouteMatch()->getParams();
        $this->getServiceLocator()->get('FormatPesanTable')->hapus($allParams["id"]);
        return $this->getResponse();
    }

}
