<?php

namespace Bphtb\Helper;

use Zend\Mvc\Controller\Plugin\AbstractPlugin;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use QRcode;

class CetakSSPD extends AbstractPlugin implements ServiceLocatorAwareInterface
{

    protected $service;

    public function getServiceLocator()
    {
        return $this->serviceLocator;
    }

    public function setServiceLocator(ServiceLocatorInterface $serviceLocator)
    {
        $this->serviceLocator = $serviceLocator;
    }
    public function getService($serviceName)
    {
        $this->service = $this->getServiceLocator()->getServiceLocator()->get($serviceName);
        return $this->service;
    }

    /**
     * $date:date string to be formatted
     * $type:format output
     * $type: 0 (dd-mm-yyyy) ex: 20-04-2016
     *        1 (dd MM yyyy) ex: 20 April 2016
     *        2 (MM)         ex: April
     */
    public function dateFormat($date, $type)
    {

        $bulan = [null, "Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"];
        $tgl = explode('-', $date);
        switch ($type) {
            case 0:
                $hasil = date('d-m-Y', strtotime($date));
                break;
            case 1:
                $hasil = $tgl[2] . " " . $bulan[(int) $tgl[1]] . " " . $tgl[0];
                break;
            case 2:
                $hasil = $bulan[(int) $tgl[1]];
                break;
            default:
                $hasil = $date;
                break;
        }

        return $hasil;
    }

    public function getSession()
    {
        return $this->getService("Sessi_Manager")->getStorage();
    }

    public function getBasePath()
    {
        $server = $this->getService('ViewHelperManager')->get('serverUrl');
        $base = $this->getService('ViewHelperManager')->get('basePath');
        return $server('') . $base('/');
    }

    function terbilang($x, $style = 4)
    {
        if ($x < 0) {
            $hasil = "MINUS " . trim($this->kekata($x));
        } else {
            $hasil = trim($this->kekata($x));
        }
        switch ($style) {
            case 1:
                $hasil = strtoupper($hasil);
                break;
            case 2:
                $hasil = strtolower($hasil);
                break;
            case 3:
                $hasil = ucwords($hasil);
                break;
            default:
                $hasil = ucfirst($hasil);
                break;
        }
        return $hasil;
    }

    public function kekata($x)
    {
        $x = abs($x);
        $angka = array(
            "", "Satu", "Dua", "Tiga", "Empat", "Lima",
            "Enam", "Tujuh", "Delapan", "Sembilan", "Sepuluh", "Sebelas"
        );
        $temp = "";
        if ($x < 12) {
            $temp = " " . $angka[$x];
        } else if ($x < 20) {
            $temp = $this->kekata($x - 10) . " Belas";
        } else if ($x < 100) {
            $temp = $this->kekata($x / 10) . " Puluh" . $this->kekata($x % 10);
        } else if ($x < 200) {
            $temp = " Seratus" . $this->kekata($x - 100);
        } else if ($x < 1000) {
            $temp = $this->kekata($x / 100) . " Ratus" . $this->kekata($x % 100);
        } else if ($x < 2000) {
            $temp = " Seribu" . $this->kekata($x - 1000);
        } else if ($x < 1000000) {
            $temp = $this->kekata($x / 1000) . " Ribu" . $this->kekata($x % 1000);
        } else if ($x < 1000000000) {
            $temp = $this->kekata($x / 1000000) . " Juta" . $this->kekata($x % 1000000);
        } else if ($x < 1000000000000) {
            $temp = $this->kekata($x / 1000000000) . " Milyar" . $this->kekata(fmod($x, 1000000000));
        } else if ($x < 1000000000000000) {
            $temp = $this->kekata($x / 1000000000000) . " Trilyun" . $this->kekata(fmod($x, 1000000000000));
        }
        return $temp;
    }

    public function cetakSSPD($cetakanesign = 1, $data = [])
    {
        // error_reporting(0);

        $cetakversi = $data["cetakversi"];
        $data_pemda = $data["data_pemda"];
        $data_sspd = $data["data_sspd"];
        $perhitungan = $data["perhitungan"];
        // $cekurl = $data["cekurl"];
        $cekurl = str_replace("\\", "/", realpath("."));
        // die($cekurl);
        $dataidsptsebelum = $data["dataidsptsebelum"];
        $data_mengetahui = $data["data_mengetahui"];
        $imageQr = $data["imageQr"];

        $html = "<html>";
        $html .= '
        <head>
        <style>  
            .footer {
                position: fixed;
                left: 0;
                bottom: 0;
                width: 100%;
            }
        </style>
        </head>';
        $html .= '<body>';
        foreach ($data_sspd as $row) {
            if ($cetakversi == 3) {
                // SSPD 4 HALAMAN
                for ($lmbr = 1; $lmbr <= 4; $lmbr++) {
                    $html .= '
                <div' . (($lmbr != 4) ? ' style="page-break-after: always;"' : '') . '>
                <table border="1" style="border-spacing: 0px; font-size: 11px; width: 100%; border-collapse: collapse;">
                        <tr>
                            <td align="center" class="font_delapan" rowspan="2">';
                    // $gambar = $_SERVER['DOCUMENT_ROOT'] . '/' . $this->basePath() . '/' . $data_pemda->s_logo;
                    // $gambar = $cekurl . '/' . $data_pemda->s_logo;
                    $gambar = $cekurl . '/public/upload/Logo_Blora2.png';
                    // Logo_Blora2.png
                    // die($data_pemda->s_logo);
                    // die($gambar);
                    $html .= '<img src="' . $gambar . '" style=" width:70px; height:70px;" /><br>
                                PEMERINTAH ' . strtoupper($data_pemda->s_namakabkot) . '<br>
                                ' . strtoupper($data_pemda->s_namainstansi) .
                        '
                            </td>
                            <td align="center" class="border_bawah font_kecil">
                                SURAT SETORAN PAJAK DAERAH
                                <br />
                                BEA PEROLEHAN HAK ATAS TANAH DAN BANGUNAN
                                <br />
                                <strong>( SSPD-BPHTB )</strong>
                            </td>
                            <td rowspan="2">
                                <center>Lembar ' . $lmbr . '
                                    <br>';
                    if ($lmbr == 1) {
                        $html .= 'Untuk Wajib Pajak';
                    } elseif ($lmbr == 2) {
                        $html .= 'Untuk PPAT/Notaris<br>sebagai arsip';
                    } elseif ($lmbr == 3) {
                        $html .= 'Untuk Kepala Kantor<br>Badan Pertanahan';
                    } elseif ($lmbr == 4) {
                        $html .= 'Untuk ' . $data_pemda->s_namasingkatinstansi . '<br> dalam proses penelitian';
                    }
                    $html .= '
                                </center>
                                <br>
                                <center>
                                    No. Daftar : ' . $row['t_kohirspt'] . '<br>
                                    Kode Bayar/NTPD : ' . $row['t_kodebayarbanksppt'] . '</center>
                            </td>
                        </tr>
                        <tr>
                            <td align="center" class="font_sembilan">
                                BERFUNGSI SEBAGAI SURAT PEMBERITAHUAN OBJEK PAJAK
                                <br />
                                PAJAK BUMI DAN BANGUNAN (SPOP PBB)
                            </td>
                        </tr>
                    </table>
                    <table border="0" style="border-spacing: 0px; font-size: 11px; width: 100%; border-collapse: collapse;">
                    <tr>
                        <td style="border-left: 1px solid black; border-right: 1px solid black;">&nbsp;</td>
                    </tr>
                </table>
                    <table border="0" style="border-spacing: 0px; font-size: 11px; width: 100%; border-collapse: collapse;">
                    <tr>
                        <td style="width: 10px; vertical-align: top; border-left: 1px solid black;" rowspan="5">
                            A.
                        </td>
                       <td style="width: 150px">
                            1. Nama Wajib Pajak
                        </td>
                        <td colspan="4" style="border-bottom: 1px solid black;">
                            : ' . strtoupper($row['t_namawppembeli']) . '
                        </td>
                        <td style="border-right: 1px solid black; width: 10px;"></td>
                    </tr>
                    <tr>
                        <td>
                            2. NPWP
                        </td>
                        <td colspan="4" style="border-bottom: 1px solid black;">
                            : ' . $row['t_npwpwppembeli'] . '
                        </td>
                        <td style="border-right: 1px solid black;"></td>
                    </tr>
                    <tr>
                        <td>
                            3. Alamat Wajib Pajak
                        </td>
                        <td colspan="3" style="border-bottom: 1px solid black;">
                            : ' . strtoupper($row["t_alamatwppembeli"]) . '
                        </td>
                        <td style="border-bottom: 1px solid black;">Blok/Kav/Nomor :</td>
                        <td style="border-right: 1px solid black;"></td>
                    </tr>
                    <tr>
                        <td>
                            4. Kelurahan/Desa
                        </td>
                        <td style="border-bottom: 1px solid black;">
                            : ' . strtoupper($row["t_kelurahanwppembeli"]) . '
                        </td>
                        <td style="border-bottom: 1px solid black;">
                            5. RT/RW
                        </td>
                        <td style="border-bottom: 1px solid black;">: ' . strtoupper($row["t_rtwppembeli"]) . ' / ' . strtoupper($row["t_rwwppembeli"]) . '</td>
                        <td style="border-bottom: 1px solid black;">
                            6. Kecamatan : ' . strtoupper($row["t_kecamatanwppembeli"]) . '
                        </td>
                        <td style="border-right: 1px solid black;"></td>
                    </tr>
                    <tr>
                        <td>
                            7. Kabupaten/Kota
                        </td>
                        <td style="border-bottom: 1px solid black;">
                            : ' . strtoupper($row["t_kabkotawppembeli"]) . '
                        </td>
                        <td style="border-bottom: 1px solid black;">
                            8. Kode Pos 
                        </td>
                        <td colspan="2" style="border-bottom: 1px solid black;">: ' . $row["t_kodeposwppembeli"] .
                        '</td>
                        <td style="border-right: 1px solid black;"></td>
                    </tr>
                </table>
                <table border="0" style="border-spacing: 0px; font-size: 11px; width: 100%; border-collapse: collapse;">
                    <tr>
                        <td style="border-bottom: 1px solid black; border-left: 1px solid black; border-right: 1px solid black;">&nbsp;</td>
                    </tr>
                </table>
                <table border="0" style="border-spacing: 0px; font-size: 11px; width: 100%; border-collapse: collapse;">
                    <tr>
                        <td style="border-left: 1px solid black; border-right: 1px solid black;">&nbsp;</td>
                    </tr>
                </table>
                <table border="0" style="border-spacing: 0px; font-size: 11px; width: 100%; border-collapse: collapse;">
                <tr>
                    <td style="width: 10px; vertical-align: top; border-left: 1px solid black;">
                        B.
                    </td>
                    <td style="width: 200px;">
                        1. Nomor Objek Pajak (NOP) PBB 
                    </td>
                    <td style="width: 10px;">:</td>';
                    for ($i = 0; $i <= 1; $i++) {
                        $html .= '<td style="width: 20px; text-align: center; border-left: 1px solid black; border-top: 1px solid black; border-bottom: 1px solid black;">
                            ' . $row['t_nopbphtbsppt'][$i] . '
                        </td>';
                    }
                    $html .= '<td style="width: 20px; border-left: 1px solid black;"></td>';
                    for ($i = 3; $i <= 4; $i++) {
                        $html .= '<td style="width: 20px; text-align: center; border-left: 1px solid black; border-top: 1px solid black; border-bottom: 1px solid black;">
                            ' . $row['t_nopbphtbsppt'][$i] . '
                        </td>';
                    }
                    $html .= '<td style="width: 20px; border-left: 1px solid black;"></td>';
                    for ($i = 6; $i <= 8; $i++) {
                        $html .= '<td style="width: 20px; text-align: center; border-left: 1px solid black; border-top: 1px solid black; border-bottom: 1px solid black;">
                            ' . $row['t_nopbphtbsppt'][$i] . '
                        </td>';
                    }
                    $html .= '<td style="width: 20px; border-left: 1px solid black;"></td>';
                    for ($i = 10; $i <= 12; $i++) {
                        $html .= '<td style="width: 20px; text-align: center; border-left: 1px solid black; border-top: 1px solid black; border-bottom: 1px solid black;">
                            ' . $row['t_nopbphtbsppt'][$i] . '
                        </td>';
                    }
                    $html .= '<td style="width: 20px; border-left: 1px solid black;"></td>';
                    for ($i = 14; $i <= 16; $i++) {
                        $html .= '<td style="width: 20px; text-align: center; border-left: 1px solid black; border-top: 1px solid black; border-bottom: 1px solid black;">
                            ' . $row['t_nopbphtbsppt'][$i] . '
                        </td>';
                    }
                    $html .= '<td style="width: 20px; border-left: 1px solid black;"></td>';
                    for ($i = 18; $i <= 21; $i++) {
                        $html .= '<td style="width: 20px; text-align: center; border-left: 1px solid black; border-top: 1px solid black; border-bottom: 1px solid black;">
                            ' . $row['t_nopbphtbsppt'][$i] . '
                        </td>';
                    }
                    $html .= '<td style="width: 20px; border-left: 1px solid black;"></td>';
                    $html .= '<td style="width: 20px; text-align: center; border-left: 1px solid black; border-top: 1px solid black; border-bottom: 1px solid black;">
                        ' . $row['t_nopbphtbsppt'][23] . '
                    </td>';
                    $html .= '<td style="border-left: 1px solid black; border-right: 1px solid black;"></td>';
                    $html .= '
                </tr>
                </table>
                <table border="0" style="border-spacing: 0px; font-size: 11px; width: 100%; border-collapse: collapse;">
                <tr>
                    <td style="width: 13px; vertical-align: top; border-left: 1px solid black;">
                    </td>
                    <td style="width: 200px;">
                    2. Letak tanah dan atau bangunan
                    </td>
                    <td style="width: 10px;">:</td>
                    <td colspan="3" style="border-bottom: 1px solid black;">
                        ' . strtoupper($row["t_alamatop"]) . '
                    </td>
                    <td style="width: 10px; border-right: 1px solid black;"></td>
                </tr>
                <tr>
                    <td style="border-left: 1px solid black;"></td>
                    <td>
                        3. Kelurahan/Desa
                    </td>
                    <td style="width: 10px;">:</td>
                    <td style="border-bottom: 1px solid;">' . $row["t_kelurahanop"] . '</td>
                    <td style="border-bottom: 1px solid;">
                        4. RT/RW
                    </td>
                    <td style="border-bottom: 1px solid;">
                        : ' . strtoupper($row["t_rtop"]) . ' / ' . strtoupper($row["t_rwop"]) . '
                    </td>
                    <td style="border-right: 1px solid black;"></td>
                </tr>
                <tr>
                    <td style="border-left: 1px solid black;"></td>
                    <td>
                        5. Kecamatan
                    </td>
                    <td style="width: 10px;">:</td>
                    <td style="border-bottom: 1px solid;">' . $row["t_kecamatanop"] . '</td>
                    <td style="border-bottom: 1px solid;">
                        6. Kabupaten/Kota 
                    </td>
                    <td style="border-bottom: 1px solid;">
                        : ' . str_ireplace("KABUPATEN ", "", strtoupper($data_pemda->s_namakabkot)) . '
                    </td>
                    <td style="border-right: 1px solid black;"></td>
                </tr>
                </table>
                <table border="0" style="border-spacing: 0px; font-size: 11px; width: 100%; border-collapse: collapse;">
                <tr>
                    <td style="width: 13px; vertical-align: top; border-left: 1px solid black;"></td>
                    <td style="border-right: 1px solid black;">
                    Penghitungan NJOP PBB
                    </td>
                </tr>
                </table>
                <table border="0" style="border-spacing: 0px; font-size: 11px; width: 100%; border-collapse: collapse;">
                <tr>
                    <td style="width: 13px; vertical-align: top; border-left: 1px solid black;"></td>
                    <td style="width: 150px; text-align: center; border-left: 1px solid black; border-top: 1px solid black; border-right: 1px solid black; border-bottom: 1px solid black;">
                        Uraian
                    </td>
                    <td colspan="2" style="text-align: center; border-top: 1px solid black; border-right: 1px solid black; border-bottom: 1px solid black;">
                        <b>Luas</b><br><span style="font-size:8px;">(Diisi luas tanah dan atau bangunan yang haknya diperoleh)</span>
                    </td>
                    <td colspan="2" style="text-align: center; border-top: 1px solid black; border-right: 1px solid black; border-bottom: 1px solid black;">
                        <b>NJOP PBB / m <sup>2</sup></b><br><span style="font-size:8px;">(Diisi berdasarkan SPPT PBB tahun terjadinya perolehan hak/Tahun......)</span>
                    </td>
                    <td colspan="2" style="width: 200px; text-align: center; border-top: 1px solid black; border-right: 1px solid black; border-bottom: 1px solid black;">
                        Luas x NJOP PBB / m<sup>2</sup>
                    </td>
                </tr>
                <tr>
                    <td style="border-left: 1px solid;"></td>
                    <td style="border-left: 1px solid black; border-right: 1px solid black; border-bottom: 1px solid black;">
                        Tanah (Bumi)
                    </td>
                    <td style="width: 30px; text-align: center; border-right: 1px solid; border-bottom: 1px solid;">
                        7
                    </td>
                    <td style="text-align: right; border-right: 1px solid; border-bottom: 1px solid;">
                        ' . number_format($perhitungan["luastanah"], 0, ',', '.') . ' m<sup>2</sup>
                    </td>
                    <td style="width: 30px; text-align: center; border-right: 1px solid; border-bottom: 1px solid;">
                        9
                    </td>
                    <td style="text-align: right; border-right: 1px solid; border-bottom: 1px solid;">
                        ' . number_format($perhitungan["njoptanah"], 0, ',', '.') . '
                    </td>
                    <td style="text-align: center; width: 10px; border-bottom: 1px solid; border-right: 1px solid;">
                        11
                    </td>
                    <td style="text-align: right; border-right: 1px solid; border-bottom: 1px solid;">
                        ' . number_format($perhitungan["totalnjoptanah"], 0, ',', '.') . '
                    </td>
                </tr>
                <tr>
                    <td style="border-left: 1px solid;"></td>
                    <td style="border-left: 1px solid; border-right: 1px solid; border-bottom: 1px solid;">
                        Bangunan
                    </td>
                    <td style="text-align: center; border-right: 1px solid; border-bottom: 1px solid;">
                        8
                    </td>
                    <td style="text-align: right; border-right: 1px solid; border-bottom: 1px solid;">
                        ' . number_format($perhitungan["luasbangunan"], 0, ',', '.') . ' m<sup>2</sup>
                    </td>
                    <td style="text-align: center; border-right: 1px solid; border-bottom: 1px solid;">
                        10
                    </td>
                    <td style="text-align: right; border-right: 1px solid; border-bottom: 1px solid;">
                        ' . number_format($perhitungan["njopbangunan"], 0, ',', '.') . '
                    </td>
                    <td style="text-align: center; border-right: 1px solid; border-bottom: 1px solid;">
                        12
                    </td>
                    <td style="text-align: right; border-right: 1px solid; border-bottom: 1px solid;">
                        ' . number_format($perhitungan["totalnjopbangunan"], 0, ',', '.') . '
                    </td>
                </tr>
                <tr>
                    <td style="border-left: 1px solid;"></td>
                    <td colspan="5" style="border-left: 1px solid; border-right: 1px solid; border-bottom: 1px solid;">
                        NJOP PBB : &nbsp; &nbsp; &nbsp;
                    </td>
                    <td style="text-align: center; border-left: 1px solid; border-right: 1px solid; border-bottom: 1px solid;">
                        13
                    </td>
                    <td style="text-align: right; border-left: 1px solid; border-right: 1px solid; border-bottom: 1px solid;">
                        ' . number_format($perhitungan["grandnjop"], 0, ',', '.') . '
                    </td>
                </tr>';
                    if ($perhitungan["ket_aphb"] == 1) {
                        $html .= '
                <tr>
                    <td style="border-left: 1px solid;"></td>
                    <td colspan="5" style="border-left: 1px solid; border-right: 1px solid; border-bottom: 1px solid;">
                    &nbsp; &nbsp; &nbsp;
                    </td>
                    <td colspan="2" style="text-align: right; border-left: 1px solid; border-right: 1px solid; border-bottom: 1px solid;">
                    ' . $perhitungan["aphb_kali"] . '/' . $perhitungan["aphb_bagi"] . '
                    </td>
                    </tr>
                    <tr>
                    <td style="border-left: 1px solid;"></td>
                    <td colspan="5" style="border-left: 1px solid; border-right: 1px solid; border-bottom: 1px solid;">
                    NJOP PBB APHB: &nbsp; &nbsp; &nbsp;
                    </td>
                    <td colspan="2" style="text-align: right; border-left: 1px solid; border-right: 1px solid; border-bottom: 1px solid;">
                    ' . number_format($perhitungan["grandnjop_aphb"], 0, ',', '.') . '
                    </td>
                </tr>';
                    }
                    $html .= '
                </table>
                <table border="0" style="border-spacing: 0px; font-size: 11px; width: 100%; border-collapse: collapse;">
                    <tr>
                        <td style="border-left: 1px solid black; border-right: 1px solid black;">&nbsp;</td>
                    </tr>
                </table>
                <table border="0" style="border-spacing: 0px; font-size: 11px; width: 100%; border-collapse: collapse;">
                <tr>
                    <td style="width: 13px; vertical-align: top; border-left: 1px solid black;">
                    </td>
                    <td style="width: 300px;">
                    15. Jenis perolehan hak atas tanah dan/atau bangunan :
                    </td>
                    <td style="width: 20px; text-align: center; border-top: 1px solid; border-left: 1px solid; border-bottom: 1px solid; border-right: 1px solid;">
                        ' . substr(str_pad($row["t_idjenistransaksi"], 2, '0', STR_PAD_LEFT), 0, 1) . '
                    </td>

                    <td style="width: 20px; text-align: center; border-top: 1px solid; border-bottom: 1px solid; border-right: 1px solid;">
                        ' . substr(str_pad($row["t_idjenistransaksi"], 2, '0', STR_PAD_LEFT), 1, 1) . '
                    </td>
                    <td style="width: 187px; padding-left: 10px;">
                        14. Harga transaksi / Nilai pasar :
                    </td>
                    <td style="width: 38px;">Rp.</td>
                    <td style="text-align: right; border-top: 1px solid; border-left: 1px solid; border-bottom: 1px solid; border-right: 1px solid;">
                        ' . number_format($perhitungan["nilaitransaksispt"], 0, ',', '.') . '
                    </td>
                </tr>
                <tr>
                    <td style="border-left: 1px solid;"></td>
                    <td colspan="6" style="border-right: 1px solid;">
                        16. Nomor Sertifikat Tanah : ' . $row["t_nosertifikathaktanah"] . '
                    </td>
                </tr>
                </table>
                <table border="0" style="border-spacing: 0px; font-size: 11px; width: 100%; border-collapse: collapse;">
                    <tr>
                        <td style="border-left: 1px solid black; border-right: 1px solid black; border-bottom: 1px solid;">&nbsp;</td>
                    </tr>
                </table>
                <table border="0" style="border-spacing: 0px; font-size: 11px; width: 100%; border-collapse: collapse;">
                <tr>
                    <td style="width: 10px; vertical-align: top; border-left: 1px solid black; border-bottom: 1px solid;">
                        C.
                    </td>
                    <td colspan="5" style="border-bottom: 1px solid; border-right: 1px solid;">PENGHITUNGAN BPHTB (Hanya diisi berdasarkan penghitungan Wajib Pajak)</td>
                </tr>
                <tr>
                    <td style="border-bottom: 1px solid; border-left: 1px solid;"></td>
                    <td style="border-bottom: 1px solid;">
                        1. Nilai Perolehan Objek Pajak (NPOP) <span style="font-size:10px"><i>memperhatikan nilai pada B.13 dan B.14 </i></span>
                    </td>
                    <td style="border-bottom: 1px solid;"></td>
                    <td style="text-align: center; width: 15px; border-bottom: 1px solid; border-left: 1px solid;">
                        1
                    </td>
                    <td style="width: 10px; border-bottom: 1px solid; border-left: 1px solid;">
                        Rp.
                    </td>
                    <td style="text-align: right; border-left: 1px solid; border-bottom: 1px solid; border-right: 1px solid;">
                        ' . number_format($perhitungan["npop"], 0, ',', '.') . '
                    </td>
                </tr>
                <tr>
                    <td style="border-bottom: 1px solid; border-left: 1px solid;"></td>
                    <td style="border-bottom: 1px solid;">
                        2. Nilai Perolehan Objek Pajak Tidak Kena Pajak (NPOPTKP)
                    </td>
                    <td style="border-bottom: 1px solid;"></td>
                    <td style="text-align: center; border-bottom: 1px solid; border-left: 1px solid;">
                        2
                    </td>
                    <td style="border-bottom: 1px solid; border-left: 1px solid;">
                        Rp.
                    </td>
                    <td style="text-align: right; border-left: 1px solid; border-bottom: 1px solid; border-right: 1px solid;">
                        ' . number_format($perhitungan["potonganspt"], 0, ',', '.') .
                        '
                    </td>
                </tr>
                <tr>
                    <td style="border-bottom: 1px solid; border-left: 1px solid;"></td>
                    <td style="border-bottom: 1px solid;">
                        3. Nilai Perolehan Objek Pajak Kena Pajak (NPOPKP)
                    </td>
                    <td style="width: 90px; text-align: center; border-bottom: 1px solid; border-left: 1px solid;">
                        angka 1-angka 2
                    </td>
                    <td style="text-align: center; border-bottom: 1px solid; border-left: 1px solid;">
                        3
                    </td>
                    <td style="border-bottom: 1px solid; border-left: 1px solid;">
                        Rp.
                    </td>
                    <td style="text-align: right; border-left: 1px solid; border-bottom: 1px solid; border-right: 1px solid;">
                        ' . number_format($perhitungan["npopkp"], 0, ',', '.') . '
                    </td>
                </tr>
                <tr>
                    <td style="border-bottom: 1px solid; border-left: 1px solid;"></td>
                    <td style="border-bottom: 1px solid;">
                        4. Bea Perolehan Hak atas Tanah dan Bangunan yang terutang
                    </td>
                    <td style="text-align: center; border-bottom: 1px solid; border-left: 1px solid;">
                        ' . $perhitungan["persenbphtb"] . '% x angka 3
                    </td>
                    <td style="text-align: center; border-bottom: 1px solid; border-left: 1px solid;">
                        4
                    </td>
                    <td style="border-bottom: 1px solid; border-left: 1px solid;">
                        Rp.
                    </td>
                    <td style="text-align: right; border-left: 1px solid; border-bottom: 1px solid; border-right: 1px solid;">
                        ' . number_format($perhitungan["totalpajak_sebelum_potongan_waris"], 0, ',', '.') . '
                    </td>
                </tr>';
                    $html .= '
                    <tr>
                        <td style="border-bottom: 1px solid; border-left: 1px solid;"></td>
                        <td style="border-bottom: 1px solid;">
                            5. Pengurangan karena waris/hibah wasiat/pemberian hak pengelolaan/program pemerintah
                        </td>
                        <td style="text-align: center; border-bottom: 1px solid; border-left: 1px solid;">
                            ' . $perhitungan["potongan_waris_hibahwasiat"] . '% x angka 4
                        </td>
                        <td style="text-align: center; border-bottom: 1px solid; border-left: 1px solid;">
                            5
                        </td>
                        <td style="border-bottom: 1px solid; border-left: 1px solid;">
                            Rp.
                        </td>
                        <td style="text-align: right; border-left: 1px solid; border-bottom: 1px solid; border-right: 1px solid;">';

                    if (($perhitungan["potongan_waris_hibahwasiat"] == null) || ($perhitungan["potongan_waris_hibahwasiat"] == 0)) {
                        $html .= '';
                    } else {
                        $html .= number_format($perhitungan["totalpajak"], 0, ',', '.');
                    }
                    $html .= '
                        </td>
                    </tr>';
                    $html .= '
                    <tr>
                        <td style="border-bottom: 1px solid; border-left: 1px solid;"></td>
                        <td style="border-bottom: 1px solid;">
                            6. Bea Perolehan Hak atas Tanah dan Bangunan yang harus dibayar
                        </td>
                        <td style="border-bottom: 1px solid;"></td>
                        <td style="text-align: center; border-bottom: 1px solid; border-left: 1px solid;">
                            6
                        </td>
                        <td style="border-bottom: 1px solid; border-left: 1px solid;">
                            Rp.
                        </td>
                        <td style="text-align: right; border-left: 1px solid; border-bottom: 1px solid; border-right: 1px solid;">
                            ' . number_format($perhitungan["total_pembayaran_sspd"], 0, ',', '.') . '
                        </td>
                    </tr>
                </table>
                <table border="0" style="border-spacing: 0px; font-size: 11px; width: 100%; border-collapse: collapse;">
                    <tr>
                        <td style="border-left: 1px solid black; border-right: 1px solid black;">&nbsp;</td>
                    </tr>
                </table>
                <table border="0" style="border-spacing: 0px; font-size: 11px; width: 100%; border-collapse: collapse;">
                <tr>
                    <td style="width: 10px; vertical-align: top; border-left: 1px solid black;">
                        D.
                    </td>
                    <td colspan="8" style="border-right: 1px solid;">
                        Jumlah Setoran berdasarkan : (Beri tanda silang "X" pda kotak yang sesuai)
                    </td>
                </tr>
                <tr>
                    <td colspan="9" style="border-left: 1px solid; border-right: 1px solid;"></td>
                </tr>
                <tr>
                    <td style="width: 10px; vertical-align: top; border-left: 1px solid black;">
                    </td>
                    <td style="width: 20px; border-left: 1px solid; border-bottom: 1px solid; border-top: 1px solid; border-right: 1px solid;"></td>
                    <td style="width: 5px;"></td>
                    <td colspan="6" style="border-right: 1px solid;">a. Penghitungan Wajib Pajak</td>
                </tr> 
                <tr>
                    <td colspan="9" style="border-left: 1px solid; border-right: 1px solid;"></td>
                </tr>
                <tr>
                    <td style="border-left: 1px solid;">
                    </td>
                    <td style="border-bottom: 1px solid; border-left: 1px solid; border-top: 1px solid; border-right: 1px solid;">';
                    if (!empty($dataidsptsebelum['t_idsptsebelumnya'])) {
                        $html .= 'X';
                    } else {
                        $html .= '';
                    }
                    $html .= '
                    </td>
                    <td></td>
                    <td colspan="6" style="border-right: 1px solid;">
                        b. STPD/ SKPDKB/ SKPDKBT *) &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Nomor: ............................... &nbsp;&nbsp;&nbsp;&nbsp; Tanggal: .......................
                    </td>
                </tr>
                <tr>
                    <td colspan="9" style="border-left: 1px solid; border-right: 1px solid;"></td>
                </tr>
                <tr>
                    <td style="border-left: 1px solid;"></td>
                    <td style="border-bottom: 1px solid; border-left: 1px solid; border-top: 1px solid; border-right: 1px solid;"></td>
                    <td></td>
                    <td>
                    c. Pengurangan dihitung sendiri menjadi :
                    </td>
                    <td style="width: 20px;"></td>
                    <td style="width: 5px;"></td>
                    <td style="width: 20px;"></td>
                    <td style="width: 5px;"></td>
                    <td style="border-right: 1px solid;">% berdasarkan Peraturan Bupati Nomor ..................................</td>
                </tr>
                <tr>
                    <td colspan="9" style="border-left: 1px solid; border-right: 1px solid;"></td>
                </tr>
                <tr>
                    <td style="border-left: 1px solid;"></td>
                    <td style="border-bottom: 1px solid; border-left: 1px solid; border-top: 1px solid; border-right: 1px solid;"></td>
                    <td></td>
                    <td colspan="6" style="border-right: 1px solid;">
                    d. ..........................
                    </td>
                </tr>
                <tr>
                    <td colspan="9" style="border-left: 1px solid; border-right: 1px solid;"></td>
                </tr>
                <tr>
                    <td style="border-left: 1px solid; border-bottom: 1px solid;"></td>
                    <td colspan="8" style="border-right: 1px solid; border-bottom: 1px solid;"><i>*) Coret yang tidak perlu</i></td>
                </tr>
                </table>
                <table border="0" style="border-spacing: 0px; font-size: 11px; width: 100%; border-collapse: collapse;">
                <tr>
                    <td style="width: 13px; vertical-align: top; border-left: 1px solid black;">

                    </td>
                    <td style="border-right: 1px solid;">
                        JUMLAH YANG DISETOR (dengan angka):
                    </td>
                </tr>
                <tr>
                    <td colspan="2" style="border-left: 1px solid black; border-right: 1px solid black;">
                    </td>
                </tr>
                </table>
                <table border="0" style="border-spacing: 0px; font-size: 11px; width: 100%; border-collapse: collapse;">
                <tr>
                    <td rowspan="2" style="width: 13px; vertical-align: top; border-left: 1px solid black;">

                    </td>
                    <td rowspan="2" style="padding: 10px; text-align: center; border-right: 1px solid; border-bottom: 1px solid; border-left: 1px solid; border-top: 1px solid;">
                        Rp. ' . number_format($perhitungan["total_pembayaran_sspd"], 0, ',', '.') . '
                    </td>
                    <td style="width: 10px;"></td>
                    <td style="vertical-align: top; border-right: 1px solid black;">
                    (dengan huruf) : ';
                    if ($perhitungan["total_pembayaran_sspd"] != 0) {
                        $html .= ucwords(strtolower($this->terbilang($perhitungan["total_pembayaran_sspd"]) . " Rupiah"));
                    } else {
                        $html .= 'Nihil';
                    }
                    $html .= '
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td style="border-right: 1px solid black;"></td>
                </tr>
                </table>
                <table border="0" style="border-spacing: 0px; font-size: 11px; width: 100%; border-collapse: collapse;">
                <tr>
                    <td style="border-left: 1px solid black; border-right: 1px solid black; border-bottom: 1px solid black;">
                        &nbsp;
                    </td>
                </tr>
                </table>
                <table border="0" style="border-spacing: 0px; font-size: 11px; width: 100%; border-collapse: collapse;">
                <tr>
                    <td width="25%" style="font-size: 10px; text-align: center; border-left: 1px solid black; vertical-align: top;">
                        ' . $data_pemda->s_namaibukotakabkot . ', ';
                    if ($row['t_tglprosesspt'] != null) {
                        $html .= date('d-m-Y', strtotime($row['t_tglprosesspt']));
                    } else {
                        $html .= "________________";
                    }
                    $html .= '
                        <br>
                        WAJIB PAJAK / PENYETOR
                    </td>
                    <td width="25%" style="font-size: 10px; text-align: center; border-left: 1px solid black; vertical-align: top;">
                        MENGETAHUI :
                        <br>
                        PPAT / NOTARIS<br>
                        KABUPATEN BLORA
                    </td>
                    <td width="25%" style="font-size: 10px; text-align: center; border-left: 1px solid black; vertical-align: top;">
                        DITERIMA OLEH :
                        <br>
                        TEMPAT PEMBAYARAN BPHTB
                        <br>
                        Tanggal :
                        <b>';

                    if ($row['t_tanggalpembayaran'] != null) {
                        $html .= date('d-m-Y', strtotime($row['t_tanggalpembayaran']));
                    } else {
                        $html .= "";
                    }
                    $html .= '
                    </td>
                    <td width="25%" style="font-size: 10px; text-align: center; border-left: 1px solid black; border-right: 1px solid black; vertical-align: top;">
                        Telah di Verifikasi :
                        <b>';
                    if ($row['t_tglverifikasispt'] != null) {
                        $html .= date('d-m-Y', strtotime($row['t_tglverifikasispt']));
                    } else {
                        $html .= "";
                    }
                    $html .= '
                        </b>
                        <br>
                        ' . strtoupper($data_pemda->s_namainstansi) . '
                    </td>
                </tr>
                </table>
                <table border="0" style="border-spacing: 0px; font-size: 11px; width: 100%; border-collapse: collapse;">
                <tr>
                    <td width="25%" style="text-align: center; border-left: 1px solid;"></td>
                    <td width="25%" style="text-align: center; border-left: 1px solid;"></td>
                    <td width="25%" style="text-align: center; border-left: 1px solid;"></td>
                    <td width="25%" style="vertical-align: middle; text-align: center; border-left: 1px solid; border-right: 1px solid;">
                    ';
                    if ($cetakanesign == 2) {
                        if ($lmbr == 1) {
                            $html .= '    
                        <br>
                        <br>
                        <br>
                        <br>
                        <span style="color: white;">#</span>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>';
                        } else {
                            $html .= '    
                        <br>
                        <img src="' . $imageQr . '" style="width: 100px;">
                        <br>
                        <br>';
                        }

                        $html .= '<img src="public/upload/visual-ttd.png" style="width: 100px;">
                        <br>
                        <br>
                        <br>';
                    } else {
                        $html .= '<br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>';
                    }
                    $html .= '
                    </td>
                </tr>
                </table>
                <table border="0" style="border-spacing: 0px; font-size: 11px; width: 100%; border-collapse: collapse;">
                <tr>
                    <td width="25%" style="font-size: 10px; text-align: center; border-left: 1px solid; border-bottom: 1px solid;">
                        ' . strtoupper($row['t_namawppembeli']) . '
                    </td>
                    <td width="25%" style="font-size: 10px; text-align: center; border-left: 1px solid; border-bottom: 1px solid;">
                    ' . (($row['t_idnotarisspt'] != 110) ? strtoupper($row['s_namanotaris']) : '')  . '
                    </td>
                    <td width="25%" style="font-size: 10px; text-align: center; border-left: 1px solid; border-bottom: 1px solid;">
                    </td>
                    <td width="25%" style="font-size: 10px; text-align: center; border-left: 1px solid; border-bottom: 1px solid; border-right: 1px solid;">
                        ' . strtoupper($data_mengetahui["s_namapejabat"]) . '
                    </td>
                </tr>
                <tr>
                    <td style="font-size: 9px; text-align: center; vertical-align: top; border-left: 1px solid; border-bottom: 1px solid;">
                        Nama Lengkap dan tanda tangan
                    </td>
                    <td style="font-size: 9px; text-align: center; vertical-align: top; border-left: 1px solid; border-bottom: 1px solid;">
                        Nama Lengkap, Stempel dan tanda tangan
                    </td>
                    <td style="font-size: 9px; text-align: center; vertical-align: top; border-left: 1px solid; border-bottom: 1px solid;">
                        Nama Lengkap, Stempel dan tanda tangan
                    </td>
                    <td style="font-size: 9px; text-align: center; vertical-align: top; border-left: 1px solid; border-bottom: 1px solid; border-right: 1px solid;">
                        Nama Lengkap, Stempel dan tanda tangan
                    </td>
                </tr>
                </table>
                <table border="0" style="border-spacing: 0px; font-size: 11px; width: 100%; border-collapse: collapse;">
                <tr>
                    <td style="border-left: 1px solid; width: 150px;">
                    &nbsp;
                    </td>
                    <td style="border-left: 1px solid; border-right: 1px solid;" colspan="28"></td>
                </tr>
                <tr>
                    <td rowspan="3" style="border-left: 1px solid; font-size: 9px; vertical-align: middle; text-align: center; width: 150px; padding: 10px;">
                        Hanya diisi oleh <br>petugas ' . $data_pemda->s_namasingkatinstansi . '
                    </td>
                    <td style="width: 100px; border-left: 1px solid; padding-left: 10px;">
                        Nomor Dokumen
                    </td>
                    <td style="width: 10px;">:</td>
                    <td style="width: 20px;"></td>
                    <td style="width: 20px; border: 1px solid; text-align: center;">';
                    $tgl = explode('-', $row['t_tglprosesspt']);
                    $tahun = $tgl[0];
                    $bulan = $tgl[1];
                    $hari = $tgl[2];
                    $html .= $hari[0] . '
                    </td>
                    <td style="width: 20px; border: 1px solid; text-align: center;">
                    ' . $hari[1] . '
                    </td>
                    <td style="width: 20px;"></td>
                    <td style="width: 20px; border: 1px solid; text-align: center;">
                        ' . $bulan[0] . '
                    </td>
                    <td style="width: 20px; border: 1px solid; text-align: center;">
                        ' . $bulan[1] . '
                    </td>
                    <td style="width: 20px;"></td>
                    <td style="width: 20px; border: 1px solid; text-align: center;">
                        ' . $tahun[0] . '
                    </td>
                    <td style="width: 20px; border: 1px solid; text-align: center;">
                        ' . $tahun[1] . '
                    </td>
                    <td style="width: 20px; border: 1px solid; text-align: center;">
                        ' . $tahun[2] . '
                    </td>
                    <td style="width: 20px; border: 1px solid; text-align: center;">
                        ' . $tahun[3] . '
                    </td>
                    <td style="width: 20px;"></td>';
                    $nosspd = str_pad($row['t_kohirketetapanspt'], 7, '0', STR_PAD_LEFT);
                    $html .= '
                    <td style="width: 20px; border: 1px solid; text-align: center;">
                        ' . $nosspd[0] . '
                    </td>
                    <td style="width: 20px; border: 1px solid; text-align: center;">
                        ' . $nosspd[1] . '
                    </td>
                    <td style="width: 20px; border: 1px solid; text-align: center;">
                        ' . $nosspd[2] . '
                    </td>
                    <td style="width: 20px; border: 1px solid; text-align: center;">
                        ' . $nosspd[3] . '
                    </td>
                    <td style="width: 20px;"></td>
                    <td style="width: 20px; border: 1px solid; text-align: center;">
                        ' . $nosspd[4] . '
                    </td>
                    <td style="width: 20px; border: 1px solid; text-align: center;">
                        ' . $nosspd[5] . '
                    </td>
                    <td style="width: 20px; border: 1px solid; text-align: center;">
                        ' . $nosspd[6] . '
                    </td>
                    <td style="width: 20px;"></td>
                    <td colspan="5" style="border-right: 1px solid;"></td>
                </tr>
                <tr> 
                    <td colspan="28" style="border-left: 1px solid; border-right: 1px solid;">
                    &nbsp;
                    </td>
                </tr>
                <tr>
                    <td style="border-left: 1px solid; padding-left: 10px;">
                        NOP PBB Baru
                    </td>
                    <td>:</td>
                    <td style="width: 20px;"></td>
                    <td style="width: 20px; border: 1px solid;"></td>
                    <td style="width: 20px; border: 1px solid;"></td>
                    <td style="width: 20px;"></td>
                    <td style="width: 20px; border: 1px solid;"></td>
                    <td style="width: 20px; border: 1px solid;"></td>
                    <td style="width: 20px;"></td>
                    <td style="width: 20px; border: 1px solid;"></td>
                    <td style="width: 20px; border: 1px solid;"></td>
                    <td style="width: 20px; border: 1px solid;"></td>
                    <td style="width: 20px;"></td>
                    <td style="width: 20px; border: 1px solid;"></td>
                    <td style="width: 20px; border: 1px solid;"></td>
                    <td style="width: 20px; border: 1px solid;"></td>
                    <td style="width: 20px;"></td>
                    <td style="width: 20px; border: 1px solid;"></td>
                    <td style="width: 20px; border: 1px solid;"></td>
                    <td style="width: 20px; border: 1px solid;"></td>
                    <td style="width: 20px;"></td>
                    <td style="width: 20px; border: 1px solid;"></td>
                    <td style="width: 20px; border: 1px solid;"></td>
                    <td style="width: 20px; border: 1px solid;"></td>
                    <td style="width: 20px; border: 1px solid;"></td>
                    <td style="width: 20px;"></td>
                    <td style="width: 20px; border: 1px solid;"></td>
                    <td style="border-right: 1px solid;"></td>
                </tr>
                <tr>
                    <td style="border-left: 1px solid; border-bottom: 1px solid;">
                    &nbsp;
                    </td>
                    <td style="border-left: 1px solid; border-right: 1px solid; border-bottom: 1px solid;" colspan="28"></td>
                </tr>
                </table>
                <br>
                <span style="font-size: 9pt;">Tempat pembayaran BPHTB :<br>
                    <br>
                    <br>
                </span>';
                    if ($cetakanesign == 2) {
                        $html .= '
                    <div class="footer">
                        <table style="width: 100%; border-collapse: collapse; font-size: 11px;" border="0">
                            <tr>
                                <td colspan="3" style="border-top: 1px solid black;"></td>
                            </tr>
                            <tr>
                                <td style="width: 5px; vertical-align: top;">-</td>
                                <td style="vertical-align: top;">Dokumen ini ditandatangani secara elektronik menggunakan 
                                    Sertifikat Elektronik yang diterbitkan oleh Balai Sertifikasi Elektronik (BSrE), Badan Siber dan Sandi Negara (BSSN).</td>
                                <td style="vertical-align: top;">
                                    <img src="public/upload/bsre.png" style="width: 70px;">
                                </td>
                            </tr>
                        </table>
                    </div>';
                    }
                    $html .= '</div>';
                }
            }
            if ($cetakversi == 1) {
                // SSPD 1 HALAMAN
                $html .= '
                <div style="page-break-after: always;width: 100%;">
            <table width="100%" class="border_atas border_bawah border_kanan border_kiri font_kecil" style="padding:0;border-spacing:0px;">
                <tr>
                    <td class="border_kanan border_bawah">
                        <table align="center">
                            <tr>
                                <td align="center" class="font_delapan">';
                $gambar = $_SERVER['DOCUMENT_ROOT'] . '/' . $this->basePath() . '/' . $data_pemda->s_logo;

                $html .= '<img class="profile-img" src="' . $gambar . '" style="width:90px;height:90px;" /><br>
                                    PEMERINTAH ' . strtoupper($data_pemda->s_namakabkot) . '<br>
                                    ' . strtoupper($data_pemda->s_namainstansi) . '
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td colspan="2" class="border_kanan border_bawah">
                        <table width="100%" style="padding:0;border-spacing:0px;">
                            <tr>
                                <td align="center" class="border_bawah font_sembilan">
                                    SURAT SETORAN PAJAK DAERAH
                                    <br />
                                    BEA PEROLEHAN HAK ATAS TANAH DAN BANGUNAN
                                    <br />
                                    <strong>( SSPD-BPHTB )</strong>
                                </td>
                            </tr>
                            <tr>
                                <td align="center" class="font_sembilan">
                                    BERFUNGSI SEBAGAI SURAT PEMBERITAHUAN OBJEK PAJAK
                                    <br />
                                    PAJAK BUMI DAN BANGUNAN (SPOP PBB)
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td class="border_bawah">
                        <table width="100%">
                            <tr>
                                <td class="font_sembilan">
                                    <center>
                                        <br>';
                $html .= '
                                    </center>
                                    <br>
                                    <center>
                                        No. Daftar : ' . $row['t_kohirspt'] . '<br>
                                        Kode Bayar : ' . $row['t_kodebayarbanksppt'] . '</center>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="4" class="border_bawah font_delapan" style="padding: 10px 0px 10px 0px;">
                        <table width="100%" style="border-spacing: 0px;">
                            <tr>
                                <td align="center" style="width: 5%; padding:0;">
                                    A.
                                </td>
                                <td>
                                    <table width="95%" style="border-spacing: 0px;">
                                        <tr>
                                            <td style="width: 20%">
                                                1. Nama Wajib Pajak :
                                            </td>
                                            <td>
                                                ' . strtoupper($row['t_namawppembeli']) . '
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                                <td>
                                    <table width="95%" style="border-spacing: 0px;">
                                        <tr>
                                            <td style="width: 20%">
                                                2. NPWP :
                                            </td>
                                            <td colspan="30">
                                                ' . $row['t_npwpwppembeli'] . '
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="border_bawah">
                                                3. Alamat Wajib Pajak :
                                            </td>
                                            <td colspan="20" class="border_bawah">
                                                ' . strtoupper($row["t_alamatwppembeli"]) . '
                                            </td>
                                            <td colspan="10" class="border_bawah">
                                                Blok/Kav/Nomor :
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="border_bawah">
                                                4. Kelurahan/Desa :
                                            </td>
                                            <td colspan="10" class="border_bawah">
                                                ' . strtoupper($row["t_kelurahanwppembeli"]) . '
                                            </td>
                                            <td colspan="10" class="border_bawah">
                                                5. RT/RW :
                                                ' . strtoupper($row["t_rtwppembeli"]) . ' / ' . strtoupper($row["t_rwwppembeli"]) . '
                                            </td>
                                            <td colspan="10" class="border_bawah">
                                                6. Kecamatan : ' . strtoupper($row["t_kecamatanwppembeli"]) . '
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="border_bawah">
                                                7. Kabupaten/Kota :
                                            </td>
                                            <td colspan="20" class="border_bawah">
                                                ' . strtoupper($row["t_kabkotawppembeli"]) . '
                                            </td>
                                            <td colspan="10" class="border_bawah">
                                                8. Kode Pos : ' . $row["t_kodeposwppembeli"] . '
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="4" class="border_bawah font_delapan" style="padding: 10px 0px 10px 0px;">
                        <table width="100%" style="border-spacing: 0px;">
                            <tr>
                                <td align="center" style="width: 5%; padding-top: -47px;">
                                    B.
                                </td>
                                <td>
                                    <table width="95%" style="border-spacing: 0px;">
                                        <tr>
                                            <td style="width: 30%">
                                                1. Nomor Objek Pajak (NOP) PBB :
                                            </td>';
                for ($i = 0; $i <= 1; $i++) {
                    $html .= '<td style="height: 8px; text-align: center;" class="border_atas border_bawah border_kiri">
                                                     ' . $row['t_nopbphtbsppt'][$i] . '
                                                </td>';
                }
                $html .= '<td class="border_kiri">
                                            </td>';
                for ($i = 3; $i <= 4; $i++) {
                    $html .= '<td style="text-align: center;" class="border_atas border_bawah border_kiri">
                                                    ' . $row['t_nopbphtbsppt'][$i] . '
                                                </td>';
                }
                $html .= '<td class="border_kiri">
                                            </td>';
                for ($i = 6; $i <= 8; $i++) {
                    $html .= '<td style="text-align: center;" class="border_atas border_bawah border_kiri">
                                                    ' . $row['t_nopbphtbsppt'][$i] . '
                                                </td>';
                }
                $html .= '<td class="border_kiri">
                                            </td>';
                for ($i = 10; $i <= 12; $i++) {
                    $html .= '<td style="text-align: center;" class="border_atas border_bawah border_kiri">
                                                    ' . $row['t_nopbphtbsppt'][$i] . '
                                                </td>';
                }
                $html .= '<td class="border_kiri">
                                            </td>';
                for ($i = 14; $i <= 16; $i++) {
                    $html .= '<td style="text-align: center;" class="border_atas border_bawah border_kiri">
                                                    ' . $row['t_nopbphtbsppt'][$i] . '
                                                </td>';
                }
                $html .= '<td class="border_kiri">
                                            </td>';
                for ($i = 18; $i <= 21; $i++) {
                    $html .= '<td style="text-align: center;" class="border_atas border_bawah border_kiri">
                                                    ' . $row['t_nopbphtbsppt'][$i] . '
                                                </td>';
                }
                $html .= '<td class="border_kiri">
                                            </td>
                                            <td class="border_atas border_bawah border_kiri border_kanan" style="text-align: center;">
                                                ' . $row['t_nopbphtbsppt'][23] . '
                                            </td>
                                            <td colspan="5">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="border_bawah">
                                                2. Letak tanah dan atau bangunan :
                                            </td>
                                            <td colspan="13" class="border_bawah">
                                                ' . strtoupper($row["t_alamatop"]) . '
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="14" class="border_bawah">
                                                3. Kelurahan/Desa : ' . $row["t_kelurahanop"] . '
                                            </td>
                                            <td colspan="16" class="border_bawah">
                                                4. RT/RW :
                                                ' . strtoupper($row["t_rtop"]) . ' / ' . strtoupper($row["t_rwop"]) . '
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="14" class="border_bawah">
                                                5. Kecamatan : ' . $row["t_kecamatanop"] . '
                                            </td>
                                            <td colspan="16" class="border_bawah">
                                                6. Kabupaten/Kota : ' . $data_pemda->s_namakabkot . '
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>

                                </td>
                                <td class="font_delapan">
                                    Penghitungan NJOP PBB
                                </td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                                <td class="font_delapan">
                                    <table width="95%" style="border-spacing: 0px;">
                                        <tr>
                                            <td class="border_atas border_bawah border_kiri border_kanan" style="text-align: center;">
                                                Uraian
                                            </td>
                                            <td colspan="2" class="border_atas border_bawah border_kanan" style="text-align: center;">
                                                <p style="size: 2px; margin: 0;"><b>Luas</b><br><span style="font-size:8px;">(Diisi luas tanah dan atau bangunan yang haknya diperoleh)</span></p>
                                            </td>
                                            <td colspan="2" class="border_atas border_bawah border_kanan" style="text-align: center;">
                                                <p style="size: 2px; margin: 0;"><b>NJOP PBB / m <sup>2</sup></b><br><span style="font-size:8px;">(Diisi berdasarkan SPPT PBB tahun terjadinya perolehan hak/Tahun......)</span></p>
                                            </td>
                                            <td colspan="2" class="border_atas border_bawah border_kanan" style="text-align: center;">
                                                Luas x NJOP PBB / m<sup>2</sup>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="border_bawah border_kiri border_kanan">
                                                Tanah (Bumi)
                                            </td>
                                            <td class="border_bawah border_kanan" style="width: 10px; text-align: center;">
                                                7
                                            </td>
                                            <td class="border_bawah border_kanan" style="text-align: right">';

                if (!empty($row['p_luastanah'])) {
                    $luastanah = $row['p_luastanah'];
                } else {
                    $luastanah = $row["t_luastanah"];
                }
                $html .= number_format($luastanah, 0, ',', '.') . ' m<sup>2</sup>
                                            </td>
                                            <td class="border_bawah border_kanan" style="width: 3px; text-align: center;">
                                                9
                                            </td>
                                            <td class="border_bawah border_kanan" style="text-align: right">';

                if (!empty($row['p_njoptanah'])) {
                    $njoptanah = $row['p_njoptanah'];
                } else {
                    $njoptanah = $row["t_njoptanah"];
                }
                $html .= number_format($njoptanah, 0, ',', '.') . '
                                            </td>
                                            <td class="border_bawah border_kanan" style="width: 10px; text-align: center;">
                                                11
                                            </td>
                                            <td class="border_bawah border_kanan" style="text-align: right">';

                if (!empty($row['p_totalnjoptanah'])) {
                    $totalnjoptanah = $row['p_totalnjoptanah'];
                } else {
                    $totalnjoptanah = $row["t_totalnjoptanah"];
                }

                $html .= number_format($totalnjoptanah, 0, ',', '.') . '
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="border_bawah border_kiri border_kanan">
                                                Bangunan
                                            </td>
                                            <td class="border_bawah border_kanan" style="text-align: center;">
                                                8
                                            </td>
                                            <td class="border_bawah border_kanan" style="text-align: right">';

                if (!empty($row['p_luasbangunan'])) {
                    $luasbangunan = $row['p_luasbangunan'];
                } else {
                    $luasbangunan = $row["t_luasbangunan"];
                }

                $html .= number_format($luasbangunan, 0, ',', '.') . ' m<sup>2</sup>
                                            </td>
                                            <td class="border_bawah border_kanan" style="text-align: center;">
                                                10
                                            </td>
                                            <td class="border_bawah border_kanan" style="text-align: right">';

                if (!empty($row['p_njopbangunan'])) {
                    $njopbangunan = $row['p_njopbangunan'];
                } else {
                    $njopbangunan = $row["t_njopbangunan"];
                }

                $html .= number_format($njopbangunan, 0, ',', '.') . '
                                            </td>
                                            <td class="border_bawah border_kanan" style="text-align: center;">
                                                12
                                            </td>
                                            <td class="border_bawah border_kanan" style="text-align: right">';

                if (!empty($row['p_totalnjopbangunan'])) {
                    $totalnjopbangunan = $row['p_totalnjopbangunan'];
                } else {
                    $totalnjopbangunan = $row["t_totalnjopbangunan"];
                }

                $html .= number_format($totalnjopbangunan, 0, ',', '.') . '
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="5" class="border_kanan" align="right">
                                                NJOP PBB : &nbsp; &nbsp; &nbsp;
                                            </td>
                                            <td class="border_bawah border_kanan" style="text-align: center;">
                                                13
                                            </td>
                                            <td class="border_bawah border_kanan" style="text-align: right">';

                if (!empty($row['p_grandtotalnjop'])) {
                    $grandnjop = $row['p_grandtotalnjop'];
                } else {
                    $grandnjop = $row["t_grandtotalnjop"];
                }

                $html .= number_format($grandnjop, 0, ',', '.') . '
                                            </td>
                                        </tr>';

                if (($row['t_tarif_pembagian_aphb_kali'] == null) || ($row['t_tarif_pembagian_aphb_kali'] == '') || ($row['t_tarif_pembagian_aphb_kali'] == 0) || ($row['t_tarif_pembagian_aphb_bagi'] == null) || ($row['t_tarif_pembagian_aphb_bagi'] == '') || ($row['t_tarif_pembagian_aphb_bagi'] == 0)) {

                    $ket_aphb_pembagian = '';
                    $grandnjop_aphb = 0;
                } else {
                    $ket_aphb_pembagian = ' ' . $row['t_tarif_pembagian_aphb_kali'] . '/' . $row['t_tarif_pembagian_aphb_bagi'];
                    if (!empty($row['p_grandtotalnjop_aphb'])) {
                        $grandnjop_aphb = $row['p_grandtotalnjop_aphb'];
                    } else {
                        $grandnjop_aphb = $row["t_grandtotalnjop_aphb"];
                    }
                    $html .= '
                                            <tr>
                                                <td colspan="5" class="border_kanan" align="right">
                                                    &nbsp; &nbsp; &nbsp;
                                                </td>
                                                <td class="border_bawah border_kanan" colspan="2" style="text-align: right">
                                                    ' . $ket_aphb_pembagian . '
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="5" class="border_kanan" align="right">
                                                    NJOP PBB APHB: &nbsp; &nbsp; &nbsp;
                                                </td>
                                                <td class="border_bawah border_kanan" colspan="2" style="text-align: right">
                                                    ' . number_format($grandnjop_aphb, 0, ',', '.') . '
                                                </td>
                                            </tr>';
                }
                $html .= '
                                        <tr>
                                            <td colspan="7">
                                                &nbsp;&nbsp;
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="3" align="left">
                                                15. Jenis perolehan hak atas tanah dan/atau bangunan :
                                                <table align="right" style="margin-top:-20px;">
                                                    <tr>
                                                        <td style="width: 20px; text-align: center;" class="border_atas border_bawah border_kanan border_kiri">
                                                            ' . substr(str_pad($row["t_idjenistransaksi"], 2, '0', STR_PAD_LEFT), 0, 1) . '
                                                        </td>
                                                        <td style="width: 20px; text-align: center;" class="border_atas border_bawah border_kanan border_kiri">
                                                            ' . substr(str_pad($row["t_idjenistransaksi"], 2, '0', STR_PAD_LEFT), 1, 1) . '
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td colspan="2" align="left">
                                                &nbsp; &nbsp;14. Harga transaksi / Nilai pasar :
                                            </td>
                                            <td class="border_kiri border_bawah border_atas" align="left"> Rp.</td>

                                            <td class="border_bawah border_kanan border_atas " align="right">';

                if (!empty($row['p_nilaitransaksispt'])) {
                    $nilaitransaksispt = $row['p_nilaitransaksispt'];
                } else {
                    $nilaitransaksispt = $row["t_nilaitransaksispt"];
                }

                $html .= number_format($nilaitransaksispt, 0, ',', '.') . '
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>

                                </td>
                                <td class="font_delapan">
                                </td>
                            </tr>
                            <tr>
                                <td>

                                </td>
                                <td class="font_delapan">
                                    16. Nomor Sertifikat Tanah : ' . $row["t_nosertifikathaktanah"] . '
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="4" class="font_delapan border_bawah">
                        <table width="100%" style="border-spacing: 0">
                            <tr>
                                <td class="border_bawah" colspan="5">
                                    &nbsp;&nbsp;C. PENGHITUNGAN BPHTB (Hanya diisi berdasarkan penghitungan Wajib Pajak)';

                // Untuk Perhitungan NPOP
                $aphb_kali = $row['t_tarif_pembagian_aphb_kali'];
                $aphb_bagi = $row['t_tarif_pembagian_aphb_kali'];
                if (($aphb_kali == null) || ($aphb_kali == '') || ($aphb_kali == 0) || ($aphb_bagi == null) || ($aphb_bagi == '') || ($aphb_bagi == 0)) {

                    if ($grandnjop > $nilaitransaksispt) {
                        $npop = $grandnjop;
                    } else {
                        $npop = $nilaitransaksispt;
                    }
                } else {
                    if ($grandnjop_aphb > $nilaitransaksispt) {
                        $npop = $grandnjop_aphb;
                    } else {
                        $npop = $nilaitransaksispt;
                    }
                }

                // Untuk Potongan (NPOPTKP)
                $potonganspt = $row["t_potonganspt"];

                if (($row['t_potongan_waris_hibahwasiat'] == null) || ($row['t_potongan_waris_hibahwasiat'] == 0)) {

                    $t_potongan_waris_hibahwasiat = 0;
                    $hitung_potonganwaris = 1;
                } else {

                    $t_potongan_waris_hibahwasiat = $row['t_potongan_waris_hibahwasiat'];
                    $hitung_potonganwaris = $t_potongan_waris_hibahwasiat / 100;
                }

                // Untuk Perhitungan NPOPKP
                if ($npop < $row["t_potonganspt"]) {
                    $npopkp = 0;
                } else {
                    $npopkp = $npop - $row["t_potonganspt"];
                }

                // Untuk Perhitungan Pajak BPHTB
                if ($npopkp == 0) {
                    $totalpajak = 0;
                    $totalpajak_ori = 0;
                } else {
                    $totalpajak_ori = ceil($npopkp * $row['t_persenbphtb'] / 100);
                    $totalpajak = ceil($npopkp * $row['t_persenbphtb'] / 100 * $hitung_potonganwaris);
                }


                if (!empty($row['t_approve_pengurangpembebas'])) {
                    $totalspt = $row['t_totalspt'];
                } else {
                    if (!empty($row['p_totalspt'])) {
                        $totalspt = $row['p_totalspt'];
                    } else {
                        $totalspt = $row["t_totalspt"];
                    }
                }
                $html .= '
                                </td>
                            </tr>
                            <tr>

                                <td colspan="2" class=" border_bawah">
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1. Nilai Perolehan Objek Pajak (NPOP) <span style="font-size:10px"><i>memperhatikan nilai pada B.13 dan B.14 </i></span>
                                </td>
                                <td class="border_kanan border_kiri border_bawah" style="width: 20px; text-align: center;">
                                    1
                                </td>
                                <td class=" border_bawah" style="width: 10px;">
                                    Rp.
                                </td>
                                <td class=" border_bawah" style="text-align: right;">
                                    ' . number_format($npop, 0, ',', '.') . '
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" class=" border_bawah">
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;2. Nilai Perolehan Objek Pajak Tidak Kena Pajak (NPOPTKP)
                                </td>
                                <td class="border_kanan border_kiri border_bawah" style="text-align: center;">
                                    2
                                </td>
                                <td class=" border_bawah">
                                    Rp.
                                </td>
                                <td class=" border_bawah" style="text-align: right;">
                                    ' . number_format($potonganspt, 0, ',', '.') . '
                                </td>
                            </tr>
                            <tr>
                                <td class=" border_bawah" style="width: 450px;">
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;3. Nilai Perolehan Objek Pajak Kena Pajak (NPOPKP)
                                </td>
                                <td class="border_kiri border_bawah" style="width: 80px; text-align: center;">
                                    angka 1-angka 2
                                </td>
                                <td class="border_kanan border_kiri border_bawah" style="text-align: center;">
                                    3
                                </td>
                                <td class=" border_bawah">
                                    Rp.
                                </td>
                                <td class=" border_bawah" style="text-align: right;">
                                    ' . number_format($npopkp, 0, ',', '.') . '
                                </td>
                            </tr>
                            <tr>
                                <td class=" border_bawah">
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;4. Bea Perolehan Hak atas Tanah dan Bangunan yang terutang
                                </td>
                                <td class="border_kiri border_bawah" style="width: 80px; text-align: center;">
                                    ' . $row['t_persenbphtb'] . '% x angka 3
                                </td>
                                <td class="border_kanan border_kiri border_bawah" style="text-align: center;">
                                    4
                                </td>
                                <td class=" border_bawah">
                                    Rp.
                                </td>
                                <td class=" border_bawah" style="text-align: right;">
                                    ' . number_format($totalspt, 0, ',', '.') . '
                                </td>
                            </tr>
                            <tr>
                                <td class=" border_bawah">
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;5. Pengenaan 50% karena waris / Hibah Wasiat / pemberian hak pengelolaan*)
                                </td>
                                <td class="border_kiri border_bawah" style="width: 80px; text-align: center;">
                                    ' . $row['t_potongan_waris_hibahwasiat'] . '% x angka 4
                                </td>
                                <td class="border_kanan border_kiri border_bawah" style="text-align: center;">
                                    5
                                </td>
                                <td class=" border_bawah">
                                    Rp.
                                </td>
                                <td class=" border_bawah" style="text-align: right;">';

                if (($row['t_potongan_waris_hibahwasiat'] == null) || ($row['t_potongan_waris_hibahwasiat'] == 0)) {
                    $html .= '';
                } else {
                    $html .= number_format($row['t_totalspt'], 0, ',', '.');
                }
                $html .= '
                                </td>
                            </tr>';
                // Untuk Nilai Pembayaran Yang Pernah Dibayarkan
                if (!empty($dataidsptsebelum['t_idsptsebelumnya'])) {
                    $total_pembayaran_sspd = $totalpajak - $ar_sebelum['t_nilaipembayaranspt'];
                } else {
                    $total_pembayaran_sspd = $totalspt;
                }
                $html .= '
                            <tr>
                                <td class=" border_bawah">
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;6. Bea Perolehan Hak atas Tanah dan Bangunan yang harus dibayar
                                </td>
                                <td class="border_kiri border_bawah" style="width: 80px; text-align: center;">
                                    
                                </td>
                                <td class="border_kanan border_kiri border_bawah" style="text-align: center;">
                                    6
                                </td>
                                <td class=" border_bawah">
                                    Rp.
                                </td>
                                <td class=" border_bawah" style="text-align: right;">
                                    ' . number_format($totalspt, 0, ',', '.') . '
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="4" class="font_delapan border_bawah">
                        <table width="100%" style="border-spacing: 5px">
                            <tr>
                                <td colspan="3">
                                    D. Jumlah Setoran berdasarkan : (Beri tanda silang "X" pda kotak yang sesuai)
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 5px; height: 5px; margin-left: 10px" class="border_atas border_bawah border_kanan border_kiri">

                                </td>
                                <td colspan="2">
                                    a. Penghitungan Wajib Pajak
                                </td>
                            </tr>

                            <tr>
                                <td style="width: 5px; height: 5px; margin-left: 10px; vertical-align:middle; " class="border_atas border_bawah border_kanan border_kiri">';

                if (!empty($dataidsptsebelum['t_idsptsebelumnya'])) {
                    $html .= 'X';
                } else {
                    $html .= '';
                }
                $html .= '
                                </td>
                                <td colspan="2">
                                    b. STPD/ SKPDKB/ SKPDKBT *) &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Nomor: ............................... &nbsp;&nbsp;&nbsp;&nbsp; Tanggal: .......................
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 5px; height: 5px; margin-left: 10px" class="border_atas border_bawah border_kanan border_kiri">

                                </td>
                                <td align="left">
                                    c. Pengurangan dihitung sendiri menjadi :
                                    <table align="right" style="margin-top:-20px; margin-right: 50px;">
                                        <tr>
                                            <td style="width: 20px; height: 20px; text-align: center;" class="border_atas border_bawah border_kanan border_kiri">

                                            </td>
                                            <td style="width: 20px; height: 20px; text-align: center;" class="border_atas border_bawah border_kanan border_kiri">

                                            </td>
                                        </tr>
                                    </table>

                                </td>
                                <td>

                                    % berdasarkan Peraturan Bupati Nomor ..................................
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 5px; height: 5px; margin-left: 10px" class="border_atas border_bawah border_kanan border_kiri">

                                </td>
                                <td colspan="2">
                                    d. ..........................
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3">
                                    <i>*) Coret yang tidak perlu</i>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="4" class="border_bawah border_kanan font_lima" style="border-spacing: 10px; height: 50px;">
                        <table align="left">
                            <tr>
                                <td width="150" style="text-align: center;" align="center">JUMLAH YANG DISETOR (dengan angka): </td>
                                <td style="text-align: left;" rowspan="2">
                                    (dengan huruf) :';

                if ($totalpajak != 0) {
                    $html .= ucwords(strtolower(terbilang($total_pembayaran_sspd) . " Rupiah"));
                } else {
                    $html .= 'Nihil';
                }
                $html .= '
                                </td>
                            </tr>
                            <tr>
                                <td style="height: 20px; text-align: center;" class="border_atas border_bawah border_kanan border_kiri" align="center" width="120">
                                    Rp. ' . number_format($total_pembayaran_sspd, 0, ',', '.') . ' 
                                </td>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="4">
                        <table width="100%" style="border-spacing: 0">
                            <tr>
                                <td width="27%" class="border_kanan font_tujuh" style="text-align: center;">
                                    ' . $data_pemda->s_namaibukotakabkot . ', tgl';

                if ($row['t_tglprosesspt'] != null) {
                    $html .= date('d-m-Y', strtotime($row['t_tglprosesspt']));
                } else {
                    $html .= "________________";
                }
                $html .= '
                                    <br>
                                    WAJIB PAJAK / PENYETOR
                                    <br><br><br><br><br><br>
                                    ' . strtoupper($row['t_namawppembeli']) . '
                                </td>
                                <td width="27%" class="border_kanan font_tujuh" style="text-align: center;">
                                    MENGETAHUI :
                                    <br>
                                    PPAT / NOTARIS<br>
                                    KABUPATEN BLORA<br>
                                    <br><br><br><br><br>
                                    ' . strtoupper($row['s_namanotaris']) . '
                                </td>
                                <td width="25%" class="border_kanan" style="text-align: center; font-size: 6pt">
                                    DITERIMA OLEH :
                                    <br>
                                    TEMPAT PEMBAYARAN BPHTB
                                    <br>
                                    Tanggal :
                                    <b>';

                if ($row['t_tanggalpembayaran'] != null) {
                    $html .= date('d-m-Y', strtotime($row['t_tanggalpembayaran']));
                } else {
                    $html .= "";
                }
                $html .= '
                                    </b>
                                    <br><br><br><br><br>';
                if ($row['t_idpenerimasetoran'] == 3) {
                    if ($row['t_nilaipembayaranspt'] == 0) {
                        $html .= '';
                    } else {
                        $html .= 'BANK JATENG';
                    }
                } else {
                    $html .= strtoupper($row['namapenerimasetoran']);
                }
                $html .= '
                                </td>

                                <td width="25%" class="font_tujuh" style="text-align: center;">
                                    Telah di Verifikasi :
                                    <b>';
                if ($row['t_tglverifikasispt'] != null) {
                    $html .= date('d-m-Y', strtotime($row['t_tglverifikasispt']));
                } else {
                    $html .= "";
                }
                $html .= '
                                    </b>
                                    <br>
                                    ' . strtoupper($data_pemda->s_namainstansi) . '
                                    <br><br><br><br><br>
                                    ' . strtoupper($data_mengetahui->s_namapejabat) . '
                                </td>
                            </tr>
                            <tr>
                                <td class="border_bawah border_kanan border_atas font_lima" style="text-align: center; border-spacing: 10px">
                                    Nama Lengkap dan tanda tangan
                                </td>
                                <td class="border_bawah border_kanan border_atas font_lima" style="text-align: center;">
                                    Nama Lengkap, Stempel dan tanda tangan
                                </td>
                                <td class="border_bawah border_kanan border_atas font_lima" style="text-align: center;">
                                    Nama Lengkap, Stempel dan tanda tangan
                                </td>
                                <td class="border_bawah font_lima border_atas" style="text-align: center;">
                                    Nama Lengkap, Stempel dan tanda tangan
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="4">
                        <table width="100%" style="border-spacing: 0">
                            <tr>
                                <td class="font_lima border_kanan" style="text-align: center; width: 80px;">
                                    Hanya diisi oleh <br>petugas <?= $data_pemda->s_namasingkatinstansi ?>
                                </td>
                                <td>
                                    <table width="100%" style="border-spacing: 0; margin: 10px">
                                        <tr>
                                            <td width="80">
                                                Nomor Dokumen
                                            </td>
                                            <td width="5">
                                                :
                                            </td>
                                            <td style="height: 8px; text-align: center;" class="border_atas border_bawah border_kiri" width="12">';

                $tgl = explode('-', $row['t_tglprosesspt']);
                $tahun = $tgl[0];
                $bulan = $tgl[1];
                $hari = $tgl[2];

                $html .= $hari[0] . '
                                            </td>
                                            <td style="text-align: center;" class="border_atas border_bawah border_kiri border_kanan" width="12">
                                                ' . $hari[1] . '
                                            </td>
                                            <td width="5">&nbsp;
                                            </td>
                                            <td style="text-align: center;" class="border_atas border_bawah border_kiri" width="12">
                                                ' . $bulan[0] . '
                                            </td>
                                            <td style="text-align: center;" class="border_atas border_bawah border_kiri border_kanan" width="12">
                                                ' . $bulan[1] . '
                                            </td>
                                            <td width="5">
                                                &nbsp;
                                            </td>
                                            <td style="text-align: center;" class="border_atas border_bawah border_kiri" width="12">
                                                ' . $tahun[0] . '
                                            </td>
                                            <td style="text-align: center;" class="border_atas border_bawah border_kiri" width="12">
                                                ' . $tahun[1] . '
                                            </td>
                                            <td style="text-align: center;" class="border_atas border_bawah border_kiri" width="12">
                                                ' . $tahun[2] . '
                                            </td>
                                            <td style="text-align: center;" class="border_atas border_bawah border_kiri border_kanan" width="12">
                                                ' . $tahun[3] . '
                                            </td>
                                            <td width="12">&nbsp;
                                            </td>';
                $nosspd = str_pad($row['t_kohirketetapanspt'], 7, '0', STR_PAD_LEFT);
                $html .= '
                                            <td style="text-align: center;" class="border_atas border_bawah border_kiri" width="12">
                                                ' . $nosspd[0] . '
                                            </td>
                                            <td style="text-align: center;" class="border_atas border_bawah border_kiri" width="12">
                                                ' . $nosspd[1] . '
                                            </td>
                                            <td style="text-align: center;" class="border_atas border_bawah border_kiri" width="12">
                                                ' . $nosspd[2] . '
                                            </td>
                                            <td style="text-align: center;" class="border_atas border_bawah border_kiri border_kanan" width="12">
                                                ' . $nosspd[3] . '
                                            </td>
                                            <td width="12">&nbsp;
                                            </td>
                                            <td class="border_atas border_bawah border_kiri" width="12">
                                                ' . $nosspd[4] . '
                                            </td>
                                            <td class="border_atas border_bawah border_kiri" width="12">
                                                ' . $nosspd[5] . '
                                            </td>
                                            <td class="border_atas border_bawah border_kiri border_kanan" width="12">
                                                ' . $nosspd[6] . '
                                            </td>
                                            <td colspan="5">&nbsp;
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="22">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                NOP PBB Baru
                                            </td>
                                            <td width="5">
                                                :
                                            </td>
                                            <td style="height: 8px;" class="border_bawah border_kiri border_atas">&nbsp;
                                            </td>
                                            <td class="border_bawah border_kiri border_kanan border_atas">&nbsp;
                                            </td>
                                            <td>&nbsp;
                                            </td>
                                            <td class="border_bawah border_kiri border_atas">&nbsp;
                                            </td>
                                            <td class="border_bawah border_kiri border_kanan border_atas">&nbsp;
                                            </td>
                                            <td>&nbsp;
                                            </td>
                                            <td class="border_bawah border_kiri border_atas">&nbsp;
                                            </td>
                                            <td class="border_bawah border_kiri border_atas">&nbsp;
                                            </td>
                                            <td class="border_bawah border_kiri border_atas">&nbsp;
                                            </td>
                                            <td class="border_kanan border_kiri ">&nbsp;
                                            </td>
                                            <td class="border_bawah border_atas">&nbsp;
                                            </td>
                                            <td class="border_bawah border_kiri border_atas">&nbsp;
                                            </td>
                                            <td class="border_bawah border_kiri border_atas">&nbsp;
                                            </td>
                                            <td class="border_kiri">&nbsp;
                                            </td>
                                            <td class="border_bawah border_kiri border_kanan border_atas">&nbsp;
                                            </td>
                                            <td class="border_bawah border_atas">&nbsp;
                                            </td>
                                            <td class="border_bawah border_kiri border_atas">&nbsp;
                                            </td>
                                            <td class="border_kiri">&nbsp;
                                            </td>
                                            <td class="border_bawah border_kiri border_kanan border_atas">&nbsp;
                                            </td>
                                            <td class="border_atas border_bawah">&nbsp;
                                            </td>
                                            <td class="border_atas border_bawah border_kiri">&nbsp;
                                            </td>
                                            <td class="border_atas border_bawah border_kiri border_kanan">&nbsp;
                                            </td>
                                            <td>&nbsp;
                                            </td>
                                            <td class="border_atas border_bawah border_kiri border_kanan">&nbsp;
                                            </td>

                                        </tr>

                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <table>
                <tr>
                    <td><span style="font-size: 8px;">Tempat pembayaran BPHTB :</span></td>
                    <td><span style="font-size: 8px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></td>
                    <td><span style="font-size: 8px;">Keterangan</span></td>
                </tr>
                <tr>
                    <td><span style="font-size: 8px;">1. '. $data_pemda->s_namabank .'<br></span></td>
                    <td><span style="font-size: 8px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></td>
                    <td><span style="font-size: 8px;">Lembar 1 (warna putih) : Untuk Wajib Pajak</span></td>

                </tr>
                <tr>
                    <td><span style="font-size: 8px;"> </span></td>
                    <td><span style="font-size: 8px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></td>
                    <td><span style="font-size: 8px;">Lembar 2 (warna merah) : Untuk PPAT/Notaris sebagai arsip</span></td>

                </tr>
                <tr>
                    <td><span style="font-size: 8px;"> </span></td>
                    <td><span style="font-size: 8px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></td>
                    <td><span style="font-size: 8px;">Lembar 3 (warna kuning) : Untuk Kepala Kantor Bidang Pertanahan</span></td>

                </tr>
                <tr>
                    <td><span style="font-size: 8px;"></span></td>
                    <td><span style="font-size: 8px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></td>
                    <td><span style="font-size: 8px;">Lembar 4 (warna putih) : Untuk <?= $data_pemda->s_namasingkatinstansi ?> dalam proses penelitian</span></td>

                </tr>
            </table>

        </div>
                ';
            }
            if ($cetakversi == 2) {
                // SKPDKB
                if (!empty($row['p_nilaitransaksispt'])) {
                    $nilaitransaksispt = $row['p_nilaitransaksispt'];
                } else {
                    $nilaitransaksispt = $row["t_nilaitransaksispt"];
                }

                if (!empty($row['p_grandtotalnjop'])) {
                    $grandnjop = $row['p_grandtotalnjop'];
                } else {
                    $grandnjop = $row["t_grandtotalnjop"];
                }

                if (($row['t_tarif_pembagian_aphb_kali'] == null) || ($row['t_tarif_pembagian_aphb_kali'] == '') || ($row['t_tarif_pembagian_aphb_kali'] == 0) || ($row['t_tarif_pembagian_aphb_bagi'] == null) || ($row['t_tarif_pembagian_aphb_bagi'] == '') || ($row['t_tarif_pembagian_aphb_bagi'] == 0)) {


                    $grandnjop_aphb = 0;
                } else {

                    if (!empty($row['p_grandtotalnjop_aphb'])) {
                        $grandnjop_aphb = $row['p_grandtotalnjop_aphb'];
                    } else {
                        $grandnjop_aphb = $row["t_grandtotalnjop_aphb"];
                    }
                }

                // Untuk Perhitungan NPOP
                $aphb_kali = $row['t_tarif_pembagian_aphb_kali'];
                $aphb_bagi = $row['t_tarif_pembagian_aphb_kali'];
                if (($aphb_kali == null) || ($aphb_kali == '') || ($aphb_kali == 0) || ($aphb_bagi == null) || ($aphb_bagi == '') || ($aphb_bagi == 0)) {

                    if ($grandnjop > $nilaitransaksispt) {
                        $npop = $grandnjop;
                    } else {
                        $npop = $nilaitransaksispt;
                    }
                } else {
                    if ($grandnjop_aphb > $nilaitransaksispt) {
                        $npop = $grandnjop_aphb;
                    } else {
                        $npop = $nilaitransaksispt;
                    }
                }
                // Untuk Potongan (NPOPTKP)
                $potonganspt = $row["t_potonganspt"];

                if (($row['t_potongan_waris_hibahwasiat'] == null) || ($row['t_potongan_waris_hibahwasiat'] == 0)) {

                    $t_potongan_waris_hibahwasiat = 0;
                    $hitung_potonganwaris = 1;
                } else {

                    $t_potongan_waris_hibahwasiat = $row['t_potongan_waris_hibahwasiat'];
                    $hitung_potonganwaris = $t_potongan_waris_hibahwasiat / 100;
                }

                // Untuk Perhitungan NPOPKP
                if ($npop < $row["t_potonganspt"]) {
                    $npopkp = 0;
                } else {
                    $npopkp = $npop - $row["t_potonganspt"];
                }

                // Untuk Perhitungan Pajak BPHTB
                if ($npopkp == 0) {
                    $totalpajak = 0;
                    $totalpajak_ori = 0;
                } else {
                    $totalpajak_ori = ceil($npopkp * $row['t_persenbphtb'] / 100);
                    $totalpajak = ceil($npopkp * $row['t_persenbphtb'] / 100 * $hitung_potonganwaris);
                }

                if (!empty($dataidsptsebelum['t_idsptsebelumnya'])) {

                    $sudahdibayar = $ar_sebelum['t_nilaipembayaranspt'];

                    $total_pembayaran_sspd = $totalpajak - $ar_sebelum['t_nilaipembayaranspt'];
                    //$nilaipembayaranspt = $ar_sebelum['t_nilaipembayaranspt'] + $row['t_nilaipembayaranspt'];
                } else {
                    $sudahdibayar = " ";
                    $total_pembayaran_sspd = $totalpajak;

                    //$nilaipembayaranspt = $row['t_nilaipembayaranspt'];
                }
                $html .= '
        <div style="page-break-after: always;width: 100%;">
            <table style="border-spacing: 0; font-size:9pt; width: 100%; margin-right: 30px;">
                <tr>
                    <td colspan="2">
                        <table style="border-spacing: 0;">
                            <tr>
                                <td width="50" class="border_bawah">
                                    <center><img src="https://' . $_SERVER['HTTP_HOST'] . '/' . $this->basePath() . '/' . $data_pemda->s_logo . '" alt="" width="60" style="float:left;margin-top:10px; " title="LOGO <?= $data_pemda->s_namakabkot ?>" /></center>
                                </td>
                                <td class="border_bawah">
                                    <center>
                                        <b style="font-size:14pt;">PEMERINTAH ' . strtoupper($data_pemda->s_namakabkot) . ', ' . strtoupper($data_pemda->s_namaprov) . '</b>
                                        <br>
                                        <b style="font-size:11pt;">' . strtoupper($data_pemda->s_namainstansi) . ' (' . strtoupper($data_pemda->s_namasingkatinstansi) . ')</b>
                                        <br>
                                        <b style="font-size:10pt;">' . strtoupper($data_pemda->s_alamatinstansi) . ' Telp/Fax ' . $data_pemda->s_notelinstansi . ', ' . $data_pemda->s_namaibukotakabkot . ' ' . $data_pemda->s_kodepos . '</b>
                                    </center>
                                </td>
                            </tr>
                        </table>
                    </td>
                <tr>
                <td colspan="2">
                        <center><b style="font-size:10pt;">SURAT KETETAPAN PAJAK DAERAH KURANG BAYAR</b></center><br><br>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <table style="width:100%">
                            <tr>
                                <td style="width: 60%;">
                                    &nbsp;
                                </td>
                                <td>
                                    Kepada Yth : ' . strtoupper($row['t_namawppembeli']) . '<br>
                                    Di ' . strtoupper($row["t_alamatwppembeli"]) . '
                                    RT/RW ' . strtoupper($row["t_rtwppembeli"]) . ' / ' . strtoupper($row["t_rwwppembeli"]) . '<br>
                                    Kel. ' . strtoupper($row["t_kelurahanwppembeli"]) . ' Kec. ' . strtoupper($row["t_kecamatanwppembeli"]) . '
                                    Kab. ' . strtoupper($row["t_kabkotawppembeli"]) . '<br>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <table>
                            <tr>
                                <td>Nomor</td>
                                <td>:</td>
                                <td>' . $row['t_kohirspt'] . '</td>
                            </tr>
                            <tr>
                                <td>Tanggal penerbitan</td>
                                <td>:</td>
                                <td>' . date('d-m-Y', strtotime($row['t_tglprosesspt'])) . '</td>
                            </tr>

                            <tr>
                                <td>Tanggal jatuh tempo</td>
                                <td>:</td>
                                <td>' . date('d-m-Y', strtotime(date('Y-m-d', strtotime($row['t_tglprosesspt'])) . "+1 months")) . '</td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <table>
                            <tr>
                                <td style="vertical-align:top; width: 10px;">I.</td>
                                <td colspan="3">Berdasarkan Peraturan Bupati Tegal Nomor 32 Tahun 2012
                                    tentang Bea Perolehan Hak Atas Tanah dan Bangunan telah dilakukan
                                    pemeriksaan atau berdasarkan keterangan lain mengenai pelaksanaan
                                    kewajiban Bea Perolehan Hak atas Tanah dan Bangunan terhadap :</td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td style="width: 120px; vertical-align: top;">Nama</td>
                                <td style="width: 3px; vertical-align: top;">:</td>
                                <td style="vertical-align: top;">' . strtoupper($row['t_namawppembeli']) . '</td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td style="vertical-align: top;">Alamat</td>
                                <td style="vertical-align: top;">:</td>
                                <td style="vertical-align: top;">' . strtoupper($row["t_alamatwppembeli"]) . '
                                    RT/RW ' . strtoupper($row["t_rtwppembeli"]) . ' / ' . strtoupper($row["t_rwwppembeli"]) . '
                                    Kel. ' . strtoupper($row["t_kelurahanwppembeli"]) . '<br>
                                    Kec. ' . strtoupper($row["t_kecamatanwppembeli"]) . '
                                    Kab. ' . strtoupper($row["t_kabkotawppembeli"]) . '</td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td colspan="3">Atas perolehan hak atas tanah dan atau bangunannnya dengan : <br>
                                    Akta. Risalah Lelang/Pendaftaran Hak *)</td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td style="vertical-align: top;">Nama</td>
                                <td style="vertical-align: top;">:</td>
                                <td style="vertical-align: top;">' . strtoupper($row["t_namasppt"]) . '</td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td style="vertical-align: top;">NOP</td>
                                <td style="vertical-align: top;">:</td>
                                <td style="vertical-align: top;">' . strtoupper($row["t_nopbphtbsppt"]) . '</td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td style="vertical-align: top;">Alamat</td>
                                <td style="vertical-align: top;">:</td>
                                <td style="vertical-align: top;">' . strtoupper($row["t_alamatop"]) . '
                                    ' . strtoupper($row["t_rtop"]) . ' / ' . strtoupper($row["t_rwop"]) . '
                                    Kel. ' . $row["t_kelurahanop"] . ' Kec. ' . $row["t_kecamatanop"] . '<br><br>
                                </td>
                            </tr>
                            <tr>
                                <td style="vertical-align:top; width: 10px;">II.</td>
                                <td colspan="3">Dari Pemeriksaan tersebut di atas, jumlah yang masih harus dibayar adalah sebagai berikut :
                                </td>
                            </tr>
                            <tr>
                                <td style="vertical-align:top; width: 10px;">&nbsp;</td>
                                <td colspan="3">
                                    <table width="100%" style="padding:0;border-spacing:0px;">
                                        <tr>
                                            <td class="border_atas border_bawah border_kiri border_kanan" style="width: 10px; vertical-align: middle; ">
                                                1.
                                            </td>
                                            <td class="border_atas border_bawah ">
                                                Nilai Perolehan Objek Pajak (NPOP)
                                            </td>
                                            <td class="border_atas border_bawah border_kiri" style="width: 15px;">
                                                Rp.
                                            </td>
                                            <td class="border_atas border_bawah border_kanan" style="text-align: right; width: 100px;">
                                                ' . number_format($npop, 0, ',', '.') . '
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="border_bawah border_kiri border_kanan">
                                                2.
                                            </td>
                                            <td class="border_bawah ">
                                                Nilai Perolehan Objek Pajak Tidak Kena Pajak (NPOPTKP)
                                            </td>
                                            <td class="border_bawah border_kiri">
                                                Rp.
                                            </td>
                                            <td class="border_bawah border_kanan" style="text-align: right;">
                                                ' . number_format($row["t_potonganspt"], 0, ',', '.') . '
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="border_bawah border_kiri border_kanan">
                                                3.
                                            </td>
                                            <td class="border_bawah ">
                                                Nilai Perolehan Objek Pajak Kena Pajak (1-2)
                                            </td>
                                            <td class="border_bawah border_kiri">
                                                Rp.
                                            </td>
                                            <td class="border_bawah border_kanan" style="text-align: right;">
                                                ' . number_format($npopkp, 0, ',', '.') . '
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="border_bawah border_kiri border_kanan">
                                                4.
                                            </td>
                                            <td class="border_bawah ">
                                                Pajak yang seharusnya terutang : 5% X Rp ' . number_format($npopkp, 0, ',', '.') . ' (3)
                                            </td>
                                            <td class="border_bawah border_kiri">
                                                Rp.
                                            </td>
                                            <td class="border_bawah border_kanan" style="text-align: right;">
                                                ' . number_format($totalpajak_ori, 0, ',', '.') . '
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="border_bawah border_kiri border_kanan">
                                                5.
                                            </td>
                                            <td class="border_bawah ">
                                                Pengenaan Hak Pengelolaan/Hibah Wasiat/Waris : ' . $t_potongan_waris_hibahwasiat . '% X Rp. ' . number_format($totalpajak_ori, 0, ',', '.') . ' (4)
                                            </td>
                                            <td class="border_bawah border_kiri">
                                                Rp.
                                            </td>
                                            <td class="border_bawah border_kanan" style="text-align: right;">
                                                ' . number_format($totalpajak, 0, ',', '.') . '
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="border_bawah border_kiri border_kanan">
                                                6.
                                            </td>
                                            <td class="border_bawah ">
                                                Pajak yang seharusnya dibayar ( 4 atau 5 )
                                            </td>
                                            <td class="border_bawah border_kiri">
                                                Rp.
                                            </td>
                                            <td class="border_bawah border_kanan" style="text-align: right;">
                                                ' . number_format($totalpajak, 0, ',', '.') . '
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="border_bawah border_kiri border_kanan">
                                                7.
                                            </td>
                                            <td class="border_bawah ">
                                                Pajak yang telah dibayar
                                            </td>
                                            <td class="border_bawah border_kiri">
                                                Rp.
                                            </td>
                                            <td class="border_bawah border_kanan" style="text-align: right;">
                                                ' . number_format($sudahdibayar, 0, ',', '.') . '
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="border_bawah border_kiri border_kanan">
                                                8.
                                            </td>
                                            <td class="border_bawah ">
                                                Diperhitungkan :
                                            </td>
                                            <td class="border_bawah border_kiri">
                                                Rp.
                                            </td>
                                            <td class="border_bawah border_kanan" style="text-align: right;">
                                                0
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="border_bawah border_kiri ">

                                            </td>
                                            <td class="border_bawah ">
                                                8.a Pokok STPD
                                            </td>
                                            <td class="border_bawah border_kiri">
                                                Rp.
                                            </td>
                                            <td class="border_bawah border_kanan" style="text-align: right;">
                                                0
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="border_bawah border_kiri border_kiri ">

                                            </td>
                                            <td class="border_bawah ">
                                                8.b. Pengurangan
                                            </td>
                                            <td class="border_bawah border_kiri">
                                                Rp.
                                            </td>
                                            <td class="border_bawah border_kanan" style="text-align: right;">
                                                0
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="border_bawah border_kiri ">

                                            </td>
                                            <td class="border_bawah ">
                                                8.c. Jumlah (8.a. + 8.b)
                                            </td>
                                            <td class="border_bawah border_kiri">
                                                Rp.
                                            </td>
                                            <td class="border_bawah border_kanan" style="text-align: right;">
                                                0
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="border_bawah border_kiri ">

                                            </td>
                                            <td class="border_bawah ">
                                                8.d. Dikurangi pokok SKPDLB
                                            </td>
                                            <td class="border_bawah border_kiri">
                                                Rp.
                                            </td>
                                            <td class="border_bawah border_kanan" style="text-align: right;">
                                                0
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="border_bawah border_kiri ">

                                            </td>
                                            <td class="border_bawah ">
                                                8.e. Jumlah (8.c - 8.d.)
                                            </td>
                                            <td class="border_bawah border_kiri">
                                                Rp.
                                            </td>
                                            <td class="border_bawah border_kanan" style="text-align: right;">
                                                0
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="border_bawah border_kiri border_kanan">
                                                9.
                                            </td>
                                            <td class="border_bawah ">
                                                Jumlah yang dapat diperhitungkan (7 + 8.e.)
                                            </td>
                                            <td class="border_bawah border_kiri">
                                                Rp.
                                            </td>
                                            <td class="border_bawah border_kanan" style="text-align: right;">
                                                ' . number_format($sudahdibayar, 0, ',', '.') . '
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="border_bawah border_kiri border_kanan">
                                                10.
                                            </td>
                                            <td class="border_bawah ">
                                                Pajak yang kurang dibayar (6-9)
                                            </td>
                                            <td class="border_bawah border_kiri">
                                                Rp.
                                            </td>
                                            <td class="border_bawah border_kanan" style="text-align: right;">
                                                ' . number_format($total_pembayaran_sspd, 0, ',', '.') . '
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="border_bawah border_kiri border_kanan">
                                                11.
                                            </td>
                                            <td class="border_bawah ">
                                                Sanksi administrasi berupa bunga (Pasal 84 Perda Pajak Daerah No 1 Tahun 2012):
                                            </td>
                                            <td class="border_bawah border_kiri">
                                                Rp.
                                            </td>
                                            <td class="border_bawah border_kanan" style="text-align: right;">
                                                0
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="border_bawah border_kiri border_kanan">
                                                12.
                                            </td>
                                            <td class="border_bawah ">
                                                Jumlah yang masih harus dibayar (10+11)
                                            </td>
                                            <td class="border_bawah border_kiri">
                                                Rp.
                                            </td>
                                            <td class="border_bawah border_kanan" style="text-align: right;">
                                                ' . number_format($total_pembayaran_sspd, 0, ',', '.') . '
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <table style="width:100%">
                            <tr>
                                <td style="width: 60%;">
                                    &nbsp;
                                </td>
                                <td>
                                    <center>Biak, &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <br>
                                        A.n Kepala Dinas<br>
                                        Kepala Bidang PPP<br><br><br><br><br>
                                        ( ' . strtoupper($data_mengetahui->s_namapejabat) . ' )</center>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">*) coret yang tidak perlu<br>
                        NIP ................................
                    </td>
                </tr>
            </table>
            <table width="100%" class="border_atas border_bawah border_kanan border_kiri font_kecil" style="border-spacing:0px; font-size: 9px;">
                <tr>
                    <td style="width: 75%;">
                        <table style="border-spacing:0px;">
                            <tr>
                                <td width="50">Nama Wajib Pajak</td>
                                <td style="width: 3px; text-align: center;">:</td>
                                <td>' . strtoupper($row['t_namawppembeli']) . '</td>
                            </tr>
                            <tr>
                                <td colspan="3">Atas perolehan Hak atas tanah dan bangunan dengan,</td>
                            </tr>
                            <tr>
                                <td>Alamat</td>
                                <td style="width: 3px; text-align: center;">:</td>
                                <td>' . strtoupper($row["t_alamatop"]) . '
                                    ' . strtoupper($row["t_rtop"]) . ' / ' . strtoupper($row["t_rwop"]) . '
                                    Kel. ' . $row["t_kelurahanop"] . ' Kec. ' . $row["t_kecamatanop"] . '</td>
                            </tr>
                            <tr>
                                <td>NOP</td>
                                <td style="width: 3px; text-align: center;">:</td>
                                <td>' . $row['t_nopbphtbsppt'] . '</td>
                            </tr>
                            <tr>
                                <td>Nomor SPTPD</td>
                                <td style="width: 3px; text-align: center;">:</td>
                                <td>' . $row['t_kohirspt'] . '</td>
                            </tr>
                            <tr>
                                <td>Tanggal Penerbitan</td>
                                <td style="width: 3px; text-align: center;">:</td>
                                <td>' . date('d-m-Y', strtotime($row['t_tglprosesspt'])) . '</td>
                            </tr>
                        </table>
                    </td>
                    <td>
                        Diterima tanggal, ........................ <br>
                        Oleh : <br>
                        <br><br><br>
                        ( &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;)<br>
                    </td>
                </tr>
            </table>
        </div>
        ';
            }
        }
        $html .= "</body>";
        $html .= "</html>";
        // die($html);
        return $html;
    }
}
