<?php

namespace Bphtb\Helper;

use Zend\View\Helper\AbstractHelper;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class MenuHelper extends AbstractHelper implements ServiceLocatorAwareInterface
{

    protected $tbl;

    public function __invoke()
    {
        return $this;
    }

    public function setServiceLocator(ServiceLocatorInterface $serviceLocator)
    {
        $this->serviceLocator = $serviceLocator;
        return $this;
    }

    public function getServiceLocator()
    {
        return $this->serviceLocator;
    }

    public function GetDataSyarat($id)
    {
        $data = $this->gettbl("PersyaratanTable")->getpersyaratan($id);

        return $data;
    }

    public function getfilesyarat($t_idjenistransaksi, $t_idspt)
    {
        $data = $this->gettbl("PersyaratanTable")->syaratfileupload($t_idjenistransaksi, $t_idspt);

        return $data;
    }


    public function SimpanFileUpload($idjenistransaksi, $id_detailspt, $fileupload, $keterangan_file, $id_syarat, $letak_dir, $folderidspt, $folderiddetailspt)
    {
        $data = $this->gettbl("UploadTable")->savedata_syarat($idjenistransaksi, $id_detailspt, $fileupload, $keterangan_file, $id_syarat, $letak_dir, $folderidspt, $folderiddetailspt);

        return $data;
    }


    public function gettbl($tbl_service)
    {
        $this->tbl = $this->getServiceLocator()->getServiceLocator()->get($tbl_service);
        return $this->tbl;
    }
    // =============== CETAK SPOP / LSPOP
    public function GetJmlBangunan($nopcari)
    {
        $data = $this->gettbl("SPPTTable")->cekjml_bangunan($nopcari);

        return $data;
    }

    public function GetDatFasilitasBangunan($nopcari, $nobangunan, $kdfasilitas)
    {
        $data = $this->gettbl("SPPTTable")->dat_fasilitas_bangunan($nopcari, $nobangunan, $kdfasilitas);

        return $data;
    }

    public function GetJmlFasilitas($nopcari, $nobangunan, $kdfasilitas)
    {
        $data = $this->gettbl("SPPTTable")->jml_fasilitas_bangunan($nopcari, $nobangunan, $kdfasilitas);

        return $data;
    }

    public function GetDatJpb2($nopcari, $nobangunan)
    {
        $data = $this->gettbl("SPPTTable")->dat_jpb2($nopcari, $nobangunan);

        return $data;
    }

    public function GetDatJpb3($nopcari, $nobangunan)
    {
        $data = $this->gettbl("SPPTTable")->dat_jpb3($nopcari, $nobangunan);

        return $data;
    }

    public function GetDatJpb4($nopcari, $nobangunan)
    {
        $data = $this->gettbl("SPPTTable")->dat_jpb4($nopcari, $nobangunan);

        return $data;
    }

    public function GetDatJpb5($nopcari, $nobangunan)
    {
        $data = $this->gettbl("SPPTTable")->dat_jpb5($nopcari, $nobangunan);

        return $data;
    }

    public function GetDatJpb6($nopcari, $nobangunan)
    {
        $data = $this->gettbl("SPPTTable")->dat_jpb6($nopcari, $nobangunan);

        return $data;
    }

    public function GetDatJpb7($nopcari, $nobangunan)
    {
        $data = $this->gettbl("SPPTTable")->dat_jpb7($nopcari, $nobangunan);

        return $data;
    }

    public function GetDatJpb8($nopcari, $nobangunan)
    {
        $data = $this->gettbl("SPPTTable")->dat_jpb8($nopcari, $nobangunan);

        return $data;
    }

    public function GetDatJpb9($nopcari, $nobangunan)
    {
        $data = $this->gettbl("SPPTTable")->dat_jpb9($nopcari, $nobangunan);

        return $data;
    }

    public function GetDatJpb12($nopcari, $nobangunan)
    {
        $data = $this->gettbl("SPPTTable")->dat_jpb12($nopcari, $nobangunan);

        return $data;
    }

    public function GetDatJpb13($nopcari, $nobangunan)
    {
        $data = $this->gettbl("SPPTTable")->dat_jpb13($nopcari, $nobangunan);

        return $data;
    }

    public function GetDatJpb14($nopcari, $nobangunan)
    {
        $data = $this->gettbl("SPPTTable")->dat_jpb14($nopcari, $nobangunan);

        return $data;
    }

    public function GetDatJpb15($nopcari, $nobangunan)
    {
        $data = $this->gettbl("SPPTTable")->dat_jpb15($nopcari, $nobangunan);

        return $data;
    }

    public function GetDatJpb16($nopcari, $nobangunan)
    {
        $data = $this->gettbl("SPPTTable")->dat_jpb16($nopcari, $nobangunan);

        return $data;
    }

    // =============== END CETAK SPOP / LSPOP
    public function kekata($x)
    {
        $x = abs($x);
        $angka = array(
            "", "Satu", "Dua", "Tiga", "Empat", "Lima",
            "Enam", "Tujuh", "Delapan", "Sembilan", "Sepuluh", "Sebelas"
        );
        $temp = "";
        if ($x < 12) {
            $temp = " " . $angka[$x];
        } else if ($x < 20) {
            $temp = $this->kekata($x - 10) . " Belas";
        } else if ($x < 100) {
            $temp = $this->kekata($x / 10) . " Puluh" . $this->kekata($x % 10);
        } else if ($x < 200) {
            $temp = " Seratus" . $this->kekata($x - 100);
        } else if ($x < 1000) {
            $temp = $this->kekata($x / 100) . " Ratus" . $this->kekata($x % 100);
        } else if ($x < 2000) {
            $temp = " Seribu" . $this->kekata($x - 1000);
        } else if ($x < 1000000) {
            $temp = $this->kekata($x / 1000) . " Ribu" . $this->kekata($x % 1000);
        } else if ($x < 1000000000) {
            $temp = $this->kekata($x / 1000000) . " Juta" . $this->kekata($x % 1000000);
        } else if ($x < 1000000000000) {
            $temp = $this->kekata($x / 1000000000) . " Milyar" . $this->kekata(fmod($x, 1000000000));
        } else if ($x < 1000000000000000) {
            $temp = $this->kekata($x / 1000000000000) . " Trilyun" . $this->kekata(fmod($x, 1000000000000));
        }
        return $temp;
    }
    public function terbilang($x, $style = 4)
    {
        if ($x < 0) {
            $hasil = "MINUS " . trim($this->kekata($x));
        } else {
            $hasil = trim($this->kekata($x));
        }
        switch ($style) {
            case 1:
                $hasil = strtoupper($hasil);
                break;
            case 2:
                $hasil = strtolower($hasil);
                break;
            case 3:
                $hasil = ucwords($hasil);
                break;
            default:
                $hasil = ucfirst($hasil);
                break;
        }
        return $hasil;
    }

    # $date:date string to be formatted
    # $type:format output
    # $type: 0 (dd-mm-yyyy) ex: 20-04-2016
    #        1 (dd MM yyyy) ex: 20 April 2016
    #        2 (MM)         ex: April
    public function dateFormat($date, $type)
    {

        $bulan = [null, "Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"];
        $tgl = explode('-', $date);
        switch ($type) {
            case 0:
                $hasil = date('d-m-Y', strtotime($date));
                break;
            case 1:
                $hasil = $tgl[2] . " " . $bulan[(int)$tgl[1]] . " " . $tgl[0];
                break;
            case 2:
                $hasil = $bulan[(int)$tgl[1]];
                break;
            default:
                $hasil = $date;
                break;
        }

        return $hasil;
    }

    public function DateToIndo($date)
    {
        $BulanIndo = array(
            "Januari",
            "Februari",
            "Maret",
            "April",
            "Mei",
            "Juni",
            "Juli",
            "Agustus",
            "September",
            "Oktober",
            "November",
            "Desember"
        );
        // $bulan = substr($date, 5, 2);
        $bulan = date("m", strtotime($date));
        $result = $BulanIndo[(int) $bulan - 1];
        return ($result);
    }

    public function getNamaHari($tanggal)
    {
        $day = date('D', strtotime($tanggal));
        $dayList = array(
            'Sun' => 'Minggu',
            'Mon' => 'Senin',
            'Tue' => 'Selasa',
            'Wed' => 'Rabu',
            'Thu' => 'Kamis',
            'Fri' => 'Jumat',
            'Sat' => 'Sabtu'
        );
        return $dayList[$day];
    }

    public function getDataPejabat()
    {
        return $this->gettbl("PejabatBphtbTable")->getdata();
    }
}
