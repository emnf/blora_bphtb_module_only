<?php

return array(
    'controllers' => array(
        'invokables' => array(
            'BPHTBINDEX' => 'Bphtb\Controller\MainController',
            'LoginAccess' => 'Bphtb\Controller\LoginAccess',
            //Modul Setting
            'SettingPemda' => 'Bphtb\Controller\Setting\SettingPemda',
            'SettingKecamatan' => 'Bphtb\Controller\Setting\SettingKecamatan',
            'SettingKelurahan' => 'Bphtb\Controller\Setting\SettingKelurahan',
            'SettingPejabat' => 'Bphtb\Controller\Setting\SettingPejabat',
            'SettingNotaris' => 'Bphtb\Controller\Setting\SettingNotaris',
            'SettingJenisTransaksi' => 'Bphtb\Controller\Setting\SettingJenisTransaksi',
            'SettingTarifNpotkp' => 'Bphtb\Controller\Setting\SettingTarifNpotkp',
            'SettingHakTanah' => 'Bphtb\Controller\Setting\SettingHakTanah',
            'SettingJenisKetetapan' => 'Bphtb\Controller\Setting\SettingJenisKetetapan',
            'SettingTarifBphtb' => 'Bphtb\Controller\Setting\SettingTarifBphtb',
            'SettingUser' => 'Bphtb\Controller\Setting\SettingUser',
            'SettingPersyaratanTransaksi' => 'Bphtb\Controller\Setting\SettingPersyaratanTransaksi',
            'SettingDokTanah' => 'Bphtb\Controller\Setting\SettingDokTanah',
            'Pengurangan' => 'Bphtb\Controller\Pengurangan\Pengurangan',
            'SettingHargaAcuan' => 'Bphtb\Controller\Setting\SettingHargaAcuan',
            //Modul Pendataan
            'PendataanSSPD' => 'Bphtb\Controller\Pendataan\PendataanSSPD',
            //Modul Pembayaran
            'PembayaranSPT' => 'Bphtb\Controller\Pembayaran\PembayaranSPT',
            //Modul Verifikasi Berkas
            'VerifikasiBerkas' => 'Bphtb\Controller\Verifikasiberkas\VerifikasiBerkas',
            //Modul Verifikasi
            'VerifikasiSPT' => 'Bphtb\Controller\Verifikasi\VerifikasiSPT',
            //Modul Pencetakan
            'CetakLaporan' => 'Bphtb\Controller\Pencetakan\CetakLaporan',
            //Modul BPN
            'Bpn' => 'Bphtb\Controller\BPN\Bpn',
            'BpnApi' => 'Bphtb\Controller\BPN\BpnApi',
            //Modul Informasi OP
            'Informasiop' => 'Bphtb\Controller\Informasiop\Informasiop',
            //Modul Pelaporan Notaris
            'PelaporanNotaris' => 'Bphtb\Controller\Pelaporan\PelaporanNotaris',
            //Modul Monitoring
            'Monitoring' => 'Bphtb\Controller\Monitoring\Monitoring',
            'PendataanSismiop' => 'Bphtb\Controller\Pendataan\PendataanPbb',
            'KppPratama' => 'Bphtb\Controller\Kpppratama\KppPratama',

            'Penandatanganan' => 'Bphtb\Controller\Penandatanganan\Penandatanganan',
            'ParafPenandatanganan' => 'Bphtb\Controller\Penandatanganan\ParafPenandatanganan',
            'Dokumen' => 'Bphtb\Controller\Dokumen\Dokumen',
            'SettingFormatPesan' => 'Bphtb\Controller\Setting\SettingFormatPesan',
            'SettingButton' => 'Bphtb\Controller\Setting\SettingButton',

            // API
            'ApiController' => 'Bphtb\Controller\Api\ApiController'
            // API
        ),
    ),
    'controller_plugins' => array(
        'invokables' => array(
            'ControllerHelper' => 'Bphtb\Helper\ControllerHelper',
            'Esign' => 'Bphtb\Helper\Esign',
            'CetakSSPD' => 'Bphtb\Helper\CetakSSPD',
            'UploadHelper' => 'Bphtb\Helper\UploadHelper',

            // API
            'ApiLoginHelper' => 'Bphtb\Helper\Api\ApiLoginHelper',
            // API
        ),
    ),
    'router' => array(
        'routes' => array(
            'sign_in' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/sign_in[/:action][/:id]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id' => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'LoginAccess',
                        'action' => 'index',
                    ),
                ),
            ),
            'setting_persyaratan_transaksi_bphtb' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/setting_persyaratan_transaksi_bphtb[/:action][/:page][/:rows][/:direction][/:s_namapersyaratan][/:s_namajenistransaksi]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id' => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'SettingPersyaratanTransaksi',
                        'action' => 'index',
                    ),
                ),
            ),
            'setting_user_bphtb' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/setting_user_bphtb[/:action][/:page][/:rows][/:direction][/:s_username][/:s_jabatan]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id' => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'SettingUser',
                        'action' => 'index',
                    ),
                ),
            ),
            'setting_tarif_bphtb' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/setting_tarif_bphtb[/:action][/:page][/:rows][/:direction][/:s_tarifbphtb][/:s_tanggaltarifbphtb][/:s_dasarhukumtarifbphtb][/:s_statustarifbphtb]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id' => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'SettingTarifBphtb',
                        'action' => 'index',
                    ),
                ),
            ),
            'setting_jenis_ketetapan' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/setting_jenis_ketetapan[/:action][/:page][/:rows][/:direction]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id' => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'SettingJenisKetetapan',
                        'action' => 'index',
                    ),
                ),
            ),
            'setting_hak_tanah' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/setting_hak_tanah[/:action][/:page][/:rows][/:direction][/:s_kodehaktanah][/:s_namahaktanah]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id' => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'SettingHakTanah',
                        'action' => 'index',
                    ),
                ),
            ),
            'setting_tarif_npotkp' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/setting_tarif_npotkp[/:action][/:page][/:rows][/:direction][/:s_kodejenistransaksi][/:s_namajenistransaksi][/:s_tarifnpotkp][/:s_tarifnpotkptahunajb1]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id' => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'SettingTarifNpotkp',
                        'action' => 'index',
                    ),
                ),
            ),
            'setting_jenis_transaksi_bphtb' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/setting_jenis_transaksi_bphtb[/:action][/:page][/:rows][/:direction][/:s_kodejenistransaksi][/:s_namajenistransaksi]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id' => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'SettingJenisTransaksi',
                        'action' => 'index',
                    ),
                ),
            ),
            'setting_notaris_bphtb' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/setting_notaris_bphtb[/:action][/:page][/:rows][/:direction][/:s_namanotaris][/:s_kodenotaris][/:s_alamatnotaris]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id' => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'SettingNotaris',
                        'action' => 'index',
                    ),
                ),
            ),
            'setting_pejabat_bphtb' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/setting_pejabat_bphtb[/:action][/:page][/:rows][/:direction][/:s_namapejabat][/:s_jabatanpejabat][/:s_nippejabat]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id' => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'SettingPejabat',
                        'action' => 'index',
                    ),
                ),
            ),
            'setting_kelurahan_bphtb' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/setting_kelurahan_bphtb[/:action][/:page][/:rows][/:direction]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id' => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'SettingKelurahan',
                        'action' => 'index',
                    ),
                ),
            ),
            'setting_kecamatan_bphtb' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/setting_kecamatan_bphtb[/:action][/:page][/:rows][/:direction]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id' => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'SettingKecamatan',
                        'action' => 'index',
                    ),
                ),
            ),
            'setting_pemda' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/setting_pemda[/:action][/:page][/:rows][/:direction]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id' => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'SettingPemda',
                        'action' => 'index',
                    ),
                ),
            ),
            'setting_dokumen_tanah_bphtb' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/setting_dokumen_tanah_bphtb[/:action][/:page][/:rows][/:direction][/:s_namadoktanah]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id' => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'SettingDokTanah',
                        'action' => 'index',
                    ),
                ),
            ),
            'setting_harga_acuan_bphtb' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/setting_harga_acuan_bphtb[/:action][/:page][/:rows][/:direction][/:s_kd_propinsi][/:s_kd_dati2][/:s_kd_kecamatan][/:s_kd_kelurahan][/:s_kd_blok][/:s_permetertanah]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id' => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'SettingHargaAcuan',
                        'action' => 'index',
                    ),
                ),
            ),
            'pendataan_sspd' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/pendataan_sspd[/:action][/:page][/:rows][/:direction][/:t_kohirspt][/:t_periodespt][/:t_tglprosesspt][/:t_namawppembeli][/:t_totalspt][/:s_namajenistransaksi][/:t_statusbayarspt]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id' => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'PendataanSSPD',
                        'action' => 'index',
                    ),
                ),
            ),
            'pendataan_pbb' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/pendataan_pbb[/:action][/:page][/:rows][/:direction][/:t_kohirspt][/:t_periodespt][/:t_tglprosesspt][/:t_namawppembeli][/:t_totalspt][/:s_namajenistransaksi][/:t_noajbbaru][/:t_tglajbbaru]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id' => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'PendataanSismiop',
                        'action' => 'index',
                    ),
                ),
            ),
            'pembayaran_sptbphtb' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/pembayaran_sptbphtb[/:action][/:page][/:rows][/:direction][/:t_kohirspt][/:t_periodepembayaran][/:t_tanggalpembayaran][/:t_namawppembeli][/:t_kodebayarbanksppt][/:t_statusbayarspt][/:t_nilaipembayaranspt]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id' => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'PembayaranSPT',
                        'action' => 'index',
                    ),
                ),
            ),
            'kpppratama' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/kpppratama[/:action][/:page][/:rows][/:direction][/:t_kohirspt][/:t_periodepembayaran][/:t_tanggalpembayaran][/:t_namawppembeli][/:t_kodebayarbanksppt][/:t_statusbayarspt][/:t_nilaipembayaranspt]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id' => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'KppPratama',
                        'action' => 'index',
                    ),
                ),
            ),
            'verifikasi_berkas' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/verifikasi_berkas[/:action][/:page][/:rows][/:direction][/:t_kohirspt][/:t_periodespt][/:t_tglverifikasispt][/:t_namawppembeli][/:s_namajenistransaksi][/:t_kodebayarbanksppt][/:t_statusbayarspt]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id' => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'VerifikasiBerkas',
                        'action' => 'index',
                    ),
                ),
            ),
            'verifikasi_spt' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/verifikasi_spt[/:action][/:page][/:rows][/:direction][/:t_kohirspt][/:t_periodespt][/:t_tglverifikasispt][/:t_namawppembeli][/:s_namajenistransaksi][/:t_kodebayarbanksppt][/:t_statusbayarspt]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id' => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'VerifikasiSPT',
                        'action' => 'index',
                    ),
                ),
            ),
            'pengurangan' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/pengurangan[/:action][/:page][/:rows][/:direction][/:t_kohirspt][/:t_periodespt][/:t_tglprosesspt][/:t_namawppembeli][/:t_totalspt][/:s_namajenistransaksi][/:t_statusbayarspt]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id' => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'Pengurangan',
                        'action' => 'index',
                    ),
                ),
            ),
            'cetak_laporan' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/cetak_laporan[/:action][/:page][/:rows][/:direction]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id' => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'CetakLaporan',
                        'action' => 'index',
                    ),
                ),
            ),
            'monitoring' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/monitoring[/:action]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id' => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'Monitoring',
                        'action' => 'index',
                    ),
                ),
            ),
            'bpn_lihatdata' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/bpn_lihatdata[/:action][/:page][/:rows][/:direction][/:t_kohirspt][/:t_periodepembayaran][/:t_tglprosesspt][/:t_namawppembeli][/:t_tglverifikasispt][/:t_tanggalpembayaran][/:t_luastanah][/:t_luasbangunan][/:t_luastanahbpn][/:t_luasbangunanbpn][/:t_nosertifikatbaru]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id' => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'Bpn',
                        'action' => 'index',
                    ),
                ),
            ),
            'informasiop' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/informasiop[/:action][/:page][/:rows][/:direction]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id' => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'Informasiop',
                        'action' => 'index',
                    ),
                ),
            ),
            'pelaporan_notaris' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/pelaporan_notaris[/:action][/:page][/:rows][/:direction][/:t_kohirspt][/:t_tglprosesspt][/:t_namawppembeli][/:t_totalspt][/:t_bulanselesaibayar][/:t_tanggalpembayaran][/:t_noajbbaru][/:t_tglajbbaru][/:t_idnotarisspt]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id' => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'PelaporanNotaris',
                        'action' => 'index',
                    ),
                ),
            ),
            'main_bphtb' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/main_bphtb[/:action][/:id]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id' => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'BPHTBINDEX',
                        'action' => 'index',
                    ),
                ),
            ),
            'bpnapi' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/bpnapi[/:action][/:id]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id' => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'BpnApi',
                        'action' => 'index',
                    ),
                ),
            ),
            'penandatanganan' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/penandatanganan[/:action][/:id]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        // 'id' => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'Penandatanganan',
                        'action' => 'index',
                    ),
                ),
            ),
            'dokumen' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/dokumen[/:action][/:id]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        // 'id' => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'Dokumen',
                        'action' => 'index',
                    ),
                ),
            ),
            'paraf_penandatanganan' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/paraf_penandatanganan[/:action][/:id]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id' => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'ParafPenandatanganan',
                        'action' => 'index',
                    ),
                ),
            ),
            'setting_format_pesan' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/setting_format_pesan[/:action][/:id]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id' => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'SettingFormatPesan',
                        'action' => 'index',
                    ),
                ),
            ),
            'setting_format_pesan' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/setting_format_pesan[/:action][/:id]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id' => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'SettingFormatPesan',
                        'action' => 'index',
                    ),
                ),
            ),
            'setting_button' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/setting_button[/:action][/:id]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id' => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'SettingButton',
                        'action' => 'index',
                    ),
                ),
            ),

            // API
            'api' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/api[/:action][/:id]',
                    // 'constraints' => array(
                    //     'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    //     'id' => '[0-9]+',
                    // ),
                    'defaults' => array(
                        'controller' => 'ApiController',
                        'action' => 'index',
                    ),
                ),
            ),

            // API
        ),
    ),
    'view_manager' => array(
        'template_map' => array(
            'layout/layout' => __DIR__ . '/../view/layout/layout.phtml',
            'error/404' => __DIR__ . '/../view/error/404.phtml',
            'error/index' => __DIR__ . '/../view/error/index.phtml',
            'layout/informasi' => __DIR__ . '/../view/layout/layout-informasi.phtml',
        ),
        'template_path_stack' => array(
            'bphtb' => __DIR__ . '/../view',
        ),
    ),
);
