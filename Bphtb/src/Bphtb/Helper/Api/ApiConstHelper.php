<?php

namespace Bphtb\Helper\Api;

class ApiConstHelper
{
    const METHOD_GET = 'GET';
    const METHOD_POST = 'POST';

    const CODE_SUCCESS = "00";
    const MESSAGE_SUCCESS = "SUCCESS";

    const CODE_ERROR = "01";
    const MESSAGE_ERROR = "ERROR";

    const CODE_USER_NOT_FOUND = "02";
    const MESSAGE_USER_NOT_FOUND = "USER NOT FOUND";
    
    const CODE_USER_AND_PASSWORD_ERROR = "03";
    const MESSAGE_USER_AND_PASSWORD_ERROR = "USER OR PASSWORD NOT FOUND";
    
    const CODE_DATA_NOT_FOUND = "04";
    const MESSAGE_DATA_NOT_FOUND = "DATA NOT FOUND";

    const TYPE_KODEBAYAR = 01;
}
