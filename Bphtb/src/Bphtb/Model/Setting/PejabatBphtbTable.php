<?php

namespace Bphtb\Model\Setting;

use Bphtb\Helper\CacheHelper;
use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Where;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Expression;

class PejabatBphtbTable extends AbstractTableGateway
{

    protected $table = 's_pejabat';
    protected $tableppob = 't_detailsptbphtb';
    public $tablewp = 't_spt';

    public function __construct(Adapter $adapter)
    {
        $this->adapter = $adapter;
        $this->resultSetPrototype = new ResultSet();
        $this->resultSetPrototype->setArrayObjectPrototype(new PejabatBphtbBase());
        $this->initialize();
    }

    public function getdata()
    {
        $key = 'PejabatBphtbTable-getdata';

        $cache = (new CacheHelper)->cache();

        if (!$cache->hasItem($key)) {
            $sql = new Sql($this->adapter);
            $select = $sql->select();
            $select->from($this->table);
            $select->order(new Expression("(
                CASE WHEN s_idpejabat = 12 THEN 1 ELSE 2 END
            )"));
            $state = $sql->prepareStatementForSqlObject($select);
            $res = $state->execute();
            // return $res;

            $arr = [];
            foreach ($res as $key => $value) {
                $arr[] = $value;
            }
            // return $arr;
            $cache->setItem($key, $arr);
        }

        return $cache->getItem($key);
    }

    public function getdatappob()
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from($this->tableppob);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res;
    }

    public function getdatawp()
    {
        $sql = "select * from t_spt where t_kohirspt='1' ";
        $statement = $this->adapter->query($sql);
        return $statement->execute();
    }

    public function getdataNotaris()
    {
        $key = 'PejabatBphtbTable-getdataNotaris';
        $cache = (new CacheHelper)->cache();

        if (!$cache->hasItem($key)) {
            $sql = "select a.*,b.* from s_users a 
            left join s_notaris b ON b.s_idnotaris::text = a.s_idpejabat_idnotaris::text
            where s_akses=3";
            $statement = $this->adapter->query($sql);
            // return $statement->execute();
            $resultSet = new ResultSet();
            $resultSet->initialize($statement->execute());

            $cache->setItem($key, $resultSet->toArray());
        }

        return $cache->getItem($key);
    }

    public function getdataKecamatan()
    {
        $key = 'PejabatBphtbTable-getdataKecamatan';
        $cache = (new CacheHelper)->cache();

        if (!$cache->hasItem($key)) {
            $sql = "select * from s_kecamatan order by s_idkecamatan";
            $statement = $this->adapter->query($sql);
            // return $statement->execute();
            $resultSet = new ResultSet();
            $resultSet->initialize($statement->execute());

            $cache->setItem($key, $resultSet->toArray());
        }

        return $cache->getItem($key);
    }

    public function getdataKelurahan()
    {
        $key = 'PejabatBphtbTable-getdataKelurahan';
        $cache = (new CacheHelper)->cache();

        if (!$cache->hasItem($key)) {
            $sql = "select * from s_kelurahan order by s_idkelurahan";
            $statement = $this->adapter->query($sql);
            // return $statement->execute();
            $resultSet = new ResultSet();
            $resultSet->initialize($statement->execute());

            $cache->setItem($key, $resultSet->toArray());
        }

        return $cache->getItem($key);
    }

    public function getdataid($pj)
    {
        $rowset = $this->select(array('s_idpejabat' => $pj));
        $row = $rowset->current();
        return $row;
    }

    public function getdataidvalidasi($pj1)
    {
        $rowset = $this->select(array('s_idpejabat' => $pj1));
        $row = $rowset->current();
        return $row;
    }

    public function checkExist(PejabatBphtbBase $pj)
    {
        $rowset = $this->select(array('s_nippejabat' => $pj->s_nippejabat));
        $row = $rowset->current();
        return $row;
    }

    public function checkId($id)
    {
        $rowset = $this->select(array('s_idpejabat' => $id));
        $row = $rowset->current();
        return $row;
    }

    public function savedata(PejabatBphtbBase $pj)
    {
        if (empty($pj->s_golonganpejabat)) {
            $pj->s_golonganpejabat = NULL;
        }
        $data = array(
            's_namapejabat' => $pj->s_namapejabat,
            's_nippejabat' => $pj->s_nippejabat,
            's_jabatanpejabat' => $pj->s_jabatanpejabat,
            's_golonganpejabat' => $pj->s_golonganpejabat,
            's_nik' => $pj->s_nik,
        );
        $id = (int) $pj->s_idpejabat;
        if ($id == 0) {
            $this->insert($data);
        } else {
            if ($this->checkId($id)) {
                $this->update($data, array('s_idpejabat' => $pj->s_idpejabat));
            } else {
                throw new \Exception('Form id does not exist');
            }
        }
    }

    public function comboBoxGolongan()
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from('ss_golonganpejabat');
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res;
    }

    public function getGridCount(PejabatBphtbBase $base)
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from($this->table);
        $where = new Where();
        if ($base->s_namapejabat != 'undefined')
            $where->literal("$this->table.s_namapejabat::text LIKE '%$base->s_namapejabat%'");
        if ($base->s_jabatanpejabat != 'undefined')
            $where->literal("$this->table.s_jabatanpejabat::text LIKE '%$base->s_jabatanpejabat%'");
        if ($base->s_nippejabat != 'undefined')
            $where->literal("$this->table.s_nippejabat::text LIKE '%$base->s_nippejabat%'");
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res->count();
    }

    public function getGridData(PejabatBphtbBase $base, $offset)
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from($this->table);
        $where = new \Zend\Db\Sql\Where();
        if ($base->s_namapejabat != 'undefined')
            $where->literal("$this->table.s_namapejabat::text LIKE '%$base->s_namapejabat%'");
        if ($base->s_jabatanpejabat != 'undefined')
            $where->literal("$this->table.s_jabatanpejabat::text LIKE '%$base->s_jabatanpejabat%'");
        if ($base->s_nippejabat != 'undefined')
            $where->literal("$this->table.s_nippejabat::text LIKE '%$base->s_nippejabat%'");
        $select->where($where);
        $select->limit($base->rows = (int) $base->rows);
        $select->offset($offset = (int) $offset);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res;
    }

    public function hapusData($pb)
    {
        $this->delete(array('s_idpejabat' => $pb));
    }

    public function getallid($id)
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from($this->table)
            ->join('ss_golonganpejabat', 'ss_golonganpejabat.ss_idgolongan = s_pejabat.s_golonganpejabat');
        $where = new Where();
        $where->equalTo("$this->table.s_idpejabat", $id);
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute()->current();
        return $res;
    }

    public function getDataPejabatInArray($array)
    {
        // var_dump(implode("", $array));exit();
        $sql = new Sql($this->adapter);
        $select = $sql->select($this->table);
        $where = new Where();
        if (implode("", $array) == "") {
            $where->equalTo("s_idpejabat", 0);
        } else {
            $where->in("s_idpejabat", $array);
        }
        $select->where($where);
        // die($sql->getSqlStringForSqlObject($select));
        $res = $sql->prepareStatementForSqlObject($select)->execute();
        return $res;
    }
}
