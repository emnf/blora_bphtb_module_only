<?php

namespace Bphtb\Model\Espop;

use Bphtb\Helper\Spop\ReffSpopHelper;
use Bphtb\Helper\Spop\TipeEspopHelper;
use Zend\Db\Adapter\Adapter;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Where;
use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Debug\Debug;
use Zend\Math\Rand;

class EspopTable extends AbstractTableGateway
{
    protected $table = "";
    protected $adapter;
    protected $adapter2;
    protected $adapter3;

    public static function generateUUID()
    {
        $uuid = sprintf(
            '%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
            Rand::getInteger(0, 0xffff),
            Rand::getInteger(0, 0xffff),
            Rand::getInteger(0, 0xffff),
            Rand::getInteger(0, 0x0fff) | 0x4000,
            Rand::getInteger(0, 0x3fff) | 0x8000,
            Rand::getInteger(0, 0xffff),
            Rand::getInteger(0, 0xffff),
            Rand::getInteger(0, 0xffff)
        );

        return $uuid;
    }

    public function __construct(Adapter $adapter, $adapter2) //Adapter $adapter2, Adapter $adapter3, Adapter $adapter
    {
        $this->adapter = $adapter; //ESPOP
        $this->adapter2 = $adapter2; //BPHTB
        // $this->adapter3 = $adapter3; //PBB
        // $this->adapter = $adapter; //ESPOP-PG
    }

    public function getComboUserEspop()
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select('S_USERS');

        $res = $sql->prepareStatementForSqlObject($select)->execute();

        $combo = [];
        foreach ($res as $val) {
            // var_dump($val);exit();
            $combo[$val['S_IDUSER']] = $val['S_USERNAME'] . ' | ' . $val['S_NAMA'];
        }

        return $combo;
    }

    public function getPersyaratan($idJenisTransaksi)
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select('S_PERSYARATAN');
        $select->columns(['PERSYARATAN' => new Expression("LISTAGG(S_ID_PERSYARATAN, ',') WITHIN GROUP (ORDER BY S_ID_PERSYARATAN)")]);
        $select->where('S_ID_JENIS_PELAYANAN = ' . $idJenisTransaksi);
        $select->group('S_ID_JENIS_PELAYANAN');
        $res = $sql->prepareStatementForSqlObject($select)->execute()->current();
        return $res;
    }

    public function simpanPermohonan($arr, $session, $tipeEspop, $dataSpop)
    {
        $noPermohonan = $this->getNoPermohonan(date('Y'));
        $noUrutPelayanan = $this->getnoUrutPelayanan(date('Y'));

        $jenisPelayanan = $tipeEspop == TipeEspopHelper::BALIK_NAMA ? ReffSpopHelper::PELAYANAN_MUTASI_PENUH : ReffSpopHelper::PELAYANAN_MUTASI_SEBAGIAN;
        // debug::dump($jenisPelayanan);
        // exit;
        $data = [
            't_tgl_pelayanan' => date('Y-m-d'),
            't_id_jenis_pelayanan' => $jenisPelayanan,
            't_nop' => $tipeEspop == TipeEspopHelper::BALIK_NAMA ? str_replace('.', '', $arr['t_nopbphtbsppt']) : null,
            't_nama_pemohon' => strtoupper(substr($arr['t_namawppembeli'], 0, 50)),
            't_nik_pemohon' => $arr['t_nikwppembeli'],
            't_jalan_pemohon' => substr(strtoupper($arr['t_alamatwppembeli']), 0, 30),
            't_rt_pemohon' => strtoupper($arr['t_rtwppembeli']),
            't_rw_pemohon' => strtoupper($arr['t_rwwppembeli']),
            't_kelurahan_pemohon' => strtoupper($arr['t_kelurahanwppembeli']),
            't_kecamatan_pemohon' => strtoupper($arr['t_kecamatanwppembeli']),
            't_kabupaten_pemohon' => strtoupper($arr['t_kabkotawppembeli']),
            't_kode_pos_pemohon' => $arr['t_kodeposwppembeli'],
            't_no_hp_pemohon' => $arr['t_telponwppembeli'],
            't_email_pemohon' => $arr['t_emailwppembeli'],
            't_keterangan' => 'DARI BPHTB NO DAFTAR ' . $arr['t_kohirspt'],
            't_id_jenis_pajak' => 10,
            'created_by' => 1,
            'created_at' => date('Y-m-d H:i:s'),
            'uuid' => $this->generateUUID(),
            't_id_spt_bphtb' => $arr['t_idspt'],
            't_kode_bayar_bphtb' => $arr['t_kodebayarbanksppt'],
            't_redirect_validasi_by' => $session['s_id_user_espop'],
            't_redirect_validasi_tgl' => date('Y-m-d H:i:s'),
            't_tipe_dari_bphtb' => $tipeEspop == TipeEspopHelper::BALIK_NAMA ? 2 : 1,
            't_nomor_permohonan' => $noPermohonan,
            't_no_pelayanan' => $noUrutPelayanan
        ];

        $sql = new Sql($this->adapter);

        $insertPermohonan = 0;
        while ($insertPermohonan > 0) {
            $check = $this->checkNomorPermohonanExists($data['t_nomor_permohonan']);
            if ($check == null) {
                $insertPermohonan = 1;
            } else {
                $noBaru = $data['t_nomor_permohonan'] + 1;
                $data['t_nomor_permohonan'] = $noBaru;
            }
        }
        $permohonan = $this->getPermohonanByKodeBayar($arr['t_kodebayarbanksppt']);

        if ($permohonan) {
            $insert = $sql->update('t_pelayanan')->set($data)->where(['t_kode_bayar_bphtb' => $arr['t_kodebayarbanksppt']]);
            $sql->prepareStatementForSqlObject($insert)->execute();
        } else {
            $insert = $sql->insert('t_pelayanan')->values($data);
            $sql->prepareStatementForSqlObject($insert)->execute();
        }


        $approve = [
            'created_by' => 1,
            't_tgl_perkiraan_selesai' => date('Y-m-d', strtotime('+ 7 days'))
        ];

        $sql2 = new Sql($this->adapter);
        $permohonan = $this->getPermohonanByKodeBayar($arr['t_kodebayarbanksppt']);
        $getVerApp = $this->getVerApp($permohonan['t_id_pelayanan']);


        $approve['t_id_pelayanan'] = $permohonan['t_id_pelayanan'];
        if ($getVerApp) {
            $insertApprove = $sql->update('t_pelayanan_approve')->set($data)->where(['t_id_pelayanan' => $permohonan['t_id_pelayanan']]);
            $sql->prepareStatementForSqlObject($insert)->execute();
        } else {
            $insertApprove = $sql2->insert('t_pelayanan_approve')->values($approve);
            $res = $sql2->prepareStatementForSqlObject($insertApprove)->execute();
        }
        // $insertApprove = $sql2->insert('t_pelayanan_approve')->values($approve);
        // $res = $sql2->prepareStatementForSqlObject($insertApprove)->execute();

        return $permohonan['t_id_pelayanan'];
    }

    public function getNoPermohonan($tahun)
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select('t_pelayanan');
        // $select->where("extract(YEAR from t_tgl_pelayanan = '" . $tahun . "'");
        $select->where("t_tahun_pajak = '" . $tahun . "'");
        $select->columns([
            'NO' => new Expression("MAX(t_nomor_permohonan::int)")
        ]);
        $res = $sql->prepareStatementForSqlObject($select)->execute()->current();
        // var_dump($res);
        // exit();
        return (int) $res['NO'] + 1;
    }

    public function getnoUrutPelayanan($tahun)
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select('t_pelayanan');
        $select->where("t_tahun_pajak = '" . $tahun . "'");
        $select->columns([
            'NO' => new Expression("MAX(t_no_pelayanan::int)")
        ]);
        $res = $sql->prepareStatementForSqlObject($select)->execute()->current();

        return (int) $res['NO'] + 1;
    }

    public function nomorPermohonan($tahun, $jenis, $nomor)
    {
        return $tahun . '.'
            . str_pad($jenis, 3, '0', STR_PAD_LEFT) . '.' . str_pad($nomor, 7, '0', STR_PAD_LEFT);
    }

    public function checkNomorPermohonanExists($noPermohonan)
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select('t_pelayanan')->where("t_nomor_permohonan = '" . $noPermohonan . "'");
        $res = $sql->prepareStatementForSqlObject($select)->execute();
        return $res->current();
    }

    public function getDatObjekPajak($nop)
    {

        $ex = explode('.', $nop);
        // var_dump($ex);exit();

        $sql = new Sql($this->adapter2);
        $select = $sql->select('DAT_OBJEK_PAJAK');
        $where = new Where();
        $where->equalTo('KD_PROPINSI', $ex[0]);
        $where->equalTo('KD_DATI2', $ex[1]);
        $where->equalTo('KD_KECAMATAN', $ex[2]);
        $where->equalTo('KD_KELURAHAN', $ex[3]);
        $where->equalTo('KD_BLOK', $ex[4]);
        $where->equalTo('NO_URUT', $ex[5]);
        $where->equalTo('KD_JNS_OP', $ex[6]);
        $select->where($where);

        $res = $sql->prepareStatementForSqlObject($select)->execute();
        return $res->current();
    }

    public function getDatOpBumi($nop)
    {

        $ex = explode('.', $nop);
        // var_dump($ex);exit();

        $sql = new Sql($this->adapter2);
        $select = $sql->select('DAT_OP_BUMI');
        $where = new Where();
        $where->equalTo('KD_PROPINSI', $ex[0]);
        $where->equalTo('KD_DATI2', $ex[1]);
        $where->equalTo('KD_KECAMATAN', $ex[2]);
        $where->equalTo('KD_KELURAHAN', $ex[3]);
        $where->equalTo('KD_BLOK', $ex[4]);
        $where->equalTo('NO_URUT', $ex[5]);
        $where->equalTo('KD_JNS_OP', $ex[6]);
        $select->where($where);

        $res = $sql->prepareStatementForSqlObject($select)->execute();
        return $res->current();
    }

    public function getDatOpBangunan($nop)
    {

        $ex = explode('.', $nop);

        $sql = new Sql($this->adapter2);
        $select = $sql->select('DAT_OP_BANGUNAN');
        $select->columns([
            '*',
            'LISTRIK' => new Expression("(SELECT JML_SATUAN FROM DAT_FASILITAS_BANGUNAN WHERE 
            KD_PROPINSI = DAT_OP_BANGUNAN.KD_PROPINSI AND 
            KD_DATI2 = DAT_OP_BANGUNAN.KD_DATI2 AND 
            KD_KECAMATAN = DAT_OP_BANGUNAN.KD_KECAMATAN AND 
            KD_KELURAHAN = DAT_OP_BANGUNAN.KD_KELURAHAN AND 
            KD_BLOK = DAT_OP_BANGUNAN.KD_BLOK AND 
            NO_URUT = DAT_OP_BANGUNAN.NO_URUT AND 
            KD_JNS_OP = DAT_OP_BANGUNAN.KD_JNS_OP AND 
            NO_BNG = DAT_OP_BANGUNAN.NO_BNG AND 
            KD_FASILITAS = '" . ReffSpopHelper::LISTRIK . "')"),
            'AC_SPLIT' => new Expression("(SELECT JML_SATUAN FROM DAT_FASILITAS_BANGUNAN WHERE 
            KD_PROPINSI = DAT_OP_BANGUNAN.KD_PROPINSI AND 
            KD_DATI2 = DAT_OP_BANGUNAN.KD_DATI2 AND 
            KD_KECAMATAN = DAT_OP_BANGUNAN.KD_KECAMATAN AND 
            KD_KELURAHAN = DAT_OP_BANGUNAN.KD_KELURAHAN AND 
            KD_BLOK = DAT_OP_BANGUNAN.KD_BLOK AND 
            NO_URUT = DAT_OP_BANGUNAN.NO_URUT AND 
            KD_JNS_OP = DAT_OP_BANGUNAN.KD_JNS_OP AND 
            NO_BNG = DAT_OP_BANGUNAN.NO_BNG AND 
            KD_FASILITAS = '" . ReffSpopHelper::AC_SPLIT . "')"),
            'AC_WINDOW' => new Expression("(SELECT JML_SATUAN FROM DAT_FASILITAS_BANGUNAN WHERE 
            KD_PROPINSI = DAT_OP_BANGUNAN.KD_PROPINSI AND 
            KD_DATI2 = DAT_OP_BANGUNAN.KD_DATI2 AND 
            KD_KECAMATAN = DAT_OP_BANGUNAN.KD_KECAMATAN AND 
            KD_KELURAHAN = DAT_OP_BANGUNAN.KD_KELURAHAN AND 
            KD_BLOK = DAT_OP_BANGUNAN.KD_BLOK AND 
            NO_URUT = DAT_OP_BANGUNAN.NO_URUT AND 
            KD_JNS_OP = DAT_OP_BANGUNAN.KD_JNS_OP AND 
            NO_BNG = DAT_OP_BANGUNAN.NO_BNG AND 
            KD_FASILITAS = '" . ReffSpopHelper::AC_WINDOW . "')"),
            'AC_CENTRAL' => new Expression("(SELECT JML_SATUAN FROM DAT_FASILITAS_BANGUNAN WHERE 
            KD_PROPINSI = DAT_OP_BANGUNAN.KD_PROPINSI AND 
            KD_DATI2 = DAT_OP_BANGUNAN.KD_DATI2 AND 
            KD_KECAMATAN = DAT_OP_BANGUNAN.KD_KECAMATAN AND 
            KD_KELURAHAN = DAT_OP_BANGUNAN.KD_KELURAHAN AND 
            KD_BLOK = DAT_OP_BANGUNAN.KD_BLOK AND 
            NO_URUT = DAT_OP_BANGUNAN.NO_URUT AND 
            KD_JNS_OP = DAT_OP_BANGUNAN.KD_JNS_OP AND 
            NO_BNG = DAT_OP_BANGUNAN.NO_BNG AND 
            KD_FASILITAS IN ('" . ReffSpopHelper::AC_CENTRAL_APARTEMEN . "'
            ,'" . ReffSpopHelper::AC_CENTRAL_BANGUNAN_LAIN . "'
            , '" . ReffSpopHelper::AC_CENTRAL_KAMAR_HOTEL . " '
            , '" . ReffSpopHelper::AC_CENTRAL_KAMAR_RUMAH_SAKIT . " '
            , '" . ReffSpopHelper::AC_CENTRAL_KANTOR . " '
            , '" . ReffSpopHelper::AC_CENTRAL_PERTOKOAN . "' 
            ) AND ROWNUM = 1)"),
            'PEKERASAN_KONTRUKSI' => new Expression("(SELECT JML_SATUAN FROM DAT_FASILITAS_BANGUNAN WHERE 
            KD_PROPINSI = DAT_OP_BANGUNAN.KD_PROPINSI AND 
            KD_DATI2 = DAT_OP_BANGUNAN.KD_DATI2 AND 
            KD_KECAMATAN = DAT_OP_BANGUNAN.KD_KECAMATAN AND 
            KD_KELURAHAN = DAT_OP_BANGUNAN.KD_KELURAHAN AND 
            KD_BLOK = DAT_OP_BANGUNAN.KD_BLOK AND 
            NO_URUT = DAT_OP_BANGUNAN.NO_URUT AND 
            KD_JNS_OP = DAT_OP_BANGUNAN.KD_JNS_OP AND 
            NO_BNG = DAT_OP_BANGUNAN.NO_BNG AND 
            KD_FASILITAS IN ('" . ReffSpopHelper::PERKERASAN_KONSTRUKSI_RINGAN . "'
            , '" . ReffSpopHelper::PERKERASAN_KONSTRUKSI_SEDANG . "'
            , '" . ReffSpopHelper::PERKERASAN_KONSTRUKSI_BERAT . "'
            ) AND ROWNUM = 1)"),
            'PAGAR_BAJA_BESI' => new Expression("(SELECT JML_SATUAN FROM DAT_FASILITAS_BANGUNAN WHERE 
            KD_PROPINSI = DAT_OP_BANGUNAN.KD_PROPINSI AND 
            KD_DATI2 = DAT_OP_BANGUNAN.KD_DATI2 AND 
            KD_KECAMATAN = DAT_OP_BANGUNAN.KD_KECAMATAN AND 
            KD_KELURAHAN = DAT_OP_BANGUNAN.KD_KELURAHAN AND 
            KD_BLOK = DAT_OP_BANGUNAN.KD_BLOK AND 
            NO_URUT = DAT_OP_BANGUNAN.NO_URUT AND 
            KD_JNS_OP = DAT_OP_BANGUNAN.KD_JNS_OP AND 
            NO_BNG = DAT_OP_BANGUNAN.NO_BNG AND 
            KD_FASILITAS IN ('" . ReffSpopHelper::PAGAR_BAJA_BESI . "') AND ROWNUM = 1)"),
            'PAGAR_BATA_BATAKO' => new Expression("(SELECT JML_SATUAN FROM DAT_FASILITAS_BANGUNAN WHERE 
            KD_PROPINSI = DAT_OP_BANGUNAN.KD_PROPINSI AND 
            KD_DATI2 = DAT_OP_BANGUNAN.KD_DATI2 AND 
            KD_KECAMATAN = DAT_OP_BANGUNAN.KD_KECAMATAN AND 
            KD_KELURAHAN = DAT_OP_BANGUNAN.KD_KELURAHAN AND 
            KD_BLOK = DAT_OP_BANGUNAN.KD_BLOK AND 
            NO_URUT = DAT_OP_BANGUNAN.NO_URUT AND 
            KD_JNS_OP = DAT_OP_BANGUNAN.KD_JNS_OP AND 
            NO_BNG = DAT_OP_BANGUNAN.NO_BNG AND 
            KD_FASILITAS IN ('" . ReffSpopHelper::PAGAR_BATA_BATAKO . "') AND ROWNUM = 1)"),
            'KOLAM_RENANG_DENGAN_PELAPIS' => new Expression("(SELECT JML_SATUAN FROM DAT_FASILITAS_BANGUNAN WHERE 
            KD_PROPINSI = DAT_OP_BANGUNAN.KD_PROPINSI AND 
            KD_DATI2 = DAT_OP_BANGUNAN.KD_DATI2 AND 
            KD_KECAMATAN = DAT_OP_BANGUNAN.KD_KECAMATAN AND 
            KD_KELURAHAN = DAT_OP_BANGUNAN.KD_KELURAHAN AND 
            KD_BLOK = DAT_OP_BANGUNAN.KD_BLOK AND 
            NO_URUT = DAT_OP_BANGUNAN.NO_URUT AND 
            KD_JNS_OP = DAT_OP_BANGUNAN.KD_JNS_OP AND 
            NO_BNG = DAT_OP_BANGUNAN.NO_BNG AND 
            KD_FASILITAS IN ('" . ReffSpopHelper::KOLAM_RENANG_DENGAN_PELAPIS . "') AND ROWNUM = 1)"),
            'KOLAM_RENANG_DIPLESTER' => new Expression("(SELECT JML_SATUAN FROM DAT_FASILITAS_BANGUNAN WHERE 
            KD_PROPINSI = DAT_OP_BANGUNAN.KD_PROPINSI AND 
            KD_DATI2 = DAT_OP_BANGUNAN.KD_DATI2 AND 
            KD_KECAMATAN = DAT_OP_BANGUNAN.KD_KECAMATAN AND 
            KD_KELURAHAN = DAT_OP_BANGUNAN.KD_KELURAHAN AND 
            KD_BLOK = DAT_OP_BANGUNAN.KD_BLOK AND 
            NO_URUT = DAT_OP_BANGUNAN.NO_URUT AND 
            KD_JNS_OP = DAT_OP_BANGUNAN.KD_JNS_OP AND 
            NO_BNG = DAT_OP_BANGUNAN.NO_BNG AND 
            KD_FASILITAS IN ('" . ReffSpopHelper::KOLAM_RENANG_DIPLESTER . "') AND ROWNUM = 1)"),
            'SUMUR_ARTESIS' => new Expression("(SELECT JML_SATUAN FROM DAT_FASILITAS_BANGUNAN WHERE 
            KD_PROPINSI = DAT_OP_BANGUNAN.KD_PROPINSI AND 
            KD_DATI2 = DAT_OP_BANGUNAN.KD_DATI2 AND 
            KD_KECAMATAN = DAT_OP_BANGUNAN.KD_KECAMATAN AND 
            KD_KELURAHAN = DAT_OP_BANGUNAN.KD_KELURAHAN AND 
            KD_BLOK = DAT_OP_BANGUNAN.KD_BLOK AND 
            NO_URUT = DAT_OP_BANGUNAN.NO_URUT AND 
            KD_JNS_OP = DAT_OP_BANGUNAN.KD_JNS_OP AND 
            NO_BNG = DAT_OP_BANGUNAN.NO_BNG AND 
            KD_FASILITAS IN ('" . ReffSpopHelper::SUMUR_ARTESIS . "') AND ROWNUM = 1)"),

        ]);
        $where = new Where();
        $where->equalTo('KD_PROPINSI', $ex[0]);
        $where->equalTo('KD_DATI2', $ex[1]);
        $where->equalTo('KD_KECAMATAN', $ex[2]);
        $where->equalTo('KD_KELURAHAN', $ex[3]);
        $where->equalTo('KD_BLOK', $ex[4]);
        $where->equalTo('NO_URUT', $ex[5]);
        $where->equalTo('KD_JNS_OP', $ex[6]);
        $select->where($where);
        $select->order('NO_BNG');

        // die($sql->buildSqlString($select));
        $res = $sql->prepareStatementForSqlObject($select)->execute();
        // return $res;
        $resultSet = new ResultSet();
        $resultSet->initialize($res);
        return $resultSet->toArray();
    }

    public function getPermohonanByKodeBayar($kodeBayar)
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select('t_pelayanan')->where("t_kode_bayar_bphtb = '" . $kodeBayar . "'");
        $res = $sql->prepareStatementForSqlObject($select)->execute()->current();
        return $res;
    }
    public function getVerApp($idpelayanan)
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select('t_pelayanan_approve')->where("t_id_pelayanan = '" . $idpelayanan . "'");
        $res = $sql->prepareStatementForSqlObject($select)->execute()->current();
        return $res;
    }

    public function getWp($idpelayanan)
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select('t_wp')->where("t_id_pelayanan = '" . $idpelayanan . "'");
        $res = $sql->prepareStatementForSqlObject($select)->execute()->current();
        return $res;
    }

    public function getWpLama($idpelayanan)
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select('t_wp_lama')->where("t_id_pelayanan = '" . $idpelayanan . "'");
        $res = $sql->prepareStatementForSqlObject($select)->execute()->current();
        return $res;
    }

    public function getOp($idpelayanan)
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select('t_op')->where("t_id_pelayanan = '" . $idpelayanan . "'");
        $res = $sql->prepareStatementForSqlObject($select)->execute()->current();
        return $res;
    }

    public function getOpLama($idpelayanan)
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select('t_op_lama')->where("t_id_pelayanan = '" . $idpelayanan . "'");
        $res = $sql->prepareStatementForSqlObject($select)->execute()->current();
        return $res;
    }

    public function simpanwp($arr, $session, $tipeEspop, $dataSpop, $idpely, $idop)
    {
        $data = [
            "t_id_pelayanan" => $idpely,
            "t_nop" => str_replace('.', '', $arr['t_nopbphtbsppt']),
            "t_nama_wp" => $arr['t_namawppembeli'],
            "t_nik_wp" => $arr['t_nikwppembeli'],
            "t_jalan_wp" => substr(strtoupper($arr['t_alamatwppembeli']), 0, 30),
            "t_rt_wp" => $arr['t_rtwppembeli'],
            "t_rw_wp" => $arr['t_rwwppembeli'],
            "t_kelurahan_wp" => $arr['t_kelurahanwppembeli'],
            "t_kecamatan_wp" => $arr['t_kecamatanwppembeli'],
            "t_kabupaten_wp" => $arr['t_kabkotawppembeli'],
            "t_no_hp_wp" => $arr['t_telponwppembeli'],
            "t_npwpd" => $arr['t_npwpwppembeli'],
            "t_id_op" => $idop,
            "t_email" => $arr['t_emailwppembeli'],
            "created_by" => 1,
            // "t_id_objek" => $a
        ];

        $sql = new Sql($this->adapter);
        $permohonan = $this->getPermohonanByKodeBayar($arr['t_kodebayarbanksppt']);
        $wp = $this->getWp($permohonan['t_id_pelayanan']);

        if ($wp) {
            $insert1 = $sql->update('t_wp')->set($data)->where(['t_id_pelayanan' => $permohonan['t_id_pelayanan']]);
            $sql->prepareStatementForSqlObject($insert1)->execute();
        } else {
            $insert1 = $sql->insert('t_wp')->values($data);
            $sql->prepareStatementForSqlObject($insert1)->execute();
        }
    }

    public function simpanop($arr, $session, $tipeEspop, $dataSpop, $idpely, $arrd)
    {
        // Debug::dump($arr);
        $nop = explode('.', $arr['t_nopbphtbsppt']);
        $data = [
            "t_nop_asal" => $arr['t_nopbphtbsppt'],
            "t_id_pelayanan" => $idpely,
            "kd_propinsi" => $nop[0],
            "kd_dati2" => $nop[1],
            "kd_kecamatan" => $nop[2],
            "kd_kelurahan" => $nop[3],
            "kd_blok" => $nop[4],
            "no_urut" => $nop[5],
            "kd_jns_op" => $nop[6],
            "t_jalan_op" => $arrd['JALAN_OP'],
            "t_rt_op" => $arrd['RT_OP'] == null ? '---' : $arrd['RT_OP'],
            "t_rw_op" => $arrd['RW_OP'] == null ? '---' : $arrd['RW_OP'],
            "t_kelurahan_op" => $arrd['NM_KELURAHAN'],
            "t_kecamatan_op" => $arrd['NM_KECAMATAN'],
            "t_jenis_tanah" => $arrd['JNS_BUMI'],
            // "t_kode_lookup_item" => $arrd[''],
            "t_luas_tanah" => (int)$arr['t_luastanah'],
            "t_luas_bangunan" => (int)$arr['t_luasbangunan'],
            "t_latitude" => $arrd['LATITUDE'] != null ? $arrd['LATITUDE'] : null,
            "t_longitude" => $arrd['LONGITUDE'] != null ? $arrd['LONGITUDE'] : null,
            "created_by" => 1,

        ];
        $sql = new Sql($this->adapter);
        $permohonan = $this->getPermohonanByKodeBayar($arr['t_kodebayarbanksppt']);
        $op = $this->getOp($permohonan['t_id_pelayanan']);
        if ($op) {
            $insert1 = $sql->update('t_op')->set($data)->where(['t_id_pelayanan' => $permohonan['t_id_pelayanan']]);
            $asu = $sql->prepareStatementForSqlObject($insert1)->execute();
        } else {
            $insert1 = $sql->insert('t_op')->values($data);
            $asu = $sql->prepareStatementForSqlObject($insert1)->execute();
        }
        $op = $this->getOp($permohonan['t_id_pelayanan']);
        return $op['t_id_op'];
        // $insert1 = $sql->insert('t_op');
        // $insert1->values($data);

        // $sql->prepareStatementForSqlObject($insert1)->execute();

        // $sql2 = "select t_id_op from t_op where t_nomor_op = '" . $nomor . "' and t_nop_asal = '" . $arr['t_nopbphtbsppt'] . "'";

        // $opp = $this->adapter->query($sql2)->execute()->current();
    }

    public function simpanopPecah($arr, $session, $tipeEspop, $dataSpop, $idpely, $arrd, $opecah)
    {

        foreach ($opecah as $key => $value) {
            $nop = explode('.', $value['t_nop']);
            $nomor = $key + 1;

            $dataOpBaru = [
                "t_id_pelayanan" => $idpely,
                "kd_kecamatan" => $nop[2],
                "kd_kelurahan" => $nop[3],
                "t_jalan_op" => $value['t_jalan_op'],
                "t_rt_op" => $value['t_rt_op'],
                "t_rw_op" => $value['t_rw_op'],
                "t_kelurahan_op" => $arrd['NM_KELURAHAN'],
                "t_kecamatan_op" => $arrd['NM_KECAMATAN'],
                "t_luas_tanah" => $value['t_luas_tanah'],
                "t_jenis_tanah" => $value['t_jenis_tanah'],
                't_nomor_op' => $nomor,
                "t_nop_asal" => $value['t_nop'],
                "created_by" => 1,
            ];

            $sql = new Sql($this->adapter);
            $permohonan = $this->getPermohonanByKodeBayar($arr['t_kodebayarbanksppt']);
            $op = $this->getOp($permohonan['t_id_pelayanan']);

            if ($op) {
                $insert1 = $sql->update('t_op')->set($dataOpBaru)->where(['t_id_pelayanan' => $permohonan['t_id_pelayanan']]);
                $asu = $sql->prepareStatementForSqlObject($insert1)->execute();
                $sql2 = "select t_id_op from t_op where t_id_pelayanan = '" . $permohonan['t_id_pelayanan'] . "'";

                $opp = $this->adapter->query($sql2)->execute()->current();
            } else {
                $insert1 = $sql->insert('t_op')->values($dataOpBaru);
                $asu = $sql->prepareStatementForSqlObject($insert1)->execute();
                $sql2 = "select t_id_op from t_op where t_nomor_op = '" . $nomor . "' and t_nop_asal = '" . $value['t_nop'] . "'";

                $opp = $this->adapter->query($sql2)->execute()->current();
            }
            $op = $this->getOp($permohonan['t_id_pelayanan']);

            // $insert1 = $sql->insert('t_op');
            // $insert1->values($dataOpBaru);
            // $sql->prepareStatementForSqlObject($insert1)->execute();

            $lspopBphtb = $this->getBphtbLspopByIdSpop($value['id']);



            foreach ($lspopBphtb as $key1 => $value1) {
                $dataBangunan = [
                    't_id_pelayanan' => $idpely,
                    't_jenis_penggunaan_bangunan' => $value1['t_jenis_bangunan'],
                    't_tahun_bangunan' => $value1['t_tahun_dibangun'],
                    't_tahun_renovasi' => $value1['t_tahun_direnovasi'],
                    't_kondisi_bangunan' => $value1['t_kondisi_bangunan'],
                    't_konstruksi' => $value1['t_konstruksi'],
                    't_atap' => $value1['t_atap'],
                    't_dinding' => $value1['t_dinding'],
                    't_lantai' => $value1['t_lantai'],
                    't_langit_langit' => $value1['t_langit'],
                    't_ac_split' => $value1['t_jmlh_ac_split'],
                    't_ac_window' => $value1['t_jmlh_ac_window'],
                    't_panjang_pagar' => $value1['t_panjang_pagar'],
                    't_bahan_pagar' => $value1['t_bahan_pagar'],
                    't_jumlah_lantai' => $value1['t_jumlah_lantai'],
                    't_luas' => $value1['t_luas_bangunan'],
                    't_listrik' => $value1['t_daya_listrik'],
                    't_no_urut_bangunan' => $value1['t_nomor_bangunan'],
                    'created_by' => 1,
                    "t_id_op" => $opp['t_id_op'],
                ];
                // var_dump($data1);exit();

                $insert11 = $sql->insert('t_detail_bangunan');
                $insert11->values($dataBangunan);
                // die($sql->buildSqlString($insert11));
                $sql->prepareStatementForSqlObject($insert11)->execute();
            }

            // $dataWpBaru = [
            //     "t_id_pelayanan" => $idpely,
            //     "t_nama_wp" => $value['t_nama_wp'],
            //     "t_nik_wp" => $value['t_nik_wp'],
            //     "t_jalan_wp" => $value['t_jalan_wp'],
            //     "t_rt_wp" => $value['t_rt_wp'],
            //     "t_rw_wp" => $value['t_rw_wp'],
            //     "t_kelurahan_wp" => $value['t_kelurahan_wp'],
            //     "t_kecamatan_wp" => $value['t_kecamatan_wp'],
            //     "t_kabupaten_wp" => $value['t_kabupaten_wp'],
            //     "t_no_hp_wp" => $value['t_no_hp_wp'],
            //     "created_by" => 1,
            //     "t_id_op" => $opp['t_id_op'],
            // ];
            // Debug::dump($dataWpBaru);
            // exit;
            // $sql3 = new Sql($this->adapter);
            // $insert2 = $sql3->insert('t_wp');
            // $insert2->values($dataWpBaru);
            // die($sql->buildSqlString($insert2));

            // $sql->prepareStatementForSqlObject($insert2)->execute();
        }

        return $opp['t_id_op'];
    }

    public function simpanwplama($arr, $session, $tipeEspop, $dataSpop, $idpely, $arrd)
    {
        $data = [
            "t_id_pelayanan" => $idpely,
            "t_nama_wp" => $arrd['NM_WP'],
            "t_nik_wp" => substr(str_replace(' ', '', $arrd['SUBJEK_PAJAK_ID']), 0, 18),
            "t_jalan_wp" => $arrd['JALAN_WP'],
            "t_rt_wp" => $arrd['RT_WP'],
            "t_rw_wp" => $arrd['RW_WP'],
            "t_kelurahan_wp" => $arrd['KELURAHAN_WP'],
            "t_kecamatan_wp" => $arrd['KOTA_WP'],
            "t_kabupaten_wp" => $arrd['KOTA_WP'],
            "t_no_hp_wp" => $arrd['TELP_WP'],
            "created_by" => 1,
            "t_blok_kav_wp" => $arrd['BLOK_KAV_NO_WP'],
        ];

        $sql = new Sql($this->adapter);
        $permohonan = $this->getPermohonanByKodeBayar($arr['t_kodebayarbanksppt']);
        $wplama = $this->getWplama($permohonan['t_id_pelayanan']);
        if ($wplama) {
            $insert1 = $sql->update('t_wp_lama')->set($data)->where(['t_id_pelayanan' => $permohonan['t_id_pelayanan']]);
            $sql->prepareStatementForSqlObject($insert1)->execute();
        } else {
            $insert1 = $sql->insert('t_wp_lama')->values($data);
            $sql->prepareStatementForSqlObject($insert1)->execute();
        }
        // $insert1 = $sql->insert('t_wp_lama');
        // $insert1->values($data);
        // die($sql->buildSqlString($insert));
        $sql->prepareStatementForSqlObject($insert1)->execute();
    }

    public function simpanoplama($arr, $session, $tipeEspop, $dataSpop, $idpely, $arrd)
    {
        $nop = explode('.', $arr['t_nopbphtbsppt']);
        $data = [
            "t_id_pelayanan" => $idpely,
            "kd_propinsi" => $nop[0],
            "kd_dati2" => $nop[1],
            "kd_kecamatan" => $nop[2],
            "kd_kelurahan" => $nop[3],
            "kd_blok" => $nop[4],
            "no_urut" => $nop[5],
            "kd_jns_op" => $nop[6],
            "t_jalan_op" => $arrd['JALAN_OP'],
            "t_rt_op" => $arrd['RT_OP'] == null ? '---' : $arrd['RT_OP'],
            "t_rw_op" => $arrd['RW_OP'] == null ? '---' : $arrd['RW_OP'],
            "t_kelurahan_op" => $arrd['NM_KELURAHAN'],
            "t_kecamatan_op" => $arrd['NM_KECAMATAN'],
            // "t_kode_lookup_item" => $arrd[''],
            "t_luas_tanah" => $arrd['TOTAL_LUAS_BUMI'],
            "t_luas_bangunan" => $arrd['TOTAL_LUAS_BNG'],
            "created_by" => 1
        ];

        $sql = new Sql($this->adapter);
        // $insert1 = $sql->insert('t_op_lama');
        // $insert1->values($data);
        $permohonan = $this->getPermohonanByKodeBayar($arr['t_kodebayarbanksppt']);
        $oplama = $this->getOpLama($permohonan['t_id_pelayanan']);
        if ($oplama) {
            $insert1 = $sql->update('t_op_lama')->set($data)->where(['t_id_pelayanan' => $permohonan['t_id_pelayanan']]);
            $sql->prepareStatementForSqlObject($insert1)->execute();
        } else {
            $insert1 = $sql->insert('t_op_lama')->values($data);
            $sql->prepareStatementForSqlObject($insert1)->execute();
        }
        // die($sql->buildSqlString($insert));
        // $sql->prepareStatementForSqlObject($insert1)->execute();
    }

    public function simpanSpopBalikNama($arr, $tipeEspop, $session, $idPermohonan)
    {
        // $permohonan = $this->getPermohonanByKodeBayar($arr['t_kodebayarbanksppt']);
        $jenisPelayanan = $tipeEspop == TipeEspopHelper::BALIK_NAMA ? ReffSpopHelper::PELAYANAN_MUTASI_PENUH : ReffSpopHelper::PELAYANAN_MUTASI_SEBAGIAN;

        $datObjekPajak = $this->getDatObjekPajak($arr['t_nopbphtbsppt']);
        $datOpBumi = $this->getDatOpBumi($arr['t_nopbphtbsppt']);

        $datOpBangunan = $this->getDatOpBangunan($arr['t_nopbphtbsppt']);
        // var_dump($datOpBangunan);exit();

        $noSpop = $this->getMaxNoSpop(date('Y'));
        // var_dump($noSpop);exit();

        $data = [
            'T_ID_SPOP' => new Expression("T_ID_SPOP_SEQ.NEXTVAL"),
            // 'T_ID_PERMOHONAN' => $permohonan['T_ID_PERMOHONAN'],
            'T_ID_PERMOHONAN' => $idPermohonan,
            'T_JENIS_KEPEMILIKAN' => $datObjekPajak['KD_STATUS_WP'],
            'T_NIK_WP' => strtoupper($arr['t_nikwppembeli']),
            'T_NAMA_WP' => strtoupper($arr['t_namawppembeli']),
            'T_RT_WP' => strtoupper($arr['t_rtwppembeli']),
            'T_RW_WP' => substr($datObjekPajak['RW_OP'], -2),
            //strtoupper($arr['t_rwwppembeli']),
            'T_JALAN_WP' => substr(strtoupper($arr['t_alamatwppembeli']), 0, 30),
            'T_KELURAHAN_WP' => strtoupper($arr['t_kelurahanwppembeli']),
            'T_KECAMATAN_WP' => strtoupper($arr['t_kecamatanwppembeli']),
            'T_KABUPATEN_WP' => strtoupper($arr['t_kabkotawppembeli']),
            'T_NO_HP_WP' => $arr['t_telponwppembeli'],
            'KD_PROPINSI' => $datObjekPajak['KD_PROPINSI'],
            'KD_DATI2' => $datObjekPajak['KD_DATI2'],
            'KD_KECAMATAN' => $datObjekPajak['KD_KECAMATAN'],
            'KD_KELURAHAN' => $datObjekPajak['KD_KELURAHAN'],
            'KD_BLOK' => $datObjekPajak['KD_BLOK'],
            'NO_URUT' => $datObjekPajak['NO_URUT'],
            'KD_JNS_OP' => $datObjekPajak['KD_JNS_OP'],
            'T_RT_OP' => $datObjekPajak['RT_OP'],
            'T_RW_OP' => substr($datObjekPajak['RW_OP'], -2),
            //$datObjekPajak['RW_OP'],
            'T_JALAN_OP' => $datObjekPajak['JALAN_OP'],
            'T_KELURAHAN_OP' => $datObjekPajak['KD_KELURAHAN'],
            'T_KECAMATAN_OP' => $datObjekPajak['KD_KECAMATAN'],
            'T_JENIS_TANAH' => $datOpBumi['JNS_BUMI'],
            'T_LUAS_TANAH' => $datOpBumi['LUAS_BUMI'],
            'T_KD_ZNT' => $datOpBumi['KD_ZNT'],
            'T_JMLH_BANGUNAN' => count($datOpBangunan),
            'T_LATITUDE' => '0',
            'T_LONGITUDE' => '0',
            'CREATED_DATE' => new Expression("SYSDATE"),
            'CREATED_BY' => $session['s_id_user_espop'],
            // 'T_ID_JENIS_PELAYANAN' => $permohonan['T_ID_JENIS_PELAYANAN'],
            // 'T_ID_JENIS_PELAYANAN' => $permohonan['T_ID_JENIS_PELAYANAN'],
            'T_ID_JENIS_PELAYANAN' => $jenisPelayanan,
            // 'T_JMLH_BIDANG',
            // 'T_NOMOR_BIDANG',
            'T_NOMOR_SERTIFIKAT' => $datObjekPajak['NO_PERSIL'],
            'T_BLOK_OP' => substr($datObjekPajak['BLOK_KAV_NO_OP'], 0, 5),
            //$datObjekPajak['BLOK_KAV_NO_OP'],
            'T_BLOK_WP' => null,
            'T_PEKERJAAN_WP' => 5,
            'T_NPWP_WP' => $arr['t_npwpwppembeli'],
            'T_KODE_POS_WP' => $arr['t_kodeposwppembeli'],
            // 'T_LUAS_BANGUNAN',
            'T_KODE_BLOK_OP' => $datObjekPajak['KD_BLOK'],
            // 'T_NOP_ASAL',
            // 'T_KETERANGAN',
            // 'THN_PELAYANAN',
            // 'BUNDEL_PELAYANAN',
            // 'NO_URUT_PELAYANAN',
            'T_TAHUN_SPOP' => date('Y'),
            'T_NO_URUT_SPOP' => $noSpop,
            'T_NO_SPOP' => $this->noSpop(date('Y'), $noSpop),
            // 'T_NOP_TERDEKAT',
            'T_DARI_BPHTB' => 1,
            'T_KODEBAYAR_BPHTB' => $arr['t_kodebayarbanksppt'],
            'T_IDSPT_BPHTB' =>  $arr['t_idspt'],
            'T_TIPE_DARI_BPHTB' => $tipeEspop == TipeEspopHelper::BALIK_NAMA ? 2 : 1
        ];

        // var_dump($data['T_ID_SPOP']);
        // exit();

        $sql = new Sql($this->adapter);


        $noSpopFix = 0;
        while ($noSpopFix > 0) {
            $checkNoSpop = $this->checkNoSpop($data['T_NO_SPOP']);
            if ($checkNoSpop == null) {
                $noSpopFix = 1;
            } else {
                $no = $data['T_NO_URUT_SPOP'] + 1;
                $data['T_NO_URUT_SPOP'] = $no;
                $data['T_NO_SPOP'] = $this->noSpop($data['T_TAHUN_SPOP'], $no);
            }
        }

        $insert = $sql->insert('T_SPOP')->values($data);
        $res = $sql->prepareStatementForSqlObject($insert)->execute();
        // return $res;

        // LSPOP
        foreach ($datOpBangunan as $key => $value) {

            // var_dump($value);exit();

            $spop = $this->getSpopByIdPermohonan($data['T_ID_PERMOHONAN']);

            $data1 = [
                'T_ID_LSPOP' => new Expression("T_ID_LSPOP_SEQ.NEXTVAL"),
                'T_ID_SPOP' => $spop['T_ID_SPOP'],
                'T_JENIS_TRANSAKSI' => $value['JNS_TRANSAKSI_BNG'],
                'T_JENIS_BANGUNAN' => $value['KD_JPB'],
                'T_LUAS_BANGUNAN' => $value['LUAS_BNG'],
                'T_JUMLAH_LANTAI' => $value['JML_LANTAI_BNG'],
                'T_TAHUN_DIBANGUN' => $value['THN_DIBANGUN_BNG'],
                'T_TAHUN_DIRENOVASI' => $value['THN_RENOVASI_BNG'],
                'T_DAYA_LISTRIK' => $value['LISTRIK'],
                'T_KONDISI_BANGUNAN' => $value['KONDISI_BNG'],
                'T_KONSTRUKSI' => $value['JNS_KONSTRUKSI_BNG'],
                'T_ATAP' => $value['JNS_ATAP_BNG'],
                'T_DINDING' => $value['KD_DINDING'],
                'T_LANTAI' => $value['KD_LANTAI'],
                'T_LANGIT' => $value['KD_LANGIT_LANGIT'],
                'T_JMLH_AC_SPLIT' => ($value['AC_SPLIT'] != null ? $value['AC_SPLIT'] : 0),
                'T_JMLH_AC_WINDOW' => ($value['AC_WINDOW'] != null ? $value['AC_WINDOW'] : 0),
                'T_AC_CENTRAL' => ($value['AC_CENTRAL'] != null ? $value['AC_CENTRAL'] : 0),
                'T_LUAS_KOLAM' => ($value['KOLAM_RENANG_DENGAN_PELAPIS'] != null ? $value['KOLAM_RENANG_DENGAN_PELAPIS'] : ($value['KOLAM_RENANG_DIPLESTER'] != null ? $value['KOLAM_RENANG_DIPLESTER'] : 0)),
                'T_PLESTER_KOLAM' => ($value['KOLAM_RENANG_DENGAN_PELAPIS'] != null ? 2 : ($value['KOLAM_RENANG_DIPLESTER'] != null ? 1 : 0)),
                'T_LUAS_PERKERASAN_HALAMAN' => ($value['PEKERASAN_KONTRUKSI'] != null ? $value['PEKERASAN_KONTRUKSI'] : 0),
                'T_PANJANG_PAGAR' => ($value['PAGAR_BAJA_BESI'] != null ? $value['PAGAR_BAJA_BESI'] : ($value['PAGAR_BATA_BATAKO'] != null ? $value['PAGAR_BATA_BATAKO'] : 0)),
                'T_BAHAN_PAGAR' => ($value['PAGAR_BAJA_BESI'] != null ? 1 : ($value['PAGAR_BATA_BATAKO'] != null ? 2 : 0)),
                'T_KEDALAMAN_SUMUR' => ($value['SUMUR_ARTESIS'] != null ? $value['SUMUR_ARTESIS'] : 0),
                'CREATED_DATE' => new Expression("SYSDATE"),
                'CREATED_BY' => $session['s_id_user_espop'],
                'T_NOMOR_BANGUNAN' => $value['NO_BNG'],
                'T_JMLH_BANGUNAN' => count($datOpBangunan),
                'T_TAHUN_LSPOP' => NULL,
                'T_NO_URUT_LSPOP' => NULL

            ];

            // var_dump($data1);exit();

            $insert1 = $sql->insert('T_LSPOP');
            $insert1->values($data1);
            // die($sql->buildSqlString($insert));
            $sql->prepareStatementForSqlObject($insert1)->execute();
        }
    }

    public function getMaxNoSpop($tahun)
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select('T_SPOP');
        $select->columns(['NO' => new Expression("MAX(T_NO_URUT_SPOP)")]);
        $select->where("T_TAHUN_SPOP = '" . $tahun . "'");
        $res = $sql->prepareStatementForSqlObject($select)->execute()->current();
        return (int) $res['NO'] + 1;
    }

    public function noSpop($tahun, $no)
    {
        return $tahun . '.' . str_pad($no, 7, '0', STR_PAD_LEFT);
    }

    public function checkNoSpop($noSpop)
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select('T_SPOP')->where("T_NO_SPOP = '" . $noSpop . "'");
        $res = $sql->prepareStatementForSqlObject($select)->execute()->current();
        return $res;
    }

    public function getSpopByIdPermohonan($idPermohonan)
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select('T_SPOP');
        $select->where('T_ID_PERMOHONAN = ' . $idPermohonan);
        $res = $sql->prepareStatementForSqlObject($select)->execute()->current();
        return $res;
    }

    public function simpanVerifikasi($arr, $session, $idPermohonan)
    {
        $noVerifikasi = $this->getMaxNoVerifikasiPermohonan(date('Y'));

        $data = [
            't_id_pelayanan' => $idPermohonan,
            't_tgl_verifikasi' => date('Y-m-d'),
            't_no_verifikasi' => $noVerifikasi,
            't_id_status_verifikasi' => 5,
            't_keterangan_verifikasi' => 'DARI BPHTB NO DAFTAR ' . $arr['t_kohirspt'],
            'created_by' => 1,
            't_kejelasan' => 0,
            't_kebenaran' => 0,
            't_keabsahan' => 0,
        ];
        // var_dump($data);exit();

        $sql = new Sql($this->adapter);
        $check1 = $sql->select('t_verifikasi')->where(['t_id_pelayanan' => $idPermohonan]);
        $resCheck1 = $sql->prepareStatementForSqlObject($check1)->execute()->current();


        if ($resCheck1) {
            $insert = $sql->update('t_verifikasi')->set($data)->where(['t_id_pelayanan' => $idPermohonan]);
            $sql->prepareStatementForSqlObject($insert)->execute();
        } else {
            $insert = $sql->insert('t_verifikasi')->values($data);
            $sql->prepareStatementForSqlObject($insert)->execute();
        }

        // $dataSpop = $this->getListSpopByIdPermohonan($idPermohonan);

        // foreach ($dataSpop as $val) {
        //     $noVerifikasiLapangan = $this->getMaxNoVerifikasiLapangan(date('Y'));
        //     $data1 = [
        //         // 'T_ID_VERIFIKASI_LAPANGAN' => new Expression("T_ID_VERIFIKASI_LAPANGAN_SEQ.NEXTVAL"),
        //         'T_ID_SPOP' => $val['T_ID_SPOP'],
        //         'T_TGL_VERIFIKASI' => new Expression("SYSDATE"),
        //         'T_NO_VERIFIKASI' => $noVerifikasiLapangan,
        //         'T_KETERANGAN_VERIFIKASI' => 'DARI BPHTB NO DAFTAR ' . $arr['t_kohirspt'],
        //         'CREATED_DATE' => new Expression("SYSDATE"),
        //         'CREATED_BY' => $session['s_id_user_espop']

        //     ];

        //     $check2 = $sql->select('T_VERIFIKASI_LAPANGAN')->where(['T_ID_SPOP' => $val['T_ID_SPOP']]);
        //     $resCheck2 = $sql->prepareStatementForSqlObject($check2)->execute()->current();

        //     if ($resCheck2) {
        //         $insert1 = $sql->update('T_VERIFIKASI_LAPANGAN')->where(['T_ID_SPOP' => $val['T_ID_SPOP']])->set($data1);
        //         $sql->prepareStatementForSqlObject($insert1)->execute();
        //     } else {
        //         $data1['T_ID_VERIFIKASI_LAPANGAN'] = new Expression("T_ID_VERIFIKASI_LAPANGAN_SEQ.NEXTVAL");

        //         $insert1 = $sql->insert('T_VERIFIKASI_LAPANGAN')->values($data1);
        //         $sql->prepareStatementForSqlObject($insert1)->execute();
        //     }
        // }
    }

    public function getMaxNoVerifikasiPermohonan($tahun)
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select('t_verifikasi');
        $select->columns(['NO' => new Expression("MAX(t_no_verifikasi)")]);
        $select->where("TO_CHAR(t_tgl_verifikasi, 'YYYY') = '" . $tahun . "'");
        $res = $sql->prepareStatementForSqlObject($select)->execute()->current();

        return (int) $res['NO'] + 1;
    }

    public function getMaxNoVerifikasiLapangan($tahun)
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select('T_VERIFIKASI_LAPANGAN');
        $select->columns(['NO' => new Expression("MAX(T_NO_VERIFIKASI)")]);
        $select->where("TO_CHAR(T_TGL_VERIFIKASI, 'YYYY') = '" . $tahun . "'");
        $res = $sql->prepareStatementForSqlObject($select)->execute()->current();

        return (int) $res['NO'] + 1;
    }

    public function getListSpopByIdPermohonan($idPermohonan)
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select('T_SPOP')->where("T_ID_PERMOHONAN = '" . $idPermohonan . "'");
        $res = $sql->prepareStatementForSqlObject($select)->execute();
        return $res;
    }

    public function simpanSpopPemecahan($arr, $tipeEspop, $session, $idPermohonan, $dataSpop)
    {
        $jenisPelayanan = $tipeEspop == TipeEspopHelper::BALIK_NAMA ? ReffSpopHelper::PELAYANAN_MUTASI_PENUH : ReffSpopHelper::PELAYANAN_MUTASI_SEBAGIAN;

        foreach ($dataSpop as $key => $value) {
            // var_dump($value);
            // exit();
            $noSpop = $this->getMaxNoSpop(date('Y'));

            $data = [
                'T_ID_SPOP' => new Expression("T_ID_SPOP_SEQ.NEXTVAL"),
                'T_ID_PERMOHONAN' => $idPermohonan,
                'T_JENIS_KEPEMILIKAN' => $value['t_jenis_kepemilikan'],
                'T_NIK_WP' => $value['t_nik_wp'],
                'T_NAMA_WP' => $value['t_nama_wp'],
                'T_RT_WP' => $value['t_rt_wp'],
                'T_RW_WP' => $value['t_rw_wp'],
                'T_JALAN_WP' => $value['t_jalan_wp'],
                'T_KELURAHAN_WP' => $value['t_kelurahan_wp'],
                'T_KECAMATAN_WP' => $value['t_kecamatan_wp'],
                'T_KABUPATEN_WP' => $value['t_kabupaten_wp'],
                'T_NO_HP_WP' => $value['t_no_hp_wp'],
                'KD_PROPINSI' => $value['kd_propinsi'],
                'KD_DATI2' => $value['kd_dati2'],
                'KD_KECAMATAN' => $value['kd_kecamatan'],
                'KD_KELURAHAN' => $value['kd_kelurahan'],
                'KD_BLOK' => $value['kd_blok'],
                'NO_URUT' => $value['no_urut'],
                'KD_JNS_OP' => $value['kd_jns_op'],
                'T_RT_OP' => $value['t_rt_op'],
                'T_RW_OP' => $value['t_rw_op'],
                'T_JALAN_OP' => $value['t_jalan_op'],
                'T_KELURAHAN_OP' => $value['t_kelurahan_op'],
                'T_KECAMATAN_OP' => $value['t_kecamatan_op'],
                'T_JENIS_TANAH' => $value['t_jenis_tanah'],
                'T_LUAS_TANAH' => $value['t_luas_tanah'],
                'T_KD_ZNT' => $value['t_kd_znt'],
                'T_JMLH_BANGUNAN' => $value['t_jmlh_bangunan'],
                'T_LATITUDE' => '0',
                'T_LONGITUDE' => '0',
                'CREATED_DATE' => new Expression("SYSDATE"),
                'CREATED_BY' => $session['s_id_user_espop'],
                'T_ID_JENIS_PELAYANAN' => $jenisPelayanan,
                'T_JMLH_BIDANG' => $value['t_jmlh_bidang'],
                'T_NOMOR_BIDANG' => $value['t_nomor_bidang'],
                'T_NOMOR_SERTIFIKAT' => $value['t_nomor_sertifikat'],
                'T_BLOK_OP' => $value['t_blok_op'],
                'T_BLOK_WP' => $value['t_blok_wp'],
                'T_PEKERJAAN_WP' => $value['t_pekerjaan_wp'],
                'T_NPWP_WP' => $value['t_npwp_wp'],
                'T_KODE_POS_WP' => $value['t_kode_pos_wp'],
                'T_LUAS_BANGUNAN' => $value['t_luas_bangunan'],
                'T_KODE_BLOK_OP' => $value['t_kode_blok_op'],
                'T_NOP_ASAL' => $value['t_nop_asal'],
                // 'T_KETERANGAN',
                // 'THN_PELAYANAN',
                // 'BUNDEL_PELAYANAN',
                // 'NO_URUT_PELAYANAN',
                'T_TAHUN_SPOP' => date('Y'),
                'T_NO_URUT_SPOP' => $noSpop,
                'T_NO_SPOP' => $this->noSpop(date('Y'), $noSpop),
                // 'T_NOP_TERDEKAT',
                'T_DARI_BPHTB' => 1,
                'T_KODEBAYAR_BPHTB' => $arr['t_kodebayarbanksppt'],
                'T_IDSPT_BPHTB' =>  $arr['t_idspt'],
                'T_TIPE_DARI_BPHTB' => $tipeEspop == TipeEspopHelper::BALIK_NAMA ? 2 : 1
            ];

            // var_dump($data['T_ID_SPOP']);
            // exit();

            $sql = new Sql($this->adapter);


            $noSpopFix = 0;
            while ($noSpopFix > 0) {
                $checkNoSpop = $this->checkNoSpop($data['T_NO_SPOP']);
                if ($checkNoSpop == null) {
                    $noSpopFix = 1;
                } else {
                    $no = $data['T_NO_URUT_SPOP'] + 1;
                    $data['T_NO_URUT_SPOP'] = $no;
                    $data['T_NO_SPOP'] = $this->noSpop($data['T_TAHUN_SPOP'], $no);
                }
            }

            $insert = $sql->insert('T_SPOP')->values($data);
            $res = $sql->prepareStatementForSqlObject($insert)->execute();


            $spop = $this->getSpopByKodebayarNomorBidang($arr['t_kodebayarbanksppt'], $data['T_NOMOR_BIDANG']);

            $lspopBphtb = $this->getBphtbLspopByIdSpop($value['id']);

            foreach ($lspopBphtb as $key1 => $value1) {
                // var_dump($value1);exit();

                $data1 = [
                    'T_ID_LSPOP' => new Expression("T_ID_LSPOP_SEQ.NEXTVAL"),
                    'T_ID_SPOP' => $spop['T_ID_SPOP'],
                    'T_JENIS_TRANSAKSI' => $value1['t_jenis_transaksi'],
                    'T_JENIS_BANGUNAN' => $value1['t_jenis_bangunan'],
                    'T_LUAS_BANGUNAN' => $value1['t_luas_bangunan'],
                    'T_JUMLAH_LANTAI' => $value1['t_jumlah_lantai'],
                    'T_TAHUN_DIBANGUN' => $value1['t_tahun_dibangun'],
                    'T_TAHUN_DIRENOVASI' => $value1['t_tahun_direnovasi'],
                    'T_DAYA_LISTRIK' => $value1['t_daya_listrik'],
                    'T_KONDISI_BANGUNAN' => $value1['t_kondisi_bangunan'],
                    'T_KONSTRUKSI' => $value1['t_konstruksi'],
                    'T_ATAP' => $value1['t_atap'],
                    'T_DINDING' => $value1['t_dinding'],
                    'T_LANTAI' => $value1['t_lantai'],
                    'T_LANGIT' => $value1['t_langit'],
                    'T_JMLH_AC_SPLIT' => $value1['t_jmlh_ac_split'],
                    'T_JMLH_AC_WINDOW' => $value1['t_jmlh_ac_window'],
                    'T_AC_CENTRAL' => $value1['t_ac_central'],
                    'T_LUAS_KOLAM' => $value1['t_luas_kolam'],
                    'T_PLESTER_KOLAM' => $value1['t_plester_kolam'],
                    'T_LUAS_PERKERASAN_HALAMAN' => $value1['t_luas_perkerasan_halaman'],
                    'T_PANJANG_PAGAR' => $value1['t_panjang_pagar'],
                    'T_BAHAN_PAGAR' => $value1['t_bahan_pagar'],
                    'T_KEDALAMAN_SUMUR' => $value1['t_kedalaman_sumur'],
                    'CREATED_DATE' => new Expression("SYSDATE"),
                    'CREATED_BY' => $session['s_id_user_espop'],
                    'T_NOMOR_BANGUNAN' => $value1['t_nomor_bangunan'],
                    'T_JMLH_BANGUNAN' => $value1['t_jmlh_bangunan'],
                    'T_TAHUN_LSPOP' => NULL,
                    'T_NO_URUT_LSPOP' => NULL

                ];

                // var_dump($data1);exit();

                $insert1 = $sql->insert('T_LSPOP');
                $insert1->values($data1);
                // die($sql->buildSqlString($insert));
                $sql->prepareStatementForSqlObject($insert1)->execute();
            }
        }
    }

    public function getSpopByKodebayarNomorBidang($kodeBayar, $noBidang)
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select('T_SPOP')
            ->where("T_KODEBAYAR_BPHTB = '" . $kodeBayar . "' AND T_NOMOR_BIDANG = '" . $noBidang . "'");
        $res = $sql->prepareStatementForSqlObject($select)->execute()->current();
        return $res;
    }

    public function getBphtbLspopByIdSpop($idSpop)
    {
        $sql = new Sql($this->adapter2);
        $select = $sql->select('t_spt_lspop')->where("id_spop = '" . $idSpop . "'");
        $select->order('t_nomor_bangunan');
        $res = $sql->prepareStatementForSqlObject($select)->execute();
        return $res;
    }

    public function tes()
    {
        $sql2 = "select t_id_op from t_op limit 1";

        $opp = $this->adapter->query($sql2)->execute()->current();
    }

    public function getdatabykodebayar($kodeBayar)
    {
        $sql2 = "select t_id_op from t_op limit 1";

        $opp = $this->adapter->query($sql2)->execute()->current();
    }

    public function getdatabyidpelayanan($id)
    {
        $sql2 = "
        SELECT * FROM t_pelayanan
left join t_wp on t_wp.t_id_pelayanan = t_pelayanan.t_id_pelayanan
left join t_op_lama on t_op_lama.t_id_pelayanan = t_pelayanan.t_id_pelayanan
where t_pelayanan.t_id_pelayanan = 
        " . $id;

        $data = $this->adapter->query($sql2)->execute()->current();
        return $data;
    }

    public function getdataWpLama($id)
    {
        $sql2 = "
        SELECT * FROM t_wp_lama
where t_id_pelayanan = 
        " . $id;

        $data = $this->adapter->query($sql2)->execute()->current();
        return $data;
    }
    public function getdataOpLama($id)
    {
        $sql2 = "
        SELECT * FROM t_op_lama
where t_id_pelayanan = 
        " . $id;

        $data = $this->adapter->query($sql2)->execute()->current();
        return $data;
    }

    public function getdataWpBaru($id)
    {
        $sql2 = "
        SELECT * FROM t_wp
where t_id_pelayanan = 
        " . $id;

        $data = $this->adapter->query($sql2)->execute();
        return $data;
    }
    public function getdataOpBaru($id)
    {
        $sql2 = "
        SELECT * FROM t_op
where t_id_op = 
        " . $id;

        $data = $this->adapter->query($sql2)->execute()->current();
        return $data;
    }

    public function getdataBangunan($id)
    {
        $sql2 = "
        SELECT COALESCE(t_luas, 0) AS t_luas
FROM t_detail_bangunan
where t_id_op = 
        " . $id;

        $data = $this->adapter->query($sql2)->execute()->current();
        return $data;
    }

    public function getDataPelayananBphtb($tgl1, $tgl2, $kec, $kel)
    {
        $wherekec = '';
        $wherekel = '';
        if ($kec != "") {
            $wherekec = "AND t_op_lama.kd_kecamatan ='" . $kec . "'";
        }
        if ($kel != "") {
            $kodekel = substr($kel, -2);
            $wherekel = "AND RIGHT(t_op_lama.kd_kelurahan, 2) ='" . $kodekel . "'";
        }

        $sql = "
        SELECT DISTINCT REPLACE
	( t_pelayanan.t_keterangan, 'DARI BPHTB NO DAFTAR', '' ) AS no_pendaftaran,
	t_pelayanan.t_nama_pemohon,
	t_pelayanan.t_jalan_pemohon,
	t_pelayanan.t_rt_pemohon,
	t_pelayanan.t_rw_pemohon,
	t_pelayanan.t_kelurahan_pemohon,
	t_pelayanan.t_kecamatan_pemohon,
	t_pelayanan.t_kabupaten_pemohon,
	t_pelayanan.t_id_jenis_pelayanan,
	s_jenis_pelayanan.s_nama_jenis_pelayanan,
	t_wp_lama.t_nik_wp AS nik_wp_lama,
	t_wp_lama.t_nama_wp AS nama_wp_lama,
	t_wp_lama.t_jalan_wp AS jalan_wp_lama,
	t_wp_lama.t_rt_wp AS rt_wp_lama,
	t_wp_lama.t_rw_wp AS rw_wp_lama,
	t_wp_lama.t_kecamatan_wp AS kecamatan_wp_lama,
	t_wp_lama.t_kelurahan_wp AS kelurahan_wp_lama,
	t_op_lama.kd_propinsi || '.' || t_op_lama.kd_dati2 || '.' || t_op_lama.kd_kecamatan || '.' || t_op_lama.kd_kelurahan || '.' || t_op_lama.kd_blok || '.' || t_op_lama.no_urut || '.' || t_op_lama.kd_jns_op AS nop_lama,
	t_op_lama.t_jalan_op AS jalan_op_lama,
	t_op_lama.t_rt_op AS rt_op_lama,
	t_op_lama.t_rw_op AS rw_op_lama,
	t_op_lama.t_kecamatan_op AS kecamatan_op_lama,
	t_op_lama.t_kelurahan_op AS kelurahan_op_lama,
	t_wp.t_nama_wp,
	t_wp.t_nik_wp,
	t_wp.t_jalan_wp,
	t_wp.t_rt_wp,
	t_wp.t_rw_wp,
	t_wp.t_kelurahan_wp,
	t_wp.t_kecamatan_wp,
	t_wp.t_kabupaten_wp,
	t_wp.t_no_hp_wp,
	t_op.kd_propinsi,
	t_op.kd_dati2,
	t_op.kd_kecamatan,
	t_op.kd_kelurahan,
	t_op.kd_blok,
	t_op.no_urut,
	t_op.kd_jns_op,
	t_op.t_jalan_op,
	t_op.t_rt_op,
	t_op.t_rw_op,
	t_op.t_kelurahan_op,
	t_op.t_kecamatan_op,
    t_pelayanan.t_tgl_pelayanan
FROM
	t_pelayanan
	LEFT JOIN s_jenis_pelayanan ON s_jenis_pelayanan.s_id_jenis_pelayanan = t_pelayanan.t_id_jenis_pelayanan
	LEFT JOIN t_wp_lama ON t_wp_lama.t_id_pelayanan = t_pelayanan.t_id_pelayanan
	LEFT JOIN t_op_lama ON t_op_lama.t_id_pelayanan = t_pelayanan.t_id_pelayanan
	LEFT JOIN t_wp ON t_wp.t_id_pelayanan = t_pelayanan.t_id_pelayanan
	LEFT JOIN t_op ON t_op.t_id_op = t_wp.t_id_op 
WHERE
	t_kode_bayar_bphtb IS NOT NULL 
	AND t_pelayanan.t_id_pelayanan IN ( SELECT t_id_pelayanan FROM t_validasi ) 
    " . $wherekec . $wherekel . "
    AND t_tgl_pelayanan between '" . date('Y-m-d', strtotime($tgl1)) . "' and '" . date('Y-m-d', strtotime($tgl2)) . "'
ORDER BY
	t_tgl_pelayanan
        ";
        // die($sql);
        $data = $this->adapter->query($sql)->execute();
        return $data;
    }
}
