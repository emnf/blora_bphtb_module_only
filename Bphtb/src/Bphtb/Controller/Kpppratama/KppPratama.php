<?php

// Modul Pembayaran

namespace Bphtb\Controller\Kpppratama;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Bphtb\Form\Pendataan\SSPDFrm;
use Bphtb\Model\Pendataan\SSPDBphtbBase;

class KppPratama extends AbstractActionController {

    protected $tbl_pembayaran, $tbl_pendataan, $tbl_jenistransaksi, $tbl_haktanah, $tbl_pemda, $tbl_sspd, $tbl_pejabat;

    public function cekurl()
     {
        $basePath = $this->getRequest()->getBasePath();
            $uri = new \Zend\Uri\Uri($this->getRequest()->getUri());
            $uri->setPath($basePath);
            $uri->setQuery(array());
            $uri->setFragment('');
            
        return $uri->getScheme() . '://' . $uri->getHost() . '' . $uri->getPath(); //:'.$_SERVER['SERVER_PORT'].'
    
     }
    
    // Index Pembayaran
    public function indexAction() {
        // $session = new \Zend\Session\Container('user_session');
        $session = $this->getServiceLocator()
                ->get('EtaxService')
                ->getStorage()
                ->read();
        $ar_pemda = $this->getPemda()->getdata();
        
        $data_mengetahui = $this->getTblPejabat()->getdata();
        $data_mengetahui_periksa = $this->getTblPejabat()->getdata();
        $panggildata = $this->getServiceLocator()->get("JenisTransaksiBphtbTable");
        $datajenistransaksi = $panggildata->comboBox();
        
        $form = new \Bphtb\Form\Pembayaran\PembayaranSptFrm();
        
        $ar_notaris = $this->getServiceLocator()->get('NotarisBphtbTable')->getdataCombo();
        
        $view = new ViewModel(array(
            'form' => $form,
            'data_mengetahui' => $data_mengetahui,
            'data_mengetahui_periksa' => $data_mengetahui_periksa,
            'datajenistransaksi' => $datajenistransaksi ,
            'datanotaris' => $ar_notaris
        ));
        $data = array(
            'menu_kpppratama' => 'active',
            'role_id' => $session['s_akses'],
            'data_pemda' => $ar_pemda,
            'aturgambar' => 1,
            'username' => $session['s_username']
        );
        $this->layout()->setVariables($data);
        return $view;
    }
    
    public function dataGridAction() {
        $session = $this->getServiceLocator()->get('EtaxService')->getStorage()->read();
        $s_iduser = $session['s_iduser']; 
        $s_tipe_pejabat = $session['s_tipe_pejabat'];
        
        $sTable = 'fr_pembayaran_v5';
        $count = 't_idspt';
        
        $input = $this->getRequest();
        $order_default = " t_kohirspt DESC";
        $aColumns = array('t_idspt', 't_kohirspt', 't_idnotarisspt', 't_tanggalpembayaran', 't_nopbphtbsppt', 't_namawppembeli', 't_alamatwppembeli', 't_nikwppembeli', 'p_idpemeriksaan', 't_grandtotalnjop', 'p_grandtotalnjop', 't_nilaitransaksispt', 'p_nilaitransaksispt', 't_inputbpn',  't_tglprosesspt', 't_tarif_pembagian_aphb_kali', 't_tarif_pembagian_aphb_bagi', 't_grandtotalnjop_aphb',  's_namanotaris'); 
        
        
        $panggildata = $this->getServiceLocator()->get("PembayaranSptTable");
        $rResult = $panggildata->semuadatakpppratama($sTable, $count, $input, $order_default, $aColumns, $session, $this->cekurl());    
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($rResult));
    }

    
    
    public function viewdataAction() {
        $ar_pemda = $this->getPemda()->getdata();
        $session = $this->getServiceLocator()
                ->get('EtaxService')
                ->getStorage()
                ->read();
        $req = $this->getRequest();
        if ($req->isGet()) {
            $id = (int) $req->getQuery()->get('t_idspt');
            $data = $this->getPendataanBphtb()->getDataId_all($id);
            $data['t_tglprosesspt'] = date('d-m-Y', strtotime($data['t_tglprosesspt']));
            $data['t_tglajb'] = date('d-m-Y', strtotime($data['t_tglajb']));
            $t_luastanah = str_ireplace('.', '', $data['t_luastanah']);
            $t_luasbangunan = str_ireplace('.', '', $data['t_luasbangunan']);
            $data['t_luastanah'] = number_format(($t_luastanah / 100), 0, ',', '.');
            $data['t_luasbangunan'] = number_format(($t_luasbangunan / 100), 0, ',', '.');
        }
        $view = new ViewModel(array(
            'datasspd' => $data
        ));

        $data = array(
            'menu_kpppratama' => 'active',
            'role_id' => $session['s_akses'],
            'data_pemda' => $ar_pemda,
            'aturgambar' => 2,
            'username' => $session['s_username']
        );
        $this->layout()->setVariables($data);
        return $view;
    }

    

    public function cetaklapkpppratamaAction() {
        $frm = new \Bphtb\Form\Pencetakan\SSPDFrm();
        $req = $this->getRequest();
        if ($req->isGet()) {
            $base = new \Bphtb\Model\Pencetakan\SSPDBase();
            $frm->setData($req->getQuery());
            if ($frm->isValid()) {
                $base->exchangeArray($frm->getData());
                $data_get = $req->getQuery();
                //============================ Model\Pencetakan\SSPDTable
                $ar_PencatatanSetoran = $this->getSSPD()->getDataPencatatanSetoranBulanan_kpppratama($data_get->tgl_setor1, $data_get->tgl_setor2); //getDataPencatatanSetoran
                $ar_tglcetak = $data_get->tgl_cetakan;
                //$ar_periodespt = $base->periode_spt;
                $ar_pemda = $this->getPemda()->getdata();
            }
            //$ar_Mengetahui = $this->getTblPejabat()->getdataid($data_get->mengetahui);
            //$ar_Pejabat = $this->getTblPejabat()->getdataid($data_get->mengetahui_periksa);
        }
        $view = new ViewModel(array(
            'data_pencatatanSetoran' => $ar_PencatatanSetoran,
            'tgl_cetakan' => $ar_tglcetak,
            'tgl_setor1' => $data_get->tgl_setor1,
            'tgl_setor2' => $data_get->tgl_setor2,
            'ar_pemda' => $ar_pemda,
            //'data_mengetahui' => $ar_Mengetahui,
            //'data_pejabat' => $ar_Pejabat,
            'cetak' => $data_get->cetak
        ));    
        $data = array(
            'nilai' => '3' //no layout
        );
        $this->layout()->setVariables($data);
        return $view;
    }
    
    

    public function getSSPD() {
        if (!$this->tbl_sspd) {
            $sm = $this->getServiceLocator();
            $this->tbl_sspd = $sm->get("SSPDTable");
        }
        return $this->tbl_sspd;
    }


    public function getTblPembayaran() {
        if (!$this->tbl_pembayaran) {
            $sm = $this->getServiceLocator();
            $this->tbl_pembayaran = $sm->get('PembayaranSptTable');
        }
        return $this->tbl_pembayaran;
    }

    public function getPendataanBphtb() {
        if (!$this->tbl_pendataan) {
            $sm = $this->getServiceLocator();
            $this->tbl_pendataan = $sm->get('SSPDBphtbTable');
        }
        return $this->tbl_pendataan;
    }
    
    

    public function getTblJenTran() {
        if (!$this->tbl_jenistransaksi) {
            $sm = $this->getServiceLocator();
            $this->tbl_jenistransaksi = $sm->get('JenisTransaksiBphtbTable');
        }
        return $this->tbl_jenistransaksi;
    }

    public function getTblHakTan() {
        if (!$this->tbl_haktanah) {
            $sm = $this->getServiceLocator();
            $this->tbl_haktanah = $sm->get('HakTanahTable');
        }
        return $this->tbl_haktanah;
    }

    public function populateComboJenisTransaksi() {
        $data = $this->getTblJenTran()->comboBox();
        $selectData = array();
        foreach ($data as $row) {
            $selectData[$row->s_idjenistransaksi] = $row->s_namajenistransaksi;
        }
        return $selectData;
    }

    public function populateComboHakTanah() {
        $data = $this->getTblHakTan()->comboBox();
        $selectData = array();
        foreach ($data as $row) {
            $selectData[$row->s_idhaktanah] = $row->s_namahaktanah;
        }
        return $selectData;
    }

    public function getPemda() {
        if (!$this->tbl_pemda) {
            $sm = $this->getServiceLocator();
            $this->tbl_pemda = $sm->get("PemdaTable");
        }
        return $this->tbl_pemda;
    }
    
    public function getTblPejabat() {
        if (!$this->tbl_pejabat) {
            $sm = $this->getServiceLocator();
            $this->tbl_pejabat = $sm->get("PejabatBphtbTable");
        }
        return $this->tbl_pejabat;
    }

}
