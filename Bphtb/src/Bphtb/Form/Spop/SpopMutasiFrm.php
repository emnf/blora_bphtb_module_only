<?php

namespace Bphtb\Form\Spop;

use Zend\Form\Form;

class SpopMutasiFrm extends Form
{

    public function __construct(
        $t_jenis_kepemilikan = null,
        $t_pekerjaan_wp = null,
        $t_kode_blok_op = null,
        $t_kelurahan_op = null,
        $t_kecamatan_op = null,
        $t_jenis_tanah = null,
        $t_kd_znt = null
    ) {
        parent::__construct();

        $this->setAttribute("method", "post");

        $this->add(array(
            'name' => 'id',
            'type' => 'hidden',
            'attributes' => array(
                'id' => 'id'
            )
        ));

        $this->add(array(
            'name' => 't_idspt',
            'type' => 'hidden',
            'attributes' => array(
                'id' => 't_idspt'
            )
        ));

        $this->add(array(
            'name' => 't_jenis_kepemilikan',
            'type' => 'Zend\Form\Element\Select',
            'attributes' => array(
                'id' => 't_jenis_kepemilikan',
                'class' => 'form-control',
                'required' => true,
            ),
            'options' => array(
                'empty_option' => 'Silahkan Pilih',
                'value_options' => $t_jenis_kepemilikan,
                'disable_inarray_validator' => true, // <-- disable
            )
        ));

        $this->add(array(
            'name' => 't_pekerjaan_wp',
            'type' => 'Zend\Form\Element\Select',
            'attributes' => array(
                'id' => 't_pekerjaan_wp',
                'class' => 'form-control',
                'required' => true,
            ),
            'options' => array(
                'empty_option' => 'Silahkan Pilih',
                'value_options' => $t_pekerjaan_wp,
                'disable_inarray_validator' => true, // <-- disable
            )
        ));

        $this->add(array(
            'name' => 't_kode_blok_op',
            'type' => 'Zend\Form\Element\Select',
            'attributes' => array(
                'id' => 't_kode_blok_op',
                'class' => 'form-control',
                'required' => true,
            ),
            'options' => array(
                'empty_option' => 'Silahkan Pilih',
                'value_options' => $t_kode_blok_op,
                'disable_inarray_validator' => true, // <-- disable
            )
        ));

        $this->add(array(
            'name' => 't_kelurahan_op',
            'type' => 'Zend\Form\Element\Select',
            'attributes' => array(
                'id' => 't_kelurahan_op',
                'class' => 'form-control',
                'required' => true,
            ),
            'options' => array(
                'empty_option' => 'Silahkan Pilih',
                'value_options' => $t_kelurahan_op,
                'disable_inarray_validator' => true, // <-- disable
            )
        ));

        $this->add(array(
            'name' => 't_kecamatan_op',
            'type' => 'Zend\Form\Element\Select',
            'attributes' => array(
                'id' => 't_kecamatan_op',
                'class' => 'form-control',
                'required' => true,
            ),
            'options' => array(
                'empty_option' => 'Silahkan Pilih',
                'value_options' => $t_kecamatan_op,
                'disable_inarray_validator' => true, // <-- disable
            )
        ));

        $this->add(array(
            'name' => 't_jenis_tanah',
            'type' => 'Zend\Form\Element\Select',
            'attributes' => array(
                'id' => 't_jenis_tanah',
                'class' => 'form-control',
                'required' => true,
                'onchange' => "cariJenisTanah()"
            ),
            'options' => array(
                'empty_option' => 'Silahkan Pilih',
                'value_options' => $t_jenis_tanah,
                'disable_inarray_validator' => true, // <-- disable
            )
        ));

        $this->add(array(
            'name' => 't_nik_wp',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_nik_wp',
                'class' => 'form-control',
                'required' => true,
                'onkeypres' => 'return numbersonly(this, event)',
                'maxlength' => 16,
                'readonly' => true
            )
        ));

        $this->add(array(
            'name' => 't_nama_wp',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_nama_wp',
                'class' => 'form-control',
                'required' => true,
                'maxlength' => 30,
                'readonly' => true
            )
        ));

        $this->add(array(
            'name' => 't_rt_wp',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_rt_wp',
                'class' => 'form-control',
                'required' => true,
                'onkeypres' => 'return numbersonly(this, event)',
                'maxlength' => 2,
                'readonly' => true
            )
        ));

        $this->add(array(
            'name' => 't_rw_wp',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_rw_wp',
                'class' => 'form-control',
                'required' => true,
                'maxlength' => 2,
                'onkeypres' => 'return numbersonly(this, event)',
                'readonly' => true
            )
        ));

        $this->add(array(
            'name' => 't_jalan_wp',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_jalan_wp',
                'class' => 'form-control',
                'required' => true,
                'maxlength' => 40,
                'readonly' => true
            )
        ));

        $this->add(array(
            'name' => 't_kelurahan_wp',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_kelurahan_wp',
                'class' => 'form-control',
                'required' => true,
                'maxlength' => 40,
                'readonly' => true
            )
        ));

        $this->add(array(
            'name' => 't_kecamatan_wp',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_kecamatan_wp',
                'class' => 'form-control',
                'required' => true,
                'maxlength' => 40,
                'readonly' => true
            )
        ));

        $this->add(array(
            'name' => 't_kabupaten_wp',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_kabupaten_wp',
                'class' => 'form-control',
                'required' => true,
                'maxlength' => 30,
                'readonly' => true
            )
        ));

        $this->add(array(
            'name' => 't_no_hp_wp',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_no_hp_wp',
                'class' => 'form-control',
                'required' => true,
                'maxlength' => 12,
                'onkeypres' => 'return numbersonly(this, event)',
                'readonly' => true
            )
        ));

        $this->add(array(
            'name' => 't_nop',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_nop',
                'class' => 'form-control',
                'required' => true,
                'data-inputmask' => '"mask": "99.99.999.999.999.9999.9"',
                'data-mask' => true,
                'readonly' => true
            )
        ));

        $this->add(array(
            'name' => 't_nop_terdekat',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_nop_terdekat',
                'class' => 'form-control',
                // 'required' => true
            )
        ));

        $this->add(array(
            'name' => 'kd_propinsi',
            'type' => 'text',
            'attributes' => array(
                'id' => 'kd_propinsi',
                'class' => 'form-control',
                // 'required' => true
            )
        ));

        $this->add(array(
            'name' => 'kd_dati2',
            'type' => 'text',
            'attributes' => array(
                'id' => 'kd_dati2',
                'class' => 'form-control',
                // 'required' => true
            )
        ));

        $this->add(array(
            'name' => 'kd_kecamatan',
            'type' => 'text',
            'attributes' => array(
                'id' => 'kd_kecamatan',
                'class' => 'form-control',
                // 'required' => true
            )
        ));

        $this->add(array(
            'name' => 'kd_kelurahan',
            'type' => 'text',
            'attributes' => array(
                'id' => 'kd_kelurahan',
                'class' => 'form-control',
                // 'required' => true
            )
        ));

        $this->add(array(
            'name' => 'kd_blok',
            'type' => 'text',
            'attributes' => array(
                'id' => 'kd_blok',
                'class' => 'form-control',
                // 'required' => true
            )
        ));

        $this->add(array(
            'name' => 'no_urut',
            'type' => 'text',
            'attributes' => array(
                'id' => 'no_urut',
                'class' => 'form-control',
                // 'required' => true
            )
        ));

        $this->add(array(
            'name' => 'kd_jns_op',
            'type' => 'text',
            'attributes' => array(
                'id' => 'kd_jns_op',
                'class' => 'form-control',
                // 'required' => true
            )
        ));

        $this->add(array(
            'name' => 't_rt_op',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_rt_op',
                'class' => 'form-control',
                'required' => true,
                'maxlength' => 2,
                'onkeypres' => 'return numbersonly(this, event)',
                'readonly' => true
            )
        ));

        $this->add(array(
            'name' => 't_rw_op',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_rw_op',
                'class' => 'form-control',
                'required' => true,
                'maxlength' => 2,
                'onkeypres' => 'return numbersonly(this, event)',
                'readonly' => true
            )
        ));

        $this->add(array(
            'name' => 't_jalan_op',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_jalan_op',
                'class' => 'form-control',
                'required' => true,
                'maxlength' => 40,
                'readonly' => true
            )
        ));

        $this->add(array(
            'name' => 't_luas_tanah',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_luas_tanah',
                'class' => 'form-control',
                'required' => true,
                'onkeypres' => 'return numbersonly(this, event)',
                'readonly' => true
            )
        ));

        $this->add(array(
            'name' => 't_kd_znt',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_kd_znt',
                'class' => 'form-control',
                'required' => true,
                'onkeypres' => 'return numbersonly(this, event)',
                'readonly' => true
            )
        ));

        // $this->add(array(
        //     'name' => 't_kd_znt',
        //     'type' => 'Zend\Form\Element\Select',
        //     'attributes' => array(
        //         'id' => 't_kd_znt',
        //         'class' => 'form-control',
        //         'required' => true,
        //     ),
        //     'options' => array(
        //         'empty_option' => 'Silahkan Pilih',
        //         'value_options' => $t_kd_znt,
        //         'disable_inarray_validator' => true, // <-- disable
        //     )
        // ));

        $this->add(array(
            'name' => 't_jmlh_bangunan',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_jmlh_bangunan',
                'class' => 'form-control',
                // 'required' => true
            )
        ));

        $this->add(array(
            'name' => 't_latitude',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_latitude',
                'class' => 'form-control',
                // 'required' => true
            )
        ));

        $this->add(array(
            'name' => 't_longitude',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_longitude',
                'class' => 'form-control',
                // 'required' => true
            )
        ));

        $this->add(array(
            'name' => 't_id_jenis_pelayanan',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_id_jenis_pelayanan',
                'class' => 'form-control',
                'required' => true
            )
        ));

        // $this->add(array(
        //     'name' => 't_nomor_bidang',
        //     'type' => 'text',
        //     'attributes' => array(
        //         'id' => 't_nomor_bidang',
        //         'class' => 'form-control',
        //         // 'required' => true
        //     )
        // ));

        $this->add(array(
            'name' => 't_nomor_bidang',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_nomor_bidang',
                'class' => 'form-control',
                'required' => true
            )
        ));

        $this->add(array(
            'name' => 't_jmlh_bidang',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_jmlh_bidang',
                'class' => 'form-control',
                'required' => true
            )
        ));

        $this->add(array(
            'name' => 't_nomor_sertifikat',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_nomor_sertifikat',
                'class' => 'form-control',
                'required' => true,
                'maxlength' => 5,
                'readonly' => true
            )
        ));

        $this->add(array(
            'name' => 't_blok_op',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_blok_op',
                'class' => 'form-control',
                // 'required' => true,
                'maxlength' => 15
            )
        ));

        $this->add(array(
            'name' => 't_blok_wp',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_blok_wp',
                'class' => 'form-control',
                'maxlength' => 5
                // 'required' => true
            )
        ));

        $this->add(array(
            'name' => 't_npwp_wp',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_npwp_wp',
                'class' => 'form-control',
                // 'required' => true,

                'data-inputmask' => '"mask": "99.999.999.9-999.99"',
                'data-mask' => true,
                'readonly' => true
            )
        ));

        $this->add(array(
            'name' => 't_kode_pos_wp',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_kode_pos_wp',
                'class' => 'form-control',
                // 'required' => true,
                'maxlength' => 5,
                'onkeypres' => 'return numbersonly(this, event)',
                'readonly' => true
            )
        ));

        $this->add(array(
            'name' => 't_nop_asal',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_nop_asal',
                'class' => 'form-control',
                // 'required' => true,
                'data-inputmask' => '"mask": "99.99.999.999.999.9999.9"',
                'data-mask' => true,
                'readonly' => true
            )
        ));

        $this->add(array(
            'name' => 't_keterangan',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_keterangan',
                'class' => 'form-control',
                // 'required' => true
            )
        ));

        $this->add(array(
            'name' => 'thn_pelayanan',
            'type' => 'text',
            'attributes' => array(
                'id' => 'thn_pelayanan',
                'class' => 'form-control',
                // 'required' => true
            )
        ));

        $this->add(array(
            'name' => 'bundel_pelayanan',
            'type' => 'text',
            'attributes' => array(
                'id' => 'bundel_pelayanan',
                'class' => 'form-control',
                // 'required' => true
            )
        ));

        $this->add(array(
            'name' => 'no_urut_pelayanan',
            'type' => 'text',
            'attributes' => array(
                'id' => 'no_urut_pelayanan',
                'class' => 'form-control',
                // 'required' => true
            )
        ));

        $this->add(array(
            'name' => 't_tahun_spop',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_tahun_spop',
                'class' => 'form-control',
                // 'required' => true
            )
        ));

        $this->add(array(
            'name' => 't_no_urut_spop',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_no_urut_spop',
                'class' => 'form-control',
                // 'required' => true
            )
        ));

        $this->add(array(
            'name' => 't_no_spop',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_no_spop',
                'class' => 'form-control',
                // 'required' => true
            )
        ));

        $this->add(array(
            'name' => 't_id_spop',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_id_spop',
                'class' => 'form-control',
                // 'required' => true
            )
        ));

        $this->add(array(
            'name' => 't_id_permohonan',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_id_permohonan',
                'class' => 'form-control',
                // 'required' => true
            )
        ));
    }
}
