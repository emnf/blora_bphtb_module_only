<?php

namespace Bphtb\Form\Pendataan;

use Zend\Form\Form;

class SPOPSptFrm extends Form {

    public function __construct($combojenistransaksi = null, $combohaktanah = null, $combojenisdoktanah = null, $unix_id = null, $combonotaris = null, $persyaratan = null, $combonotaris1 = null) {
        parent::__construct();

        $this->setAttribute("method", "post");

        // ============================================================================================================== //
        // SPOP //
        $this->add(array(
            'name' => 't_idjenistransaksi',
            'type' => 'Zend\Form\Element\Select',
            'attributes' => array(
                'id' => 't_idjenistransaksi',
                'class' => 'selectpicker selectpicker1 form-control',
                'onChange' => 'TampilPersyaratan();hitungBphtb();', //TampilKeringanan();
                'onBlur' => 'TampilPersyaratan();hitungBphtb();', //TampilKeringanan();
                'data-live-search' => true,
                
            ),
            'options' => array(
                'empty_option' => 'Silahkan Pilih',
                'value_options' => $combojenistransaksi,
                'disable_inarray_validator' => true, // <-- disable
            )
        ));

        $this->add(array(
            'name' => 't_idjenisdoktanah',
            'type' => 'Zend\Form\Element\Select',
            'attributes' => array(
                'id' => 't_idjenisdoktanah',
                'class' => 'selectpicker form-control', //selectpicker1
                'data-live-search' => true,
                
            ),
            'options' => array(
                'empty_option' => 'Silahkan Pilih',
                'value_options' => $combojenisdoktanah,
                'disable_inarray_validator' => true, // <-- disable
            )
        ));

        $this->add(array(
            'name' => 't_idnotarisspt',
            'type' => 'Zend\Form\Element\Select',
            'attributes' => array(
                'id' => 't_idnotarisspt',
                'class' => 'selectpicker form-control',
                'data-live-search' => true,
                
            ),
            'options' => array(
                'empty_option' => 'Silahkan Pilih',
                'value_options' => $combonotaris,
                'disable_inarray_validator' => true, // <-- disable
            )
        ));

        $this->add(array(
            'name' => 't_idnotarisspt1',
            'type' => 'Zend\Form\Element\Select',
            'attributes' => array(
                'id' => 't_idnotarisspt1',
                'class' => 'selectpicker form-control',
                'data-live-search' => true
            ),
            'options' => array(
                'empty_option' => 'Silahkan Pilih',
                'value_options' => $combonotaris1,
                'disable_inarray_validator' => true, // <-- disable
            )
        ));

        $this->add(array(
            'name' => 't_kodebayarbanksppt',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_kodebayarbanksppt',
                'class' => 'form-control',
                'value' => $unix_id,
                'readonly' => true
            )
        ));

        $this->add(array(
            'name' => 't_persyaratan',
            'type' => 'Zend\Form\Element\MultiCheckbox',
            'attributes' => array(
                'id' => 't_persyaratan',
            //'class' => 'form-control',                
            ),
            'options' => array(
                'value_options' => $persyaratan,
                'disable_inarray_validator' => true,
            )
        ));

        $this->add(array(
            'name' => 't_idspt',
            'type' => 'hidden',
            'options' => array(
                'label' => 'ID SPT'
            ),
            'attributes' => array(
                'id' => 't_idspt'
            )
        ));

        $this->add(array(
            'name' => 't_iddetailsptbphtb',
            'type' => 'hidden',
            'attributes' => array(
                'id' => 't_iddetailsptbphtb'
            )
        ));

        $this->add(array(
            'name' => 't_kohirspt',
            'type' => 'hidden',
            'attributes' => array(
                'id' => 't_kohirspt'
            )
        ));
        
        $this->add(array(
            'name' => 't_idds',
            'type' => 'hidden',
            'attributes' => array(
                'id' => 't_idds'
            )
        ));

        $this->add(array(
            'name' => 't_tglprosesspt',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_tglprosesspt',
                'class' => 'form-control',
                'value' => date('d-m-Y'),
                
            )
        ));

        $this->add(array(
            'name' => 't_jenistransaksispop',
            'type' => 'Zend\Form\Element\Select',
            'attributes' => array(
                'id' => 't_jenistransaksispop',
                'class' => 'form-control',
                'data-live-search' => true,
                'required' => 'true'
            ),
            'options' => array(
                'value_options' => array(
                    '1' => ' Perekaman Data',
                    '2' => ' Pemutakhiran Data',
                    '3' => ' Penghapusan Data',
                ),
                'disable_inarray_validator' => true, // <-- disable
            )
        ));

        $this->add(array(
            'name' => 't_nopbphtbspop',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_nopbphtbspop',
                'data-inputmask' => '"mask": "99.99.999.999.999.9999.9"',
                'data-mask' => true,
                'placeholder' => '__.__.___.___.___.____._',
                'class' => 'form-control',
                
            )
        ));

        $this->add(array(
            'name' => 't_nopbphtbbersama',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_nopbphtbbersama',
                'data-inputmask' => '"mask": "99.99.999.999.999.9999.9"',
                'data-mask' => true,
                'placeholder' => '__.__.___.___.___.____._',
                'class' => 'form-control',
                
            )
        ));

        // informasi tambahan data baru
        $this->add(array(
            'name' => 't_nopbphtbasal',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_nopbphtbasal',
                'data-inputmask' => '"mask": "99.99.999.999.999.9999.9"',
                'data-mask' => true,
                'placeholder' => '__.__.___.___.___.____._',
                'class' => 'form-control',
                
            )
        ));


         $this->add(array(
            'name' => 't_nospptlamaspop',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_nospptlamaspop',
                'data-inputmask' => '"mask": "9999"',
                'data-mask' => true,
                'placeholder' => '____',
                'class' => 'form-control',
                
            )
        ));

         $this->add(array(
            'name' => 't_statusspop',
            'type' => 'Zend\Form\Element\Select',
            'attributes' => array(
                'id' => 't_statusspop',
                'class' => 'form-control',
                'data-live-search' => true,
                'required' => 'true',
            ),
            'options' => array(
                'value_options' => array(
                    '1' => ' Pemilik',
                    '2' => ' Penyewa',
                    '3' => ' Pengelola',
                    '4' => ' Pemakai',
                    '5' => ' Sengketa',
                ),
                'disable_inarray_validator' => true, // <-- disable
            )
        ));

         $this->add(array(
            'name' => 't_pekerjaanspop',
            'type' => 'Zend\Form\Element\Select',
            'attributes' => array(
                'id' => 't_pekerjaanspop',
                'class' => 'form-control',
                'data-live-search' => true,
                'required' => 'true',
            ),
            'options' => array(
                'value_options' => array(
                    '1' => ' PNS',
                    '2' => ' TNI',
                    '3' => ' POLRI',
                    '4' => ' Pensiunan',
                    '5' => ' Badan',
                    '6' => ' Lainnya',
                ),
                'disable_inarray_validator' => true, // <-- disable
            )
        ));


        $this->add(array(
            'name' => 't_nikspop',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_nikspop',
                'class' => 'form-control',
                'data-inputmask' => '"mask": "9999999999999999"',
                'data-mask' => true,
                'placeholder' => '________________',
                'maxlength' => 16,
                'onchange' => 'historybphtb();',
                'onblur' => 'historybphtb();',
                
            )
        ));

        $this->add(array(
            'name' => 't_npwpspop',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_npwpspop',
                'data-inputmask' => '"mask": "P.99999999.99.99"',
                'data-mask' => true,
                'placeholder' => '_.________.__.__',
                'class' => 'form-control',
            )
        ));

         $this->add(array(
            'name' => 't_namasubjekpajak',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_namasubjekpajak',
                'class' => 'form-control',
                
            )
        ));

         $this->add(array(
            'name' => 't_gelarsubjekpajak',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_gelarsubjekpajak',
                'class' => 'form-control',
                
            )
        ));

        $this->add(array(
            'name' => 't_namajalansubjekpajak',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_namajalansubjekpajak',
                'class' => 'form-control',
                
            )
        ));


         $this->add(array(
            'name' => 't_bloksubjekpajak',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_bloksubjekpajak',
                'class' => 'form-control',
                
            )
        ));

          $this->add(array(
            'name' => 't_rtsubjekpajak',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_rtsubjekpajak',
                'class' => 'form-control',
                'maxlength' => 3,
                'onKeyPress' => "return numbersonly(this, event)",
                
            )
        ));

        $this->add(array(
            'name' => 't_rwsubjekpajak',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_rwsubjekpajak',
                'class' => 'form-control',
                'maxlength' => 2,
                'onKeyPress' => "return numbersonly(this, event)",
                
            )
        ));
        
        $this->add(array(
            'name' => 't_kelurahsubjekpajak',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_kelurahsubjekpajak',
                'class' => 'form-control',

            )
        ));

        $this->add(array(
            'name' => 't_kecamatansubjekpajak',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_kecamatansubjekpajak',
                'class' => 'form-control',
                
            )
        ));

        $this->add(array(
            'name' => 't_kabupatenssubjekpajak',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_kabupatenssubjekpajak',
                'class' => 'form-control',
                'value' => 'BLORA',
                
            )
        ));

        $this->add(array(
            'name' => 't_kodepossubjekpajak',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_kodepossubjekpajak',
                'class' => 'form-control',
                'data-inputmask' => '"mask": "99999"',
                'data-mask' => true,
                'placeholder' => '_____',
            )
        ));

        // letak objek pajak //
        $this->add(array(
            'name' => 't_nomorpersilop',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_nomorpersilop',
                'class' => 'form-control',
                'data-inputmask' => '"mask": "99999"',
                'data-mask' => true,
                'placeholder' => '_____',
            )
        ));

        $this->add(array(
            'name' => 't_namajalanop',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_namajalanop',
                'class' => 'form-control',
                
            )
        ));


         $this->add(array(
            'name' => 't_blokop',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_blokop',
                'class' => 'form-control',
                
            )
        ));

          $this->add(array(
            'name' => 't_rtopsopo',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_rtopsopo',
                'class' => 'form-control',
                'maxlength' => 3,
                'onKeyPress' => "return numbersonly(this, event)",
                
            )
        ));

        $this->add(array(
            'name' => 't_rwopspop',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_rwopspop',
                'class' => 'form-control',
                'maxlength' => 2,
                'onKeyPress' => "return numbersonly(this, event)",
                
            )
        ));
        
        $this->add(array(
            'name' => 't_kelurahopspop',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_kelurahopspop',
                'class' => 'form-control',

            )
        ));

        // data tanah //
        $this->add(array(
            'name' => 't_luastanahspop',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_luastanahspop',
                'class' => 'form-control',
                'onKeyPress' => "return numbersonly(this, event)",
                'onkeyup' => 'hitungBphtb();this.value = formatCurrency(this.value);', //tambahi hitungBphtb(); //hitungnjop();
                'onchange' => 'hitungBphtb();this.value = formatCurrency(this.value);', //hitungnjop();
                'onblur' => 'hitungBphtb();this.value = formatCurrency(this.value);', //hitungnjop();
                'onfocus' => 'this.value = unformatCurrency(this.value)',
            )
        ));

        $this->add(array(
            'name' => 't_zonanilaitanah',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_zonanilaitanah',
                'class' => 'form-control',
                'data-inputmask' => '"mask": "AA"',
                'data-mask' => true,
                'placeholder' => '__',
            )
        ));

        $this->add(array(
            'name' => 't_jenistanahspop',
            'type' => 'Zend\Form\Element\Select',
            'attributes' => array(
                'id' => 't_jenistanahspop',
                'class' => 'form-control',
                'data-live-search' => true,
                'required' => 'true',
            ),
            'options' => array(
                'value_options' => array(
                    '1' => ' Tanah Bangunan',
                    '2' => ' Kavling Siap Bangun',
                    '3' => ' Tanah Kosong',
                    '4' => ' Fasilitas Umum',
                ),
                'disable_inarray_validator' => true, // <-- disable
            )
        ));

        $this->add(array(
            'name' => 't_kepemilikanspptspop',
            'type' => 'Zend\Form\Element\Select',
            'attributes' => array(
                'id' => 't_kepemilikanspptspop',
                'class' => 'form-control',
                'data-live-search' => true,
                'required' => 'true',
            ),
            'options' => array(
                'value_options' => array(
                    '1' => ' SPPT Milik Sendiri',
                    '2' => ' SPPT Milik Tetangga',
                    
                ),
                'disable_inarray_validator' => true, // <-- disable
            )
        ));

        // data bangunan //
        $this->add(array(
            'name' => 't_jumlahbangunanspop',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_jumlahbangunanspop',
                'class' => 'form-control',
                'data-inputmask' => '"mask": "999"',
                'data-mask' => true,
                'placeholder' => '___',
            )
        ));

        // ========================================================================================================== //
        // LSPOP //
         // data bangunan //
         $this->add(array(
            'name' => 't_urutanbangunanlspop',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_urutanbangunanlspop',
                'class' => 'form-control',
                'data-inputmask' => '"mask": "999"',
                'data-mask' => true,
                'placeholder' => '___',
            )
        ));

        $this->add(array(
            'name' => 't_idjenishaktanah',
            'type' => 'Zend\Form\Element\Select',
            'attributes' => array(
                'id' => 't_idjenishaktanah',
                'class' => 'selectpicker form-control',
                'data-live-search' => true,
                
            ),
            'options' => array(
                'empty_option' => 'Silahkan Pilih',
                'value_options' => $combohaktanah,
                'disable_inarray_validator' => true, // <-- disable
            )
        ));

         $this->add(array(
            'name' => 't_jenispenggunaanbangunanlspop',
            'type' => 'Zend\Form\Element\Select',
            'attributes' => array(
                'id' => 't_jenispenggunaanbangunanlspop',
                'class' => 'form-control',
                'data-live-search' => true,
                'required' => 'true'
            ),
            'options' => array(
                'value_options' => array(
                    '1' => ' Perumahan',
                    '2' => ' Perkantoran Swasta',
                    '3' => ' Pabrik',
                    '4' => ' Toko/Apotik/Pasar/Ruko',
                    '5' => ' Rumah Sakit/Klinik',
                    '6' => ' Olah Raga/Rekreasi',
                    '7' => ' Hotel/Wisma',
                    '8' => ' Bengkel/Gudang/Pertanian',
                    '9' => ' Gedung Pemerintahan',
                    '10' => ' Bangunan Tidak Kena Pajak',
                    '11' => ' Bangunan Parkir',
                    '12' => ' Apartemen/Rumah Susun',
                    '13' => ' Pompa Bensin',
                    '14' => ' Tangki Minyak',
                    '15' => ' Gedung Sekolah',
                    '16' => ' Lain-Lain',
                ),
                'disable_inarray_validator' => true, // <-- disable
            )
        ));

        $this->add(array(
            'name' => 't_luasbangunanlspop',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_luasbangunanlspop',
                'class' => 'form-control',
                
            )
        ));

        $this->add(array(
            'name' => 't_jumlahlantailspop',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_jumlahlantailspop',
                'class' => 'form-control',
            )
        ));

       $this->add(array(
            'name' => 't_periodebangunanlspop',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_periodebangunanlspop',
                'class' => 'form-control',
               
            )
       ));

       $this->add(array(
            'name' => 't_dayalistriklspop',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_dayalistriklspop',
                'class' => 'form-control',
            )
        ));

       $this->add(array(
            'name' => 't_perioderenovasilspop',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_perioderenovasilspop',
                'class' => 'form-control',
                
            )
        ));

        $this->add(array(
            'name' => 't_kondisipadaumumnyalspop',
            'type' => 'Zend\Form\Element\Select',
            'attributes' => array(
                'id' => 't_kondisipadaumumnyalspop',
                'class' => 'form-control',
                'data-live-search' => true,
                'required' => 'true'
            ),
            'options' => array(
                'value_options' => array(
                    '1' => ' Sangat Baik',
                    '2' => ' Baik',
                    '3' => ' Sedang',
                    '4' => ' Jelek',

                ),
                'disable_inarray_validator' => true, // <-- disable
            )
        ));

         $this->add(array(
            'name' => 't_konstruksilspop',
            'type' => 'Zend\Form\Element\Select',
            'attributes' => array(
                'id' => 't_konstruksilspop',
                'class' => 'form-control',
                'data-live-search' => true,
                'required' => 'true'
            ),
            'options' => array(
                'value_options' => array(
                    '1' => ' Baja',
                    '2' => ' Beton/Alumunium',
                    '3' => ' Batu Bata',
                    '4' => ' Kayu',

                ),
                'disable_inarray_validator' => true, // <-- disable
            )
        ));

        $this->add(array(
            'name' => 't_ataplspop',
            'type' => 'Zend\Form\Element\Select',
            'attributes' => array(
                'id' => 't_ataplspop',
                'class' => 'form-control',
                'data-live-search' => true,
                'required' => 'true'
            ),
            'options' => array(
                'value_options' => array(
                    '1' => ' Decarbone/Beton/Gtg Glazur',
                    '2' => ' Gtg Beton/Alumunium',
                    '3' => ' Gtg Biasa/Sirap',
                    '4' => ' Asbes/Seng',
                    '5' => ' Tidak Ada',
                ),
                'disable_inarray_validator' => true, // <-- disable
            )
        ));

        $this->add(array(
            'name' => 't_dindinglspop',
            'type' => 'Zend\Form\Element\Select',
            'attributes' => array(
                'id' => 't_dindinglspop',
                'class' => 'form-control',
                'data-live-search' => true,
                'required' => 'true'
            ),
            'options' => array(
                'value_options' => array(
                    '1' => ' Kaca/Alumunium',
                    '2' => ' Beton',
                    '3' => ' Batu Bata/Comblok',
                    '4' => ' Kayu/Seng',
                    '5' => ' Tidak Ada',
                ),
                'disable_inarray_validator' => true, // <-- disable
            )
        ));

        $this->add(array(
            'name' => 't_lantailspop',
            'type' => 'Zend\Form\Element\Select',
            'attributes' => array(
                'id' => 't_lantailspop',
                'class' => 'form-control',
                'data-live-search' => true,
                'required' => 'true'
            ),
            'options' => array(
                'value_options' => array(
                    '1' => ' Marmer',
                    '2' => ' Keramik/Batu Ayam',
                    '3' => ' Teraso',
                    '4' => ' UbinPC/Papan',
                    '5' => ' Semen/Tanah',
                ),
                'disable_inarray_validator' => true, // <-- disable
            )
        ));

        $this->add(array(
            'name' => 't_langit_langitlspop',
            'type' => 'Zend\Form\Element\Select',
            'attributes' => array(
                'id' => 't_langit_langitlspop',
                'class' => 'form-control',
                'data-live-search' => true,
                'required' => 'true'
            ),
            'options' => array(
                'value_options' => array(
                    '1' => ' Akustik/Jati',
                    '2' => ' Triplrek/Asbes/Bambu',
                    '3' => ' Semen/Tanah',
                ),
                'disable_inarray_validator' => true, // <-- disable
            )
        ));


        // fasilitas //
        $this->add(array(
            'name' => 't_jenisaclspop',
            'type' => 'Zend\Form\Element\Select',
            'attributes' => array(
                'id' => 't_jenisaclspop',
                'class' => 'form-control',
                'data-live-search' => true,
                'required' => 'true'
            ),
            'options' => array(
                'value_options' => array(
                    '1' => ' Split',
                    '2' => ' Window'
                ),
                'disable_inarray_validator' => true, // <-- disable
            )
        ));

        $this->add(array(
            'name' => 't_jumlahaclspop',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_jumlahaclspop',
                'class' => 'form-control',
                
            )
        ));

        $this->add(array(
            'name' => 't_accentral',
            'type' => 'Zend\Form\Element\Select',
            'attributes' => array(
                'id' => 't_accentral',
                'class' => 'form-control',
                'data-live-search' => true,
                'required' => 'true'
            ),
            'options' => array(
                'value_options' => array(
                    '1' => ' Ada',
                    '2' => ' Tidak Ada'
                ),
                'disable_inarray_validator' => true, // <-- disable
            )
        ));

        $this->add(array(
            'name' => 't_jeniskolamrenang',
            'type' => 'Zend\Form\Element\Select',
            'attributes' => array(
                'id' => 't_jeniskolamrenang',
                'class' => 'form-control',
                'data-live-search' => true,
                'required' => 'true'
            ),
            'options' => array(
                'value_options' => array(
                    '1' => ' Diplester',
                    '2' => ' Dengan Pelapis'
                ),
                'disable_inarray_validator' => true, // <-- disable
            )
        ));

        $this->add(array(
            'name' => 't_luaskolamrenanglspop',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_luaskolamrenanglspop',
                'class' => 'form-control',
                
            )
        ));

        $this->add(array(
            'name' => 't_jenisperkerasanhalaman',
            'type' => 'Zend\Form\Element\Select',
            'attributes' => array(
                'id' => 't_jenisperkerasanhalaman',
                'class' => 'form-control',
                'data-live-search' => true,
                'required' => 'true'
            ),
            'options' => array(
                'value_options' => array(
                    '1' => ' Ringan',
                    '2' => ' Sedang',
                    '3' => ' Berat',
                    '4' => ' Dengan Penutup Lantai',
                ),
                'disable_inarray_validator' => true, // <-- disable
            )
        ));

        $this->add(array(
            'name' => 't_jumlahperkerasanhalaman',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_jumlahperkerasanhalaman',
                'class' => 'form-control',
                
            )
        ));

         $this->add(array(
            'name' => 't_jenislapangantenis',
            'type' => 'Zend\Form\Element\Select',
            'attributes' => array(
                'id' => 't_jenislapangantenis',
                'class' => 'form-control',
                'data-live-search' => true,
                'required' => 'true'
            ),
            'options' => array(
                'value_options' => array(
                    '1' => ' Dengan Lampu',
                    '2' => ' Tanpa Lampu',
                ),
                'disable_inarray_validator' => true, // <-- disable
            )
        ));

         $this->add(array(
            'name' => 't_jumlahlapangantenis',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_jumlahlapangantenis',
                'class' => 'form-control',
                
            )
        ));

        $this->add(array(
            'name' => 't_jenislift',
            'type' => 'Zend\Form\Element\Select',
            'attributes' => array(
                'id' => 't_jenislift',
                'class' => 'form-control',
                'data-live-search' => true,
                'required' => 'true'
            ),
            'options' => array(
                'value_options' => array(
                    '1' => ' Penumpang',
                    '2' => ' Kapsul',
                    '3' => ' Barang',
                ),
                'disable_inarray_validator' => true, // <-- disable
            )
        ));

         $this->add(array(
            'name' => 't_jumlahlift',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_jumlahlift',
                'class' => 'form-control',
                
            )
        ));

        $this->add(array(
            'name' => 't_jenistanggaberjalan',
            'type' => 'Zend\Form\Element\Select',
            'attributes' => array(
                'id' => 't_jenistanggaberjalan',
                'class' => 'form-control',
                'data-live-search' => true,
                'required' => 'true'
            ),
            'options' => array(
                'value_options' => array(
                    '1' => ' Lbr < 0,80',
                    '2' => ' Lbr > 0,80',
                ),
                'disable_inarray_validator' => true, // <-- disable
            )
        ));

         $this->add(array(
            'name' => 't_jumlahtanggaberjalan',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_jumlahtanggaberjalan',
                'class' => 'form-control',
                
            )
        ));

        $this->add(array(
            'name' => 't_bahanpagar',
            'type' => 'Zend\Form\Element\Select',
            'attributes' => array(
                'id' => 't_bahanpagar',
                'class' => 'form-control',
                'data-live-search' => true,
                'required' => 'true'
            ),
            'options' => array(
                'value_options' => array(
                    '1' => ' Baja/Besi',
                    '2' => ' Baja/Batako',
                ),
                'disable_inarray_validator' => true, // <-- disable
            )
        ));

         $this->add(array(
            'name' => 't_panjangpagar',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_panjangpagar',
                'class' => 'form-control',
            )
        ));


        $this->add(array(
            'name' => 't_jenispemadam',
            'type' => 'Zend\Form\Element\Select',
            'attributes' => array(
                'id' => 't_jenispemadam',
                'class' => 'form-control',
                'data-live-search' => true,
                'required' => 'true'
            ),
            'options' => array(
                'value_options' => array(
                    '1' => ' Hydrant',
                    '2' => ' Sprinkler',
                    '3' => ' Fire Alrm',
                ),
                'disable_inarray_validator' => true, // <-- disable
            )
        ));

         $this->add(array(
            'name' => 't_jenispemadam2',
            'type' => 'Zend\Form\Element\Select',
            'attributes' => array(
                'id' => 't_jenispemadam2',
                'class' => 'form-control',
                'data-live-search' => true,
                'required' => 'true'
            ),
            'options' => array(
                'value_options' => array(
                    '1' => ' Ada',
                    '2' => ' Tidak/Ada',
                ),
                'disable_inarray_validator' => true, // <-- disable
            )
        ));
         
        $this->add(array(
            'name' => 't_jumlahsaluranpespabx',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_jumlahsaluranpespabx',
                'class' => 'form-control',
            )
        ));

        $this->add(array(
            'name' => 't_kedalamansumurartesis',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_kedalamansumurartesis',
                'class' => 'form-control',
            )
        ));

        $this->add(array(
            'name' => 't_tinggikolom',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_tinggikolom',
                'class' => 'form-control',
            )
        ));

        $this->add(array(
            'name' => 't_dayadukunglantai',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_dayadukunglantai',
                'class' => 'form-control',
            )
        ));

        $this->add(array(
            'name' => 't_lebarbentang',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_lebarbentang',
                'class' => 'form-control',
            )
        ));

        $this->add(array(
            'name' => 't_kelilingdinding',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_kelilingdinding',
                'class' => 'form-control',
            )
        ));

        $this->add(array(
            'name' => 't_luasmezzinane',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_luasmezzinane',
                'class' => 'form-control',
            )
        ));     

        $this->add(array(
            'name' => 't_kelasbngkantor',
            'type' => 'Zend\Form\Element\Select',
            'attributes' => array(
                'id' => 't_kelasbngkantor',
                'class' => 'form-control',
                'data-live-search' => true,
                'required' => 'true'
            ),
            'options' => array(
                'value_options' => array(
                    '1' => ' Kelas 1',
                    '2' => ' Kelas 2',
                    '3' => ' Kelas 3',
                    '4' => ' Kelas 4',
                ),
                'disable_inarray_validator' => true, // <-- disable
            )
        ));

        $this->add(array(
            'name' => 't_kelasbngtoko',
            'type' => 'Zend\Form\Element\Select',
            'attributes' => array(
                'id' => 't_kelasbngtoko',
                'class' => 'form-control',
                'data-live-search' => true,
                'required' => 'true'
            ),
            'options' => array(
                'value_options' => array(
                    '1' => ' Kelas 1',
                    '2' => ' Kelas 2',
                    '3' => ' Kelas 3',
                ),
                'disable_inarray_validator' => true, // <-- disable
            )
        ));

        $this->add(array(
            'name' => 't_kelasbngrs',
            'type' => 'Zend\Form\Element\Select',
            'attributes' => array(
                'id' => 't_kelasbngrs',
                'class' => 'form-control',
                'data-live-search' => true,
                'required' => 'true'
            ),
            'options' => array(
                'value_options' => array(
                    '1' => ' Kelas 1',
                    '2' => ' Kelas 2',
                    '3' => ' Kelas 3',
                    '4' => ' Kelas 4',
                ),
                'disable_inarray_validator' => true, // <-- disable
            )
        ));

        $this->add(array(
            'name' => 't_luaskamarrs',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_luaskamarrs',
                'class' => 'form-control',
            )
        )); 

        $this->add(array(
            'name' => 't_luasruanglainrs',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_luasruanglainrs',
                'class' => 'form-control',
            )
        ));
        
        $this->add(array(
            'name' => 't_kelasbngolahraga',
            'type' => 'Zend\Form\Element\Select',
            'attributes' => array(
                'id' => 't_kelasbngolahraga',
                'class' => 'form-control',
                'data-live-search' => true,
                'required' => 'true'
            ),
            'options' => array(
                'value_options' => array(
                    '1' => ' Kelas 1',
                    '2' => ' Kelas 2',
                ),
                'disable_inarray_validator' => true, // <-- disable
            )
        ));

        $this->add(array(
            'name' => 't_jenishotellspop',
            'type' => 'Zend\Form\Element\Select',
            'attributes' => array(
                'id' => 't_jenishotellspop',
                'class' => 'form-control',
                'data-live-search' => true,
                'required' => 'true'
            ),
            'options' => array(
                'value_options' => array(
                    '1' => ' Non Resort',
                    '2' => ' Resort',
                ),
                'disable_inarray_validator' => true, // <-- disable
            )
        ));

        $this->add(array(
            'name' => 't_kelasbintanghotel',
            'type' => 'Zend\Form\Element\Select',
            'attributes' => array(
                'id' => 't_kelasbintanghotel',
                'class' => 'form-control',
                'data-live-search' => true,
                'required' => 'true'
            ),
            'options' => array(
                'value_options' => array(
                    '1' => ' Bintang 5',
                    '2' => ' Bintang 4',
                    '3' => ' Bintang 3',
                    '4' => ' Bintang 2',
                    '5' => ' Bintang 1',
                    '6' => ' Non Bintang',
                ),
                'disable_inarray_validator' => true, // <-- disable
            )
        ));

         $this->add(array(
            'name' => 't_jumlahkamarhotel',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_jumlahkamarhotel',
                'class' => 'form-control',
            )
        ));

        $this->add(array(
            'name' => 't_luaskamarhotel',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_luaskamarhotel',
                'class' => 'form-control',
            )
        ));

        $this->add(array(
            'name' => 't_luasruanglainhotel',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_luasruanglainhotel',
                'class' => 'form-control',
            )
        ));

        $this->add(array(
            'name' => 't_kelasbngparkir',
            'type' => 'Zend\Form\Element\Select',
            'attributes' => array(
                'id' => 't_kelasbngparkir',
                'class' => 'form-control',
                'data-live-search' => true,
                'required' => 'true'
            ),
            'options' => array(
                'value_options' => array(
                    '1' => ' Tipe 4',
                    '2' => ' Tipe 3',
                    '3' => ' Tipe 2',
                    '4' => ' Tipe 1',
                ),
                'disable_inarray_validator' => true, // <-- disable
            )
        ));

        $this->add(array(
            'name' => 't_kelasbgnapartemen',
            'type' => 'Zend\Form\Element\Select',
            'attributes' => array(
                'id' => 't_kelasbgnapartemen',
                'class' => 'form-control',
                'data-live-search' => true,
                'required' => 'true'
            ),
            'options' => array(
                'value_options' => array(
                    '1' => ' Kelas 1',
                    '2' => ' Kelas 2',
                    '3' => ' Kelas 3',
                    '4' => ' Kelas 4',
                ),
                'disable_inarray_validator' => true, // <-- disable
            )
        ));

        $this->add(array(
            'name' => 't_jumlahapartemen',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_jumlahapartemen',
                'class' => 'form-control',
            )
        ));

        $this->add(array(
            'name' => 't_luasapartemen',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_luasapartemen',
                'class' => 'form-control',
            )
        ));

        $this->add(array(
            'name' => 't_luasruanglainapartemen',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_luasruanglainapartemen',
                'class' => 'form-control',
            )
        ));

        $this->add(array(
            'name' => 't_kelasbngpompabensin',
            'type' => 'Zend\Form\Element\Select',
            'attributes' => array(
                'id' => 't_kelasbngpompabensin',
                'class' => 'form-control',
                'data-live-search' => true,
                'required' => 'true'
            ),
            'options' => array(
                'value_options' => array(
                    '1' => ' Kelas 1',
                    '2' => ' Kelas 2',
                    '3' => ' Kelas 3',
                    '4' => ' Kelas 4',
                ),
                'disable_inarray_validator' => true, // <-- disable
            )
        ));

        $this->add(array(
            'name' => 't_kapasitastangki',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_kapasitastangki',
                'class' => 'form-control',
            )
        ));

        $this->add(array(
            'name' => 't_letaktangki',
            'type' => 'Zend\Form\Element\Select',
            'attributes' => array(
                'id' => 't_letaktangki',
                'class' => 'form-control',
                'data-live-search' => true,
                'required' => 'true'
            ),
            'options' => array(
                'value_options' => array(
                    '1' => ' Di Atas Tanah',
                    '2' => ' Di Bawah tanah',
                ),
                'disable_inarray_validator' => true, // <-- disable
            )
        ));

        $this->add(array(
            'name' => 't_gedungsekolah',
            'type' => 'Zend\Form\Element\Select',
            'attributes' => array(
                'id' => 't_gedungsekolah',
                'class' => 'form-control',
                'data-live-search' => true,
                'required' => 'true'
            ),
            'options' => array(
                'value_options' => array(
                    '1' => ' Kelas 1',
                    '2' => ' Kelas 2',
                ),
                'disable_inarray_validator' => true, // <-- disable
            )
        ));

        $this->add(array(
            'name' => 't_nilaisistem',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_nilaisistem',
                'class' => 'form-control',
            )
        ));

        $this->add(array(
            'name' => 't_nilaiindividual',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_nilaiindividual',
                'class' => 'form-control',
            )
        ));

        // ===========================================================================================================//
    }

}