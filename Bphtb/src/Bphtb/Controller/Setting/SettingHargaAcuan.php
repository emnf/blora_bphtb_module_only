<?php

namespace Bphtb\Controller\Setting;

class SettingHargaAcuan extends \Zend\Mvc\Controller\AbstractActionController {

    protected $tbl_pemda, $tbl_harga_acuan;

    public function indexAction() {
        $session = $this->getServiceLocator()->get('EtaxService')->getStorage()->read();
        $ar_pemda = $this->getPemda()->getdata();
        $form = new \Bphtb\Form\Setting\HargaAcuanFrm();
        $view = new \Zend\View\Model\ViewModel(array('form' => $form));
        $data = array(
            'menu_setting' => 'active',
            'side_setting' => 'active',
            'side_harga_acuan_tanah' => 'active',
            'role_id' => $session['s_akses'],
            'data_pemda' => $ar_pemda,
            'aturgambar' => 1,
            'username' => $session['s_username']
        );
        $this->layout()->setVariables($data);
        return $view;
    }

    public function dataGridAction() {
        $allParams = (array) $this->getEvent()->getRouteMatch()->getParams();
        $base = new \Bphtb\Model\Setting\HargaAcuanBase();
        $base->exchangeArray($allParams);
        if ($base->direction == 2)
            $base->page = $base->page + 1;
        if ($base->direction == 1)
            $base->page = $base->page - 1;
        if ($base->page <= 0)
            $base->page = 1;
        $page = $base->page;
        $limit = $base->rows;
        $count = $this->getTblAcuan()->getGridCountAcuan($base);
        if ($count > 0 && $limit > 0) {
            $total_pages = ceil($count / $limit);
        } else {
            $total_pages = 0;
        }

        if ($page > $total_pages)
            $page = $total_pages;
        $start = $limit * $page - $limit;
        if ($start < 0)
            $start = 0;
        $data = $this->getTblAcuan()->getGridDataAcuan($base, $start);
        $s = "";
        foreach ($data as $row) {
            $s .= "<tr>";
            $s .= "<td>" . $row['s_kd_propinsi'] . "</td>";
            $s .= "<td>" . $row['s_kd_dati2'] . "</td>";
            $s .= "<td>" . $row['s_kd_kecamatan'] . "</td>";
            $s .= "<td>" . $row['s_kd_kelurahan'] . "</td>";
            $s .= "<td>" . $row['s_kd_blok'] . "</td>";
            $s .= "<td>" . number_format($row['s_permetertanah'], 0, ',', '.') . "</td>";
            $s .= "<td><center><a href='setting_harga_acuan_bphtb/edit?s_idacuan=$row[s_idacuan]' class='btn btn-warning btn-sm btn-flat' style='width:55px'>Edit</a> <a href='#' onclick='hapus(" . $row['s_idacuan'] . ");return false;' class='btn btn-danger btn-sm btn-flat' style='width:55px'>Hapus</a></center></td>";
            $s .= "</tr>";
        }
        $data_render = array(
            "grid" => $s,
            "rows" => $base->rows,
            "count" => $count,
            "page" => $page,
            "start" => $start,
            "total_halaman" => $total_pages
        );
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($data_render));
    }

    public function tambahAction() {
        $session = $this->getServiceLocator()->get('EtaxService')->getStorage()->read();
        $ar_pemda = $this->getPemda()->getdata();
        $form = new \Bphtb\Form\Setting\HargaAcuanFrm();
        if ($this->getRequest()->isPost()) {
            $base = new \Bphtb\Model\Setting\HargaAcuanBase();
            $form->setInputFilter($base->getInputFilter());
            $form->setData($this->getRequest()->getPost());
            if ($form->isValid()) {
                $base->exchangeArray($form->getData());
                $this->getTblAcuan()->savedata($base);
                return $this->redirect()->toRoute('setting_harga_acuan_bphtb');
            }
        }
        $view = new \Zend\View\Model\ViewModel(array("frm" => $form));
        $data = array(
            'menu_setting' => 'active',
            'side_setting' => 'active',
            'side_harga_acuan' => 'active',
            'role_id' => $session['s_akses'],
            'data_pemda' => $ar_pemda,
            'aturgambar' => 2,
            'username' => $session['s_username']
        );
        $this->layout()->setVariables($data);
        return $view;
    }

    public function editAction() {
        $session = $this->getServiceLocator()->get('EtaxService')->getStorage()->read();
        $ar_pemda = $this->getPemda()->getdata();
        $frm = new \Bphtb\Form\Setting\HargaAcuanFrm();
        $req = $this->getRequest();
        if ($req->isGet()) {
            $id = (int) $req->getQuery()->get('s_idacuan');
            $data = $this->getTblAcuan()->getDataId($id);
            $data->t_nopacuan = $data->s_kd_propinsi . '.' . $data->s_kd_dati2 . '.' . $data->s_kd_kecamatan . '.' . $data->s_kd_kelurahan . '.' . $data->s_kd_blok;
            $data->s_permetertanah = number_format($data->s_permetertanah, 0, ',', '.');
            $frm->bind($data);
        }
        $view = new \Zend\View\Model\ViewModel(array(
            'frm' => $frm
        ));
        $data = array(
            'menu_setting' => 'active',
            'side_setting' => 'active',
            'side_harga_acuan' => 'active',
            'role_id' => $session['s_akses'],
            'data_pemda' => $ar_pemda,
            'aturgambar' => 2,
            'username' => $session['s_username']
        );
        $this->layout()->setVariables($data);
        return $view;
    }

    public function hapusAction() {
        $this->getTblAcuan()->hapusData($this->params('page'));
        return $this->getResponse();
    }

    public function getTblAcuan() {
        if (!$this->tbl_harga_acuan) {
            $this->tbl_harga_acuan = $this->getServiceLocator()->get("HargaAcuanTable");
        }
        return $this->tbl_harga_acuan;
    }

    public function getPemda() {
        if (!$this->tbl_pemda) {
            $sm = $this->getServiceLocator();
            $this->tbl_pemda = $sm->get("PemdaTable");
        }
        return $this->tbl_pemda;
    }

}
