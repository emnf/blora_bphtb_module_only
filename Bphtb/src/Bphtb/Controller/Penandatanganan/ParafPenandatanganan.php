<?php

namespace Bphtb\Controller\Penandatanganan;

use Zend\Debug\Debug;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class ParafPenandatanganan extends AbstractActionController
{
    public function indexAction()
    {
        $session = $this->getServiceLocator()->get('EtaxService')->getStorage()->read();
        $ar_pemda = $this->getServiceLocator()->get("PemdaTable")->getdata();
        $ar_pejabat = $this->getServiceLocator()->get("PenandatangananTable")->arPejabatTandaTangan($session["s_idpejabat"]);
        $form = new \Bphtb\Form\Pendataan\SSPDFrm();
        $view = new ViewModel(array(
            "form" => $form,
            "ar_pejabat" => $ar_pejabat
        ));
        $data = array(
            'role_id' => $session['s_akses'],
            'data_pemda' => $ar_pemda,
            'menu_paraf_penandatanganan' => 'active'
        );
        $this->layout()->setVariables($data);
        return $view;
    }

    public function gridBelumAction()
    {
        $session = $this->getServiceLocator()->get('EtaxService')->getStorage()->read();
        $allParams = (array) $this->getEvent()->getRouteMatch()->getParams();

        $input = $this->getRequest();
        $aColumns = ["a.t_idspt", "a.t_idspt", "a.t_tglprosesspt", "a.t_kohirspt", "a.t_kohirketetapanspt", "a.t_namawppembeli", "a.t_nopbphtbsppt", "a.t_kodebayarbanksppt", "a.jml_pajak_v1"];

        $rResult = $this->getServiceLocator()->get('PenandatangananTable')->gridBelumParaf($input, $aColumns, $session, null, $allParams);

        $aaData = [];
        $no = $rResult["no"];
        foreach ($rResult["rResult"] as $aRow) {
            $perintah = '<input type="checkbox" class="input-checkbox" value="' . $aRow["t_idspt"] . '">';
            $row = array(
                "<center>" . $no . "</center>",
                "<center>" . $perintah . "</center>",
                "<center>" . date("d-m-Y", strtotime($aRow["t_tglprosesspt"])) . "</center>",
                "<center>" . $aRow["t_kohirspt"] . "</center>",
                "<center>" . $aRow["t_kohirketetapanspt"] . "</center>",
                $aRow["t_namawppembeli"],
                "<center>" . $aRow["t_nopbphtbsppt"] . "</center>",
                "<center>" . $aRow["t_kodebayarbanksppt"] . "</center>",
                "<center>" . number_format($aRow["jml_pajak_v1"], 0, ',', '.') . "</center>",
            );

            $aaData[] = $row;
            $no++;
        }

        return $this->getResponse()->setContent(\Zend\Json\Json::encode([
            "sEcho" => intval($input->getPost('sEcho')),
            "iTotalRecords" => $rResult['iTotal'],
            "iTotalDisplayRecords" => $rResult['iTotal'],
            "aaData" => $aaData,
        ]));
    }

    public function cekurl()
    {
        $basePath = $this->getRequest()->getBasePath();
        $uri = new \Zend\Uri\Uri($this->getRequest()->getUri());
        $uri->setPath($basePath);
        $uri->setQuery(array());
        $uri->setFragment('');

        return $uri->getScheme() . '://' . $uri->getHost() . ':' . $_SERVER['SERVER_PORT'] . '' . $uri->getPath();
    }

    public function gridSudahAction()
    {
        $session = $this->getServiceLocator()->get('EtaxService')->getStorage()->read();
        $allParams = (array) $this->getEvent()->getRouteMatch()->getParams();

        $input = $this->getRequest();
        $aColumns = ["a.t_idspt", "a.t_idspt", "a.t_kohirspt", "a.t_kohirketetapanspt", "a.t_namawppembeli", "a.t_nopbphtbsppt", "a.t_kodebayarbanksppt", "a.jml_pajak_v1"];

        $rResult = $this->getServiceLocator()->get('PenandatangananTable')->gridSudahParaf($input, $aColumns, $session, null, $allParams);

        $aaData = [];
        $no = $rResult["no"];
        foreach ($rResult["rResult"] as $aRow) {
            $perintah = '';
            if ($aRow["t_id_penandatanganan"] != null) {
                $perintah .= '<a href="javascript:void(0)" class="btn btn-success">Sudah TTE</a>&nbsp;&nbsp;';
            } else {
                $perintah .= '<a href="javascript:hapus(\'' . $aRow["t_idspt"] . '\')" class="btn btn-danger">Hapus</a>&nbsp;&nbsp;';
            }
            $row = array(
                "<center>" . $no . "</center>",
                "<center>" . $perintah . "</center>",
                "<center>" . $aRow["t_kohirspt"] . "</center>",
                "<center>" . $aRow["t_kohirketetapanspt"] . "</center>",
                "<center>" . $aRow["t_namawppembeli"] . "</center>",
                "<center>" . $aRow["t_nopbphtbsppt"] . "</center>",
                "<center>" . $aRow["t_kodebayarbanksppt"] . "</center>",
                "<center>" . number_format($aRow["jml_pajak_v1"], 0, ',', '.') . "</center>",
            );

            $aaData[] = $row;
            $no++;
        }

        return $this->getResponse()->setContent(\Zend\Json\Json::encode([
            "sEcho" => intval($input->getPost('sEcho')),
            "iTotalRecords" => $rResult['iTotal'],
            "iTotalDisplayRecords" => $rResult['iTotal'],
            "aaData" => $aaData,
        ]));
    }

    public function hapusAction()
    {
        $session = $this->getServiceLocator()->get('EtaxService')->getStorage()->read();
        $allParams = (array) $this->getEvent()->getRouteMatch()->getParams();
        $res = $this->getServiceLocator()->get('PenandatangananTable')->hapusParaf($allParams["id"], $session);
        return $this->getResponse()->setContent($res);
    }

    public function prosesAction()
    {
        $session = $this->getServiceLocator()->get('EtaxService')->getStorage()->read();
        $req = $this->getRequest();
        $post = $req->getPost();
        $this->getServiceLocator()->get('PenandatangananTable')->simpanParaf($post, $session);
        return $this->redirect()->toRoute('paraf_penandatanganan');
    }
}
