<?php

namespace Bphtb\Model\Log;

use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Sql;

class LoginLogTable extends AbstractTableGateway
{

    protected $table = 's_users_login_log';

    public function __construct(Adapter $adapter)
    {
        $this->adapter = $adapter;
        $this->resultSetPrototype = new ResultSet();
        $this->resultSetPrototype->setArrayObjectPrototype(new LoginLogBase());
        $this->initialize();
    }

    public function saveLog($post)
    {
        $data = [
            "created_at" => date("Y-m-d H:i:s"),
            "s_username" => $post["s_username"],
            "s_message" => $post["s_message"],
            "s_ipaddress" => $post["s_ipaddress"]
        ];
        return $this->insert($data);
    }

    public function dataGridLoginLog($input, $aColumns, $session, $cekurl, $allParams)
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from(["a" => "s_users_login_log"]);
        $where = new \Zend\Db\Sql\Where();
        if ($input->getPost("sSearch_2")) {
            $where->literal("a.s_username ILIKE '%" . $input->getPost("sSearch_2") . "%'");
        }
        if ($input->getPost("sSearch")) {
            $where->literal("a.s_username ILIKE '%" . $input->getPost("sSearch") . "%'");
        }
        $select->where($where);

        $iTotal = $sql->prepareStatementForSqlObject($select)->execute()->count();

        //================ ordernya coy
        $aOrderingRules = array();
        if ($input->getPost('iSortCol_0')) {
            $iSortingCols = intval($input->getPost('iSortingCols'));
            for ($i = 0; $i < $iSortingCols; $i++) {
                if ($input->getPost('bSortable_' . intval($input->getPost('iSortCol_' . $i))) == 'true') {
                    $aOrderingRules[] = $aColumns[intval($input->getPost('iSortCol_' . $i))] . " " . ($input->getPost('sSortDir_' . $i) === 'asc' ? 'asc' : 'desc');
                }
            }
        }


        if (!empty($aOrderingRules)) {
            $select->order(implode(", ", $aOrderingRules));
        } else {
            $select->order("a.created_at DESC");
        }

        if ($input->getPost('iDisplayStart') && $input->getPost('iDisplayLength') != '-1') {
            $select->limit(intval($input->getPost('iDisplayLength')));
            $select->offset(intval($input->getPost('iDisplayStart')));
            $no = 1 + intval($input->getPost('iDisplayStart'));
        } else {
            if (intval($input->getPost('iDisplayLength')) >= 1) {
                $select->limit(intval($input->getPost('iDisplayLength')));
                $select->offset(intval($input->getPost('iDisplayStart')));
                $no = 1 + intval($input->getPost('iDisplayStart'));
            } else {
                $select->limit(10);
                $select->offset(0);
                $no = 1;
            }
        }

        $statement = $sql->prepareStatementForSqlObject($select);
        $rResult = $statement->execute();

        return [
            'rResult' => $rResult,
            'iTotal' => $iTotal,
            'no' => $no
        ];
    }
}
