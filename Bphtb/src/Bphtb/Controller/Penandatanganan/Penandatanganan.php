<?php

namespace Bphtb\Controller\Penandatanganan;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use mPDF;
use QRcode;

class Penandatanganan extends AbstractActionController
{
    public function indexAction()
    {
        $session = $this->getServiceLocator()->get('EtaxService')->getStorage()->read();
        // var_dump($session);exit();
        $ar_pemda = $this->getServiceLocator()->get("PemdaTable")->getdata();
        $ar_pejabat = $this->getServiceLocator()->get("PenandatangananTable")->arPejabatTandaTangan($session["s_idpejabat"]);
        // var_dump($ar_pejabat);exit();
        $form = new \Bphtb\Form\Pendataan\SSPDFrm();
        $view = new ViewModel(array(
            "form" => $form,
            "ar_pejabat" => $ar_pejabat
        ));
        $data = array(
            'role_id' => $session['s_akses'],
            'data_pemda' => $ar_pemda,
            'menu_penandatangan' => 'active'
        );
        $this->layout()->setVariables($data);
        return $view;
    }

    public function gridBelumAction()
    {
        $session = $this->getServiceLocator()->get('EtaxService')->getStorage()->read();
        $allParams = (array) $this->getEvent()->getRouteMatch()->getParams();

        $input = $this->getRequest();
        $aColumns = ["", "", "a.t_kohirspt", "a.t_kohirketetapanspt", "a.t_namawppembeli", "a.t_nopbphtbsppt", "a.t_kodebayarbanksppt", ""];

        $rResult = $this->getServiceLocator()->get('PenandatangananTable')->gridBelum($input, $aColumns, $session, null, $allParams);

        $aaData = [];
        $no = $rResult["no"];
        foreach ($rResult["rResult"] as $aRow) {
            $perintah = '<input type="checkbox" class="input-checkbox" value="' . $aRow["t_idspt"] . '">';
            $row = array(
                "<center>" . $no . "</center>",
                "<center>" . $perintah . "</center>",
                "<center>" . $aRow["t_kohirspt"] . "</center>",
                "<center>" . $aRow["t_kohirketetapanspt"] . "</center>",
                "<center>" . $aRow["t_namawppembeli"] . "</center>",
                "<center>" . $aRow["t_nopbphtbsppt"] . "</center>",
                "<center>" . $aRow["t_kodebayarbanksppt"] . "</center>",
                "<center>" . number_format($aRow["jml_pajak_v1"], 0, ',', '.') . "</center>",
            );

            $aaData[] = $row;
            $no++;
        }

        return $this->getResponse()->setContent(\Zend\Json\Json::encode([
            "sEcho" => intval($input->getPost('sEcho')),
            "iTotalRecords" => $rResult['iTotal'],
            "iTotalDisplayRecords" => $rResult['iTotal'],
            "aaData" => $aaData,
        ]));
    }

    public function cekurl()
    {
        $basePath = $this->getRequest()->getBasePath();
        $uri = new \Zend\Uri\Uri($this->getRequest()->getUri());
        $uri->setPath($basePath);
        $uri->setQuery(array());
        $uri->setFragment('');

        return $uri->getScheme() . '://' . $uri->getHost() . ':' . $_SERVER['SERVER_PORT'] . '' . $uri->getPath();
    }

    public function prosesAction()
    {
        $session = $this->getServiceLocator()->get('EtaxService')->getStorage()->read();
        $req = $this->getRequest();
        $ar_pemda = $this->getServiceLocator()->get("PemdaTable")->getdata();

        if ($req->isPost()) {
            $post = $req->getPost();
            // var_dump($post);exit();
            $explode = explode(",", $post["t_idspt"]);
            // var_dump($explode);exit();
            foreach ($explode as $key => $value) {
                $ar_sspd = $this->getServiceLocator()->get("SSPDTable")->getviewcetakssp($value);
                $perhitungan = $this->ControllerHelper()->hitungBphtbCetakan($ar_sspd[0]);
                $dataidsptsebelum = $this->getServiceLocator()->get("SSPDTable")->getdataidsptsebelumnya($value);
                $ar_sebelum = null;
                if (!empty($dataidsptsebelum['t_idsptsebelumnya'])) { // jika ada spt sebelumnya yang dikoreksi bpn
                    $ar_sebelum = $this->getSSPD()->getdatassspdsebelumnya($dataidsptsebelum['t_idsptsebelumnya']);
                }
                $ar_tglcetak = date('d-m-Y');
                $ar_Mengetahui = (array) $this->getServiceLocator()->get("PejabatBphtbTable")->getdataid($post["s_idpejabat"]);

                $array = [];

                require_once 'public/barcodecoy/qrlib.php';

                $guid = $this->ControllerHelper()->guidv4();
                $linkQr = $this->cekurl() . '/dokumen/informasi/' . $guid;
                $filepath = "public/export/sspd/";
                $this->ControllerHelper()->mkdir($filepath);
                //chmod($filepath,0777);


                $errorCorrectionLevel = 'L';
                $matrixPointSize = 3;
                $imageQr = $filepath . $guid . '.png';
                QRcode::png($linkQr, $imageQr, $errorCorrectionLevel, $matrixPointSize, 2);

                require_once 'public/cetakan/MPDF57/mpdf.php';
                $mpdf = new mPDF('utf-8', 'Legal', 0, '', 5, 5, 5, 5, 5, 5, 'P');
                $array = [
                    'data_sspd' => $ar_sspd,
                    'data_pemda' => $ar_pemda,
                    'tgl_cetak' => $ar_tglcetak,
                    'data_mengetahui' => $ar_Mengetahui,
                    'cetakversi' => 3,
                    'perhitungan' => $perhitungan,
                    'cekurl' => $this->cekurl(),
                    'dataidsptsebelum' => $dataidsptsebelum,
                    'ar_sebelum' => $ar_sebelum,
                    'imageQr' => $imageQr
                ];
                $html = $this->CetakSSPD()->cetakSSPD(2, $array);
                $mpdf->WriteHTML($html);
                $fileoutput = $filepath . $value . ".pdf";
                $mpdf->Output(str_replace("\\", "/", realpath(".")) . '/' . $fileoutput, "F");

                $nameFileSave = $guid . ".pdf";
                $filesave = $filepath . $nameFileSave;
                $nik = $ar_Mengetahui["s_nik"];
                $esign = $this->Esign()->signed_document($fileoutput, $filesave, $nik, $post["passphrase"], $linkQr, 80, 80, $imageQr);

                // var_dump($esign);exit();

                unlink($fileoutput);
                unlink($imageQr);

                $this->getServiceLocator()->get("PenandatangananTable")->saveLog($session, [
                    "t_nik" => $nik,
                    "s_idsurat" => 1,
                    "t_idspt" => $value,
                    "t_response_code" => $esign["httpcode"],
                    "t_response_message" => (($esign["httpcode"] != 200) ? $esign["body"] : 'OK'),
                    "s_idpejabat" => $ar_Mengetahui["s_idpejabat"]
                ]);

                if ($esign["httpcode"] == 200) {
                    if ($esign["saveDocument"]) {
                        $this->getServiceLocator()->get("PenandatangananTable")->save($session, [
                            "s_idsurat" => 1,
                            "t_idspt" => $value,
                            "t_path_file" => $filepath,
                            "t_name_file" => $nameFileSave,
                            "s_idpejabat" => $post["s_idpejabat"],
                            "id_dokumen" => $esign["headers"]["id_dokumen"][0],
                            "guid" => $guid,
                            "t_nik" => $nik,
                            "t_nama_penandatanganan" => $ar_Mengetahui["s_namapejabat"],
                            "s_idpejabat" => $ar_Mengetahui["s_idpejabat"]
                        ]);
                    }
                } else {
                    $this->flashMessenger()->addMessage(array('error' => (($esign["body"] != "") ? $esign["body"] : $esign["curl_error"])));
                    return $this->redirect()->toRoute('penandatanganan');
                }

                $this->flashMessenger()->addMessage(array('success' => 'Penandatanganan Berhasil'));
            }
        }

        return $this->redirect()->toRoute('penandatanganan');
    }

    public function gridSudahAction()
    {
        $session = $this->getServiceLocator()->get('EtaxService')->getStorage()->read();
        $allParams = (array) $this->getEvent()->getRouteMatch()->getParams();

        $input = $this->getRequest();
        $aColumns = ["", "", "a.t_kohirspt", "a.t_kohirketetapanspt", "a.t_namawppembeli", "a.t_nopbphtbsppt", "a.t_kodebayarbanksppt", ""];

        $rResult = $this->getServiceLocator()->get('PenandatangananTable')->gridSudah($input, $aColumns, $session, null, $allParams);

        $aaData = [];
        $no = $rResult["no"];
        foreach ($rResult["rResult"] as $aRow) {
            $perintah = '';
            $perintah .= '<a href="./dokumen/download/' . $aRow["guid"] . '" target="_blank" class="btn btn-success">Download</a>&nbsp;&nbsp;';
            $perintah .= '<a href="javascript:hapus(\'' . $aRow["guid"] . '\')" class="btn btn-danger">Hapus</a>&nbsp;&nbsp;';
            $row = array(
                "<center>" . $no . "</center>",
                "<center>" . $perintah . "</center>",
                "<center>" . $aRow["t_kohirspt"] . "</center>",
                "<center>" . $aRow["t_kohirketetapanspt"] . "</center>",
                "<center>" . $aRow["t_namawppembeli"] . "</center>",
                "<center>" . $aRow["t_nopbphtbsppt"] . "</center>",
                "<center>" . $aRow["t_kodebayarbanksppt"] . "</center>",
                "<center>" . number_format($aRow["jml_pajak_v1"], 0, ',', '.') . "</center>",
            );

            $aaData[] = $row;
            $no++;
        }

        return $this->getResponse()->setContent(\Zend\Json\Json::encode([
            "sEcho" => intval($input->getPost('sEcho')),
            "iTotalRecords" => $rResult['iTotal'],
            "iTotalDisplayRecords" => $rResult['iTotal'],
            "aaData" => $aaData,
        ]));
    }

    public function gridLogAction()
    {
        $session = $this->getServiceLocator()->get('EtaxService')->getStorage()->read();
        $allParams = (array) $this->getEvent()->getRouteMatch()->getParams();

        $input = $this->getRequest();
        $aColumns = [];

        $rResult = $this->getServiceLocator()->get('PenandatangananTable')->gridLog($input, $aColumns, $session, null, $allParams);

        $aaData = [];
        $no = $rResult["no"];
        foreach ($rResult["rResult"] as $aRow) {
            $perintah = '';
            $perintah .= '';
            $row = array(
                "<center>" . $no . "</center>",
                "<center>" . $aRow["t_nik"] . "</center>",
                "<center>" . $aRow["created_at"] . "</center>",
                "<center>" . $aRow["t_response_code"] . "</center>",
                "" . $aRow["t_response_message"] . "",
            );

            $aaData[] = $row;
            $no++;
        }

        return $this->getResponse()->setContent(\Zend\Json\Json::encode([
            "sEcho" => intval($input->getPost('sEcho')),
            "iTotalRecords" => $rResult['iTotal'],
            "iTotalDisplayRecords" => $rResult['iTotal'],
            "aaData" => $aaData,
        ]));
    }

    public function hapusAction()
    {
        $session = $this->getServiceLocator()->get('EtaxService')->getStorage()->read();
        $allParams = (array) $this->getEvent()->getRouteMatch()->getParams();
        $res = $this->getServiceLocator()->get('PenandatangananTable')->hapus($allParams["id"], $session);
        return $this->getResponse()->setContent($res);
    }
}
