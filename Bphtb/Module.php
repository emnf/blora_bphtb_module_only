<?php

namespace Bphtb;

use Zend\ModuleManager\Feature\AutoloaderProviderInterface;
use Zend\Authentication\AuthenticationService;
use Zend\Authentication\Adapter\DbTable as DbTableAuthAdapter;
use Zend\Session\SessionManager;
use Bphtb\Helper\MenuHelper;
use Bphtb\Helper\Spop\MenuSpopHelper;

class Module implements AutoloaderProviderInterface
{

    // Common Code
    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\ClassMapAutoloader' => array(
                __DIR__ . '/autoload_classmap.php'
            ),
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__
                )
            )
        );
    }

    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    // Custom Code
    public function getServiceConfig()
    {
        return array(
            'factories' => array(
                'NotifikasiTable' => function ($sm) {
                    $dbAdapter = $sm->get('bphtb');
                    $table = new Model\Notifikasi\NotifikasiTable($dbAdapter);
                    return $table;
                },
                'PersyaratanTable' => function ($sm) {
                    $dbAdapter = $sm->get('bphtb');
                    $table = new Model\Setting\PersyaratanTable($dbAdapter);
                    return $table;
                },
                'UserTable' => function ($sm) {
                    $dbAdapter = $sm->get('bphtb');
                    $table = new Model\Setting\SettingUserTable($dbAdapter);
                    return $table;
                },
                'TarifBphtbTable' => function ($sm) {
                    $dbAdapter = $sm->get('bphtb');
                    $table = new Model\Setting\TarifBphtbTable($dbAdapter);
                    return $table;
                },
                'JenisKetetapanTable' => function ($sm) {
                    $dbAdapter = $sm->get('bphtb');
                    $table = new Model\Setting\JenisKetetapanTable($dbAdapter);
                    return $table;
                },
                'HakTanahTable' => function ($sm) {
                    $dbAdapter = $sm->get('bphtb');
                    $table = new Model\Setting\HakTanahTable($dbAdapter);
                    return $table;
                },
                'TarifNpotkpTable' => function ($sm) {
                    $dbAdapter = $sm->get('bphtb');
                    $table = new Model\Setting\TarifNpotkpTable($dbAdapter);
                    return $table;
                },
                'JenisTransaksiBphtbTable' => function ($sm) {
                    $dbAdapter = $sm->get('bphtb');
                    $table = new Model\Setting\JenisTransaksiBphtbTable($dbAdapter);
                    return $table;
                },
                'NotarisBphtbTable' => function ($sm) {
                    $dbAdapter = $sm->get('bphtb');
                    $table = new Model\Setting\NotarisBphtbTable($dbAdapter);
                    return $table;
                },
                'PejabatBphtbTable' => function ($sm) {
                    $dbAdapter = $sm->get('bphtb');
                    $table = new Model\Setting\PejabatBphtbTable($dbAdapter);
                    return $table;
                },
                'KelurahanBphtbTable' => function ($sm) {
                    $dbAdapter = $sm->get('bphtb');
                    $table = new Model\Setting\KelurahanBphtbTable($dbAdapter);
                    return $table;
                },
                'KecamatanBphtbTable' => function ($sm) {
                    $dbAdapter = $sm->get('bphtb');
                    $table = new Model\Setting\KecamatanBphtbTable($dbAdapter);
                    return $table;
                },
                'PemdaTable' => function ($sm) {
                    $dbAdapter = $sm->get('bphtb');
                    $table = new Model\Setting\PemdaTable($dbAdapter);
                    return $table;
                },
                'DokTanahTable' => function ($sm) {
                    $dbAdapter = $sm->get('bphtb');
                    $table = new Model\Setting\DokTanahTable($dbAdapter);
                    return $table;
                },
                'HargaAcuanTable' => function ($sm) {
                    $dbAdapter = $sm->get('bphtb');
                    $table = new Model\Setting\HargaAcuanTable($dbAdapter);
                    return $table;
                },
                'SPTTable' => function ($sm) {
                    $dbAdapter = $sm->get('bphtb');
                    $table = new Model\Pendataan\SPTTable($dbAdapter);
                    return $table;
                },
                'SSPDBphtbTable' => function ($sm) {
                    $dbAdapter = $sm->get('bphtb');
                    $dbAdapter3 = $sm->get('espop_db_pg');
                    $table = new Model\Pendataan\SSPDBphtbTable($dbAdapter, $dbAdapter3);
                    return $table;
                },
                'SPPTTable' => function ($sm) {
                    $dbAdapter = $sm->get('oracle_db');
                    $table = new Model\Pendataan\SPPTTable($dbAdapter);
                    return $table;
                },
                'SpbbTable' => function ($sm) {
                    $dbAdapter = $sm->get('oracle_db');
                    $table = new Model\DataSismiop\DatsubjekpajakTable($dbAdapter);
                    return $table;
                },
                'OpbbTable' => function ($sm) {
                    $dbAdapter = $sm->get('oracle_db');
                    $table = new Model\DataSismiop\DatobjekpajakTable($dbAdapter);
                    return $table;
                },
                'PembayaranSptTable' => function ($sm) {
                    $dbAdapter = $sm->get('bphtb');
                    $table = new Model\Pembayaran\PembayaranSptTable($dbAdapter);
                    return $table;
                },
                'VerifikasiBerkasTable' => function ($sm) {
                    $dbAdapter = $sm->get('bphtb');
                    $table = new Model\Verifikasiberkas\VerifikasiBerkasTable($dbAdapter);
                    return $table;
                },
                'VerifikasiSptTable' => function ($sm) {
                    $dbAdapter = $sm->get('bphtb');
                    $table = new Model\Verifikasi\VerifikasiSPTTable($dbAdapter);
                    return $table;
                },
                'PenguranganTable' => function ($sm) {
                    $dbAdapter = $sm->get('bphtb');
                    $table = new Model\Pengurangan\PenguranganTable($dbAdapter);
                    return $table;
                },
                'PelaporanTable' => function ($sm) {
                    $dbAdapter = $sm->get('bphtb');
                    $table = new Model\Pelaporan\PelaporanTable($dbAdapter);
                    return $table;
                },
                'SSPDTable' => function ($sm) {
                    $dbAdapter = $sm->get('bphtb');
                    $table = new Model\Pencetakan\SSPDTable($dbAdapter);
                    return $table;
                },
                //============== Upload File
                'UploadTable' => function ($sm) {
                    $dbAdapter = $sm->get('bphtb');
                    $table = new Model\Pendataan\UploadTable($dbAdapter);
                    return $table;
                },
                //============== End Upload File              
                'SPTWarisTable' => function ($sm) {
                    $dbAdapter = $sm->get('bphtb');
                    $table = new Model\Pendataan\SPTWarisTable($dbAdapter);
                    return $table;
                },
                'DataSertifikatTable' => function ($sm) {
                    $dbAdapter = $sm->get('bphtb');
                    $table = new Model\BpnApi\DataSertifikatTable($dbAdapter);
                    return $table;
                },
                'PBBKecamatanTable' => function ($sm) {
                    $dbAdapter = $sm->get('oracle_db');
                    $table = new Model\Setting\PBBKecamatanTable($dbAdapter);
                    return $table;
                },
                'PBBKelurahanTable' => function ($sm) {
                    $dbAdapter = $sm->get('oracle_db');
                    $table = new Model\Setting\PBBKelurahanTable($dbAdapter);
                    return $table;
                },
                // 'AnggaranSimpatdaTable' => function ($sm) {
                //     $dbAdapter = $sm->get('simpatda');
                //     $table = new Model\DataSimpatda\AnggaranSimpatdaTable($dbAdapter);
                //     return $table;
                // },
                'EtaxService' => function ($sm) {
                    $dbAdapter = $sm->get('bphtb');
                    $dbTableAuthAdapter = new DbTableAuthAdapter($dbAdapter, "s_users", "s_username", "s_password", "MD5(?)");
                    $eTaxService = new AuthenticationService();
                    $eTaxService->setAdapter($dbTableAuthAdapter);
                    return $eTaxService;
                },
                'bphtbRoleTable' => function ($serviceManager) {
                    return new Model\Secure\Role($serviceManager->get('bphtb'));
                },
                'UserRoleTable' => function ($serviceManager) {
                    return new Model\Secure\UserRole($serviceManager->get('bphtb'));
                },
                'PermissionTable' => function ($serviceManager) {
                    return new Model\Secure\PermissionTable($serviceManager->get('bphtb'));
                },
                'bphtbResourceTable' => function ($serviceManager) {
                    return new Model\Secure\ResourceTable($serviceManager->get('bphtb'));
                },
                'bphtbRolePermissionTable' => function ($serviceManager) {
                    return new Model\Secure\RolePermissionTable($serviceManager->get('bphtb'));
                },
                'bphtbAcl' => function ($serviceManager) {
                    return new Utility\Acl();
                },
                'app_path' => function ($sm) {
                    //                    $path_aplikasi = "/var/www/html/simpatda";
                    $path_aplikasi = "/var/www/simbphtb";
                    return $path_aplikasi;
                },
                'PtslTable' => function ($sm) {
                    $dbAdapter = $sm->get('bphtb');
                    $table = new Model\Ptsl\PtslTable($dbAdapter);
                    return $table;
                },
                'LoginLogTable' => function ($sm) {
                    $dbAdapter = $sm->get('bphtb');
                    $table = new Model\Log\LoginLogTable($dbAdapter);
                    return $table;
                },
                'PenandatangananTable' => function ($sm) {
                    $dbAdapter = $sm->get('bphtb');
                    $table = new Model\Penandatanganan\PenandatangananTable($dbAdapter);
                    return $table;
                },
                'FormatPesanTable' => function ($sm) {
                    $dbAdapter = $sm->get('bphtb');
                    $table = new Model\Setting\FormatPesanTable($dbAdapter);
                    return $table;
                },
                'ButtonTable' => function ($sm) {
                    $dbAdapter = $sm->get('bphtb');
                    $table = new Model\Setting\ButtonTable($dbAdapter);
                    return $table;
                },
                'KodeRekeningTable' => function ($sm) {
                    $dbAdapter = $sm->get('bphtb');
                    $table = new Model\Setting\KodeRekeningTable($dbAdapter);
                    return $table;
                },

                // SPOP
                'InputSpopTable' => function ($sm) {
                    $dbAdapter = $sm->get('bphtb');
                    $table = new Model\Spop\InputSpopTable($dbAdapter);
                    return $table;
                },
                'ReffSpopTable' => function ($sm) {
                    $dbAdapter = $sm->get('oracle_db');
                    $table = new Model\Spop\ReffSpopTable($dbAdapter);
                    return $table;
                },
                // SPOP

                // ESPOP
                // 'EspopTable' => function ($sm) {
                //     // $dbAdapter = $sm->get('espop_db');
                //     $dbAdapter1 = $sm->get('oracle_db');
                //     $dbAdapter2 = $sm->get('bphtb');
                //     $dbAdapter3 = $sm->get('espop_db_pg');
                //     $table = new Model\Espop\EspopTable($dbAdapter1, $dbAdapter2, $dbAdapter3); //$dbAdapter,
                //     return $table;
                // },
                'EspopTable' => function ($sm) {
                    // $dbAdapter = $sm->get('espop_db');
                    $dbAdapter1 = $sm->get('oracle_db');
                    $dbAdapter2 = $sm->get('bphtb');
                    $dbAdapter3 = $sm->get('espop_db_pg');
                    $table = new Model\Espop\EspopTable($dbAdapter3, $dbAdapter2); //$dbAdapter,
                    return $table;
                },
                // ESPOP

            )
        );
    }

    public function getViewHelperConfig()
    {
        return array(
            'factories' => array(
                'MenuHelper' => function ($serviceManager) {
                    $helper = new MenuHelper();
                    return $helper;
                },
                'MenuSpopHelper' => function ($serviceManager) {
                    $helper = new MenuSpopHelper();
                    return $helper;
                },
                //'CustomHelp' => function ($serviceManager) {
                //    $helper = new CustomHelper ();
                //    return $helper;
                //}
            )
        );
    }

    // public function init(\Zend\ModuleManager\ModuleManager $mm)
    // {
    //     $mm->getEventManager()->getSharedManager()->attach(__NAMESPACE__, 'dispatch', function ($e) {
    //         $e->getTarget()->layout('bphtb/layout');
    //     });
    // }

    public function onBootstrap($e)
    {
        $serviceLocator = $e->getApplication()->getServiceManager();
        $translator = $e->getApplication()->getServiceManager()->get('translator');
        $eventManager = $e->getApplication()->getEventManager();
        $moduleRouteListener = new \Zend\Mvc\ModuleRouteListener();
        $moduleRouteListener->attach($eventManager);

        $sessionManager = new SessionManager();
        $sessionManager->setName('eBPHTBTOLI');
        $sessionManager->rememberMe('31536000');
        $sessionManager->start();

        $translator->addTranslationFile('phpArray', './vendor/zendframework/zend-i18n-resources/languages/id/Zend_Validate.php', 'default');
        \Zend\Validator\AbstractValidator::setDefaultTranslator($translator);

        $eventManager->attach(\Zend\Mvc\MvcEvent::EVENT_DISPATCH, array(
            $this,
            'boforeDispatch'
        ), 100);
    }

    function boforeDispatch(\Zend\Mvc\MvcEvent $event)
    {
        $authenticationService = $event->getApplication()->getServiceManager()->get('EtaxService');
        $request = $event->getRequest();
        $response = $event->getResponse();
        // $target = $event->getTarget();
        $whiteList = array(
            'LoginAccess-logout',
            'Dokumen-informasi'
        );
        // $requestUri = $request->getRequestUri();
        $controller = $event->getRouteMatch()->getParam('controller');
        // var_dump($controller);exit();
        $action = $event->getRouteMatch()->getParam('action');
        $requestedResourse = $controller . "-" . $action;
        /*
          if ($authenticationService->hasIdentity()) {
          if ($requestedResourse == 'LoginAccess-logout' || in_array($requestedResourse, $whiteList)) {
          $url = 'main_bphtb';
          $response->setHeaders($response->getHeaders()->addHeaderLine('Location', $url));
          $response->setStatusCode(302);
          } else {
          $serviceManager = $event->getApplication()->getServiceManager();
          $storage = $serviceManager->get('EtaxService')->getStorage()->read();
          // $userRole = $storage['s_namauserrole'];
          $userRole = $storage ['s_username'];
          $acl = $serviceManager->get('bphtbAcl');
          $acl->initAcl();

          $status = $acl->isAccessAllowed($userRole, $controller, $action);
          if (!$status) {
          die('Anda tidak di ijinkan untuk membuka halaman ini');
          }
          }
          } else {

          if ($requestedResourse != 'LoginAccess-index' && !in_array($requestedResourse, $whiteList)) {
          $url = 'sign_in';
          $response->setHeaders($response->getHeaders()->addHeaderLine('Location', $url));
          $response->setStatusCode(302);
          }
          $response->sendHeaders();
          } */
        # protect route with session

        $apiController = ['ApiController'];

        if (!in_array($controller, $apiController)) {
            // api not login web

            if (!$authenticationService->hasIdentity()) {
                // die("non authentication");
                if ($requestedResourse != 'LoginAccess-index' && !in_array($requestedResourse, $whiteList)) {
                    $event->getRouteMatch()->setParam('controller', 'LoginAccess')->setParam('action', 'index');
                }
            } else {
                // die("authentication");
                if ($requestedResourse == 'LoginAccess-index') {
                    $url = 'main_bphtb';
                    $response->setHeaders($response->getHeaders()->addHeaderLine('Location', $url));
                    $response->setStatusCode(302);
                }

                $response->sendHeaders();
            }
        }
    }

    // Custom Code
}
