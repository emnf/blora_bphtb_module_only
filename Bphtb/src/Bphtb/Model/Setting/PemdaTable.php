<?php

namespace Bphtb\Model\Setting;

use Bphtb\Helper\CacheHelper;
use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Where;
use Zend\Db\ResultSet\ResultSet;

class PemdaTable extends AbstractTableGateway
{

    protected $table = "s_pemda";

    public function __construct(Adapter $adapter)
    {
        $this->adapter = $adapter;
        $this->resultSetPrototype = new ResultSet();
        $this->resultSetPrototype->setArrayObjectPrototype(new \Bphtb\Model\Setting\PemdaBase());
        $this->initialize();
    }

    public function getdata()
    {
        $key = 'PemdaTable-getdata';
        $chache = (new CacheHelper)->cache();

        if (!$chache->hasItem($key)) {
            $rowset = $this->select();
            $row = $rowset->current();
            // return $row;

            $chache->setItem($key, $row);
        }

        return $chache->getItem($key);
    }

    public function savedata(\Bphtb\Model\Setting\PemdaBase $db, $path)
    {
        $data = array(
            "s_namaprov" => $db->s_namaprov,
            "s_namakabkot" => $db->s_namakabkot,
            "s_namaibukotakabkot" => $db->s_namaibukotakabkot,
            "s_kodeprovinsi" => $db->s_kodeprovinsi,
            "s_kodekabkot" => $db->s_kodekabkot,
            "s_namainstansi" => $db->s_namainstansi,
            "s_namasingkatinstansi" => $db->s_namasingkatinstansi,
            "s_alamatinstansi" => $db->s_alamatinstansi,
            "s_notelinstansi" => $db->s_notelinstansi,
            "s_namabank" => $db->s_namabank,
            "s_norekbank" => $db->s_norekbank
        );
        if (!empty($path)) {
            $data["s_logo"] = $path;
        }
        $id = (int) $db->s_idpemda;
        if ($id == 0) {
            $this->insert($data);
        } else {
            $this->update($data, array("s_idpemda" => $db->s_idpemda));
        }
    }
}
