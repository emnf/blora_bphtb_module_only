<?php

namespace Bphtb\Model\Verifikasiberkas;

use Zend\InputFilter\InputFilter;
use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class VerifikasiBerkasBase implements InputFilterAwareInterface
{

    public $t_idspt, $t_kodebayarspt, $t_verifikasispt, $t_tglverifikasiberkas, $t_kohirpembayaran, $t_idpembayaranspt, $t_idnotarisspt, $t_kodebayarbanksppt, $t_persyaratanverifikasi, $t_kodedaftarspt;
    public $t_idnotaris, $t_pejabatverifikasispt, $t_totalspt, $t_persyaratan, $p_totalspt;
    public $t_ket_verifikasi_berkas, $s_id_format_pesan;
    public $page, $direction;
    public $rows;
    public $sidx;
    public $sord;
    protected $inputFilter;

    public function exchangeArray($data)
    {
        $this->t_idspt = (isset($data['t_idspt'])) ? $data['t_idspt'] : null;
        $this->t_idnotarisspt = (isset($data['t_idnotarisspt'])) ? $data['t_idnotarisspt'] : null;
        $this->t_kodebayarspt = (isset($data['t_kodebayarspt'])) ? $data['t_kodebayarspt'] : null;
        $this->t_idpembayaranspt = (isset($data['t_idpembayaranspt'])) ? $data['t_idpembayaranspt'] : null;
        $this->t_kohirpembayaran = (isset($data['t_kohirpembayaran'])) ? $data['t_kohirpembayaran'] : null;
        $this->t_kodebayarbanksppt = (isset($data['t_kodebayarbanksppt'])) ? $data['t_kodebayarbanksppt'] : null;
        $this->t_persyaratanverifikasi = (isset($data['t_persyaratanverifikasi'])) ? $data['t_persyaratanverifikasi'] : null;
        $this->t_verifikasispt = (isset($data['t_verifikasispt'])) ? $data['t_verifikasispt'] : null;
        $this->t_tglverifikasiberkas = (isset($data['t_tglverifikasiberkas'])) ? $data['t_tglverifikasiberkas'] : null;
        $this->t_idnotaris = (isset($data['t_idnotaris'])) ? $data['t_idnotaris'] : null;
        $this->t_pejabatverifikasispt = (isset($data['t_pejabatverifikasispt'])) ? $data['t_pejabatverifikasispt'] : null;
        $this->t_totalspt = (isset($data['t_totalspt'])) ? $data['t_totalspt'] : null;
        $this->p_idpemeriksaan = (isset($data['p_idpemeriksaan'])) ? $data['p_idpemeriksaan'] : null;
        $this->t_persyaratan = (isset($data['t_persyaratan'])) ? $data['t_persyaratan'] : null;
        $this->p_totalspt = (isset($data['p_totalspt'])) ? $data['p_totalspt'] : null;
        $this->t_kodedaftarspt = (isset($data['t_kodedaftarspt'])) ? $data['t_kodedaftarspt'] : null;
        $this->direction = (isset($data['direction'])) ? $data['direction'] : null;
        $this->page = (isset($data['page'])) ? $data['page'] : null;
        $this->rows = (isset($data['rows'])) ? $data['rows'] : null;
        $this->sidx = (isset($data['sidx'])) ? $data['sidx'] : null;
        $this->sord = (isset($data['sord'])) ? $data['sord'] : null;
        $this->t_tglverifikasispt = (isset($data['t_tglverifikasispt'])) ? $data['t_tglverifikasispt'] : null;
        $this->t_ket_verifikasi_berkas = (isset($data['t_ket_verifikasi_berkas'])) ? $data['t_ket_verifikasi_berkas'] : null;
        $this->s_id_format_pesan = (isset($data['s_id_format_pesan'])) ? $data['s_id_format_pesan'] : null;
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not Used");
    }

    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();

            $factory = new InputFactory();

            $inputFilter->add([
                "name" => "t_pemeriksaanop",
                "required" => false
            ]);
            $inputFilter->add([
                "name" => "t_idjenishaktanah",
                "required" => false
            ]);
            $inputFilter->add([
                "name" => "t_idjenishaktanah",
                "required" => false
            ]);
            $inputFilter->add([
                "name" => "p_terbukti",
                "required" => false
            ]);
            $inputFilter->add([
                "name" => "checkboxTerbukti",
                "required" => false
            ]);
            $inputFilter->add([
                "name" => "t_persyaratan",
                "required" => false
            ]);
            $inputFilter->add([
                "name" => "t_persyaratanverifikasi",
                "required" => false
            ]);

            $inputFilter->add([
                "name" => "s_id_format_pesan",
                "required" => false
            ]);

            $this->inputFilter = $inputFilter;
        }
        return $this->inputFilter;
    }
}
