<?php

namespace Bphtb\Model\Setting;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Where;

class NotarisBphtbTable extends \Zend\Db\TableGateway\AbstractTableGateway {

    protected $table = "s_notaris",$table_user = 's_users';

    public function __construct(\Zend\Db\Adapter\Adapter $adapter) {
        $this->adapter = $adapter;
        $this->resultSetPrototype = new \Zend\Db\ResultSet\ResultSet();
        $this->resultSetPrototype->setArrayObjectPrototype(new NotarisBphtbBase());
        $this->initialize();
    }
    
    public function checkId($id) {
        $rowset = $this->select(array('s_idnotaris' => $id));
        $row = $rowset->current();
        return $row;
    }
    
    public function getdataCombo() {
        
        $sql = "select * from s_notaris a "
                . "left join s_users b on b.s_idpejabat_idnotaris::text = a.s_idnotaris::text "
                . "where b.s_tipe_pejabat = '2' order by s_namanotaris asc";
        $statement = $this->adapter->query($sql);
        return $statement->execute();
//        
//        $sql = new \Zend\Db\Sql\Sql($this->adapter);
//        $select = $sql->select();
//        $select->from($this->table);
//        $select->join($this->table_user, "$this->table_user.s_idpejabat_idnotaris::int = $this->table.s_idnotaris");
//        $where = new Where();
//        $where->equalTo("$this->table_user.s_tipe_pejabat", "2");
//        $select->where($where);
//        $state = $sql->prepareStatementForSqlObject($select);
//        $res = $state->execute();
//        return $res;
    }

    public function getdata() {
        $sql = new \Zend\Db\Sql\Sql($this->adapter);
        $select = $sql->select();
        $select->from($this->table);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res;
    }
    
      public function getNamaNotaris($pj) {
        $sql = "select coalesce(s_namanotaris) from s_notaris where s_idnotaris=$pj";        
        $statement = $this->adapter->query($sql);
        return $statement->execute();
    }
    
    public function checkExist(NotarisBphtbBase $pj) {
        $rowset = $this->select(array('s_namanotaris' => $pj->s_namanotaris));
        $row = $rowset->current();
        return $row;
    }

    public function savedata(NotarisBphtbBase $np) {
        $data = array(
        "s_namanotaris" => $np->s_namanotaris,
        "s_alamatnotaris" => $np->s_alamatnotaris,
        "s_npwpd" => $np->s_npwpd,
        "s_sknotaris" => $np->s_sknotaris,
//        "s_tgl1notaris" => date('Y-m-d', strtotime($np->s_tgl1notaris)),
//        "s_tgl2notaris" => date('Y-m-d', strtotime($np->s_tgl2notaris)),
        "s_statusnotaris" => $np->s_statusnotaris
        );
        
        $id = (int) $np->s_idnotaris;
        if($id == 0){
            $this->insert($data);
        }else{
            $this->update($data,array("s_idnotaris"=>$np->s_idnotaris));
        }
    }

    public function getGridCount(NotarisBphtbBase $base) {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from($this->table);
        $where = new Where();
        if($base->s_namanotaris != 'undefined')
            $where->literal("$this->table.s_namanotaris::text LIKE '%$base->s_namanotaris%'");     
        if($base->s_npwpd != 'undefined')
            $where->literal("$this->table.s_npwpd::text LIKE '%$base->s_npwpd%'");     
        if($base->s_alamatnotaris != 'undefined')
            $where->literal("$this->table.s_alamatnotaris::text LIKE '%$base->s_alamatnotaris%'");   
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res->count();
    }

    public function getGridData(NotarisBphtbBase $base, $offset) {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from($this->table);
        $where = new \Zend\Db\Sql\Where();
        if($base->s_namanotaris != 'undefined')
            $where->literal("$this->table.s_namanotaris::text LIKE '%$base->s_namanotaris%'");     
        if($base->s_npwpd != 'undefined')
            $where->literal("$this->table.s_npwpd::text LIKE '%$base->s_npwpd%'");     
        if($base->s_alamatnotaris != 'undefined')
            $where->literal("$this->table.s_alamatnotaris::text LIKE '%$base->s_alamatnotaris%'");    
        $select->where($where);
        $select->order("s_idnotaris ASC");
        $select->limit($base->rows = (int) $base->rows);
        $select->offset($offset = (int) $offset);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res;
    }
    
    public function hapusData($pb) {
        $this->delete(array('s_idnotaris' => $pb));
    }

}
