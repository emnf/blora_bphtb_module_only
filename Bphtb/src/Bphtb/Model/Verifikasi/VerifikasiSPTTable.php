<?php

namespace Bphtb\Model\Verifikasi;

use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Expression;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Where;

class VerifikasiSPTTable extends AbstractTableGateway
{
    protected $table = 't_pembayaranspt';

    public function __construct(Adapter $adapter)
    {
        $this->adapter = $adapter;
        $this->resultSetPrototype = new ResultSet();
        $this->resultSetPrototype->setArrayObjectPrototype(new VerifikasiSPTBase());
        $this->initialize();
    }

    //================= datagrid verifikasi
    public function getjumlahdata($sTable, $count, $sWhere)
    {
        $sql = "SELECT " . $count . " FROM " . $sTable . "" . $sWhere;
        $statement = $this->adapter->query($sql);
        $res = $statement->execute()->count();
        return $res;
    }

    public function semuadatavalidasi($sTable, $count, $input, $order_default, $order_default_thn, $aColumns, $session, $cekurl)
    {
        $aOrderingRules = array();
        $sLimit = "";
        $sOffset = "";
        if ($input->getPost('iDisplayStart') && $input->getPost('iDisplayLength') != '-1') {
            // $sLimit = " LIMIT " . intval($input->getPost('iDisplayLength')) . " OFFSET " . intval($input->getPost('iDisplayStart'));
            $sLimit = intval($input->getPost('iDisplayLength'));
            $sOffset = intval($input->getPost('iDisplayStart'));
            $no = 1 + intval($input->getPost('iDisplayStart'));
        } else {
            if (intval($input->getPost('iDisplayLength')) >= 1) {
                // $sLimit = " LIMIT " . intval($input->getPost('iDisplayLength')) . " OFFSET " . intval($input->getPost('iDisplayStart'));
                $sLimit = intval($input->getPost('iDisplayLength'));
                $sOffset = intval($input->getPost('iDisplayStart'));
                $no = 1 + intval($input->getPost('iDisplayStart'));
            } else {
                // $sLimit = " LIMIT 10 OFFSET 0";
                $sLimit = 10;
                $sOffset = 0;
                $no = 1;
            }
        }

        $aOrderingRules = array();
        if ($input->getPost('iSortCol_0')) {
            $iSortingCols = intval($input->getPost('iSortingCols'));
            // var_dump($iSortingCols);exit();
            for ($i = 0; $i < $iSortingCols; $i++) {
                // var_dump(intval($input->getPost('iSortCol_' . $i)));exit();
                if ($input->getPost('bSortable_' . intval($input->getPost('iSortCol_' . $i))) == 'true') {
                    // $aOrderingRules[] = " " . $aColumns[intval($input->getPost('iSortCol_' . $i))] . ' ' . ($input->getPost('sSortDir_' . $i) === 'asc' ? 'asc' : 'desc');
                    // var_dump($aColumns[intval($input->getPost('iSortCol_' . $i))]);exit();
                    $aOrderingRules[] = $aColumns[intval($input->getPost('iSortCol_' . $i))] . ' ' . ($input->getPost('sSortDir_' . $i) === 'asc' ? 'asc' : 'desc');
                }
            }
        }

        if (!empty($aOrderingRules)) {
            // $sOrder = " ORDER BY " . implode(", ", $aOrderingRules);
            $sOrder = implode(", ", $aOrderingRules);
        } else {
            // $sOrder = " ORDER BY " . $order_default_thn . " ," . $order_default . "";
            $sOrder = $order_default_thn;
        }

        $iColumnCount = count($aColumns);

        if ($input->getPost('sSearch') && $input->getPost('sSearch') != "") {
            $aFilteringRules = array();
            for ($i = 0; $i < $iColumnCount; $i++) {
                if ($input->getPost('bSearchable_' . $i) && $input->getPost('bSearchable_' . $i) == 'true') {
                    $tanggal = explode('-', $input->getPost('sSearch'));
                    if (count($tanggal) > 1) {
                        if (count($tanggal) > 2) {
                            $tanggalcari = "" . $tanggal[2] . "-" . $tanggal[1] . "-" . $tanggal[0] . "";
                            $aFilteringRules[] = " " . $aColumns[$i] . "::text  ILIKE '%" . $tanggalcari . "%'";
                        } else {
                            $tanggalcari = "" . $tanggal[1] . "-" . $tanggal[0] . "";
                            $aFilteringRules[] = " " . $aColumns[$i] . "::text  ILIKE '%" . $tanggalcari . "%'";
                        }
                    } else {
                        $aFilteringRules[] = " " . $aColumns[$i] . "::text  ILIKE '%" . $input->getPost('sSearch') . "%'";
                    }
                }
            }
            if (!empty($aFilteringRules)) {
                $aFilteringRules = array('(' . implode(" OR ", $aFilteringRules) . ')');
            }
        }

        $pencarianstatusvalidasi = 2;
        for ($i = 0; $i < $iColumnCount; $i++) {
            if ($input->getPost('bSearchable_' . $i) && $input->getPost('bSearchable_' . $i) == 'true' && $input->getPost('sSearch_' . $i) != '') {
                $tanggal = explode('-', $input->getPost('sSearch_' . $i));

                if (count($tanggal) > 1) {
                    if (count($tanggal) > 2) {
                        $tanggalcari = "" . $tanggal[2] . "-" . $tanggal[1] . "-" . $tanggal[0] . "";
                        $aFilteringRules[] = " " . $aColumns[$i] . "::text  ILIKE '%" . $tanggalcari . "%'";
                        $pencarianstatusvalidasi = 2;
                    } else {
                        $tanggalcari = "" . $tanggal[1] . "-" . $tanggal[0] . "";
                        $aFilteringRules[] = " " . $aColumns[$i] . "::text  ILIKE '%" . $tanggalcari . "%'";
                        $pencarianstatusvalidasi = 2;
                    }
                } else {

                    if ($aColumns[$i] == 's_idjenistransaksi') {
                        $aFilteringRules[] = " " . $aColumns[$i] . "::text  = '" . $input->getPost('sSearch_' . $i) . "'";
                        $pencarianstatusvalidasi = 2;
                    } elseif ($aColumns[$i] == 't_idnotarisspt') {
                        $aFilteringRules[] = " " . $aColumns[$i] . "::text  = '" . $input->getPost('sSearch_' . $i) . "'";
                        $pencarianstatusvalidasi = 2;
                    } elseif ($aColumns[$i] == 't_statusbayarspt') {
                        $aFilteringRules[] = " " . $aColumns[$i] . "::text  = '" . $input->getPost('sSearch_' . $i) . "'";
                        $pencarianstatusvalidasi = 2;
                    } elseif ($aColumns[$i] == 'status_pendaftaran') {
                        $aFilteringRules[] = " " . $aColumns[$i] . "::text  = '" . $input->getPost('sSearch_' . $i) . "'";
                        $pencarianstatusvalidasi = 2;
                    } elseif ($aColumns[$i] == 'status_validasi') {
                        //$aFilteringRules[] = " exists (select * from fr_cekstatus_validasi b where " . $aColumns[$i] . "::text ='" . $input->getPost('sSearch_' . $i) . "') ";
                        $aFilteringRules[] = " " . $aColumns[$i] . "::text  = '" . $input->getPost('sSearch_' . $i) . "'";
                        $pencarianstatusvalidasi = 1;
                    } else {
                        $aFilteringRules[] = " " . $aColumns[$i] . "::text  ILIKE '%" . $input->getPost('sSearch_' . $i) . "%'";
                        $pencarianstatusvalidasi = 2;
                    }
                    //$aFilteringRules[] = " " . $aColumns[$i] . "::text  ILIKE '%" . $input->getPost('sSearch_' . $i) . "%'";
                }

                $datacariall = $input->getPost('sSearch_' . $i);
            }
        }

        // if (!empty($aFilteringRules)) {
        //     if ($pencarianstatusvalidasi == 1) {
        //         $statusvalidasi = " ";
        //     } else {
        //         $statusvalidasi = " AND status_validasi::text = ANY (VALUES ('1'),('2')) AND t_tglverifikasispt IS NOT NULL";
        //         //exists (select * from fr_cekstatus_validasi b where status_validasi::text = ANY (VALUES ('1'),('2')))"; //status_validasi::text IN (VALUES ('1'),('2'))"; //in ('1','2')";
        //     }
        //     $sWhere = " WHERE " . implode(" AND ", $aFilteringRules) . " " . $statusvalidasi . " ";
        // } else {
        //     $sWhere = " WHERE status_validasi::text = ANY (VALUES ('1'),('2')) AND t_tglverifikasispt IS NOT NULL ";
        //     //exists (select * from fr_cekstatus_validasi b where status_validasi::text = ANY (VALUES ('1'),('2')))"; //status_validasi::text IN (VALUES ('1'),('2'))"; // in ('1','2')";
        // }

        // $aQueryColumns = array();
        // foreach ($aColumns as $col) {
        //     if ($col != ' ') {
        //         $aQueryColumns[] = $col;
        //     }
        // }

        // $sql = "SELECT " . implode(", ", $aQueryColumns) . "
        //                 FROM " . $sTable . " " . $sWhere . $sOrder . $sLimit; //count(*) OVER() AS SQL_CALC_FOUND_ROWS,
        // $statement = $this->adapter->query($sql);
        // $rResult = $statement->execute();

        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from(["a" => $this->fr_pendaftaran_v6()]);
        $where = new Where();
        if (!empty($aFilteringRules)) {
            if ($pencarianstatusvalidasi != 1) {
                $where->literal("status_validasi::text = ANY (VALUES ('1'),('2')) AND t_tglverifikasispt IS NOT NULL");
            }
            $where->literal(implode(" AND ", $aFilteringRules));
        } else {
            $where->literal("status_validasi::text = ANY (VALUES ('1'),('2')) AND t_tglverifikasispt IS NOT NULL");
        }
        $select->where($where);
        // echo ($select->getSqlString());
        // exit;
        $totaldata = $sql->prepareStatementForSqlObject($select)->execute()->count();
        $iTotal = $totaldata;
        $select->order($sOrder);
        $select->limit($sLimit);
        // die($select->getSqlString());

        $rResult = $sql->prepareStatementForSqlObject($select)->execute();

        // $totaldata = $this->getjumlahdata($sTable, $count, $sWhere);
        // $iTotal = $totaldata; //$totaldata['COUNT('.$count.')'];

        $output = array(
            "sEcho" => intval($input->getPost('sEcho')),
            "iTotalRecords" => $iTotal,
            "iTotalDisplayRecords" => $iTotal,
            "aaData" => array(),
        );

        foreach ($rResult as $aRow) {
            $row = array();
            //$btn = '<a class="btn btn-success btn-xs" href="#" onClick="showModals(\'' . $aRow['t_idspt'] . '\')" title="Edit"><i class="fa fa-edit"></i></a> <a class="btn btn-danger btn-xs" href="#" onClick="deleteUser(\'' . $aRow['t_idspt'] . '\')" title="Hapus"><i class="fa fa-bitbucket"></i></a>';
            // for ($i = 0; $i < $iColumnCount; $i++) {
            //     $row[] = $aRow[$aColumns[$i]];
            // }

            if ($aRow['status_pendaftaran'] == 1) {
                $status_pendaftaran = '<img title="Syarat Pendaftaran Lengkap" width="20" height="20" src="' . $cekurl . '/public/img/syaratlengkap.png">';
            } else {
                $status_pendaftaran = '<img title="Syarat Pendaftaran Tidak Lengkap" width="20" height="20" src="' . $cekurl . '/public/img/syarattidaklengkap.png">';
            }

            if ($aRow['t_statusbayarspt'] == 'TRUE') {
                $status_bayar = '<a class="btn btn-success btn-sm_fr" style="cursor:default;"><i class="fa fa-fw fa-money"></i> SUDAH</a>';
            } else {
                $status_bayar = '<a class="btn btn-danger btn-sm_fr" style="cursor:default;"><i class="fa fa-fw fa-money"></i> BELUM</a>';
            }

            if (($session['s_namauserrole'] == "Administrator")) {
                $admin_hapus = '<a style="background-color:red;color: #fff;" href="#" onclick="hapus(' . $aRow['t_idpembayaranspt'] . ');return false;"><i class="fa fa-fw fa-bitbucket"></i> Hapus Verifikasi</a>';
            } else {
                $admin_hapus = '';
            }

            if ($aRow['status_pendaftaran'] == 1) {
                $status_pendaftaran = '<img title="Syarat Pendaftaran Lengkap" width="20" height="20" src="' . $cekurl . '/public/img/syaratlengkap.png">';
            } else {
                $status_pendaftaran = '<img title="Syarat Pendaftaran Tidak Lengkap" width="20" height="20" src="' . $cekurl . '/public/img/syarattidaklengkap.png">';
            }

            if (($aRow['status_pendaftaran'] == 1) && ($aRow['status_validasi'] == 1)) {
                $batal = '';
                $cetaksurat = '';
                $status_verifikasi = '<img title="Data sudah di Validasi" width="20" height="20" src="' . $cekurl . '/public/img/tervalidasi.png">';
                if ($aRow['t_statusbayarspt'] == 'TRUE') {
                    $edit = '';
                    if (($aRow['fr_tervalidasidua'] == 1) || ($aRow['fr_validasidua'] == 1)) {
                        $validasikedua = '';
                    } else {
                        $validasikedua = '<a style="background-color:#008d4c;color: #fff;" href="verifikasi_spt/inputvalidasikedua?t_idspt=' . $aRow['t_idspt'] . '"><i class="fa fa-fw fa-check-square-o"></i> Validasi Ke Dua</a>    ';
                    }
                } else {
                    $validasikedua = '';
                    $edit = '<a href="verifikasi_spt/edit?t_idpembayaranspt=' . $aRow['t_idpembayaranspt'] . '"><i class="fa fa-fw fa-edit"></i> Edit</a>';
                }
            } else {
                $status_verifikasi = '<img title="Syarat Validasi Tidak Lengkap" width="20" height="20" src="' . $cekurl . '/public/img/validasi_tidaklengkap.png">';

                $cetaksurat = '<a href="#" onclick="openCetakBukti(' . $aRow['t_kohirspt'] . ');return false;"><i class="fa fa-fw fa-print"></i> Surat Pemberitahuan</a>';
                $validasikedua = '';

                if ($aRow['t_inputbpn'] == true) {
                    $edit = '<a href="verifikasi_spt/edit?t_idpembayaranspt=' . $aRow['t_idpembayaranspt'] . '"><i class="fa fa-fw fa-edit"></i> Edit</a>';
                    $batal = '';
                } else {
                    $edit = '<a href="verifikasi_spt/edit?t_idpembayaranspt=' . $aRow['t_idpembayaranspt'] . '"><i class="fa fa-fw fa-edit"></i> Edit</a>';
                    $batal = '<a href="#" onclick="hapus(' . $aRow['t_idpembayaranspt'] . ');return false;"><i class="fa fa-fw fa-undo"></i> Batal</a>';
                }
            }

            if (!empty($aRow['p_idpemeriksaan'])) {
                $jmlpajak = "<span style='float:right;color:blue;'>" . number_format($aRow['p_totalspt'], 0, ',', '.') . "</span>";
                // $cetaksuratpenelitian = '<a href="#" onclick="openCetakPenelitian(' . $aRow['t_kohirspt'] . ');return false;"><i class="fa fa-fw fa-print"></i> Surat Penelitian</a>';
            } else {
                $jmlpajak = "<span style='float:right;'>" . number_format($aRow['t_totalspt'], 0, ',', '.') . "</span>";
                // $cetaksuratpenelitian = '';
            }

            if ($aRow['fr_validasidua'] == 1) {
                $warnatr = '<span style="background-color:red;color: #fff;"> &nbsp; <i class="fa fa-fw fa-check-square-o"></i> </span> &nbsp;';
            } elseif ($aRow['fr_tervalidasidua'] == 1) {
                $warnatr = '<span style="background-color:red;color: #fff;"> &nbsp; <i class="fa fa-fw fa-minus-circle"></i> </span> &nbsp;';
            } elseif ($aRow['fr_tervalidasidua'] == 2) {
                $warnatr = '<span style="background-color:blue;color: #fff;"> &nbsp; <i class="fa fa-fw fa-plus-circle"></i> </span> &nbsp;';
            } else {
                $warnatr = '';
            }

            // if (!empty($aRow['t_idsptsebelumnya'])) {
            //     $cetaksspd = '<a href="#" onclick="openCetakSSPD(' . $aRow['t_idspt'] . ', ' . $aRow['t_idsptsebelumnya'] . ');return false;"><i class="fa fa-fw fa-print"></i> SSPD</a>';
            // } else {
            //     $cetaksspd = '<a href="#" onclick="openCetakSSPD(' . $aRow['t_idspt'] . ');return false;"><i class="fa fa-fw fa-print"></i> SSPD</a>';
            // }
            $btnUpdateKodebayar = '';
            // $btnUpdateKodebayar = '<a style="background-color: blue; color: #fff;" href="#" onclick="openUpdateKodebayar(' . $aRow['t_idpembayaranspt'] . ', ' . $aRow["t_kodebayarbanksppt"] . ');return false;"><i class="fa fa-fw fa-pencil"></i> UPDATE KODEBAYAR</a>';

            $btn = '<div class="dropdown">
                    <button onclick="myFunction(' . $aRow['t_idspt'] . ')" class="dropbtn btn-info dropdown-toggle btn-sm_fr">&nbsp;&nbsp;&nbsp;<span class="caret"></span>&nbsp;&nbsp;&nbsp;</button>
                    <div id="myDropdown' . $aRow['t_idspt'] . '" class="dropdown-content dropdown-menu" style="left:-120px; border-color: blue;">
                                
                                <a href="#" onclick="openCetak(' . $aRow['t_kohirspt'] . ');return false;"><i class="fa fa-fw fa-print"></i> Surat Bukti</a>
                                <a href="verifikasi_spt/viewdata?t_idspt=' . $aRow['t_idspt'] . '"><i class="fa fa-fw fa-eye"></i> Lihat Data</a>
                                ' . $cetaksurat . '  
                                ' . $edit . '    
                                ' . $batal . '
                                ' . $validasikedua . '
                                ' . $admin_hapus . '  
                                ' . $btnUpdateKodebayar . ' 
                                    
                    </div>
                  </div>';


            if ($aRow['t_inputbpn'] == true) {
                $t_kohirspt = '<span class="badge" style="background-color:#CC0000;"><a href="verifikasi_spt/viewdata?t_idspt=' . $aRow['t_idspt'] . '">' . $aRow['t_kohirspt'] . '</a></span>';
            } else {
                $t_kohirspt = '<a href="verifikasi_spt/viewdata?t_idspt=' . $aRow['t_idspt'] . '">' . $aRow['t_kohirspt'] . '</a>';
            }

            if (!empty($aRow['t_kohirketetapanspt'])) {
                $novalidasi = $aRow['t_kohirketetapanspt'];
            } else {
                $novalidasi = '';
            }


            $row = array(
                "<center>" . $no . "</center>",
                "<center>" . $warnatr . " " . $t_kohirspt . "</center>",
                "<center>" . $aRow['t_kohirketetapanspt'] . "</center>",
                "<center>" . $aRow['s_namajenistransaksi'] . "</center>",
                "<center>" . $aRow['t_nopbphtbsppt'] . "</center>",
                "<center>" . date('d-m-Y', strtotime($aRow['t_tglverifikasispt'])) . "</center>",
                $aRow['t_namawppembeli'],
                $aRow['t_namawppenjual'],
                $jmlpajak,
                "<center>" . $aRow['t_kodebayarbanksppt'] . "</center>",
                "<center>" . $status_pendaftaran . "</center>",
                "<center>" . $status_verifikasi . "</center>",
                "<center>" . $status_bayar . "</center>",
                "<center>" . $btn . "</center>"
            );
            $output['aaData'][] = $row;
            $no++;
        }

        return $output;
    }

    public function semuadatapendaftaran($sTable, $count, $input, $order_default, $aColumns, $session, $cekurl)
    {
        $aOrderingRules = array();
        $sLimit = "";
        if ($input->getPost('iDisplayStart') && $input->getPost('iDisplayLength') != '-1') {
            $sLimit = " LIMIT " . intval($input->getPost('iDisplayLength')) . " OFFSET " . intval($input->getPost('iDisplayStart'));
            //var_dump($sLimit);
            //exit();
            $no = 1 + intval($input->getPost('iDisplayStart'));
        } else {
            if (intval($input->getPost('iDisplayLength')) >= 1) {
                $sLimit = " LIMIT " . intval($input->getPost('iDisplayLength')) . " OFFSET " . intval($input->getPost('iDisplayStart'));
                $no = 1 + intval($input->getPost('iDisplayStart'));
            } else {
                $sLimit = " LIMIT 10 OFFSET 0";
                $no = 1;
            }
        }


        $aOrderingRules = array();
        if ($input->getPost('iSortCol_0')) {
            $iSortingCols = intval($input->getPost('iSortingCols'));
            for ($i = 0; $i < $iSortingCols; $i++) {
                if ($input->getPost('bSortable_' . intval($input->getPost('iSortCol_' . $i))) == 'true') {
                    $aOrderingRules[] = " " . $aColumns[intval($input->getPost('iSortCol_' . $i))] . "  "
                        . ($input->getPost('sSortDir_' . $i) === 'asc' ? 'asc' : 'desc');
                }
            }
        }

        if (!empty($aOrderingRules)) {
            $sOrder = " ORDER BY " . implode(", ", $aOrderingRules);
        } else {
            $sOrder = " ORDER BY " . $order_default . "";
        }

        $iColumnCount = count($aColumns);

        if ($input->getPost('sSearch') && $input->getPost('sSearch') != "") {
            $aFilteringRules = array();
            for ($i = 0; $i < $iColumnCount; $i++) {
                if ($input->getPost('bSearchable_' . $i) && $input->getPost('bSearchable_' . $i) == 'true') {
                    $tanggal = explode('-', $input->getPost('sSearch'));
                    if (count($tanggal) > 1) {
                        if (count($tanggal) > 2) {
                            $tanggalcari = "" . $tanggal[2] . "-" . $tanggal[1] . "-" . $tanggal[0] . "";
                            $aFilteringRules[] = " " . $aColumns[$i] . "::text  ILIKE '%" . $tanggalcari . "%'";
                        } else {
                            $tanggalcari = "" . $tanggal[1] . "-" . $tanggal[0] . "";
                            $aFilteringRules[] = " " . $aColumns[$i] . "::text  ILIKE '%" . $tanggalcari . "%'";
                        }
                    } else {
                        $aFilteringRules[] = " " . $aColumns[$i] . "::text  ILIKE '%" . $input->getPost('sSearch') . "%'";
                    }
                }
            }
            if (!empty($aFilteringRules)) {
                $aFilteringRules = array('(' . implode(" OR ", $aFilteringRules) . ')');
            }
        }


        for ($i = 0; $i < $iColumnCount; $i++) {
            if ($input->getPost('bSearchable_' . $i) && $input->getPost('bSearchable_' . $i) == 'true' && $input->getPost('sSearch_' . $i) != '') {
                $tanggal = explode('-', $input->getPost('sSearch_' . $i));

                if (count($tanggal) > 1) {
                    if (count($tanggal) > 2) {
                        $tanggalcari = "" . $tanggal[2] . "-" . $tanggal[1] . "-" . $tanggal[0] . "";
                        $aFilteringRules[] = " " . $aColumns[$i] . "::text  ILIKE '%" . $tanggalcari . "%'";
                    } else {
                        $tanggalcari = "" . $tanggal[1] . "-" . $tanggal[0] . "";
                        $aFilteringRules[] = " " . $aColumns[$i] . "::text  ILIKE '%" . $tanggalcari . "%'";
                    }
                } else {
                    if ($aColumns[$i] == 't_idjenistransaksi') {
                        $aFilteringRules[] = " " . $aColumns[$i] . "::text  = '" . $input->getPost('sSearch_' . $i) . "'";
                    } else {
                        $aFilteringRules[] = " " . $aColumns[$i] . "::text  ILIKE '%" . $input->getPost('sSearch_' . $i) . "%'";
                    }
                    if ($aColumns[$i] == 't_idnotarisspt') {
                        $aFilteringRules[] = " " . $aColumns[$i] . "::text  = '" . $input->getPost('sSearch_' . $i) . "'";
                    } else {
                        $aFilteringRules[] = " " . $aColumns[$i] . "::text  ILIKE '%" . $input->getPost('sSearch_' . $i) . "%'";
                    }
                    //$aFilteringRules[] = " " . $aColumns[$i] . "::text  ILIKE '%" . $input->getPost('sSearch_' . $i) . "%'";
                }

                $datacariall = $input->getPost('sSearch_' . $i);
            }
        }



        if (!empty($aFilteringRules)) {
            $sWhere = " WHERE " . implode(" AND ", $aFilteringRules) . " "; //AND t_tglverifikasispt IS NULL AND status_pendaftaran = 1
        } else {
            $sWhere = " "; //WHERE t_tglverifikasispt IS NULL AND status_pendaftaran = 1
        }

        $aQueryColumns = array();
        foreach ($aColumns as $col) {
            if ($col != ' ') {
                $aQueryColumns[] = $col;
            }
        }
        $sql = "SELECT " . implode(", ", $aQueryColumns) . "
                        FROM " . $sTable . " " . $sWhere . $sOrder . $sLimit; //count(*) OVER() AS SQL_CALC_FOUND_ROWS,


        $statement = $this->adapter->query($sql);
        $rResult = $statement->execute();

        //var_dump($sql);
        //exit();

        $totaldata = $this->getjumlahdata($sTable, $count, $sWhere);
        $iTotal = $totaldata; //$totaldata['COUNT('.$count.')'];



        $output = array(
            "sEcho" => intval($input->getPost('sEcho')),
            "iTotalRecords" => $iTotal,
            "iTotalDisplayRecords" => $iTotal,
            "aaData" => array(),
        );



        foreach ($rResult as $aRow) {
            $row = array();
            for ($i = 0; $i < $iColumnCount; $i++) {
                $row[] = $aRow[$aColumns[$i]];
            }


            $btn = '<a href="#" class="btn btn-block btn-info btn-sm" onclick="pilihPendataanSspdBphtb(' . $aRow['t_idspt'] . ');return false;"><i class="fa fa-fw fa-hand-pointer-o"></i> PILIH</a>';



            $row = array(
                "<center>" . $no . "</center>",
                "<center>" . $aRow['t_kohirspt'] . "</center>",
                "<center>" . $aRow['t_kodedaftarspt'] . "</center>",
                "<center>" . date('d-m-Y', strtotime($aRow['t_tglprosesspt'])) . "</center>",
                "<center>" . $aRow['t_nopbphtbsppt'] . "</center>",
                $aRow['t_namawppembeli'],
                $aRow['t_namawppenjual'],
                "<center>" . $btn . "</center>",
                "<center>" . $aRow['s_namajenistransaksi'] . "</center>",
                $aRow['s_namanotaris']
            );
            $output['aaData'][] = $row;
            $no++;
        }

        return $output;
    }

    //================= end datagrid verifikasi


    public function temukanDataPembayaran(VerifikasiSPTBase $spt)
    {
        $sql = "select * from view_sspd a left join t_pembayaranspt b on b.t_idspt = a.t_idspt where  b.t_kohirpembayaran=" . $spt->t_kohirpembayaran . "";
        $statement = $this->adapter->query($sql);
        return $statement->execute()->current();
    }

    public function getDatas($spt)
    {
        $sql = "select * from view_sspd_pembayaran where t_idpembayaranspt = " . $spt . "";
        $statement = $this->adapter->query($sql);
        return $statement->execute()->current();
    }

    public function savedata(VerifikasiSPTBase $verspt, $t_pejabatverifikasi)
    {
        $id_pembayaran = (int) $verspt->t_idpembayaranspt;
        $data = array(
            't_verifikasispt' => \Zend\Json\Json::encode($verspt->t_persyaratanverifikasi),
            't_tglverifikasispt' => date('Y-m-d', strtotime($verspt->t_tglverifikasispt)),
            't_pejabatverifikasispt' => $t_pejabatverifikasi,
            't_kodebayarbanksppt' => $verspt->t_kodebayarbanksppt
        );
        if ($id_pembayaran == 0) {
            $data['t_idspt'] = $verspt->t_idspt;
            $data['t_idnotaris'] = $verspt->t_idnotarisspt;
            $this->insert($data);
        } else {
            $this->update($data, array('t_idpembayaranspt' => $verspt->t_idpembayaranspt));
        }
    }

    public function updatenosspd_validasikedua($idspt, $max)
    {
        $data = array(
            't_kohirketetapanspt' => $max
        ); //+ 1
        $t_spt = new TableGateway('t_spt', $this->adapter);
        $t_spt->update($data, array('t_idspt' => $idspt));
    }

    public function simpannpwpdbaru($post)
    {
        $data = array(
            // Data Pembeli
            't_npwpwppembeli' => $post->t_npwpwppembeli,
        );
        $t_detailsptbphtb = new \Zend\Db\TableGateway\TableGateway('t_detailsptbphtb', $this->adapter);
        $t_detailsptbphtb->update($data, array('t_iddetailsptbphtb' => $post->t_iddetailsptbphtb));
    }

    public function updatenosspd(VerifikasiSPTBase $verspt, $max)
    {
        $data = array(
            't_kohirketetapanspt' => $max
        ); //+ 1
        $t_spt = new TableGateway('t_spt', $this->adapter);
        $t_spt->update($data, array('t_idspt' => $verspt->t_idspt));
    }

    public function savedataverifikasipembayaran(VerifikasiSPTBase $verspt, $t_pejabatverifikasi)
    {
        $id = (int) $verspt->t_idpembayaranspt;
        $data = array(
            //Berhubungan dengan Verifikasi
            't_idspt' => $verspt->t_idspt,
            't_idnotaris' => $verspt->t_idnotarisspt,
            't_verifikasispt' => \Zend\Json\Json::encode($verspt->t_persyaratanverifikasi),
            't_tglverifikasispt' => date('Y-m-d', strtotime($verspt->t_tglverifikasispt)),
            't_pejabatverifikasispt' => $t_pejabatverifikasi,
            //Berhubungan dengan Pembayaran
            't_periodepembayaran' => date('Y', strtotime($verspt->t_tglverifikasispt)),
            't_tanggalpembayaran' => date('Y-m-d', strtotime($verspt->t_tglverifikasispt)),
            't_ketetapanspt' => 1,
            't_nilaipembayaranspt' => 0,
            't_statusbayarspt' => true,
            't_pejabatpembayaranspt' => $t_pejabatverifikasi,
            't_kodebayarbanksppt' => $verspt->t_kodebayarbanksppt
        );
        if ($id == 0) {
            $this->insert($data);
        } else {
            $this->update($data, array('t_idpembayaranspt' => (int) $verspt->t_idpembayaranspt));
        }
    }

    public function savedataverbayarbpn(VerifikasiSPTBase $verspt, $idspt, $kodebayarbank)
    {
        $data = array(
            //Berhubungan dengan Verifikasi
            't_idspt' => $idspt,
            't_idnotaris' => $verspt->t_idnotaris,
            't_verifikasispt' => $verspt->t_verifikasispt,
            't_tglverifikasispt' => $verspt->t_tglverifikasispt,
            't_pejabatverifikasispt' => $verspt->t_pejabatverifikasispt,
            //Berhubungan dengan Pembayaran
            't_periodepembayaran' => date('Y'),
            't_tanggalpembayaran' => date('Y-m-d'),
            't_ketetapanspt' => 1,
            't_nilaipembayaranspt' => 0,
            't_statusbayarspt' => true
        );
        $this->insert($data);
    }

    public function savedataverifikasi_validasikedua(VerifikasiSPTBase $verspt, $idspt, $kohir, $tglproses)
    {
        $data = array(
            't_idspt' => $idspt,
            't_idnotaris' => $verspt->t_idnotaris,
            't_verifikasispt' => $verspt->t_verifikasispt,
            't_tglverifikasispt' => $verspt->t_tglverifikasispt,
            't_pejabatverifikasispt' => $verspt->t_pejabatverifikasispt,
            't_kodebayarbanksppt' => '1613' . str_pad($kohir, 6, "0", STR_PAD_LEFT) . date('y', strtotime($tglproses))
        );

        $this->insert($data);
    }

    public function savedataverifikasibpn(VerifikasiSPTBase $verspt, $idspt, $kodebayarbank)
    {
        $data = array(
            't_idspt' => $idspt,
            't_idnotaris' => $verspt->t_idnotaris,
            't_verifikasispt' => $verspt->t_verifikasispt,
            't_tglverifikasispt' => $verspt->t_tglverifikasispt,
            't_pejabatverifikasispt' => $verspt->t_pejabatverifikasispt,
            't_kodebayarbanksppt' => $kodebayarbank
        );
        $this->insert($data);
    }

    public function updatenonpwpd($dt)
    {
        $data = array(
            //"p_idpembayaranspt" => $dt->t_idpembayaranspt, //$id_bayar['max'],
            "t_npwpwppembeli" => $dt->t_npwpwppembeli,
        );

        $data['t_idspt'] = $dt->t_idspt;
        $t_detailsptbphtb = new TableGateway('t_detailsptbphtb', $this->adapter);
        $t_detailsptbphtb->update($data, array('t_idspt' => $dt->t_idspt));
    }

    public function savedatapemeriksaan($dt, $p_totalnjoptanah, $p_totalnjopbangunan, $p_grandtotalnjop, $p_grandtotalnjop_aphb, $p_potonganspt, $p_totalspt)
    {
        $id_bayar = $this->getIdPembayaran();

        $p_verifikasi_adm_nilai_wajar = (($dt->p_verifikasi_adm_nilai_wajar != "") ? str_ireplace(".", "", $dt->p_verifikasi_adm_nilai_wajar) : null);
        if ($p_verifikasi_adm_nilai_wajar == 0) $p_verifikasi_adm_nilai_wajar = null;

        $p_verifikasi_lap_nilai_wajar = (($dt->p_verifikasi_lap_nilai_wajar != "") ? str_ireplace(".", "", $dt->p_verifikasi_lap_nilai_wajar) : null);
        if ($p_verifikasi_lap_nilai_wajar == 0) $p_verifikasi_lap_nilai_wajar = null;

        $data = array(
            //"p_idpembayaranspt" => $dt->t_idpembayaranspt, //$id_bayar['max'],
            "p_luastanah" => str_ireplace(".", "", $dt->p_luastanah),
            "p_luasbangunan" => str_ireplace(".", "", $dt->p_luasbangunan),
            "p_njoptanah" => str_ireplace(".", "", $dt->p_njoptanah),
            "p_njopbangunan" => str_ireplace(".", "", $dt->p_njopbangunan),
            "p_totalnjoptanah" => $p_totalnjoptanah, //str_ireplace(".", "", $dt->p_totalnjoptanah),
            "p_totalnjopbangunan" => $p_totalnjopbangunan, //str_ireplace(".", "", $dt->p_totalnjopbangunan),
            "p_grandtotalnjop" => $p_grandtotalnjop, //str_ireplace(".", "", $dt->p_grandtotalnjop),
            "p_nilaitransaksispt" => str_ireplace(".", "", $dt->p_nilaitransaksispt),
            "p_ketwaris" => $dt->p_ketwaris,
            "p_terbukti" => $dt->p_terbukti,
            "p_idjenistransaksi" => $dt->p_idjenistransaksi,
            "p_idjenishaktanah" => $dt->p_idjenishaktanah,
            "p_potonganspt" => $p_potonganspt, //str_ireplace(".", "", $dt->p_potonganspt),
            "p_totalspt" => $p_totalspt, //str_ireplace(".", "", $dt->p_totalspt)
            "p_grandtotalnjop_aphb" => $p_grandtotalnjop_aphb,

            "p_verifikasi_adm_nilai_wajar" => $p_verifikasi_adm_nilai_wajar,
            "p_verifikasi_lap_nilai_wajar" => $p_verifikasi_lap_nilai_wajar,
            "p_verifikasi_adm_pertimbangan" => (($dt->p_verifikasi_adm_pertimbangan != "") ? $dt->p_verifikasi_adm_pertimbangan : null),
            "p_verifikasi_lap_pertimbangan" => (($dt->p_verifikasi_lap_pertimbangan != "") ? $dt->p_verifikasi_lap_pertimbangan : null),
            "p_petugas_berita_acara" => ($dt->p_petugas_berita_acara != '[]') ? $dt->p_petugas_berita_acara : null,
        );

        if ($dt->p_idpemeriksaan == 0) {
            if (!empty($dt->t_idpembayaranspt)) {
                $data['p_idpembayaranspt'] = $dt->t_idpembayaranspt;
            } else {
                $data['p_idpembayaranspt'] = $id_bayar['max'];
            }
            $t_pemeriksaan = new TableGateway('t_pemeriksaan', $this->adapter);
            $t_pemeriksaan->insert($data);
        } else {
            $data['p_idpemeriksaan'] = $dt->p_idpemeriksaan;
            $t_pemeriksaan = new TableGateway('t_pemeriksaan', $this->adapter);
            $t_pemeriksaan->update($data, array('p_idpemeriksaan' => $dt->p_idpemeriksaan));
        }
    }

    public function savedatapemeriksaan2($dt)
    {
        $id_bayar = $this->getIdPembayaran();

        if (!empty($dt->p_t_grandtotalnjop_aphb)) {
            $aphbtot = str_ireplace(".", "", $dt->p_t_grandtotalnjop_aphb);
        } else {
            $aphbtot = 0;
        }

        $data = array(
            "p_idpembayaranspt" => $id_bayar['max'],
            "p_luastanah" => str_ireplace(".", "", $dt->p_luastanah),
            "p_luasbangunan" => str_ireplace(".", "", $dt->p_luasbangunan),
            "p_njoptanah" => str_ireplace(".", "", $dt->p_njoptanah),
            "p_njopbangunan" => str_ireplace(".", "", $dt->p_njopbangunan),
            "p_totalnjoptanah" => str_ireplace(".", "", $dt->p_totalnjoptanah),
            "p_totalnjopbangunan" => str_ireplace(".", "", $dt->p_totalnjopbangunan),
            "p_grandtotalnjop" => str_ireplace(".", "", $dt->p_grandtotalnjop),
            "p_nilaitransaksispt" => str_ireplace(".", "", $dt->p_nilaitransaksispt),
            "p_ketwaris" => $dt->p_ketwaris,
            "p_terbukti" => $dt->p_terbukti,
            "p_idjenistransaksi" => $dt->p_idjenistransaksi,
            "p_idjenishaktanah" => $dt->p_idjenishaktanah,
            "p_potonganspt" => str_ireplace(".", "", $dt->p_potonganspt),
            "p_totalspt" => str_ireplace(".", "", $dt->p_totalspt),
            "p_grandtotalnjop_aphb" => $aphbtot
        );
        if ($dt->p_idpemeriksaan == 0) {
            $t_pemeriksaan = new TableGateway('t_pemeriksaan', $this->adapter);
            $t_pemeriksaan->insert($data);
        } else {
            $data['p_idpemeriksaan'] = $dt->p_idpemeriksaan;
            $t_pemeriksaan = new TableGateway('t_pemeriksaan', $this->adapter);
            $t_pemeriksaan->update($data, array('p_idpemeriksaan' => $dt->p_idpemeriksaan));
        }
    }

    public function savedatapemeriksaan_hapus($dt)
    {
        $id_bayar = $this->getIdPembayaran();



        if (!empty($dt->t_idpembayaranspt)) {
            $t_pemeriksaan = new TableGateway('t_pemeriksaan', $this->adapter);
            $t_pemeriksaan->delete(array('p_idpembayaranspt' => $dt->t_idpembayaranspt));
        } else {
        }
    }

    public function batalVerifikasi($verspt)
    {
        //        $dp = $this->getDatas($verspt);
        //        $data = array(
        //            't_verifikasispt' => NULL,
        //            't_tglverifikasispt' => NULL,
        ////            't_pejabatverifikasispt' => $pembspt->t_alamatwppenjual,
        //        );
        //        if ($dp['t_statusbayarspt']) {
        //            $this->update($data, array('t_idpembayaranspt' => (int) $verspt));
        //        } else {
        $sql = "select t_idspt from t_pembayaranspt where t_idpembayaranspt = " . $verspt . " ";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute()->current();

        $data = array(
            't_kohirketetapanspt' => null
        ); //+ 1
        $t_spt = new TableGateway('t_spt', $this->adapter);
        $t_spt->update($data, array('t_idspt' => $res['t_idspt']));

        $data2 = array(
            't_statuspelaporannotaris' => null,
            't_tglpelaporannotaris' => null,
            't_noajbbaru' => null,
            't_tglajbbaru' => null
        );

        $t_detailsptbphtb = new TableGateway('t_detailsptbphtb', $this->adapter);
        $t_detailsptbphtb->update($data2, array(
            't_idspt' => $res['t_idspt']
        ));


        $this->delete(array('t_idpembayaranspt' => (int) $verspt));
        //        }
    }

    public function batalVerifikasi2($verspt)
    {
        //        $dp = $this->getDatas($verspt);
        //        $data = array(
        //            't_verifikasispt' => NULL,
        //            't_tglverifikasispt' => NULL,
        ////            't_pejabatverifikasispt' => $pembspt->t_alamatwppenjual,
        //        );
        //        if ($dp['t_statusbayarspt']) {
        //            $this->update($data, array('t_idpembayaranspt' => (int) $verspt));
        //        } else {
        $this->delete(array('t_idpembayaranspt' => (int) $verspt));
        //        }
    }

    public function hapusDataPemeriksaan($id)
    {
        $t_pemeriksaan = new TableGateway('t_pemeriksaan', $this->adapter);
        $t_pemeriksaan->delete(array('p_idpembayaranspt' => $id));
    }

    public function getIdPembayaran()
    {
        $sql = "select max(t_idpembayaranspt) from t_pembayaranspt";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        return $res->current();
    }

    public function getSptid($t_idpembayaranspt)
    {
        $rowset = $this->select(array('t_idpembayaranspt' => $t_idpembayaranspt));
        $row = $rowset->current();
        return $row;
    }

    public function getViewPendaftaran($t_idspt)
    {
        $sql = "select * from view_pendaftaran where t_idspt = " . $t_idspt . "";
        $statement = $this->adapter->query($sql);
        return $statement->execute()->current();
    }

    public function getHargaAcuan($s_kd_propinsi, $s_kd_dati2, $s_kd_kecamatan, $s_kd_kelurahan, $s_kd_blok)
    {
        $sql = "select * from s_acuan where s_kd_propinsi = '" . $s_kd_propinsi . "' 
                and s_kd_dati2 = '" . $s_kd_dati2 . "' 
                and s_kd_kecamatan = '" . $s_kd_kecamatan . "' 
                and s_kd_kelurahan = '" . $s_kd_kelurahan . "'
                and s_kd_blok = '" . $s_kd_blok . "'";
        $statement = $this->adapter->query($sql);
        return $statement->execute()->current();
    }

    public function getHargaHistoryNJOPTanah($s_kd_propinsi, $s_kd_dati2, $s_kd_kecamatan, $s_kd_kelurahan, $s_kd_blok, $t_idpembayaranspt)
    {
        $sql = "select max(njoptanahtransaksi) as njoptanahtransaksi , max(njoptanah) as njoptanah from view_harganjoptanah
                where t_nopbphtbsppt like '%" . $s_kd_propinsi . "." . $s_kd_dati2 . "." . $s_kd_kecamatan . "." . $s_kd_kelurahan . "." . $s_kd_blok . "%'
                and t_idpembayaranspt not in ($t_idpembayaranspt)";
        $statement = $this->adapter->query($sql);
        return $statement->execute()->current();
    }

    public function getHargaHistoryNJOPTanahpilih($s_kd_propinsi, $s_kd_dati2, $s_kd_kecamatan, $s_kd_kelurahan, $s_kd_blok, $t_idspt)
    {
        $sql = "select max(njoptanahtransaksi) as njoptanahtransaksi , max(njoptanah) as njoptanah from view_harganjoptanah
                where t_nopbphtbsppt like '%" . $s_kd_propinsi . "." . $s_kd_dati2 . "." . $s_kd_kecamatan . "." . $s_kd_kelurahan . "." . $s_kd_blok . "%'
                and t_idspt not in ($t_idspt)";
        $statement = $this->adapter->query($sql);
        return $statement->execute()->current();
    }

    public function getPresentase($nilaipresentase)
    {
        $sql = "select * from s_presentase where s_presentase >= " . $nilaipresentase . " and s_presentasemin <= " . $nilaipresentase . "";
        $statement = $this->adapter->query($sql);
        return $statement->execute()->current();
    }

    public function getmaxkohir()
    {
        //$sql = "select max(t_kohirketetapanspt) as t_kohirketetapanspt from t_spt";
        $sql = "SELECT COALESCE(max(t_kohirketetapanspt)+1,1) as t_kohirketetapanspt FROM t_spt WHERE t_periodespt::text='" . date('Y') . "'";
        $statement = $this->adapter->query($sql);
        return $statement->execute()->current();
    }

    public function fr_pendaftaran_v6($string = null)
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from(["a" => "t_spt"]);
        $select->columns([
            "t_idspt", "t_kohirspt", "t_periodespt", "t_tglprosesspt",
            "t_persyaratan", "t_idjenistransaksi", "t_idnotarisspt",
            "t_totalspt", "t_kohirketetapanspt",
            "t_nopbphtbsppt", "fr_tervalidasidua", "t_nilaitransaksispt",
            "t_idsptsebelumnya", "t_kodedaftarspt", "t_jenispembelian"
        ]);
        $select->join(["b" => "t_detailsptbphtb"], "a.t_idspt = b.t_idspt", [
            "t_namawppembeli", "t_namawppenjual", "t_inputbpn", "fr_validasidua",
            "t_grandtotalnjop", "t_alamatwppembeli"
        ], "LEFT");
        $select->join(["c" => "s_jenistransaksi"], "c.s_idjenistransaksi = a.t_idjenistransaksi", [
            "s_idjenistransaksi", "s_namajenistransaksi"
        ], "LEFT");
        $select->join(["e" => "t_pembayaranspt"], "a.t_idspt = e.t_idspt", [
            "t_verifikasispt", "t_tglverifikasispt", "t_tglverifikasiberkas",
            "t_idpembayaranspt", "t_kodebayarbanksppt"
        ], "LEFT");
        $select->join(["f" => "t_pemeriksaan"], "f.p_idpembayaranspt = e.t_idpembayaranspt", [
            "p_idpemeriksaan", "p_totalspt", "p_grandtotalnjop", "p_grandtotalnjop_aphb",
            "p_nilaitransaksispt"
        ], "LEFT");
        $select->join(["h" => "s_users"], "h.s_iduser = a.t_idnotarisspt", [], "LEFT");
        $select->join(["i" => "s_notaris"], new Expression("i.s_idnotaris::TEXT = h.s_idpejabat_idnotaris::TEXT"), [
            "s_namanotaris"
        ], "LEFT");
        $select->join(["j" => $this->fr_count_s_persyaratan()], new Expression("a.t_idjenistransaksi = j.s_idjenistransaksi"), [
            "jml_syarat_input" => new Expression("(LENGTH(TRANSLATE(a.t_persyaratan::TEXT, '[\"]1234567890'::TEXT, ''::TEXT)) + 1)"),
            "jml_syarat_validasi" => new Expression("(LENGTH(TRANSLATE(e.t_verifikasispt::TEXT, '[\"]1234567890'::TEXT, ''::TEXT)) + 1)"),
            "jml_syarat_validasi_berkas" => new Expression("(LENGTH(TRANSLATE(e.t_verifikasiberkas::TEXT, '[\"]1234567890'::TEXT, ''::TEXT)) + 1)"),
            "jml_syarat_sebenarnya" => "jmlsyarat",
            "status_pendaftaran" => new Expression("(
                CASE WHEN (LENGTH(TRANSLATE(a.t_persyaratan::TEXT, '[\"]1234567890'::TEXT, ''::TEXT)) + 1) = j.jmlsyarat THEN 1
                ELSE 2 END
            )"),
            "status_validasi" => new Expression("(
                CASE WHEN (LENGTH(TRANSLATE(e.t_verifikasispt::TEXT, '[\"]1234567890'::TEXT, ''::TEXT)) + 1) = j.jmlsyarat THEN 1
                WHEN (LENGTH(TRANSLATE(e.t_verifikasispt::TEXT, '[\"]1234567890'::TEXT, ''::TEXT)) + 1) IS NULL THEN 3
                ELSE 2 END
            )"),
            "status_berkas" => new Expression("(
                CASE WHEN (length(translate(e.t_verifikasiberkas::text, '[\"]1234567890'::text, ''::text)) + 1) = j.jmlsyarat THEN 1
                WHEN (length(translate(e.t_verifikasiberkas::text, '[\"]1234567890'::text, ''::text)) + 1) IS NULL THEN 3
                ELSE 2 END
            )"),
            "t_statusbayarspt" => new Expression("(
                CASE WHEN e.t_statusbayarspt::text = 'true'::text THEN 'TRUE'::text
                ELSE 'FALSE'::text END
            )"),
            "jml_pajak_v1" => new Expression("(
                CASE WHEN f.p_idpemeriksaan IS NOT NULL THEN f.p_totalspt
                ELSE a.t_totalspt END
            )"),
            "jml_pajak_v2" => new Expression("(
                CASE WHEN f.p_idpemeriksaan IS NOT NULL THEN
                    CASE WHEN a.t_totalspt < f.p_totalspt THEN f.p_totalspt
                    ELSE a.t_totalspt END
                ELSE a.t_totalspt END 
            )")
        ], "LEFT");

        if ($string != null) {
            return $select->getSqlString();
        } else {
            return $select;
        }
    }

    public function fr_count_s_persyaratan($string = null)
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from("s_persyaratan");
        $select->columns([
            "s_idjenistransaksi", "jmlsyarat" => new Expression("(COUNT(*))")
        ]);
        $select->group("s_idjenistransaksi");
        $select->order("s_idjenistransaksi");

        if ($string != null) {
            return $select->getSqlString();
        } else {
            return $select;
        }
    }

    // 03112021 - irfan ari
    // karena kodebayar sama dengan ntpd, ntpd tidak bisa diproses bpn
    // maka perlu update kodebayar
    public function updateKodebayar($dataget, $session)
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select("t_pembayaranspt")->where(["t_idpembayaranspt" => $dataget["t_idpembayaranspt"]]);
        $res = $sql->prepareStatementForSqlObject($select)->execute()->current();

        $update = $sql->update("t_pembayaranspt")->set([
            "t_kodebayarbanksppt" => $this->createKodebayar(date("y", strtotime($res["t_tglverifikasispt"]))),
            "t_kodebayarbanksppt_lama" => $res["t_kodebayarbanksppt"],
            "t_user_update_kodebayar" => $session["s_iduser"],
            "t_tgl_update_kodebayar" => date("Y-m-d H:i:s"),
            "t_ket_update_kodebayar" => $dataget["t_ket_update_kodebayar"]
        ])->where(["t_idpembayaranspt" => $dataget["t_idpembayaranspt"]]);

        $response = $sql->prepareStatementForSqlObject($update)->execute();
        return $response;
    }

    public function createKodebayar($tahun)
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select("t_pembayaranspt")->columns(["t_kodebayarbanksppt"])->where("RIGHT(t_kodebayarbanksppt, 2) = '" . $tahun . "'")->order("t_kodebayarbanksppt DESC");
        $res = $sql->prepareStatementForSqlObject($select)->execute()->current();
        if ($res) {
            // $noUrut = substr(str_replace("1613", "", $res["t_kodebayarbanksppt"]), 0, -2); //kodebayar lama
            $noUrut = substr($res["t_kodebayarbanksppt"], 4); //menghapus kode kab
            $noUrut = substr($noUrut, 0, -2); //menghapus kode tahun
            $noUrut = $noUrut + 1;
            $kodebayar = '1613' . str_pad($noUrut, 6, "0", STR_PAD_LEFT) . $tahun;
        } else {
            $kodebayar = '1613' . str_pad(1, 6, "0", STR_PAD_LEFT) . $tahun;
        }
        return $kodebayar;
    }
}
