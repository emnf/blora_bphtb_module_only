<?php

namespace Bphtb\Model\Spop;

use Bphtb\Helper\CacheHelper;
use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Where;
use Zend\Debug\Debug;

class ReffSpopTable extends AbstractTableGateway
{

    protected $cacheHelper;

    public function __construct(Adapter $adapter)
    {
        $this->adapter = $adapter;
        $this->cacheHelper = new CacheHelper();
    }

    public function getLookUpItemByKodeGroup($kodeGroup)
    {
        $cache = $this->cacheHelper->cache();
        $key = 'ReffSpopTable-getLookUpItemByKodeGroup-' . $kodeGroup;
        if (!$cache->getItem($key)) {
            $sql = new Sql($this->adapter);
            $select = $sql->select(['A' => 'LOOKUP_ITEM'])->where(['KD_LOOKUP_GROUP' => $kodeGroup]);
            $select->order('KD_LOOKUP_ITEM');
            $res = $sql->prepareStatementForSqlObject($select)->execute();
            $arr = [];
            foreach ($res as $key => $value) {
                $arr[$value['KD_LOOKUP_ITEM']] = str_pad($value['KD_LOOKUP_ITEM'], 3, '0', STR_PAD_LEFT) . ' | ' . $value['NM_LOOKUP_ITEM'];
            }

            $cache->setItem($key, $arr);
        }
        return $cache->getItem($key);
    }

    public function getComboKecamatan()
    {
        $cache = $this->cacheHelper->cache();
        $key = 'ReffSpopTable-getComboKecamatan';
        if (!$cache->getItem($key)) {
            $sql = new Sql($this->adapter);
            $select = $sql->select(['A' => 'REF_KECAMATAN'])->order('KD_KECAMATAN');
            $res = $sql->prepareStatementForSqlObject($select)->execute();
            $arr = [];
            foreach ($res as $key => $value) {
                $arr[$value['KD_KECAMATAN']] = str_pad($value['KD_KECAMATAN'], 3, '0', STR_PAD_LEFT) . ' | ' . $value['NM_KECAMATAN'];
            }

            $cache->setItem($key, $arr);
        }
        return $cache->getItem($key);
    }

    public function getComboKecamatanSelected($kodeKecamatan)
    {
        $cache = $this->cacheHelper->cache();
        $key = 'ReffSpopTable-getComboKecamatanSelected';
        if (!$cache->getItem($key)) {
            $sql = new Sql($this->adapter);
            $select = $sql->select(['A' => 'REF_KECAMATAN'])->order('KD_KECAMATAN');
            $select->where(['KD_KECAMATAN' => $kodeKecamatan]);
            $res = $sql->prepareStatementForSqlObject($select)->execute();
            $arr = [];
            foreach ($res as $key => $value) {
                $arr[$value['KD_KECAMATAN']] = str_pad($value['KD_KECAMATAN'], 3, '0', STR_PAD_LEFT) . ' | ' . $value['NM_KECAMATAN'];
            }

            $cache->setItem($key, $arr);
        }
        return $cache->getItem($key);
    }

    public function getComboKelurahanByKodeKecamatan($kodeKecamatan)
    {
        $cache = $this->cacheHelper->cache();
        $key = 'ReffSpopTable-getComboKelurahanByKodeKecamatan-' . $kodeKecamatan;
        if (!$cache->getItem($key)) {
            $sql = new Sql($this->adapter);
            $select = $sql->select(['A' => 'REF_KELURAHAN'])->order('KD_KELURAHAN');
            $select->where(['KD_KECAMATAN' => $kodeKecamatan]);
            $res = $sql->prepareStatementForSqlObject($select)->execute();
            $arr = [];
            foreach ($res as $key => $value) {
                $arr[$value['KD_KELURAHAN']] = str_pad($value['KD_KELURAHAN'], 3, '0', STR_PAD_LEFT) . ' | ' . $value['NM_KELURAHAN'];
            }

            $cache->setItem($key, $arr);
        }
        return $cache->getItem($key);
    }

    public function getComboKelurahanByKodeKecamatanSelected($kodeKecamatan, $kodeKelurahan)
    {
        $cache = $this->cacheHelper->cache();
        $key = 'ReffSpopTable-getComboKelurahanByKodeKecamatanSelected-' . $kodeKecamatan . '-' . $kodeKelurahan;
        if (!$cache->getItem($key)) {
            $sql = new Sql($this->adapter);
            $select = $sql->select(['A' => 'REF_KELURAHAN'])->order('KD_KELURAHAN');
            $select->where(['KD_KECAMATAN' => $kodeKecamatan, 'KD_KELURAHAN' => $kodeKelurahan]);
            $res = $sql->prepareStatementForSqlObject($select)->execute();
            $arr = [];
            foreach ($res as $key => $value) {
                $arr[$value['KD_KELURAHAN']] = str_pad($value['KD_KELURAHAN'], 3, '0', STR_PAD_LEFT) . ' | ' . $value['NM_KELURAHAN'];
            }

            $cache->setItem($key, $arr);
        }
        return $cache->getItem($key);
    }

    public function getComboBlokSelected($kodeKecamatan, $kodeKelurahan, $kodeBlok)
    {
        $cache = $this->cacheHelper->cache();
        $key = 'ReffSpopTable-getComboBlokSelected-' . $kodeKecamatan . '-' . $kodeKelurahan;
        if (!$cache->getItem($key)) {
            $sql = new Sql($this->adapter);
            $select = $sql->select(['A' => 'DAT_PETA_BLOK'])->order('KD_BLOK');
            $select->where(['KD_KECAMATAN' => $kodeKecamatan, 'KD_KELURAHAN' => $kodeKelurahan, 'KD_BLOK' => $kodeBlok]);
            $res = $sql->prepareStatementForSqlObject($select)->execute();
            $arr = [];
            foreach ($res as $key => $value) {
                $arr[$value['KD_BLOK']] = $value['KD_BLOK'];
            }

            $cache->setItem($key, $arr);
        }
        return $cache->getItem($key);
    }

    public function getJenisBangunan()
    {
        $cache = $this->cacheHelper->cache();
        $key = 'ReffSpopTable-getJenisBangunan';
        if (!$cache->getItem($key)) {
            $sql = new Sql($this->adapter);
            $select = $sql->select(['A' => 'REF_JPB']);
            $select->order('KD_JPB');
            $res = $sql->prepareStatementForSqlObject($select)->execute();
            $arr = [];
            foreach ($res as $key => $value) {
                $arr[$value['KD_JPB']] = str_pad($value['KD_JPB'], 3, '0', STR_PAD_LEFT) . ' | ' . $value['NM_JPB'];
            }

            $cache->setItem($key, $arr);
        }
        return $cache->getItem($key);
    }

    public function getDataZntSelected($kd_kecamatan, $kd_kelurahan, $kd_blok, $tahun, $kd_znt)
    {
        $cache = $this->cacheHelper->cache();
        $key = 'ReffSpopTable-getDataZntSelected-' . $kd_kecamatan . '-' . $kd_kelurahan
            . '-' . $kd_blok . '-' . $tahun . '-' . $kd_znt;
        if (!$cache->getItem($key)) {

            $sql = new Sql($this->adapter);
            $select = $sql->select(['A' => 'DAT_PETA_ZNT']);
            $select->columns([
                "KD_ZNT",
                "NILAI_PER_METER" => new Expression("(SELECT X.NILAI_PER_M2_TANAH * 1000 
            FROM KELAS_TANAH X 
            WHERE X.THN_AWAL_KLS_TANAH <= B.THN_NIR_ZNT AND X.THN_AKHIR_KLS_TANAH >= B.THN_NIR_ZNT 
            AND B.NIR BETWEEN X.NILAI_MIN_TANAH AND X.NILAI_MAX_TANAH AND X.NILAI_PER_M2_TANAH > 0 
            AND ROWNUM = 1)")
            ]);
            $select->join(['B' => 'DAT_NIR'], new Expression("A.KD_PROPINSI = B.KD_PROPINSI AND A.KD_DATI2 = B.KD_DATI2 
            AND A.KD_KECAMATAN = B.KD_KECAMATAN AND A.KD_KELURAHAN = B.KD_KELURAHAN 
            AND A.KD_ZNT = B.KD_ZNT"), [], "LEFT");
            $where = new Where();
            $where->equalTo("A.KD_KECAMATAN", $kd_kecamatan);
            $where->equalTo("A.KD_KELURAHAN", $kd_kelurahan);
            $where->equalTo("A.KD_BLOK", $kd_blok);
            $where->equalTo("B.THN_NIR_ZNT", $tahun);
            $where->equalTo("A.KD_ZNT", $kd_znt);

            $select->where($where);
            // die($sql->buildSqlString($select));

            $res = $sql->prepareStatementForSqlObject($select)->execute();

            $resultSet = new ResultSet();
            $resultSet->initialize($res);
            // return $resultSet->toArray();

            // $arr = [];
            // foreach ($res as $key => $value) {
            //     $arr[$value['KD_ZNT']] =  $value['KD_ZNT'];
            // }

            $cache->setItem($key, $resultSet->toArray());
        }
        return $cache->getItem($key);
    }

    public function getDataZntSelectedEdit($kd_kecamatan, $kd_kelurahan, $kd_blok, $tahun, $kd_znt)
    {
        $cache = $this->cacheHelper->cache();
        $key = 'ReffSpopTable-getDataZntSelectedEdit-' . $kd_kecamatan . '-' . $kd_kelurahan
            . '-' . $kd_blok . '-' . $tahun . '-' . $kd_znt;
        if (!$cache->getItem($key)) {

            $sql = new Sql($this->adapter);
            $select = $sql->select(['A' => 'DAT_PETA_ZNT']);
            $select->columns([
                "KD_ZNT",
                "NILAI_PER_METER" => new Expression("(SELECT X.NILAI_PER_M2_TANAH * 1000 
            FROM KELAS_TANAH X 
            WHERE X.THN_AWAL_KLS_TANAH <= B.THN_NIR_ZNT AND X.THN_AKHIR_KLS_TANAH >= B.THN_NIR_ZNT 
            AND B.NIR BETWEEN X.NILAI_MIN_TANAH AND X.NILAI_MAX_TANAH AND X.NILAI_PER_M2_TANAH > 0 
            AND ROWNUM = 1)")
            ]);
            $select->join(['B' => 'DAT_NIR'], new Expression("A.KD_PROPINSI = B.KD_PROPINSI AND A.KD_DATI2 = B.KD_DATI2 
            AND A.KD_KECAMATAN = B.KD_KECAMATAN AND A.KD_KELURAHAN = B.KD_KELURAHAN 
            AND A.KD_ZNT = B.KD_ZNT"), [], "LEFT");
            $where = new Where();
            $where->equalTo("A.KD_KECAMATAN", $kd_kecamatan);
            $where->equalTo("A.KD_KELURAHAN", $kd_kelurahan);
            $where->equalTo("A.KD_BLOK", $kd_blok);
            $where->equalTo("B.THN_NIR_ZNT", $tahun);
            $where->equalTo("A.KD_ZNT", $kd_znt);

            $select->where($where);
            // die($sql->buildSqlString($select));

            $res = $sql->prepareStatementForSqlObject($select)->execute();

            // $resultSet = new ResultSet();
            // $resultSet->initialize($res);
            // return $resultSet->toArray();

            $arr = [];
            foreach ($res as $key => $value) {
                $arr[$value['KD_ZNT']] =  $value['KD_ZNT'];
            }

            $cache->setItem($key, $arr);
        }
        return $cache->getItem($key);
    }

    // public function getDataOpWpKecKel($nop)
    // {
    //     $sql = new Sql($this->adapter);
    //     $sql = "SELECT  TO_CHAR(TRIM(DAT_SUBJEK_PAJAK.SUBJEK_PAJAK_ID)) AS SUBJEK_PAJAK_ID,
    //     DAT_SUBJEK_PAJAK.*,
    //     REF_KELURAHAN.*,
    //     REF_KECAMATAN.*,
    //     DAT_OBJEK_PAJAK.*,
    //     DAT_OP_BUMI.*

    //     FROM DAT_OBJEK_PAJAK
    //     LEFT JOIN DAT_SUBJEK_PAJAK ON DAT_SUBJEK_PAJAK.SUBJEK_PAJAK_ID = DAT_OBJEK_PAJAK.SUBJEK_PAJAK_ID

    //     LEFT JOIN DAT_OP_BUMI ON
    // DAT_OP_BUMI.KD_PROPINSI = DAT_OBJEK_PAJAK.KD_PROPINSI
    // AND DAT_OP_BUMI.KD_DATI2 = DAT_OBJEK_PAJAK.KD_DATI2
    // AND DAT_OP_BUMI.KD_KECAMATAN = DAT_OBJEK_PAJAK.KD_KECAMATAN
    // AND DAT_OP_BUMI.KD_KELURAHAN = DAT_OBJEK_PAJAK.KD_KELURAHAN
    // AND DAT_OP_BUMI.KD_BLOK = DAT_OBJEK_PAJAK.KD_BLOK
    // AND DAT_OP_BUMI.NO_URUT = DAT_OBJEK_PAJAK.NO_URUT
    // AND DAT_OP_BUMI.KD_JNS_OP = DAT_OBJEK_PAJAK.KD_JNS_OP

    //     LEFT JOIN REF_KECAMATAN ON
    // REF_KECAMATAN.KD_PROPINSI = DAT_OBJEK_PAJAK.KD_PROPINSI
    // AND REF_KECAMATAN.KD_DATI2 = DAT_OBJEK_PAJAK.KD_DATI2
    // AND REF_KECAMATAN.KD_KECAMATAN = DAT_OBJEK_PAJAK.KD_KECAMATAN

    //     LEFT JOIN REF_KELURAHAN ON
    // REF_KELURAHAN.KD_PROPINSI = DAT_OBJEK_PAJAK.KD_PROPINSI
    // AND REF_KELURAHAN.KD_DATI2 = DAT_OBJEK_PAJAK.KD_DATI2
    // AND REF_KELURAHAN.KD_KECAMATAN = DAT_OBJEK_PAJAK.KD_KECAMATAN
    // AND REF_KELURAHAN.KD_KELURAHAN = DAT_OBJEK_PAJAK.KD_KELURAHAN

    //     WHERE

    //     DAT_OBJEK_PAJAK.KD_PROPINSI = '" . $nop['KD_PROPINSI'] . "'
    //     AND DAT_OBJEK_PAJAK.KD_DATI2 = '" . $nop['KD_DATI2'] . "'
    //     AND DAT_OBJEK_PAJAK.KD_KECAMATAN = '" . $nop['KD_KECAMATAN'] . "'
    //     AND DAT_OBJEK_PAJAK.KD_KELURAHAN = '" . $nop['KD_KELURAHAN'] . "'
    //     AND DAT_OBJEK_PAJAK.KD_BLOK = '" . $nop['KD_BLOK'] . "'
    //     AND DAT_OBJEK_PAJAK.NO_URUT = '" . $nop['NO_URUT'] . "'
    //     AND DAT_OBJEK_PAJAK.KD_JNS_OP = '" . $nop['KD_JNS_OP'] . "'
    //     ";
    //     $res = $sql->execute()->current();
    // }

    public function getDataOpWpKecKel($data)
    {
        $nop = explode('.', $data['t_nopbphtbsppt']);

        $sql =
            "SELECT  TO_CHAR(TRIM(DAT_SUBJEK_PAJAK.SUBJEK_PAJAK_ID)) AS SUBJEK_PAJAK_ID,
        DAT_SUBJEK_PAJAK.*,
        REF_KELURAHAN.*,
        REF_KECAMATAN.*,
        DAT_OBJEK_PAJAK.*,
        DAT_OP_BUMI.*

        FROM DAT_OBJEK_PAJAK
        LEFT JOIN DAT_SUBJEK_PAJAK ON DAT_SUBJEK_PAJAK.SUBJEK_PAJAK_ID = DAT_OBJEK_PAJAK.SUBJEK_PAJAK_ID

        LEFT JOIN DAT_OP_BUMI ON
    DAT_OP_BUMI.KD_PROPINSI = DAT_OBJEK_PAJAK.KD_PROPINSI
    AND DAT_OP_BUMI.KD_DATI2 = DAT_OBJEK_PAJAK.KD_DATI2
    AND DAT_OP_BUMI.KD_KECAMATAN = DAT_OBJEK_PAJAK.KD_KECAMATAN
    AND DAT_OP_BUMI.KD_KELURAHAN = DAT_OBJEK_PAJAK.KD_KELURAHAN
    AND DAT_OP_BUMI.KD_BLOK = DAT_OBJEK_PAJAK.KD_BLOK
    AND DAT_OP_BUMI.NO_URUT = DAT_OBJEK_PAJAK.NO_URUT
    AND DAT_OP_BUMI.KD_JNS_OP = DAT_OBJEK_PAJAK.KD_JNS_OP

        LEFT JOIN REF_KECAMATAN ON
    REF_KECAMATAN.KD_PROPINSI = DAT_OBJEK_PAJAK.KD_PROPINSI
    AND REF_KECAMATAN.KD_DATI2 = DAT_OBJEK_PAJAK.KD_DATI2
    AND REF_KECAMATAN.KD_KECAMATAN = DAT_OBJEK_PAJAK.KD_KECAMATAN

        LEFT JOIN REF_KELURAHAN ON
    REF_KELURAHAN.KD_PROPINSI = DAT_OBJEK_PAJAK.KD_PROPINSI
    AND REF_KELURAHAN.KD_DATI2 = DAT_OBJEK_PAJAK.KD_DATI2
    AND REF_KELURAHAN.KD_KECAMATAN = DAT_OBJEK_PAJAK.KD_KECAMATAN
    AND REF_KELURAHAN.KD_KELURAHAN = DAT_OBJEK_PAJAK.KD_KELURAHAN

        WHERE

        DAT_OBJEK_PAJAK.KD_PROPINSI = '" . $nop[0] . "'
        AND DAT_OBJEK_PAJAK.KD_DATI2 = '" . $nop[1] . "'
        AND DAT_OBJEK_PAJAK.KD_KECAMATAN = '" . $nop[2] . "'
        AND DAT_OBJEK_PAJAK.KD_KELURAHAN = '" . $nop[3] . "'
        AND DAT_OBJEK_PAJAK.KD_BLOK = '" . $nop[4] . "'
        AND DAT_OBJEK_PAJAK.NO_URUT = '" . $nop[5] . "'
        AND DAT_OBJEK_PAJAK.KD_JNS_OP = '" . $nop[6] . "'
        ";
        $statement = $this->adapter->query($sql);
        $st = $statement->execute();
        return $st->current();
    }
}
