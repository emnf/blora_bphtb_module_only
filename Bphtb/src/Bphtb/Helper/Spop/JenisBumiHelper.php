<?php

namespace Bphtb\Helper\Spop;

class JenisBumiHelper
{
    const JENIS_BUMI = "20";

    const TANAH_DAN_BANGUNAN = 1;
    const KAVLING_SIAP_BANGUN = 2;
    const TANAH_KOSONG = 3;
    const FASILITAS_UMUM = 4;
    const TANAH_PERTANIAN = 5;
}
