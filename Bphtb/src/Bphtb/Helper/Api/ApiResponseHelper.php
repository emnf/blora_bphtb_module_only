<?php

namespace Bphtb\Helper\Api;

class ApiResponseHelper
{
    public static function formatResponse($response, $resp_data, $resp_code, $resp_message, $code = 200, $username)
    {
        // ApiPaymentLog::create([
        //     // "datetime" => date("Y-m-d H:i:s"),
        //     'uuid' => Uuid::uuid4()->toString(),
        //     "request" => $request,
        //     "username" => $username != null ? $username : '',
        //     "data" => (($resp_data != null) ? json_encode($resp_data) : null),
        //     "resp_code" => $resp_code,
        //     "resp_message" => $resp_message,
        //     'kode_bayar' => ($request->kodebayar != null) ? $request->kodebayar : ''
        // ]);

        $dataResponse = [
            "resp_code" => $resp_code,
            "resp_message" => $resp_message,
        ];
        if ($resp_data != null) {
            $dataResponse['resp_data'] = $resp_data;
        }

        return $response->setContent(json_encode($dataResponse));
    }
}
